﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormSHU
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormSHU))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtidshu = New System.Windows.Forms.TextBox()
        Me.txtshudibagi = New System.Windows.Forms.TextBox()
        Me.txtidanggota = New System.Windows.Forms.TextBox()
        Me.txtnamaanggota = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.txttotalsimpanan = New System.Windows.Forms.TextBox()
        Me.txttotalpinjaman = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtshuanggota = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btntambah = New System.Windows.Forms.Button()
        Me.btnkeluar = New System.Windows.Forms.Button()
        Me.btnbatal = New System.Windows.Forms.Button()
        Me.btnhapus = New System.Windows.Forms.Button()
        Me.btnkoreksi = New System.Windows.Forms.Button()
        Me.btnsimpan = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txttotalbelanja = New System.Windows.Forms.TextBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(31, 117)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "SHU Dibagi"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(355, 22)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 17)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Id Anggota"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(355, 70)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 17)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Nama"
        '
        'txtidshu
        '
        Me.txtidshu.Location = New System.Drawing.Point(129, 70)
        Me.txtidshu.Margin = New System.Windows.Forms.Padding(4)
        Me.txtidshu.Name = "txtidshu"
        Me.txtidshu.Size = New System.Drawing.Size(132, 23)
        Me.txtidshu.TabIndex = 5
        '
        'txtshudibagi
        '
        Me.txtshudibagi.Location = New System.Drawing.Point(129, 108)
        Me.txtshudibagi.Margin = New System.Windows.Forms.Padding(4)
        Me.txtshudibagi.Name = "txtshudibagi"
        Me.txtshudibagi.Size = New System.Drawing.Size(132, 23)
        Me.txtshudibagi.TabIndex = 6
        '
        'txtidanggota
        '
        Me.txtidanggota.Location = New System.Drawing.Point(476, 18)
        Me.txtidanggota.Margin = New System.Windows.Forms.Padding(4)
        Me.txtidanggota.Name = "txtidanggota"
        Me.txtidanggota.Size = New System.Drawing.Size(132, 23)
        Me.txtidanggota.TabIndex = 8
        '
        'txtnamaanggota
        '
        Me.txtnamaanggota.Location = New System.Drawing.Point(476, 70)
        Me.txtnamaanggota.Margin = New System.Windows.Forms.Padding(4)
        Me.txtnamaanggota.Name = "txtnamaanggota"
        Me.txtnamaanggota.Size = New System.Drawing.Size(132, 23)
        Me.txtnamaanggota.TabIndex = 9
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(28, 26)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 17)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Tanggal"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(31, 197)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(104, 17)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "SHU Simpanan"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(226, 197)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(99, 17)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "SHU Pinjaman"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(129, 22)
        Me.DateTimePicker1.Margin = New System.Windows.Forms.Padding(4)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 23)
        Me.DateTimePicker1.TabIndex = 13
        '
        'txttotalsimpanan
        '
        Me.txttotalsimpanan.Location = New System.Drawing.Point(31, 230)
        Me.txttotalsimpanan.Margin = New System.Windows.Forms.Padding(4)
        Me.txttotalsimpanan.Name = "txttotalsimpanan"
        Me.txttotalsimpanan.Size = New System.Drawing.Size(132, 23)
        Me.txttotalsimpanan.TabIndex = 14
        '
        'txttotalpinjaman
        '
        Me.txttotalpinjaman.Location = New System.Drawing.Point(226, 230)
        Me.txttotalpinjaman.Margin = New System.Windows.Forms.Padding(4)
        Me.txttotalpinjaman.Name = "txttotalpinjaman"
        Me.txttotalpinjaman.Size = New System.Drawing.Size(132, 23)
        Me.txttotalpinjaman.TabIndex = 15
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(614, 197)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(130, 17)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Total SHU Anggota"
        '
        'txtshuanggota
        '
        Me.txtshuanggota.Location = New System.Drawing.Point(618, 230)
        Me.txtshuanggota.Margin = New System.Windows.Forms.Padding(4)
        Me.txtshuanggota.Name = "txtshuanggota"
        Me.txtshuanggota.Size = New System.Drawing.Size(147, 23)
        Me.txtshuanggota.TabIndex = 17
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(31, 74)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(54, 17)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "ID SHU"
        '
        'btntambah
        '
        Me.btntambah.Image = CType(resources.GetObject("btntambah.Image"), System.Drawing.Image)
        Me.btntambah.Location = New System.Drawing.Point(646, 5)
        Me.btntambah.Margin = New System.Windows.Forms.Padding(4)
        Me.btntambah.Name = "btntambah"
        Me.btntambah.Size = New System.Drawing.Size(70, 50)
        Me.btntambah.TabIndex = 57
        Me.btntambah.Text = "&Tambah"
        Me.btntambah.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btntambah.UseVisualStyleBackColor = True
        '
        'btnkeluar
        '
        Me.btnkeluar.Image = CType(resources.GetObject("btnkeluar.Image"), System.Drawing.Image)
        Me.btnkeluar.Location = New System.Drawing.Point(724, 104)
        Me.btnkeluar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnkeluar.Name = "btnkeluar"
        Me.btnkeluar.Size = New System.Drawing.Size(70, 50)
        Me.btnkeluar.TabIndex = 62
        Me.btnkeluar.Text = "Kelua&r"
        Me.btnkeluar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnkeluar.UseVisualStyleBackColor = True
        '
        'btnbatal
        '
        Me.btnbatal.Image = CType(resources.GetObject("btnbatal.Image"), System.Drawing.Image)
        Me.btnbatal.Location = New System.Drawing.Point(646, 105)
        Me.btnbatal.Margin = New System.Windows.Forms.Padding(4)
        Me.btnbatal.Name = "btnbatal"
        Me.btnbatal.Size = New System.Drawing.Size(70, 50)
        Me.btnbatal.TabIndex = 61
        Me.btnbatal.Text = "&Batal"
        Me.btnbatal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnbatal.UseVisualStyleBackColor = True
        '
        'btnhapus
        '
        Me.btnhapus.Image = CType(resources.GetObject("btnhapus.Image"), System.Drawing.Image)
        Me.btnhapus.Location = New System.Drawing.Point(724, 54)
        Me.btnhapus.Margin = New System.Windows.Forms.Padding(4)
        Me.btnhapus.Name = "btnhapus"
        Me.btnhapus.Size = New System.Drawing.Size(70, 50)
        Me.btnhapus.TabIndex = 60
        Me.btnhapus.Text = "&Hapus"
        Me.btnhapus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnhapus.UseVisualStyleBackColor = True
        '
        'btnkoreksi
        '
        Me.btnkoreksi.Image = CType(resources.GetObject("btnkoreksi.Image"), System.Drawing.Image)
        Me.btnkoreksi.Location = New System.Drawing.Point(646, 55)
        Me.btnkoreksi.Margin = New System.Windows.Forms.Padding(4)
        Me.btnkoreksi.Name = "btnkoreksi"
        Me.btnkoreksi.Size = New System.Drawing.Size(70, 50)
        Me.btnkoreksi.TabIndex = 59
        Me.btnkoreksi.Text = "&Koreksi"
        Me.btnkoreksi.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnkoreksi.UseVisualStyleBackColor = True
        '
        'btnsimpan
        '
        Me.btnsimpan.Image = CType(resources.GetObject("btnsimpan.Image"), System.Drawing.Image)
        Me.btnsimpan.Location = New System.Drawing.Point(724, 4)
        Me.btnsimpan.Margin = New System.Windows.Forms.Padding(4)
        Me.btnsimpan.Name = "btnsimpan"
        Me.btnsimpan.Size = New System.Drawing.Size(70, 50)
        Me.btnsimpan.TabIndex = 58
        Me.btnsimpan.Text = "&Simpan"
        Me.btnsimpan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnsimpan.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(6, 268)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(812, 249)
        Me.DataGridView1.TabIndex = 63
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(418, 197)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(107, 17)
        Me.Label11.TabIndex = 64
        Me.Label11.Text = "SHU Pembelian"
        '
        'txttotalbelanja
        '
        Me.txttotalbelanja.Location = New System.Drawing.Point(418, 230)
        Me.txttotalbelanja.Margin = New System.Windows.Forms.Padding(4)
        Me.txttotalbelanja.Name = "txttotalbelanja"
        Me.txttotalbelanja.Size = New System.Drawing.Size(132, 23)
        Me.txttotalbelanja.TabIndex = 65
        '
        'FormSHU
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(820, 530)
        Me.Controls.Add(Me.txttotalbelanja)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.btntambah)
        Me.Controls.Add(Me.btnkeluar)
        Me.Controls.Add(Me.btnbatal)
        Me.Controls.Add(Me.btnhapus)
        Me.Controls.Add(Me.btnkoreksi)
        Me.Controls.Add(Me.btnsimpan)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtshuanggota)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txttotalpinjaman)
        Me.Controls.Add(Me.txttotalsimpanan)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtnamaanggota)
        Me.Controls.Add(Me.txtidanggota)
        Me.Controls.Add(Me.txtshudibagi)
        Me.Controls.Add(Me.txtidshu)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FormSHU"
        Me.Text = "FormSHU"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtidshu As System.Windows.Forms.TextBox
    Friend WithEvents txtshudibagi As System.Windows.Forms.TextBox
    Friend WithEvents txtidanggota As System.Windows.Forms.TextBox
    Friend WithEvents txtnamaanggota As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents txttotalsimpanan As System.Windows.Forms.TextBox
    Friend WithEvents txttotalpinjaman As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtshuanggota As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btntambah As System.Windows.Forms.Button
    Friend WithEvents btnkeluar As System.Windows.Forms.Button
    Friend WithEvents btnbatal As System.Windows.Forms.Button
    Friend WithEvents btnhapus As System.Windows.Forms.Button
    Friend WithEvents btnkoreksi As System.Windows.Forms.Button
    Friend WithEvents btnsimpan As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txttotalbelanja As System.Windows.Forms.TextBox
End Class
