﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Formlappinjaman
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Formlappinjaman))
        Me.Lstdata = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Txtkredit = New System.Windows.Forms.TextBox()
        Me.Txtdebet = New System.Windows.Forms.TextBox()
        Me.Cekauto = New System.Windows.Forms.CheckBox()
        Me.Btnexport = New System.Windows.Forms.Button()
        Me.Btncari = New System.Windows.Forms.Button()
        Me.Cmbakun = New System.Windows.Forms.ComboBox()
        Me.Dtakhir = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Dtawal = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Lstdata
        '
        Me.Lstdata.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader10, Me.ColumnHeader11, Me.ColumnHeader12, Me.ColumnHeader13, Me.ColumnHeader2})
        Me.Lstdata.Location = New System.Drawing.Point(18, 66)
        Me.Lstdata.Name = "Lstdata"
        Me.Lstdata.Size = New System.Drawing.Size(834, 356)
        Me.Lstdata.TabIndex = 33
        Me.Lstdata.UseCompatibleStateImageBehavior = False
        Me.Lstdata.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "No"
        Me.ColumnHeader1.Width = 34
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "ID Pinjaman"
        Me.ColumnHeader7.Width = 93
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "ID Anggota"
        Me.ColumnHeader8.Width = 86
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Nama Anggota"
        Me.ColumnHeader9.Width = 160
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Tgl Pinjaman"
        Me.ColumnHeader10.Width = 100
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Tgl Pelunasan"
        Me.ColumnHeader11.Width = 126
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "Nominal Pinjaman"
        Me.ColumnHeader12.Width = 123
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Banyak Angsuran"
        Me.ColumnHeader13.Width = 157
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Nominal Angsuran"
        '
        'Txtkredit
        '
        Me.Txtkredit.Location = New System.Drawing.Point(726, 428)
        Me.Txtkredit.Name = "Txtkredit"
        Me.Txtkredit.Size = New System.Drawing.Size(123, 20)
        Me.Txtkredit.TabIndex = 32
        '
        'Txtdebet
        '
        Me.Txtdebet.Location = New System.Drawing.Point(597, 428)
        Me.Txtdebet.Name = "Txtdebet"
        Me.Txtdebet.Size = New System.Drawing.Size(123, 20)
        Me.Txtdebet.TabIndex = 31
        '
        'Cekauto
        '
        Me.Cekauto.AutoSize = True
        Me.Cekauto.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Cekauto.Location = New System.Drawing.Point(633, 37)
        Me.Cekauto.Name = "Cekauto"
        Me.Cekauto.Size = New System.Drawing.Size(88, 17)
        Me.Cekauto.TabIndex = 30
        Me.Cekauto.Text = "Auto Refresh"
        Me.Cekauto.UseVisualStyleBackColor = False
        '
        'Btnexport
        '
        Me.Btnexport.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Btnexport.Location = New System.Drawing.Point(545, 28)
        Me.Btnexport.Name = "Btnexport"
        Me.Btnexport.Size = New System.Drawing.Size(73, 32)
        Me.Btnexport.TabIndex = 29
        Me.Btnexport.Text = "Export"
        Me.Btnexport.UseVisualStyleBackColor = True
        '
        'Btncari
        '
        Me.Btncari.Image = CType(resources.GetObject("Btncari.Image"), System.Drawing.Image)
        Me.Btncari.Location = New System.Drawing.Point(501, 28)
        Me.Btncari.Name = "Btncari"
        Me.Btncari.Size = New System.Drawing.Size(35, 32)
        Me.Btncari.TabIndex = 28
        Me.Btncari.UseVisualStyleBackColor = True
        '
        'Cmbakun
        '
        Me.Cmbakun.FormattingEnabled = True
        Me.Cmbakun.Location = New System.Drawing.Point(384, 35)
        Me.Cmbakun.Name = "Cmbakun"
        Me.Cmbakun.Size = New System.Drawing.Size(111, 21)
        Me.Cmbakun.TabIndex = 27
        '
        'Dtakhir
        '
        Me.Dtakhir.Location = New System.Drawing.Point(212, 35)
        Me.Dtakhir.Name = "Dtakhir"
        Me.Dtakhir.Size = New System.Drawing.Size(154, 20)
        Me.Dtakhir.TabIndex = 26
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(183, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(23, 13)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "s/d"
        '
        'Dtawal
        '
        Me.Dtawal.Location = New System.Drawing.Point(18, 35)
        Me.Dtawal.Name = "Dtawal"
        Me.Dtawal.Size = New System.Drawing.Size(159, 20)
        Me.Dtawal.TabIndex = 24
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(384, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Nama Anggota"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(18, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 13)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "Filter Data"
        '
        'Formlappinjaman
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(871, 461)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Lstdata)
        Me.Controls.Add(Me.Txtkredit)
        Me.Controls.Add(Me.Txtdebet)
        Me.Controls.Add(Me.Cekauto)
        Me.Controls.Add(Me.Btnexport)
        Me.Controls.Add(Me.Btncari)
        Me.Controls.Add(Me.Cmbakun)
        Me.Controls.Add(Me.Dtakhir)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Dtawal)
        Me.Name = "Formlappinjaman"
        Me.Text = "Formlappinjaman"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Lstdata As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader13 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Txtkredit As System.Windows.Forms.TextBox
    Friend WithEvents Txtdebet As System.Windows.Forms.TextBox
    Friend WithEvents Cekauto As System.Windows.Forms.CheckBox
    Friend WithEvents Btnexport As System.Windows.Forms.Button
    Friend WithEvents Btncari As System.Windows.Forms.Button
    Friend WithEvents Cmbakun As System.Windows.Forms.ComboBox
    Friend WithEvents Dtakhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Dtawal As System.Windows.Forms.DateTimePicker
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
