﻿Imports MySql.Data.MySqlClient
Imports System.Data.SqlClient
Imports System.Data.Odbc
Imports System.IO
Public Class ClsKoneksi
    Public SQL As String
    Public Cn As New MySqlConnection
    Public Cmd As New MySql.Data.MySqlClient.MySqlCommand
    Public Da As New MySql.Data.MySqlClient.MySqlDataAdapter
    Public Ds As New DataSet
    Public Dt As DataTable
    Public rd As MySqlDataReader
    Public fetchData(1) As String

    Public Function OpenConn() As Boolean
        Dim objStreamReader As StreamReader
        Dim strLine As String
        Dim ServerName As String = ""
        Dim DBName As String = ""
        Dim UserNameSQL As String = ""
        Dim passwordSql As String = ""
        objStreamReader = New StreamReader(My.Application.Info.DirectoryPath & "\config.inf")
        strLine = objStreamReader.ReadLine
        Do While Not strLine Is Nothing
            Select Case strLine
                Case "[SERVER NAME]"
                    ServerName = objStreamReader.ReadLine
                Case "[DATABASE NAME]"
                    DBName = objStreamReader.ReadLine
                Case "[USERNAME]"
                    UserNameSQL = objStreamReader.ReadLine
                Case "[PASSWORD CODE]"
                    passwordSql = objStreamReader.ReadLine
            End Select
            ' Console.WriteLine(strLine)

            'Read the next line.
            strLine = objStreamReader.ReadLine
        Loop
        objStreamReader.Close()
        'Console.ReadLine()
        'MsgBox(ServerName & vbCrLf & DBName & vbCrLf & UserNameSQL & vbCrLf & passwordSql)
        Try
            'MsgBox(“tidak bisa konek ke database server”)
            Cn = New MySqlConnection("server=" & ServerName & ";" _
            & "user id=" & UserNameSQL & ";" _
            & "password=" & passwordSql & ";" _
            & "database=" & DBName & ";" _
            & "character set=utf8;" _
            & "Convert Zero Datetime='True'")
            Cn.Open()

        Catch ex As Exception
            MsgBox("tidak bisa konek ke database server")
        End Try

        If Cn.State <> ConnectionState.Open Then
            Return False
        Else
            Return True
        End If
    End Function
    Sub koneksi()
        Try
            SQL = "server=localhost;user id=root; password=;database=lksnasional;"
            Cn = New MySqlConnection(SQL)
            If Cn.State = ConnectionState.Closed Then
                Cn.Open()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Sub CloseConn()
        If Not IsNothing(Cn) Then
            Cn.Close()
            Cn = Nothing
        End If
    End Sub
    Public Function ExecuteQuery(ByVal Query As String) As DataTable
        Try
            If Not OpenConn() Then
                MsgBox("Koneksi Gagal..!!", MsgBoxStyle.Critical, "Access Failed")
                Return Nothing
                Exit Function
            End If
            Cmd = New MySql.Data.MySqlClient.MySqlCommand(Query, Cn)
            Da = New MySql.Data.MySqlClient.MySqlDataAdapter
            Da.SelectCommand = Cmd

            Ds = New Data.DataSet
            Da.Fill(Ds)
            Dt = Ds.Tables(0)

        Catch ex As Exception
            MsgBox("Terjadi kesalahan dalam pemanggilan data" & Chr(13) & _
                   "Pastikan tidak menggunakan tanda (‘) atau (/) dalam penginputan" & Chr(13) & _
                   Err.Description, vbInformation)
            MsgBox("query :" & Query)
            Return Dt
            Exit Function
        End Try

        Return Dt

        Dt = Nothing
        Ds = Nothing
        Da = Nothing
        Cmd = Nothing

        CloseConn()
    End Function
    Public Function ExecuteNonQuery(ByVal Query As String) As Boolean
        Try
            If Not OpenConn() Then
                'MsgBox(“Koneksi Gagal..!!”, MsgBoxStyle.Critical, “Access Failed..!!”)
                Return False
                Exit Function
            End If

            Cmd = New MySql.Data.MySqlClient.MySqlCommand
            Cmd.Connection = Cn
            Cmd.CommandType = CommandType.Text
            Cmd.CommandText = Query
            Cmd.ExecuteNonQuery()
            Cmd = Nothing
            CloseConn()
            Return True
        Catch ex As Exception
            MsgBox("Terjadi kesalahan dalam pemanggilan data" & Chr(13) & _
                  "Pastikan tidak menggunakan tanda (‘) atau (/) dalam penginputan" & _
                  vbLf & Err.Description, vbInformation)
            MsgBox("query :" & Query)
            Return False
            Exit Function
        End Try
    End Function
End Class