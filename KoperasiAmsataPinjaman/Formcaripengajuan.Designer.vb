﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Formcaripengajuan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Dgpengajuan = New System.Windows.Forms.DataGridView()
        Me.cmdkeluar = New System.Windows.Forms.Button()
        Me.cmdpilih = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Txtcari = New System.Windows.Forms.TextBox()
        Me.Cbocari = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.Dgpengajuan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Dgpengajuan
        '
        Me.Dgpengajuan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgpengajuan.Location = New System.Drawing.Point(18, 108)
        Me.Dgpengajuan.Name = "Dgpengajuan"
        Me.Dgpengajuan.Size = New System.Drawing.Size(697, 277)
        Me.Dgpengajuan.TabIndex = 10
        '
        'cmdkeluar
        '
        Me.cmdkeluar.Location = New System.Drawing.Point(594, 404)
        Me.cmdkeluar.Name = "cmdkeluar"
        Me.cmdkeluar.Size = New System.Drawing.Size(76, 28)
        Me.cmdkeluar.TabIndex = 9
        Me.cmdkeluar.Text = "Keluar"
        Me.cmdkeluar.UseVisualStyleBackColor = True
        '
        'cmdpilih
        '
        Me.cmdpilih.Location = New System.Drawing.Point(505, 404)
        Me.cmdpilih.Name = "cmdpilih"
        Me.cmdpilih.Size = New System.Drawing.Size(83, 29)
        Me.cmdpilih.TabIndex = 8
        Me.cmdpilih.Text = "Pilih"
        Me.cmdpilih.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Txtcari)
        Me.GroupBox1.Controls.Add(Me.Cbocari)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(340, 76)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        '
        'Txtcari
        '
        Me.Txtcari.Location = New System.Drawing.Point(90, 45)
        Me.Txtcari.Name = "Txtcari"
        Me.Txtcari.Size = New System.Drawing.Size(226, 20)
        Me.Txtcari.TabIndex = 3
        '
        'Cbocari
        '
        Me.Cbocari.FormattingEnabled = True
        Me.Cbocari.Location = New System.Drawing.Point(89, 13)
        Me.Cbocari.Name = "Cbocari"
        Me.Cbocari.Size = New System.Drawing.Size(143, 21)
        Me.Cbocari.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Text Dicari"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Berdasarkan"
        '
        'Formcaripengajuan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(731, 441)
        Me.Controls.Add(Me.Dgpengajuan)
        Me.Controls.Add(Me.cmdkeluar)
        Me.Controls.Add(Me.cmdpilih)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Formcaripengajuan"
        Me.Text = "Formcaripengajuan"
        CType(Me.Dgpengajuan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Dgpengajuan As System.Windows.Forms.DataGridView
    Friend WithEvents cmdkeluar As System.Windows.Forms.Button
    Friend WithEvents cmdpilih As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
