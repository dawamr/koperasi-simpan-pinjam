﻿Public Class Formcaripengajuan
    Dim strsql As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Private Sub FormCariAnggota_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshGrid()
        load_cbocari(tabel, cbocari)
        cbocari.SelectedIndex = 0
    End Sub
    Private Sub cmdkeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdkeluar.Click
        Me.Close()
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from pengajuan_pinjaman order by IDPengajuan")
            Me.Dgpengajuan.DataSource = tabel
            With Me.Dgpengajuan
                .Columns(0).HeaderText = "ID Pengajuan Pinjaman"
                .Columns(1).HeaderText = "Tanggal Pengajuan"
                .Columns(2).HeaderText = "ID Anggota"
                .Columns(3).HeaderText = "Nama Anggota"
                .Columns(4).HeaderText = "Nominal"
                .Columns(5).HeaderText = "Status"
                .Columns(0).Width = 100
                .Columns(1).Width = 100
                .Columns(2).Width = 200
                .Columns(3).Width = 200
                .Columns(4).Width = 100
                .Columns(5).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from pengajuan_pinjaman where " & Trim(namaKolom) & " like ('%" & Trim(sCari) & "%') order by IDPengajuan")
            Me.Dgpengajuan.DataSource = tabel
            With Me.Dgpengajuan
                .Columns(0).HeaderText = "ID Pengajuan Pinjaman"
                .Columns(1).HeaderText = "Tanggal Pengajuan"
                .Columns(2).HeaderText = "ID Anggota"
                .Columns(3).HeaderText = "Nama Anggota"
                .Columns(4).HeaderText = "Nominal"
                .Columns(5).HeaderText = "Status"
                .Columns(0).Width = 100
                .Columns(1).Width = 200
                .Columns(2).Width = 200
                .Columns(3).Width = 100
                .Columns(4).Width = 100
                .Columns(5).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub cmdpilih_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdpilih.Click
        If cekpengajuan = 1 Then
            Bkp_Formpinjaman.Txtpengajuan.Text = Dgpengajuan.Rows(Dgpengajuan.CurrentCell.RowIndex).Cells(0).Value
            Bkp_Formpinjaman.Txtidanggota.Text = Dgpengajuan.Rows(Dgpengajuan.CurrentCell.RowIndex).Cells(2).Value
            Bkp_Formpinjaman.Txtnamaanggota.Text = Dgpengajuan.Rows(Dgpengajuan.CurrentCell.RowIndex).Cells(3).Value
            Bkp_Formpinjaman.Txtnominalpinj.Text = Dgpengajuan.Rows(Dgpengajuan.CurrentCell.RowIndex).Cells(4).Value
        End If
        Me.Close()
    End Sub
    Private Sub txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Txtcari.TextChanged
        refreshGridCari(Cbocari.Text, Txtcari.Text)
    End Sub
End Class