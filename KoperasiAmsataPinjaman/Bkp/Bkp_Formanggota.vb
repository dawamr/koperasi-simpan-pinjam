﻿Public Class Bkp_Formanggota
    Dim strsql As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Private Sub Formanggota_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        tombol(True)
        aktif(False)
    End Sub
    Private Sub Formanggota_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshGrid()
        baca_data(0)
        load_cbocari(tabel, Cbocari)
        Cbocari.SelectedIndex = 0
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from anggota order by id_anggota")
            Me.Dganggota.DataSource = tabel
            With Me.Dganggota
                .Columns(0).HeaderText = "ID Anggota"
                .Columns(1).HeaderText = "Nama Anggota"
                .Columns(2).HeaderText = "Jabatan Anggota"
                .Columns(3).HeaderText = "Alamat Anggota"
                .Columns(4).HeaderText = "No Telpon"
                .Columns(0).Width = 100
                .Columns(1).Width = 150
                .Columns(2).Width = 150
                .Columns(3).Width = 200
                .Columns(4).Width = 130
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from anggota where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by id_anggota")
            Me.Dganggota.DataSource = tabel
            With Me.Dganggota
                .Columns(0).HeaderText = "ID Anggota"
                .Columns(1).HeaderText = "Nama Anggota"
                .Columns(2).HeaderText = "Jabatan Anggota"
                .Columns(3).HeaderText = "Alamat Anggota"
                .Columns(4).HeaderText = "No Telpon"
                .Columns(0).Width = 100
                .Columns(1).Width = 150
                .Columns(2).Width = 150
                .Columns(3).Width = 200
                .Columns(4).Width = 130
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub baca_data(ByVal posisi As Integer)
        If posisi = 0 Then
            Txtidanggota.Text = IIf(IsDBNull(Dganggota.Rows(0).Cells(0).Value) = True, "-", _
                                   Dganggota.Rows(0).Cells(0).Value) 'idanggota
            Txtnamaanggota.Text = IIf(IsDBNull(Dganggota.Rows(0).Cells(1).Value) = True, "-", _
                                   Dganggota.Rows(0).Cells(1).Value) 'namaanggota
            Txtjabatan.Text = IIf(IsDBNull(Dganggota.Rows(0).Cells(2).Value) = True, "-", _
                                   Dganggota.Rows(0).Cells(2).Value) 'jabatan
            Txtalamat.Text = IIf(IsDBNull(Dganggota.Rows(0).Cells(3).Value) = True, "-", _
                                   Dganggota.Rows(0).Cells(3).Value) 'alamat
            Txtnotelp.Text = Dganggota.Rows(0).Cells(4).Value 'notelp

        Else

            Txtidanggota.Text = IIf(IsDBNull(Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(0).Value) = True, _
                 "-", Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(0).Value)
            Txtnamaanggota.Text = IIf(IsDBNull(Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(1).Value) = True, _
                 "-", Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(1).Value)
            Txtjabatan.Text = IIf(IsDBNull(Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(2).Value) = True, _
                 "-", Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(2).Value)
            Txtalamat.Text = IIf(IsDBNull(Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(3).Value) = True, _
                 "-", Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(3).Value)
            Txtnotelp.Text = Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(4).Value
        End If
    End Sub
    Private Sub clearData()
        Txtidanggota.Text = ""
        Txtnamaanggota.Text = ""
        Txtjabatan.Text = ""
        Txtalamat.Text = ""
        Txtnotelp.Text = ""
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        cmdtambah.Enabled = x
        cmdsimpan.Enabled = Not x
        cmdedit.Enabled = x
        cmdhapus.Enabled = x
        cmdbatal.Enabled = Not x
        cmdkeluar.Enabled = x
    End Sub
    Private Sub aktif(ByVal x As Boolean)
        Txtidanggota.Enabled = x
        Txtnamaanggota.Enabled = x
        Txtjabatan.Enabled = x
        Txtalamat.Enabled = x
        Txtnotelp.Enabled = x
    End Sub
    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdkeluar.Click
        Me.Close()
    End Sub
    Private Sub cmdTambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdtambah.Click
        tombol(False)
        aktif(True)
        kosong()
        clearData()
        Txtidanggota.Text = getID("anggota", "K0000", "id_anggota")
        pil = 1 'insert
    End Sub
    Private Sub kosong()
        Txtidanggota.Text = ""
        Txtnamaanggota.Text = ""
        Txtjabatan.Text = ""
        Txtalamat.Text = ""
        Txtnotelp.Text = ""
    End Sub
    Private Sub cmdBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdbatal.Click
        tombol(True)
        aktif(False)
    End Sub
    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsimpan.Click
        Dim msql As String
        If pil = 1 Then                         'tambah
            msql = "insert into anggota (id_anggota, nama_anggota, jabatan_anggota, alamat_anggota, telp_anggota) VALUES ('" & Txtidanggota.Text & "','" & Txtnamaanggota.Text & "','" & Txtjabatan.Text & "','" & Txtalamat.Text & "','" & Txtnotelp.Text & "')"

        Else                                    'koreksi
            msql = "update anggota set nama_anggota='" & _
            Txtnamaanggota.Text _
            & "',jabatan_anggota='" & Txtjabatan.Text _
            & "',alamat_anggota='" & Txtalamat.Text _
            & "',telp_anggota=" & Val(Txtnotelp.Text) _
            & " where id_anggota='" & Txtidanggota.Text & "'"
        End If
        proses.ExecuteNonQuery(msql)
        tombol(True)
        aktif(False)
        refreshGrid()
        MsgBox("Berhasil disimpan...", vbInformation, "Info")
    End Sub
    Private Sub dgpengajuan_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Dganggota.CellContentClick
        baca_data(1)
    End Sub
    Private Sub dgpengajuan_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles Dganggota.CellMouseClick
        baca_data(1)
    End Sub
    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdhapus.Click
        Dim psn As Long
        psn = MsgBox("Yakin menghapus data ini ", vbQuestion + vbYesNo, "Pesan")
        If psn = vbYes Then
            proses.ExecuteNonQuery("delete from anggota where id_anggota='" & _
                                   Txtidanggota.Text & "'")
        End If
        refreshGrid()
    End Sub
    Private Sub cmdedit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdedit.Click
        pil = 2
        aktif(True)
        tombol(False)
        Txtidanggota.Enabled = False
        Txtnamaanggota.Focus()
    End Sub
    Private Sub txtCari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Txtcari.TextChanged
        refreshGridCari(Cbocari.Text, Txtcari.Text)
    End Sub

    Private Sub Cbocari_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cbocari.SelectedIndexChanged

    End Sub
End Class