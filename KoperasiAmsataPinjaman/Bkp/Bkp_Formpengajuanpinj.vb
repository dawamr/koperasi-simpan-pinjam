﻿Public Class Bkp_Formpengajuanpinj
    Dim strsql As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Private Sub Formpengajuanpinj_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'tombol(True)
        'aktif(False)
    End Sub
    Private Sub Formpengajuanpinj_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshGrid()
        baca_data(0)
        load_cbocari(tabel, Cbocari)
        Cbostatus.Items.Add("Diproses")
        Cbostatus.Items.Add("Ditolak")
        Cbostatus.Items.Add("Diterima")
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from pengajuan_pinjaman order by IDAnggota")
            Me.Dgpengajuan.DataSource = tabel
            With Me.Dgpengajuan
                .Columns(0).HeaderText = "ID Pengajuan Pinjaman"
                .Columns(1).HeaderText = "Tanggal Pengajuan"
                .Columns(2).HeaderText = "ID Anggota"
                .Columns(3).HeaderText = "Nama Anggota"
                .Columns(4).HeaderText = "Nominal"
                .Columns(5).HeaderText = "Status"
                .Columns(0).Width = 100
                .Columns(1).Width = 100
                .Columns(2).Width = 200
                .Columns(3).Width = 200
                .Columns(4).Width = 100
                .Columns(5).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from pengajuan_pinjaman where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by IDAnggota")
            Me.Dgpengajuan.DataSource = tabel
            With Me.Dgpengajuan
                .Columns(0).HeaderText = "ID Pengajuan Pinjaman"
                .Columns(1).HeaderText = "Tanggal Pengajuan"
                .Columns(2).HeaderText = "ID Anggota"
                .Columns(3).HeaderText = "Nama Anggota"
                .Columns(4).HeaderText = "Nominal"
                .Columns(5).HeaderText = "Status"
                .Columns(0).Width = 100
                .Columns(1).Width = 200
                .Columns(2).Width = 200
                .Columns(3).Width = 100
                .Columns(4).Width = 100
                .Columns(5).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub baca_data(ByVal posisi As Integer)
        If posisi = 0 Then
            Txtpengajuan.Text = IIf(IsDBNull(Dgpengajuan.Rows(0).Cells(0).Value) = True, "-", _
                                   Dgpengajuan.Rows(0).Cells(0).Value) 'idpengajuan
            Txtidanggota.Text = IIf(IsDBNull(Dgpengajuan.Rows(0).Cells(2).Value) = True, "-", _
                                   Dgpengajuan.Rows(0).Cells(2).Value) 'idanggota
            Txtnamaanggota.Text = IIf(IsDBNull(Dgpengajuan.Rows(0).Cells(3).Value) = True, "-", _
                                   Dgpengajuan.Rows(0).Cells(3).Value) 'namaanggota
            Txtnominal.Text = Dgpengajuan.Rows(0).Cells(4).Value 'nominal
            Cbostatus.Text = Dgpengajuan.Rows(0).Cells(5).Value 'status

        Else
            Txtpengajuan.Text = IIf(IsDBNull(Dgpengajuan.Rows(Dgpengajuan.CurrentCell.RowIndex).Cells(0).Value) = True, _
                 "-", Dgpengajuan.Rows(Dgpengajuan.CurrentCell.RowIndex).Cells(0).Value)
            Txtidanggota.Text = IIf(IsDBNull(Dgpengajuan.Rows(Dgpengajuan.CurrentCell.RowIndex).Cells(2).Value) = True, _
                 "-", Dgpengajuan.Rows(Dgpengajuan.CurrentCell.RowIndex).Cells(2).Value)
            Txtnamaanggota.Text = IIf(IsDBNull(Dgpengajuan.Rows(Dgpengajuan.CurrentCell.RowIndex).Cells(3).Value) = True, _
                 "-", Dgpengajuan.Rows(Dgpengajuan.CurrentCell.RowIndex).Cells(3).Value)
            Txtnominal.Text = Dgpengajuan.Rows(Dgpengajuan.CurrentCell.RowIndex).Cells(4).Value
            Cbostatus.Text = Dgpengajuan.Rows(0).Cells(5).Value
        End If
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        cmdtambah.Enabled = x
        cmdsimpan.Enabled = Not x
        cmdedit.Enabled = x
        cmdhapus.Enabled = x
        cmdbatal.Enabled = Not x
        cmdkeluar.Enabled = x
    End Sub
    Private Sub aktif(ByVal x As Boolean)
        Txtpengajuan.Enabled = x
        Txtidanggota.Enabled = x
        Txtnamaanggota.Enabled = x
        Txtnominal.Enabled = x
    End Sub
    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdkeluar.Click
        Me.Close()
    End Sub
    Private Sub cmdTambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdtambah.Click
        aktif(True)
        cmdtambah.Visible = False
        cmdsimpan.Visible = True
        cmdedit.Visible = False
        cmdbatal.Visible = True

        cmdsimpan.Location = New Point(667, 80)
        cmdbatal.Location = New Point(719, 79)


        kosong()
        Txtpengajuan.Text = getID("pengajuan_pinjaman", "P0000", "IDPengajuan")
        pil = 1 'insert
    End Sub
    Private Sub kosong()
        Txtpengajuan.Text = ""
        Txtidanggota.Text = ""
        Txtnamaanggota.Text = ""
        Txtnominal.Text = ""
    End Sub
    Private Sub cmdBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdbatal.Click
        cmdtambah.Visible = True
        cmdsimpan.Visible = False
        cmdedit.Visible = True
        cmdbatal.Visible = False

        cmdtambah.Location = New Point(667, 80)
        cmdedit.Location = New Point(719, 79)
        aktif(False)
    End Sub
    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsimpan.Click
        Dim msql As String

        If pil = 1 Then                         'tambah
            msql = "insert into pengajuan_pinjaman (IDPengajuan, TanggalPengajuan, IDAnggota, NamaAnggota, Nominal, Status) Values ('" & Txtpengajuan.Text & "','" & Format(Dttgl.Value, "yyy-MM-dd HH:mm:ss") & "','" & Txtidanggota.Text & "','" & Txtnamaanggota.Text & "','" & Txtnominal.Text & "','" & Cbostatus.Text & "')"
        Else                                    'koreksi
            msql = "update pengajuan_pinjaman set namaanggota='" & _
            Txtnamaanggota.Text _
            & "',Status='" & Cbostatus.Text _
            & "',nominal=" & Val(Txtnominal.Text) _
            & " where IDAnggota='" & Txtidanggota.Text & "'"
        End If
        proses.ExecuteNonQuery(msql)
        tombol(True)
        aktif(False)
        refreshGrid()
        MsgBox("Berhasil disimpan...", vbInformation, "Info")
    End Sub
    Private Sub dgpengajuan_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Dgpengajuan.CellContentClick
        baca_data(1)
    End Sub
    Private Sub dgpengajuan_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles Dgpengajuan.CellMouseClick
        baca_data(1)
    End Sub
    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdhapus.Click
        Dim psn As Long
        psn = MsgBox("Yakin menghapus data ini ", vbQuestion + vbYesNo, "Pesan")
        If psn = vbYes Then
            proses.ExecuteNonQuery("delete from pengajuan_pinjaman where IDPengajuan='" & _
                                   Txtpengajuan.Text & "'")
        End If
        refreshGrid()
    End Sub
    Private Sub cmdedit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdedit.Click
        pil = 2
        aktif(True)
        tombol(False)
        Txtidanggota.Enabled = False
        Txtnamaanggota.Focus()
    End Sub
    Private Sub txtCari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Txtcari.TextChanged
        refreshGridCari(Cbocari.Text, Txtcari.Text)
    End Sub

    Private Sub Cbostatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cbostatus.SelectedIndexChanged
        Dim var_status As String
        var_status = Cbostatus.Text
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cekanggota = 1
        FormCariAnggota.ShowDialog()
    End Sub
    Private Sub txtidanggota_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Txtidanggota.KeyPress
        Dim tombol As Integer = Asc(e.KeyChar)
        If tombol = 13 Then Txtnamaanggota.Text = getAkun(Txtidanggota.Text)
    End Sub
    Private Function getAkun(ByVal kd As String) As String
        Dim nama As String = ""
        tabel = proses.ExecuteQuery("select * from pengajuan_pinjaman where IDAnggota like ('%" & Trim(kd) & _
                                    "%') order by KodeAkun")
        If tabel.Rows.Count > 0 Then
            nama = tabel.Rows(0).Item(1).ToString
        End If
        Return nama
    End Function
    Private Sub txtNoAkun1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Txtidanggota.KeyPress
        Dim tombol As Integer = Asc(e.KeyChar)
        If tombol = 13 Then Txtidanggota.Text = getAkun(Txtidanggota.Text)
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

    End Sub
End Class
