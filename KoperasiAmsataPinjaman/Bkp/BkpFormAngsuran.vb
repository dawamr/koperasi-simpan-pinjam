﻿Public Class BkpFormAngsuran
    Dim strsql As String
    Dim proses As New ClsKoneksi
    Dim tabel1, tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Dim angsuranke, sisaangsuran, jumlahangsuran, nominalbayar As Integer
    Private Sub Formangsuran_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        aktif(False)
    End Sub
    Private Sub Formangsuran_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
    Private Sub aktif(ByVal x As Boolean)
        txtnoangsuran.Enabled = x
        Txtidanggota.Enabled = Not x
        Txtnamaanggota.Enabled = x
        Txtangsuranke.Enabled = x
        Txtnominalangsuran.Enabled = x
        Txttotalpinjam.Enabled = x
        Txtnominalangsuran.Enabled = x
        Txtnopinjam.Enabled = x
        Txtsisaangsuran.Enabled = x
        Txtkurangbayar.Enabled = x
    End Sub
    Private Sub kosong()
        txtnoangsuran.Text = ""
        Txtidanggota.Text = ""
        Txtnamaanggota.Text = ""
        Txtangsuranke.Text = ""
        Txtnominalangsuran.Text = ""
        Txttotalpinjam.Text = ""
        Txtnominalangsuran.Text = ""
    End Sub
    Private Sub refreshGrid(ByVal idAnggota As String)
        Try
            tabel = proses.ExecuteQuery("select * from angsuran where IDAnggota = '" & idAnggota & "' order by NoAngsuran")

            angsuranke = Convert.ToInt32(tabel.Rows.Count)
            sisaangsuran = Convert.ToInt32(jumlahangsuran) - Convert.ToInt32(tabel.Rows.Count)
            Txtangsuranke.Text = angsuranke
            Txtsisaangsuran.Text = sisaangsuran

            Me.Dgangsuran.DataSource = tabel
            Dim index As Integer = 0
            With Me.Dgangsuran
                .Columns(index).HeaderText = "No Angsuran"
                index = index + 1
                .Columns(index).HeaderText = "ID Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Nomer Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Nama Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Bayar"
                index = index + 1
                .Columns(index).HeaderText = "Nominal Bayar"
                .Columns(index).DefaultCellStyle.Format = "N2"
                index = index + 1
                .Columns(index).HeaderText = "Jumlah Pinjaman"
                .Columns(index).DefaultCellStyle.Format = "N2"
                index = index + 1
                .Columns(index).HeaderText = "Angsuran-Ke"
                index = index + 1
                .Columns(index).HeaderText = "Kurang Bayar"
                .Columns(index).DefaultCellStyle.Format = "N2"

            End With
            Dgangsuran.ReadOnly = True
            Dgangsuran.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsimpan.Click
        Dim msql As String
        If (angsuranke + 1) > jumlahangsuran Then
            MsgBox("Angsuran Pinjaman Sudah Lunas!")
            Return
        End If

        If pil = 1 Then                         'tambah
            msql = "insert into angsuran (NoAngsuran, NoPinjaman, IDAnggota, NamaAnggota, TanggalBayar, JumlahAngsuran, TotalAngsuran, AngsuranKe, SisaAngsuran) VALUES ('" & txtnoangsuran.Text & "','" & Txtnopinjam.Text & "','" & Txtidanggota.Text & "','" & Txtnamaanggota.Text & "','" & Format(dttgl.Value, "yyy-MM-dd HH:mm:ss") & "','" & Txtnominalangsuran.Text & "','" & Txttotalpinjam.Text & "','" & Convert.ToInt32(Txtangsuranke.Text) + 1 & "','" & Txtkurangbayar.Text & "')"

        Else                                    'koreksi
            msql = "update angsuran set jumlahangsuran='" & _
            Txtnamaanggota.Text _
            & "',JumlahAngsuran=" & Val(Txtnominalangsuran.Text) _
            & " where IDAnggota='" & Txtidanggota.Text & "'"
        End If
        proses.ExecuteNonQuery(msql)
        refreshGrid(Txtidanggota.Text)
        aktif(False)
        txtnoangsuran.Text = getID("angsuran", "A0000", "NoAngsuran")
        Txtkurangbayar.Text = nominalbayar * sisaangsuran
        MsgBox("Berhasil disimpan...", vbInformation, "Info")
    End Sub
    Private Sub cmdkeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdkeluar.Click
        Me.Close()
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        cmdcetak.Enabled = x
        cmdsimpan.Enabled = Not x
        cmdbatal.Enabled = Not x
        cmdkeluar.Enabled = x
    End Sub
    Private Sub cmdBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdbatal.Click
        tombol(True)
        aktif(False)
    End Sub

    Private Sub btnCari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCari.Click
        Try
            Dim nopinjam As String

            nopinjam = Txtnopinjam.Text

            tabel1 = proses.ExecuteQuery("SELECT * FROM pinjaman where NoPinjaman = '" & nopinjam & "'")

            If tabel1.Rows.Count < 1 Then
                MsgBox("No Pinjaman Tidak Ditemukan!")
            Else
                jumlahangsuran = tabel1.Rows(0).Item(9)
                refreshGrid(Txtidanggota.Text)
                txtnoangsuran.Text = getID("angsuran", "A0000", "NoAngsuran")
                Txtidanggota.Text = tabel1.Rows(0).Item(2)
                Txtnamaanggota.Text = tabel1.Rows(0).Item(3)
                nominalbayar = tabel1.Rows(0).Item(8)
                Txtnominalangsuran.Text = nominalbayar
                Txttotalpinjam.Text = tabel1.Rows(0).Item(10)
                Txtkurangbayar.Text = (tabel1.Rows(0).Item(8) * sisaangsuran)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Txtidanggota_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txtidanggota.LostFocus
        Try
            Txtnopinjam.Items.Clear()
            tabel = proses.ExecuteQuery("SELECT NoPinjaman FROM pinjaman where IDAnggota = '" & Txtidanggota.Text & "'")

            If tabel.Rows.Count < 1 Then
                MsgBox("ID Anggota Tidak Ditemukan!")
            Else
                Txtnopinjam.Enabled = True
                For index As Integer = 1 To tabel.Rows.Count
                    Txtnopinjam.Items.Add(tabel.Rows(index - 1).Item(0))
                Next
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Label10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label10.Click

    End Sub
End Class