﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Bkp_Formanggota
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdsimpan = New System.Windows.Forms.Button()
        Me.cmdedit = New System.Windows.Forms.Button()
        Me.cmdhapus = New System.Windows.Forms.Button()
        Me.cmdbatal = New System.Windows.Forms.Button()
        Me.cmdkeluar = New System.Windows.Forms.Button()
        Me.cmdtambah = New System.Windows.Forms.Button()
        Me.Dganggota = New System.Windows.Forms.DataGridView()
        Me.Txtjabatan = New System.Windows.Forms.TextBox()
        Me.Txtnamaanggota = New System.Windows.Forms.TextBox()
        Me.Txtidanggota = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Txtcari = New System.Windows.Forms.TextBox()
        Me.Cbocari = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Txtnotelp = New System.Windows.Forms.TextBox()
        Me.Txtalamat = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        CType(Me.Dganggota, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdsimpan
        '
        Me.cmdsimpan.Location = New System.Drawing.Point(386, 207)
        Me.cmdsimpan.Name = "cmdsimpan"
        Me.cmdsimpan.Size = New System.Drawing.Size(79, 36)
        Me.cmdsimpan.TabIndex = 48
        Me.cmdsimpan.Text = "Simpan"
        Me.cmdsimpan.UseVisualStyleBackColor = True
        '
        'cmdedit
        '
        Me.cmdedit.Location = New System.Drawing.Point(471, 166)
        Me.cmdedit.Name = "cmdedit"
        Me.cmdedit.Size = New System.Drawing.Size(79, 36)
        Me.cmdedit.TabIndex = 47
        Me.cmdedit.Text = "Edit"
        Me.cmdedit.UseVisualStyleBackColor = True
        '
        'cmdhapus
        '
        Me.cmdhapus.Location = New System.Drawing.Point(556, 166)
        Me.cmdhapus.Name = "cmdhapus"
        Me.cmdhapus.Size = New System.Drawing.Size(79, 36)
        Me.cmdhapus.TabIndex = 46
        Me.cmdhapus.Text = "Hapus"
        Me.cmdhapus.UseVisualStyleBackColor = True
        '
        'cmdbatal
        '
        Me.cmdbatal.Location = New System.Drawing.Point(471, 208)
        Me.cmdbatal.Name = "cmdbatal"
        Me.cmdbatal.Size = New System.Drawing.Size(79, 36)
        Me.cmdbatal.TabIndex = 45
        Me.cmdbatal.Text = "Batal"
        Me.cmdbatal.UseVisualStyleBackColor = True
        '
        'cmdkeluar
        '
        Me.cmdkeluar.Location = New System.Drawing.Point(556, 208)
        Me.cmdkeluar.Name = "cmdkeluar"
        Me.cmdkeluar.Size = New System.Drawing.Size(79, 36)
        Me.cmdkeluar.TabIndex = 44
        Me.cmdkeluar.Text = "Keluar"
        Me.cmdkeluar.UseVisualStyleBackColor = True
        '
        'cmdtambah
        '
        Me.cmdtambah.Location = New System.Drawing.Point(386, 165)
        Me.cmdtambah.Name = "cmdtambah"
        Me.cmdtambah.Size = New System.Drawing.Size(79, 36)
        Me.cmdtambah.TabIndex = 43
        Me.cmdtambah.Text = "Tambah"
        Me.cmdtambah.UseVisualStyleBackColor = True
        '
        'Dganggota
        '
        Me.Dganggota.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dganggota.Location = New System.Drawing.Point(40, 272)
        Me.Dganggota.Name = "Dganggota"
        Me.Dganggota.Size = New System.Drawing.Size(767, 195)
        Me.Dganggota.TabIndex = 42
        '
        'Txtjabatan
        '
        Me.Txtjabatan.Location = New System.Drawing.Point(156, 111)
        Me.Txtjabatan.Name = "Txtjabatan"
        Me.Txtjabatan.Size = New System.Drawing.Size(141, 20)
        Me.Txtjabatan.TabIndex = 41
        '
        'Txtnamaanggota
        '
        Me.Txtnamaanggota.Location = New System.Drawing.Point(156, 69)
        Me.Txtnamaanggota.Name = "Txtnamaanggota"
        Me.Txtnamaanggota.Size = New System.Drawing.Size(141, 20)
        Me.Txtnamaanggota.TabIndex = 40
        '
        'Txtidanggota
        '
        Me.Txtidanggota.Location = New System.Drawing.Point(156, 30)
        Me.Txtidanggota.Name = "Txtidanggota"
        Me.Txtidanggota.Size = New System.Drawing.Size(142, 20)
        Me.Txtidanggota.TabIndex = 39
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Txtcari)
        Me.Panel1.Controls.Add(Me.Cbocari)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(41, 152)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(306, 88)
        Me.Panel1.TabIndex = 38
        '
        'Txtcari
        '
        Me.Txtcari.Location = New System.Drawing.Point(92, 55)
        Me.Txtcari.Name = "Txtcari"
        Me.Txtcari.Size = New System.Drawing.Size(165, 20)
        Me.Txtcari.TabIndex = 3
        '
        'Cbocari
        '
        Me.Cbocari.FormattingEnabled = True
        Me.Cbocari.Location = New System.Drawing.Point(91, 13)
        Me.Cbocari.Name = "Cbocari"
        Me.Cbocari.Size = New System.Drawing.Size(167, 21)
        Me.Cbocari.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(10, 53)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Text Dicari"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Berdasarkan"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(39, 115)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 13)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "Jabatan Anggota"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(38, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Nama Anggota"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(38, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "ID Anggota"
        '
        'Txtnotelp
        '
        Me.Txtnotelp.Location = New System.Drawing.Point(493, 68)
        Me.Txtnotelp.Name = "Txtnotelp"
        Me.Txtnotelp.Size = New System.Drawing.Size(141, 20)
        Me.Txtnotelp.TabIndex = 52
        '
        'Txtalamat
        '
        Me.Txtalamat.Location = New System.Drawing.Point(493, 29)
        Me.Txtalamat.Name = "Txtalamat"
        Me.Txtalamat.Size = New System.Drawing.Size(142, 20)
        Me.Txtalamat.TabIndex = 51
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(375, 74)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(57, 13)
        Me.Label6.TabIndex = 50
        Me.Label6.Text = "No Telpon"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(375, 35)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(82, 13)
        Me.Label7.TabIndex = 49
        Me.Label7.Text = "Alamat Anggota"
        '
        'Formanggota
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(844, 477)
        Me.Controls.Add(Me.Txtnotelp)
        Me.Controls.Add(Me.Txtalamat)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cmdsimpan)
        Me.Controls.Add(Me.cmdedit)
        Me.Controls.Add(Me.cmdhapus)
        Me.Controls.Add(Me.cmdbatal)
        Me.Controls.Add(Me.cmdkeluar)
        Me.Controls.Add(Me.cmdtambah)
        Me.Controls.Add(Me.Dganggota)
        Me.Controls.Add(Me.Txtjabatan)
        Me.Controls.Add(Me.Txtnamaanggota)
        Me.Controls.Add(Me.Txtidanggota)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Formanggota"
        Me.Text = "Input Anggota"
        CType(Me.Dganggota, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdsimpan As System.Windows.Forms.Button
    Friend WithEvents cmdedit As System.Windows.Forms.Button
    Friend WithEvents cmdhapus As System.Windows.Forms.Button
    Friend WithEvents cmdbatal As System.Windows.Forms.Button
    Friend WithEvents cmdkeluar As System.Windows.Forms.Button
    Friend WithEvents cmdtambah As System.Windows.Forms.Button
    Friend WithEvents Dganggota As System.Windows.Forms.DataGridView
    Friend WithEvents Txtjabatan As System.Windows.Forms.TextBox
    Friend WithEvents Txtnamaanggota As System.Windows.Forms.TextBox
    Friend WithEvents Txtidanggota As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Txtnotelp As System.Windows.Forms.TextBox
    Friend WithEvents Txtalamat As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
