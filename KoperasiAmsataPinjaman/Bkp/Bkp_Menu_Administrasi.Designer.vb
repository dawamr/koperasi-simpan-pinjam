﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Bkp_Menu_Administrasi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.panelMenuPengajuan = New System.Windows.Forms.Panel()
        Me.btnMenuPengajuan = New System.Windows.Forms.Button()
        Me.panelMenuSimpanan = New System.Windows.Forms.Panel()
        Me.btnMenuSimpanan = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel2.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.panelMenuPengajuan.SuspendLayout()
        Me.panelMenuSimpanan.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel2.Controls.Add(Me.Panel9)
        Me.Panel2.Controls.Add(Me.Panel8)
        Me.Panel2.Controls.Add(Me.Panel7)
        Me.Panel2.Controls.Add(Me.Panel6)
        Me.Panel2.Controls.Add(Me.Panel5)
        Me.Panel2.Controls.Add(Me.panelMenuPengajuan)
        Me.Panel2.Controls.Add(Me.panelMenuSimpanan)
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(180, 543)
        Me.Panel2.TabIndex = 2
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.Panel9.Controls.Add(Me.Button7)
        Me.Panel9.Location = New System.Drawing.Point(1, 170)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(177, 39)
        Me.Panel9.TabIndex = 2
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Button7.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button7.FlatAppearance.BorderSize = 0
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.Button7.ForeColor = System.Drawing.Color.White
        Me.Button7.Location = New System.Drawing.Point(5, 0)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(172, 39)
        Me.Button7.TabIndex = 0
        Me.Button7.Text = "Toko"
        Me.Button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.Panel8.Controls.Add(Me.Button6)
        Me.Panel8.Location = New System.Drawing.Point(1, 412)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(177, 39)
        Me.Panel8.TabIndex = 6
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Button6.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button6.FlatAppearance.BorderSize = 0
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.Location = New System.Drawing.Point(5, 0)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(172, 39)
        Me.Button6.TabIndex = 0
        Me.Button6.Text = "Akun"
        Me.Button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.Panel7.Controls.Add(Me.Button5)
        Me.Panel7.Location = New System.Drawing.Point(1, 371)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(177, 39)
        Me.Panel7.TabIndex = 5
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Button5.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button5.FlatAppearance.BorderSize = 0
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(5, 0)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(172, 39)
        Me.Button5.TabIndex = 0
        Me.Button5.Text = "Laporan"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.Panel6.Controls.Add(Me.Button4)
        Me.Panel6.Location = New System.Drawing.Point(1, 330)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(177, 39)
        Me.Panel6.TabIndex = 4
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Button4.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button4.FlatAppearance.BorderSize = 0
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(5, 0)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(172, 39)
        Me.Button4.TabIndex = 0
        Me.Button4.Text = "Angsuran"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.Panel5.Controls.Add(Me.Button3)
        Me.Panel5.Location = New System.Drawing.Point(1, 290)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(177, 39)
        Me.Panel5.TabIndex = 3
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Button3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(5, 0)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(172, 39)
        Me.Button3.TabIndex = 0
        Me.Button3.Text = "Pinjaman"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.UseVisualStyleBackColor = False
        '
        'panelMenuPengajuan
        '
        Me.panelMenuPengajuan.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuPengajuan.Controls.Add(Me.btnMenuPengajuan)
        Me.panelMenuPengajuan.Location = New System.Drawing.Point(1, 250)
        Me.panelMenuPengajuan.Name = "panelMenuPengajuan"
        Me.panelMenuPengajuan.Size = New System.Drawing.Size(177, 39)
        Me.panelMenuPengajuan.TabIndex = 2
        '
        'btnMenuPengajuan
        '
        Me.btnMenuPengajuan.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuPengajuan.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuPengajuan.FlatAppearance.BorderSize = 0
        Me.btnMenuPengajuan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuPengajuan.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.btnMenuPengajuan.ForeColor = System.Drawing.Color.White
        Me.btnMenuPengajuan.Location = New System.Drawing.Point(5, 0)
        Me.btnMenuPengajuan.Name = "btnMenuPengajuan"
        Me.btnMenuPengajuan.Size = New System.Drawing.Size(172, 39)
        Me.btnMenuPengajuan.TabIndex = 0
        Me.btnMenuPengajuan.Text = "Pengajuan Pinjaman"
        Me.btnMenuPengajuan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuPengajuan.UseVisualStyleBackColor = False
        '
        'panelMenuSimpanan
        '
        Me.panelMenuSimpanan.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuSimpanan.Controls.Add(Me.btnMenuSimpanan)
        Me.panelMenuSimpanan.Location = New System.Drawing.Point(1, 210)
        Me.panelMenuSimpanan.Name = "panelMenuSimpanan"
        Me.panelMenuSimpanan.Size = New System.Drawing.Size(177, 39)
        Me.panelMenuSimpanan.TabIndex = 1
        '
        'btnMenuSimpanan
        '
        Me.btnMenuSimpanan.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuSimpanan.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuSimpanan.FlatAppearance.BorderSize = 0
        Me.btnMenuSimpanan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuSimpanan.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.btnMenuSimpanan.ForeColor = System.Drawing.Color.White
        Me.btnMenuSimpanan.Location = New System.Drawing.Point(5, 0)
        Me.btnMenuSimpanan.Name = "btnMenuSimpanan"
        Me.btnMenuSimpanan.Size = New System.Drawing.Size(172, 39)
        Me.btnMenuSimpanan.TabIndex = 0
        Me.btnMenuSimpanan.Text = "Simpanan Sukarela"
        Me.btnMenuSimpanan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuSimpanan.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(36, 29)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(108, 108)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Administrasi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(180, 543)
        Me.Controls.Add(Me.Panel2)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "Administrasi"
        Me.Text = "Administrasi"
        Me.Panel2.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.panelMenuPengajuan.ResumeLayout(False)
        Me.panelMenuSimpanan.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents panelMenuPengajuan As System.Windows.Forms.Panel
    Friend WithEvents btnMenuPengajuan As System.Windows.Forms.Button
    Friend WithEvents panelMenuSimpanan As System.Windows.Forms.Panel
    Friend WithEvents btnMenuSimpanan As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
End Class
