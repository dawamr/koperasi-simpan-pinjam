﻿Public Class Bkp_Formpinjaman
    Dim strsql As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Private Sub Formpinjaman_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        aktif(False)
    End Sub
    Private Sub Formpinjaman_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        refreshGrid()
        baca_data(0)
        tombol(True)
    End Sub

    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("SELECT * FROM pinjaman ORDER BY NoPinjaman")
            dgpinjaman.DataSource = tabel
            With dgpinjaman
                .Columns(0).HeaderText = "No Pinjaman"
                .Columns(1).HeaderText = "ID Pengajuan Pinjaman"
                .Columns(2).HeaderText = "ID Anggota"
                .Columns(3).HeaderText = "Nama Anggota"
                .Columns(4).HeaderText = "Tanggal Pinjam"
                .Columns(5).HeaderText = "Tanggal Pelunasan"
                .Columns(6).HeaderText = "Nominal Pinjaman"
                .Columns(7).HeaderText = "Bunga Pinjaman"
                .Columns(8).HeaderText = "Nominal Angsuran"
                .Columns(9).HeaderText = "Banyak Angsuran"
                .Columns(10).HeaderText = "Total Pinjaman"
                .Columns(11).HeaderText = "Tanggal Dibuat"
                .Columns(12).HeaderText = "Terakir Diperbarui"
                .Columns(1).Visible = False
                .Columns(13).Visible = False
            End With
            dgpinjaman.ReadOnly = True
            dgpinjaman.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub baca_data(ByVal posisi As Integer)
        If posisi = 0 Then
            Txtidpinjaman.Text = IIf(IsDBNull(dgpinjaman.Rows(0).Cells(0).Value) = True, "-", _
                                           dgpinjaman.Rows(0).Cells(0).Value) 'nopinj
            Txtpengajuan.Text = IIf(IsDBNull(dgpinjaman.Rows(0).Cells(1).Value) = True, "-", _
                                  dgpinjaman.Rows(0).Cells(1).Value) 'pengajuanpinj
            Txtidanggota.Text = IIf(IsDBNull(dgpinjaman.Rows(0).Cells(2).Value) = True, "-", _
                                   dgpinjaman.Rows(0).Cells(2).Value) 'idanggota
            Txtnamaanggota.Text = IIf(IsDBNull(dgpinjaman.Rows(0).Cells(3).Value) = True, "-", _
                                   dgpinjaman.Rows(0).Cells(3).Value) 'nmanggota
            Txtnominalpinj.Text = IIf(IsDBNull(dgpinjaman.Rows(0).Cells(6).Value) = True, "-", _
                                  dgpinjaman.Rows(0).Cells(6).Value) 'nominalpinj
            Txtbungapinj.Text = IIf(IsDBNull(dgpinjaman.Rows(0).Cells(7).Value) = True, "-", _
                                 dgpinjaman.Rows(0).Cells(7).Value) 'bungapinj
            Txtnominalangs.Text = IIf(IsDBNull(dgpinjaman.Rows(0).Cells(8).Value) = True, "-", _
                                 dgpinjaman.Rows(0).Cells(8).Value) 'nominalangs
            Txtbanyakangs.Text = dgpinjaman.Rows(0).Cells(9).Value 'banyakangs

        Else
            If dgpinjaman.RowCount > 0 Then
                Txtidpinjaman.Text = IIf(IsDBNull(dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(0).Value) = True, _
                 "-", dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(0).Value)
                Txtpengajuan.Text = IIf(IsDBNull(dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(1).Value) = True, _
                     "-", dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(1).Value)
                Txtidanggota.Text = IIf(IsDBNull(dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(2).Value) = True, _
                     "-", dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(2).Value)
                Txtnamaanggota.Text = IIf(IsDBNull(dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(3).Value) = True, _
                     "-", dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(3).Value)
                Txtnominalpinj.Text = IIf(IsDBNull(dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(6).Value) = True, _
                     "-", dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(6).Value)
                Txtbungapinj.Text = IIf(IsDBNull(dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(7).Value) = True, _
                     "-", dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(7).Value)
                Txtnominalangs.Text = IIf(IsDBNull(dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(8).Value) = True, _
                     "-", dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(8).Value)
                Txtbanyakangs.Text = IIf(IsDBNull(dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(9).Value) = True, _
                     "-", dgpinjaman.Rows(dgpinjaman.CurrentCell.RowIndex).Cells(9).Value)
                Txttotalpinj.Text = "-"
            End If

        End If

    End Sub
    Private Sub tombol(ByVal x As Boolean)
        cmdcetak.Enabled = x
        cmdsimpan.Enabled = Not x
        cmdbatal.Enabled = Not x
        cmdkeluar.Enabled = x
    End Sub
    Private Sub aktif(ByVal x As Boolean)
        Txtidpinjaman.Enabled = x
        Txtidanggota.Enabled = x
        Txtnamaanggota.Enabled = x
        Txttotalpinj.Enabled = x
        Txtbungapinj.Enabled = x
        Txtnominalpinj.Enabled = x
        Txtnominalangs.Enabled = x
    End Sub
    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdkeluar.Click
        Me.Close()
    End Sub
    Private Sub cmdTambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdtambah.Click
        tombol(False)
        aktif(True)
        kosong()
        Txtidpinjaman.Text = getID("Pinjaman", "PJ0000", "NoPinjaman")
        pil = 1 'insert
    End Sub
    Private Sub kosong()
        Txtidpinjaman.Text = ""
        Txtidanggota.Text = ""
        Txtnamaanggota.Text = ""
        Txtbanyakangs.Text = ""
        Txtbungapinj.Text = ""
        Txtnominalpinj.Text = ""
        Txtnominalangs.Text = ""
        Txttotalpinj.Text = ""
        Dtpinjam.Value = DateTime.Now
        Dtpelunasan.Value = DateTime.Now

    End Sub
    Private Sub cmdBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdbatal.Click
        tombol(True)
        aktif(False)
    End Sub
    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsimpan.Click
        Dim msql As String
        If pil = 1 Then                         'tambah
            msql = "insert into pinjaman (NoPinjaman, IDPengajuan, IDAnggota, NamaAnggota, TanggalPinjam, TanggalPelunasan, NominalPinjaman, BungaPinjaman,  NominalAngsuran, BanyakAngsuran, totalpinjaman) VALUES ('" & Txtidpinjaman.Text & "','" & Txtpengajuan.Text & "','" & Txtidanggota.Text & "','" & Txtnamaanggota.Text & "','" & Format(Dtpinjam.Value, "yyy-MM-dd HH:mm:ss") & "','" & Format(Dtpelunasan.Value, "yyy-MM-dd HH:mm:ss") & "','" & Txtnominalpinj.Text & "','" & Txtbungapinj.Text & "','" & Txtnominalangs.Text & "','" & Txtbanyakangs.Text & "','" & Txttotalpinj.Text & "')"

        Else                                    'koreksi
            msql = "update pinjaman set NamaAnggota='" & _
            Txtnamaanggota.Text _
            & "', BungaPinjaman=" & Val(Txtbungapinj.Text) _
            & " where IDAnggota='" & Txtidanggota.Text & "'"
        End If
        proses.ExecuteNonQuery(msql)
        tombol(True)
        aktif(False)
        refreshGrid()
        MsgBox("Berhasil disimpan...", vbInformation, "Info")
    End Sub
    Private Sub dgpinjaman_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgpinjaman.CellContentClick
        baca_data(1)
    End Sub
    Private Sub dgpinjaman_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgpinjaman.CellMouseClick
        baca_data(1)
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        cekpengajuan = 1
        Formcaripengajuan.ShowDialog()
    End Sub

    Private Sub Txtbanyakangs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txtbanyakangs.SelectedIndexChanged
        Dim bungaangsr, angsrblnpokok As Integer
        Dim datepinjam, datelunas As Date
        If Txtbanyakangs.Text.Length > 0 Then
            angsrblnpokok = Convert.ToInt32(Txtnominalpinj.Text) / Convert.ToInt32(Txtbanyakangs.SelectedItem.ToString)
            bungaangsr = (Convert.ToInt32(Txtnominalpinj.Text) * (1.25 / 100))

            Txtbungapinj.Text = bungaangsr
            Txtnominalangs.Text = angsrblnpokok + bungaangsr
            Txttotalpinj.Text = Convert.ToInt32(Txtnominalpinj.Text) + (1.25 / 100 * (Convert.ToInt32(Txtbanyakangs.SelectedItem.ToString) * Convert.ToInt32(Txtnominalpinj.Text)))

            datepinjam = Dtpinjam.Value
            datelunas = datepinjam.AddMonths(Convert.ToInt32(Txtbanyakangs.SelectedItem.ToString))
            Dtpelunasan.Value = datelunas
            aktif(False)
        Else
            Txtnominalangs.Text = "0"
        End If
    End Sub
    'Private Sub Txtbanyakangs_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
    '  If Asc(e.KeyChar) > 58 Then
    '    e.KeyChar = ""
    '    End If
    'End Sub

    Private Sub cmdcetak_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdcetak.Click
        PrompAngsuran.ShowDialog()
    End Sub
End Class