﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Bkp_Formpinjaman
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Bkp_Formpinjaman))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Txttotalpinj = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Txtbanyakangs = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Txtpengajuan = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Txtnominalangs = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Txtbungapinj = New System.Windows.Forms.TextBox()
        Me.Txtnominalpinj = New System.Windows.Forms.TextBox()
        Me.Dtpelunasan = New System.Windows.Forms.DateTimePicker()
        Me.Dtpinjam = New System.Windows.Forms.DateTimePicker()
        Me.Txtnamaanggota = New System.Windows.Forms.TextBox()
        Me.Txtidanggota = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Txtidpinjaman = New System.Windows.Forms.TextBox()
        Me.dgpinjaman = New System.Windows.Forms.DataGridView()
        Me.cmdsimpan = New System.Windows.Forms.Button()
        Me.cmdbatal = New System.Windows.Forms.Button()
        Me.cmdcetak = New System.Windows.Forms.Button()
        Me.cmdkeluar = New System.Windows.Forms.Button()
        Me.cmdtambah = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgpinjaman, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(34, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "No Pinjaman"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.Txttotalpinj)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Txtbanyakangs)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Txtpengajuan)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Txtnominalangs)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Txtbungapinj)
        Me.GroupBox1.Controls.Add(Me.Txtnominalpinj)
        Me.GroupBox1.Controls.Add(Me.Dtpelunasan)
        Me.GroupBox1.Controls.Add(Me.Dtpinjam)
        Me.GroupBox1.Controls.Add(Me.Txtnamaanggota)
        Me.GroupBox1.Controls.Add(Me.Txtidanggota)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(35, 66)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(764, 168)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data Pinjaman"
        '
        'Txttotalpinj
        '
        Me.Txttotalpinj.Location = New System.Drawing.Point(565, 132)
        Me.Txttotalpinj.Name = "Txttotalpinj"
        Me.Txttotalpinj.Size = New System.Drawing.Size(123, 20)
        Me.Txttotalpinj.TabIndex = 25
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(433, 138)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 13)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Total  Pinjaman"
        '
        'Txtbanyakangs
        '
        Me.Txtbanyakangs.FormattingEnabled = True
        Me.Txtbanyakangs.Items.AddRange(New Object() {"6", "12"})
        Me.Txtbanyakangs.Location = New System.Drawing.Point(144, 119)
        Me.Txtbanyakangs.Name = "Txtbanyakangs"
        Me.Txtbanyakangs.Size = New System.Drawing.Size(121, 21)
        Me.Txtbanyakangs.TabIndex = 23
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(689, 110)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(27, 13)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "/Bln"
        '
        'Txtpengajuan
        '
        Me.Txtpengajuan.Location = New System.Drawing.Point(144, 22)
        Me.Txtpengajuan.Name = "Txtpengajuan"
        Me.Txtpengajuan.Size = New System.Drawing.Size(113, 20)
        Me.Txtpengajuan.TabIndex = 21
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(10, 119)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(91, 13)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Banyak Angsuran"
        '
        'Txtnominalangs
        '
        Me.Txtnominalangs.Location = New System.Drawing.Point(565, 106)
        Me.Txtnominalangs.Name = "Txtnominalangs"
        Me.Txtnominalangs.Size = New System.Drawing.Size(123, 20)
        Me.Txtnominalangs.TabIndex = 18
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(430, 108)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(88, 13)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Jumlah Angsuran"
        '
        'Txtbungapinj
        '
        Me.Txtbungapinj.Location = New System.Drawing.Point(565, 75)
        Me.Txtbungapinj.Name = "Txtbungapinj"
        Me.Txtbungapinj.Size = New System.Drawing.Size(122, 20)
        Me.Txtbungapinj.TabIndex = 15
        '
        'Txtnominalpinj
        '
        Me.Txtnominalpinj.Location = New System.Drawing.Point(144, 83)
        Me.Txtnominalpinj.Name = "Txtnominalpinj"
        Me.Txtnominalpinj.Size = New System.Drawing.Size(124, 20)
        Me.Txtnominalpinj.TabIndex = 13
        '
        'Dtpelunasan
        '
        Me.Dtpelunasan.Location = New System.Drawing.Point(563, 47)
        Me.Dtpelunasan.Name = "Dtpelunasan"
        Me.Dtpelunasan.Size = New System.Drawing.Size(157, 20)
        Me.Dtpelunasan.TabIndex = 12
        '
        'Dtpinjam
        '
        Me.Dtpinjam.Location = New System.Drawing.Point(563, 17)
        Me.Dtpinjam.Name = "Dtpinjam"
        Me.Dtpinjam.Size = New System.Drawing.Size(158, 20)
        Me.Dtpinjam.TabIndex = 11
        '
        'Txtnamaanggota
        '
        Me.Txtnamaanggota.Location = New System.Drawing.Point(234, 52)
        Me.Txtnamaanggota.Name = "Txtnamaanggota"
        Me.Txtnamaanggota.Size = New System.Drawing.Size(147, 20)
        Me.Txtnamaanggota.TabIndex = 10
        '
        'Txtidanggota
        '
        Me.Txtidanggota.Location = New System.Drawing.Point(145, 51)
        Me.Txtidanggota.Name = "Txtidanggota"
        Me.Txtidanggota.Size = New System.Drawing.Size(82, 20)
        Me.Txtidanggota.TabIndex = 9
        '
        'Button1
        '
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(272, 18)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(27, 24)
        Me.Button1.TabIndex = 8
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(430, 77)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(84, 13)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Bunga Pinjaman"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 82)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Nominal Pinjaman"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(428, 51)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(99, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Tanggal Pelunasan"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(427, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Tanggal Pinjam"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(113, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "ID dan Nama Anggota"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(118, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "ID Pengajuan Pinjaman"
        '
        'Txtidpinjaman
        '
        Me.Txtidpinjaman.Location = New System.Drawing.Point(129, 23)
        Me.Txtidpinjaman.Name = "Txtidpinjaman"
        Me.Txtidpinjaman.Size = New System.Drawing.Size(150, 20)
        Me.Txtidpinjaman.TabIndex = 3
        '
        'dgpinjaman
        '
        Me.dgpinjaman.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgpinjaman.Location = New System.Drawing.Point(37, 275)
        Me.dgpinjaman.Name = "dgpinjaman"
        Me.dgpinjaman.Size = New System.Drawing.Size(764, 189)
        Me.dgpinjaman.TabIndex = 5
        '
        'cmdsimpan
        '
        Me.cmdsimpan.Location = New System.Drawing.Point(465, 242)
        Me.cmdsimpan.Name = "cmdsimpan"
        Me.cmdsimpan.Size = New System.Drawing.Size(63, 27)
        Me.cmdsimpan.TabIndex = 6
        Me.cmdsimpan.Text = "Simpan"
        Me.cmdsimpan.UseVisualStyleBackColor = True
        '
        'cmdbatal
        '
        Me.cmdbatal.Location = New System.Drawing.Point(586, 242)
        Me.cmdbatal.Name = "cmdbatal"
        Me.cmdbatal.Size = New System.Drawing.Size(63, 27)
        Me.cmdbatal.TabIndex = 7
        Me.cmdbatal.Text = "Batal"
        Me.cmdbatal.UseVisualStyleBackColor = True
        '
        'cmdcetak
        '
        Me.cmdcetak.Location = New System.Drawing.Point(526, 242)
        Me.cmdcetak.Name = "cmdcetak"
        Me.cmdcetak.Size = New System.Drawing.Size(63, 27)
        Me.cmdcetak.TabIndex = 8
        Me.cmdcetak.Text = "Cetak"
        Me.cmdcetak.UseVisualStyleBackColor = True
        '
        'cmdkeluar
        '
        Me.cmdkeluar.Location = New System.Drawing.Point(646, 242)
        Me.cmdkeluar.Name = "cmdkeluar"
        Me.cmdkeluar.Size = New System.Drawing.Size(63, 27)
        Me.cmdkeluar.TabIndex = 9
        Me.cmdkeluar.Text = "Keluar"
        Me.cmdkeluar.UseVisualStyleBackColor = True
        '
        'cmdtambah
        '
        Me.cmdtambah.Image = CType(resources.GetObject("cmdtambah.Image"), System.Drawing.Image)
        Me.cmdtambah.Location = New System.Drawing.Point(296, 18)
        Me.cmdtambah.Name = "cmdtambah"
        Me.cmdtambah.Size = New System.Drawing.Size(36, 31)
        Me.cmdtambah.TabIndex = 10
        Me.cmdtambah.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdtambah.UseVisualStyleBackColor = True
        '
        'Bkp_Formpinjaman
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(822, 476)
        Me.Controls.Add(Me.cmdtambah)
        Me.Controls.Add(Me.cmdkeluar)
        Me.Controls.Add(Me.cmdcetak)
        Me.Controls.Add(Me.cmdbatal)
        Me.Controls.Add(Me.cmdsimpan)
        Me.Controls.Add(Me.dgpinjaman)
        Me.Controls.Add(Me.Txtidpinjaman)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Name = "Bkp_Formpinjaman"
        Me.Text = "Input Pinjaman Anggota"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgpinjaman, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Txtnominalangs As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Txtbungapinj As System.Windows.Forms.TextBox
    Friend WithEvents Txtnominalpinj As System.Windows.Forms.TextBox
    Friend WithEvents Dtpelunasan As System.Windows.Forms.DateTimePicker
    Friend WithEvents Dtpinjam As System.Windows.Forms.DateTimePicker
    Friend WithEvents Txtnamaanggota As System.Windows.Forms.TextBox
    Friend WithEvents Txtidanggota As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Txtidpinjaman As System.Windows.Forms.TextBox
    Friend WithEvents dgpinjaman As System.Windows.Forms.DataGridView
    Friend WithEvents cmdsimpan As System.Windows.Forms.Button
    Friend WithEvents cmdbatal As System.Windows.Forms.Button
    Friend WithEvents cmdcetak As System.Windows.Forms.Button
    Friend WithEvents cmdkeluar As System.Windows.Forms.Button
    Friend WithEvents cmdtambah As System.Windows.Forms.Button
    Friend WithEvents Txtpengajuan As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Txtbanyakangs As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Txttotalpinj As System.Windows.Forms.TextBox
End Class
