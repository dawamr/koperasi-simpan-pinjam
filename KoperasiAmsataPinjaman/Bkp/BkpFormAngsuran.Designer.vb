﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BkpFormAngsuran
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Txtkurangbayar = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Txtsisaangsuran = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dttgl = New System.Windows.Forms.DateTimePicker()
        Me.Txttotalpinjam = New System.Windows.Forms.TextBox()
        Me.Txtnominalangsuran = New System.Windows.Forms.TextBox()
        Me.Txtangsuranke = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtnoangsuran = New System.Windows.Forms.TextBox()
        Me.Dgangsuran = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Txtnamaanggota = New System.Windows.Forms.TextBox()
        Me.cmdsimpan = New System.Windows.Forms.Button()
        Me.cmdbatal = New System.Windows.Forms.Button()
        Me.cmdkeluar = New System.Windows.Forms.Button()
        Me.Txtidanggota = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnCari = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Txtnopinjam = New System.Windows.Forms.ComboBox()
        Me.cmdcetak = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.Dgangsuran, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(44, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "No Angsuran"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.Txtkurangbayar)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Txtsisaangsuran)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.dttgl)
        Me.GroupBox1.Controls.Add(Me.Txttotalpinjam)
        Me.GroupBox1.Controls.Add(Me.Txtnominalangsuran)
        Me.GroupBox1.Controls.Add(Me.Txtangsuranke)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(47, 123)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(763, 156)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pembayaran Angsuran"
        '
        'Txtkurangbayar
        '
        Me.Txtkurangbayar.Location = New System.Drawing.Point(165, 97)
        Me.Txtkurangbayar.Name = "Txtkurangbayar"
        Me.Txtkurangbayar.Size = New System.Drawing.Size(113, 20)
        Me.Txtkurangbayar.TabIndex = 11
        Me.Txtkurangbayar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(18, 100)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(127, 13)
        Me.Label10.TabIndex = 10
        Me.Label10.Text = "Kekurangan Pembayaran"
        '
        'Txtsisaangsuran
        '
        Me.Txtsisaangsuran.Location = New System.Drawing.Point(524, 66)
        Me.Txtsisaangsuran.Name = "Txtsisaangsuran"
        Me.Txtsisaangsuran.Size = New System.Drawing.Size(114, 20)
        Me.Txtsisaangsuran.TabIndex = 9
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(388, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Sisa Angsuran"
        '
        'dttgl
        '
        Me.dttgl.Location = New System.Drawing.Point(164, 34)
        Me.dttgl.Name = "dttgl"
        Me.dttgl.Size = New System.Drawing.Size(156, 20)
        Me.dttgl.TabIndex = 7
        '
        'Txttotalpinjam
        '
        Me.Txttotalpinjam.Location = New System.Drawing.Point(166, 124)
        Me.Txttotalpinjam.Name = "Txttotalpinjam"
        Me.Txttotalpinjam.Size = New System.Drawing.Size(112, 20)
        Me.Txttotalpinjam.TabIndex = 6
        Me.Txttotalpinjam.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Txtnominalangsuran
        '
        Me.Txtnominalangsuran.Location = New System.Drawing.Point(165, 68)
        Me.Txtnominalangsuran.Name = "Txtnominalangsuran"
        Me.Txtnominalangsuran.Size = New System.Drawing.Size(113, 20)
        Me.Txtnominalangsuran.TabIndex = 5
        Me.Txtnominalangsuran.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Txtangsuranke
        '
        Me.Txtangsuranke.Location = New System.Drawing.Point(524, 35)
        Me.Txtangsuranke.Name = "Txtangsuranke"
        Me.Txtangsuranke.Size = New System.Drawing.Size(114, 20)
        Me.Txtangsuranke.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(18, 127)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(79, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Nominal Pinjam"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(17, 69)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(75, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Nominal Bayar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Tanggal Bayar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(388, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Angsuran Ke"
        '
        'txtnoangsuran
        '
        Me.txtnoangsuran.Location = New System.Drawing.Point(140, 15)
        Me.txtnoangsuran.Name = "txtnoangsuran"
        Me.txtnoangsuran.Size = New System.Drawing.Size(125, 20)
        Me.txtnoangsuran.TabIndex = 3
        '
        'Dgangsuran
        '
        Me.Dgangsuran.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgangsuran.Location = New System.Drawing.Point(47, 295)
        Me.Dgangsuran.Name = "Dgangsuran"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Century", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Dgangsuran.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Century", 9.25!)
        Me.Dgangsuran.RowsDefaultCellStyle = DataGridViewCellStyle4
        Me.Dgangsuran.Size = New System.Drawing.Size(786, 173)
        Me.Dgangsuran.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(44, 100)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Nama Anggota"
        '
        'Txtnamaanggota
        '
        Me.Txtnamaanggota.Location = New System.Drawing.Point(140, 97)
        Me.Txtnamaanggota.Name = "Txtnamaanggota"
        Me.Txtnamaanggota.Size = New System.Drawing.Size(125, 20)
        Me.Txtnamaanggota.TabIndex = 7
        '
        'cmdsimpan
        '
        Me.cmdsimpan.Location = New System.Drawing.Point(571, 21)
        Me.cmdsimpan.Name = "cmdsimpan"
        Me.cmdsimpan.Size = New System.Drawing.Size(69, 29)
        Me.cmdsimpan.TabIndex = 9
        Me.cmdsimpan.Text = "Simpan"
        Me.cmdsimpan.UseVisualStyleBackColor = True
        '
        'cmdbatal
        '
        Me.cmdbatal.Location = New System.Drawing.Point(571, 56)
        Me.cmdbatal.Name = "cmdbatal"
        Me.cmdbatal.Size = New System.Drawing.Size(69, 29)
        Me.cmdbatal.TabIndex = 10
        Me.cmdbatal.Text = "Batal"
        Me.cmdbatal.UseVisualStyleBackColor = True
        '
        'cmdkeluar
        '
        Me.cmdkeluar.Location = New System.Drawing.Point(646, 57)
        Me.cmdkeluar.Name = "cmdkeluar"
        Me.cmdkeluar.Size = New System.Drawing.Size(69, 29)
        Me.cmdkeluar.TabIndex = 11
        Me.cmdkeluar.Text = "Keluar"
        Me.cmdkeluar.UseVisualStyleBackColor = True
        '
        'Txtidanggota
        '
        Me.Txtidanggota.Location = New System.Drawing.Point(140, 68)
        Me.Txtidanggota.Name = "Txtidanggota"
        Me.Txtidanggota.Size = New System.Drawing.Size(125, 20)
        Me.Txtidanggota.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(44, 72)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "ID Anggota"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(571, 135)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(69, 29)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Tambah"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnCari
        '
        Me.btnCari.Location = New System.Drawing.Point(271, 42)
        Me.btnCari.Name = "btnCari"
        Me.btnCari.Size = New System.Drawing.Size(37, 20)
        Me.btnCari.TabIndex = 14
        Me.btnCari.Text = "Cari"
        Me.btnCari.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(44, 46)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(67, 13)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "No Pinjaman"
        '
        'Txtnopinjam
        '
        Me.Txtnopinjam.FormattingEnabled = True
        Me.Txtnopinjam.Items.AddRange(New Object() {"PJ00001"})
        Me.Txtnopinjam.Location = New System.Drawing.Point(140, 41)
        Me.Txtnopinjam.Name = "Txtnopinjam"
        Me.Txtnopinjam.Size = New System.Drawing.Size(125, 21)
        Me.Txtnopinjam.TabIndex = 16
        '
        'cmdcetak
        '
        Me.cmdcetak.Location = New System.Drawing.Point(646, 22)
        Me.cmdcetak.Name = "cmdcetak"
        Me.cmdcetak.Size = New System.Drawing.Size(69, 29)
        Me.cmdcetak.TabIndex = 17
        Me.cmdcetak.Text = "Cetak"
        Me.cmdcetak.UseVisualStyleBackColor = True
        '
        'BkpFormAngsuran
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(863, 480)
        Me.Controls.Add(Me.cmdcetak)
        Me.Controls.Add(Me.Txtnopinjam)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.btnCari)
        Me.Controls.Add(Me.Txtidanggota)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.cmdkeluar)
        Me.Controls.Add(Me.cmdbatal)
        Me.Controls.Add(Me.cmdsimpan)
        Me.Controls.Add(Me.Txtnamaanggota)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Dgangsuran)
        Me.Controls.Add(Me.txtnoangsuran)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "BkpFormAngsuran"
        Me.Text = "Pembayaran Angsuran"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.Dgangsuran, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dttgl As System.Windows.Forms.DateTimePicker
    Friend WithEvents Txttotalpinjam As System.Windows.Forms.TextBox
    Friend WithEvents Txtnominalangsuran As System.Windows.Forms.TextBox
    Friend WithEvents Txtangsuranke As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtnoangsuran As System.Windows.Forms.TextBox
    Friend WithEvents Dgangsuran As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Txtnamaanggota As System.Windows.Forms.TextBox
    Friend WithEvents cmdsimpan As System.Windows.Forms.Button
    Friend WithEvents cmdbatal As System.Windows.Forms.Button
    Friend WithEvents cmdkeluar As System.Windows.Forms.Button
    Friend WithEvents Txtidanggota As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Txtsisaangsuran As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnCari As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Txtnopinjam As System.Windows.Forms.ComboBox
    Friend WithEvents cmdcetak As System.Windows.Forms.Button
    Friend WithEvents Txtkurangbayar As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
End Class
