﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Bkp_Formpengajuanpinj
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Bkp_Formpengajuanpinj))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Txtcari = New System.Windows.Forms.TextBox()
        Me.Cbocari = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Txtidanggota = New System.Windows.Forms.TextBox()
        Me.Txtnamaanggota = New System.Windows.Forms.TextBox()
        Me.Txtnominal = New System.Windows.Forms.TextBox()
        Me.Dgpengajuan = New System.Windows.Forms.DataGridView()
        Me.Dttgl = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Cbostatus = New System.Windows.Forms.ComboBox()
        Me.Txtpengajuan = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cmdsimpan = New System.Windows.Forms.Button()
        Me.cmdedit = New System.Windows.Forms.Button()
        Me.cmdhapus = New System.Windows.Forms.Button()
        Me.cmdbatal = New System.Windows.Forms.Button()
        Me.cmdkeluar = New System.Windows.Forms.Button()
        Me.cmdtambah = New System.Windows.Forms.Button()
        CType(Me.Dgpengajuan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(538, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ID Anggota"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(539, 47)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nama Anggota"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 84)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Nominal"
        '
        'Txtcari
        '
        Me.Txtcari.Location = New System.Drawing.Point(633, 186)
        Me.Txtcari.Name = "Txtcari"
        Me.Txtcari.Size = New System.Drawing.Size(165, 20)
        Me.Txtcari.TabIndex = 3
        '
        'Cbocari
        '
        Me.Cbocari.FormattingEnabled = True
        Me.Cbocari.Location = New System.Drawing.Point(381, 187)
        Me.Cbocari.Name = "Cbocari"
        Me.Cbocari.Size = New System.Drawing.Size(167, 21)
        Me.Cbocari.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(567, 190)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Text Dicari"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(300, 190)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Berdasarkan"
        '
        'Txtidanggota
        '
        Me.Txtidanggota.Location = New System.Drawing.Point(656, 11)
        Me.Txtidanggota.Name = "Txtidanggota"
        Me.Txtidanggota.Size = New System.Drawing.Size(142, 20)
        Me.Txtidanggota.TabIndex = 5
        '
        'Txtnamaanggota
        '
        Me.Txtnamaanggota.Location = New System.Drawing.Point(656, 43)
        Me.Txtnamaanggota.Name = "Txtnamaanggota"
        Me.Txtnamaanggota.Size = New System.Drawing.Size(142, 20)
        Me.Txtnamaanggota.TabIndex = 6
        '
        'Txtnominal
        '
        Me.Txtnominal.Location = New System.Drawing.Point(142, 81)
        Me.Txtnominal.Name = "Txtnominal"
        Me.Txtnominal.Size = New System.Drawing.Size(164, 20)
        Me.Txtnominal.TabIndex = 7
        '
        'Dgpengajuan
        '
        Me.Dgpengajuan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgpengajuan.Location = New System.Drawing.Point(12, 216)
        Me.Dgpengajuan.Name = "Dgpengajuan"
        Me.Dgpengajuan.Size = New System.Drawing.Size(818, 264)
        Me.Dgpengajuan.TabIndex = 8
        '
        'Dttgl
        '
        Me.Dttgl.Location = New System.Drawing.Point(141, 48)
        Me.Dttgl.Name = "Dttgl"
        Me.Dttgl.Size = New System.Drawing.Size(165, 20)
        Me.Dttgl.TabIndex = 15
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 52)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 13)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "Tanggal Pengajuan"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(18, 117)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(37, 13)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Status"
        '
        'Cbostatus
        '
        Me.Cbostatus.FormattingEnabled = True
        Me.Cbostatus.Location = New System.Drawing.Point(142, 117)
        Me.Cbostatus.Name = "Cbostatus"
        Me.Cbostatus.Size = New System.Drawing.Size(164, 21)
        Me.Cbostatus.TabIndex = 18
        '
        'Txtpengajuan
        '
        Me.Txtpengajuan.Location = New System.Drawing.Point(142, 14)
        Me.Txtpengajuan.Name = "Txtpengajuan"
        Me.Txtpengajuan.Size = New System.Drawing.Size(164, 20)
        Me.Txtpengajuan.TabIndex = 21
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(16, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(118, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "ID Pengajuan Pinjaman"
        '
        'Button1
        '
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button1.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.cari
        Me.Button1.Location = New System.Drawing.Point(805, 9)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(25, 25)
        Me.Button1.TabIndex = 24
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cmdsimpan
        '
        Me.cmdsimpan.BackColor = System.Drawing.Color.Transparent
        Me.cmdsimpan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdsimpan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdsimpan.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.simpan
        Me.cmdsimpan.Location = New System.Drawing.Point(805, 73)
        Me.cmdsimpan.Name = "cmdsimpan"
        Me.cmdsimpan.Size = New System.Drawing.Size(50, 35)
        Me.cmdsimpan.TabIndex = 14
        Me.cmdsimpan.UseVisualStyleBackColor = False
        Me.cmdsimpan.Visible = False
        '
        'cmdedit
        '
        Me.cmdedit.BackColor = System.Drawing.Color.Transparent
        Me.cmdedit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdedit.Image = CType(resources.GetObject("cmdedit.Image"), System.Drawing.Image)
        Me.cmdedit.Location = New System.Drawing.Point(751, 72)
        Me.cmdedit.Name = "cmdedit"
        Me.cmdedit.Size = New System.Drawing.Size(50, 35)
        Me.cmdedit.TabIndex = 13
        Me.cmdedit.UseVisualStyleBackColor = False
        '
        'cmdhapus
        '
        Me.cmdhapus.BackColor = System.Drawing.Color.Transparent
        Me.cmdhapus.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdhapus.Image = CType(resources.GetObject("cmdhapus.Image"), System.Drawing.Image)
        Me.cmdhapus.Location = New System.Drawing.Point(699, 113)
        Me.cmdhapus.Name = "cmdhapus"
        Me.cmdhapus.Size = New System.Drawing.Size(50, 35)
        Me.cmdhapus.TabIndex = 12
        Me.cmdhapus.UseVisualStyleBackColor = False
        '
        'cmdbatal
        '
        Me.cmdbatal.BackColor = System.Drawing.Color.Transparent
        Me.cmdbatal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdbatal.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.keluar
        Me.cmdbatal.Location = New System.Drawing.Point(805, 113)
        Me.cmdbatal.Name = "cmdbatal"
        Me.cmdbatal.Size = New System.Drawing.Size(50, 35)
        Me.cmdbatal.TabIndex = 11
        Me.cmdbatal.UseVisualStyleBackColor = False
        Me.cmdbatal.Visible = False
        '
        'cmdkeluar
        '
        Me.cmdkeluar.BackColor = System.Drawing.Color.Transparent
        Me.cmdkeluar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdkeluar.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.exitjpg
        Me.cmdkeluar.Location = New System.Drawing.Point(751, 113)
        Me.cmdkeluar.Name = "cmdkeluar"
        Me.cmdkeluar.Size = New System.Drawing.Size(50, 35)
        Me.cmdkeluar.TabIndex = 10
        Me.cmdkeluar.UseVisualStyleBackColor = False
        '
        'cmdtambah
        '
        Me.cmdtambah.BackColor = System.Drawing.Color.Transparent
        Me.cmdtambah.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.cmdtambah.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdtambah.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.tambah
        Me.cmdtambah.Location = New System.Drawing.Point(699, 73)
        Me.cmdtambah.Margin = New System.Windows.Forms.Padding(0)
        Me.cmdtambah.Name = "cmdtambah"
        Me.cmdtambah.Size = New System.Drawing.Size(50, 35)
        Me.cmdtambah.TabIndex = 9
        Me.cmdtambah.UseVisualStyleBackColor = False
        '
        'Bkp_Formpengajuanpinj
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(846, 490)
        Me.Controls.Add(Me.Txtcari)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Cbocari)
        Me.Controls.Add(Me.Txtpengajuan)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Cbostatus)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Dttgl)
        Me.Controls.Add(Me.cmdsimpan)
        Me.Controls.Add(Me.cmdedit)
        Me.Controls.Add(Me.cmdhapus)
        Me.Controls.Add(Me.cmdbatal)
        Me.Controls.Add(Me.cmdkeluar)
        Me.Controls.Add(Me.cmdtambah)
        Me.Controls.Add(Me.Dgpengajuan)
        Me.Controls.Add(Me.Txtnominal)
        Me.Controls.Add(Me.Txtnamaanggota)
        Me.Controls.Add(Me.Txtidanggota)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "Bkp_Formpengajuanpinj"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Pengajuan Pinjaman"
        CType(Me.Dgpengajuan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Txtidanggota As System.Windows.Forms.TextBox
    Friend WithEvents Txtnamaanggota As System.Windows.Forms.TextBox
    Friend WithEvents Txtnominal As System.Windows.Forms.TextBox
    Friend WithEvents Dgpengajuan As System.Windows.Forms.DataGridView
    Friend WithEvents cmdtambah As System.Windows.Forms.Button
    Friend WithEvents cmdkeluar As System.Windows.Forms.Button
    Friend WithEvents cmdbatal As System.Windows.Forms.Button
    Friend WithEvents cmdhapus As System.Windows.Forms.Button
    Friend WithEvents cmdedit As System.Windows.Forms.Button
    Friend WithEvents cmdsimpan As System.Windows.Forms.Button
    Friend WithEvents Dttgl As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Cbostatus As System.Windows.Forms.ComboBox
    Friend WithEvents Txtpengajuan As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class
