﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormUser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Txtcari = New System.Windows.Forms.TextBox()
        Me.Cbocari = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Dguser = New System.Windows.Forms.DataGridView()
        Me.Cbohakakses = New System.Windows.Forms.ComboBox()
        Me.Txtulangi = New System.Windows.Forms.TextBox()
        Me.Txtpassword = New System.Windows.Forms.TextBox()
        Me.Txtnama = New System.Windows.Forms.TextBox()
        Me.Txtuserid = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdsimpan = New System.Windows.Forms.Button()
        Me.cmdedit = New System.Windows.Forms.Button()
        Me.cmdhapus = New System.Windows.Forms.Button()
        Me.cmdbatal = New System.Windows.Forms.Button()
        Me.cmdkeluar = New System.Windows.Forms.Button()
        Me.cmdtambah = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        CType(Me.Dguser, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Txtcari)
        Me.Panel1.Controls.Add(Me.Cbocari)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Location = New System.Drawing.Point(107, 354)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(306, 88)
        Me.Panel1.TabIndex = 55
        '
        'Txtcari
        '
        Me.Txtcari.Location = New System.Drawing.Point(92, 55)
        Me.Txtcari.Name = "Txtcari"
        Me.Txtcari.Size = New System.Drawing.Size(165, 20)
        Me.Txtcari.TabIndex = 3
        '
        'Cbocari
        '
        Me.Cbocari.FormattingEnabled = True
        Me.Cbocari.Location = New System.Drawing.Point(91, 13)
        Me.Cbocari.Name = "Cbocari"
        Me.Cbocari.Size = New System.Drawing.Size(167, 21)
        Me.Cbocari.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 53)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Text Dicari"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(67, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Berdasarkan"
        '
        'Dguser
        '
        Me.Dguser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dguser.Location = New System.Drawing.Point(107, 191)
        Me.Dguser.Name = "Dguser"
        Me.Dguser.Size = New System.Drawing.Size(528, 138)
        Me.Dguser.TabIndex = 54
        '
        'Cbohakakses
        '
        Me.Cbohakakses.FormattingEnabled = True
        Me.Cbohakakses.Location = New System.Drawing.Point(202, 144)
        Me.Cbohakakses.Name = "Cbohakakses"
        Me.Cbohakakses.Size = New System.Drawing.Size(147, 21)
        Me.Cbohakakses.TabIndex = 53
        '
        'Txtulangi
        '
        Me.Txtulangi.Location = New System.Drawing.Point(202, 107)
        Me.Txtulangi.Name = "Txtulangi"
        Me.Txtulangi.Size = New System.Drawing.Size(149, 20)
        Me.Txtulangi.TabIndex = 52
        '
        'Txtpassword
        '
        Me.Txtpassword.Location = New System.Drawing.Point(202, 75)
        Me.Txtpassword.Name = "Txtpassword"
        Me.Txtpassword.Size = New System.Drawing.Size(149, 20)
        Me.Txtpassword.TabIndex = 51
        '
        'Txtnama
        '
        Me.Txtnama.Location = New System.Drawing.Point(202, 40)
        Me.Txtnama.Name = "Txtnama"
        Me.Txtnama.Size = New System.Drawing.Size(149, 20)
        Me.Txtnama.TabIndex = 50
        '
        'Txtuserid
        '
        Me.Txtuserid.Location = New System.Drawing.Point(202, 9)
        Me.Txtuserid.Name = "Txtuserid"
        Me.Txtuserid.Size = New System.Drawing.Size(149, 20)
        Me.Txtuserid.TabIndex = 49
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(104, 147)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 13)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "Hak Akses"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(104, 114)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 13)
        Me.Label4.TabIndex = 37
        Me.Label4.Text = "Ulangi Password"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(104, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 36
        Me.Label3.Text = "Password"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(104, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Nama"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(104, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "User ID"
        '
        'cmdsimpan
        '
        Me.cmdsimpan.Location = New System.Drawing.Point(386, 96)
        Me.cmdsimpan.Name = "cmdsimpan"
        Me.cmdsimpan.Size = New System.Drawing.Size(79, 36)
        Me.cmdsimpan.TabIndex = 61
        Me.cmdsimpan.Text = "Simpan"
        Me.cmdsimpan.UseVisualStyleBackColor = True
        '
        'cmdedit
        '
        Me.cmdedit.Location = New System.Drawing.Point(471, 55)
        Me.cmdedit.Name = "cmdedit"
        Me.cmdedit.Size = New System.Drawing.Size(79, 36)
        Me.cmdedit.TabIndex = 60
        Me.cmdedit.Text = "Edit"
        Me.cmdedit.UseVisualStyleBackColor = True
        '
        'cmdhapus
        '
        Me.cmdhapus.Location = New System.Drawing.Point(556, 55)
        Me.cmdhapus.Name = "cmdhapus"
        Me.cmdhapus.Size = New System.Drawing.Size(79, 36)
        Me.cmdhapus.TabIndex = 59
        Me.cmdhapus.Text = "Hapus"
        Me.cmdhapus.UseVisualStyleBackColor = True
        '
        'cmdbatal
        '
        Me.cmdbatal.Location = New System.Drawing.Point(471, 97)
        Me.cmdbatal.Name = "cmdbatal"
        Me.cmdbatal.Size = New System.Drawing.Size(79, 36)
        Me.cmdbatal.TabIndex = 58
        Me.cmdbatal.Text = "Batal"
        Me.cmdbatal.UseVisualStyleBackColor = True
        '
        'cmdkeluar
        '
        Me.cmdkeluar.Location = New System.Drawing.Point(556, 97)
        Me.cmdkeluar.Name = "cmdkeluar"
        Me.cmdkeluar.Size = New System.Drawing.Size(79, 36)
        Me.cmdkeluar.TabIndex = 57
        Me.cmdkeluar.Text = "Keluar"
        Me.cmdkeluar.UseVisualStyleBackColor = True
        '
        'cmdtambah
        '
        Me.cmdtambah.Location = New System.Drawing.Point(386, 54)
        Me.cmdtambah.Name = "cmdtambah"
        Me.cmdtambah.Size = New System.Drawing.Size(79, 36)
        Me.cmdtambah.TabIndex = 56
        Me.cmdtambah.Text = "Tambah"
        Me.cmdtambah.UseVisualStyleBackColor = True
        '
        'FormUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(865, 454)
        Me.Controls.Add(Me.cmdsimpan)
        Me.Controls.Add(Me.cmdedit)
        Me.Controls.Add(Me.cmdhapus)
        Me.Controls.Add(Me.cmdbatal)
        Me.Controls.Add(Me.cmdkeluar)
        Me.Controls.Add(Me.cmdtambah)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Dguser)
        Me.Controls.Add(Me.Cbohakakses)
        Me.Controls.Add(Me.Txtulangi)
        Me.Controls.Add(Me.Txtpassword)
        Me.Controls.Add(Me.Txtnama)
        Me.Controls.Add(Me.Txtuserid)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FormUser"
        Me.Text = "Input User"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Dguser, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Dguser As System.Windows.Forms.DataGridView
    Friend WithEvents Cbohakakses As System.Windows.Forms.ComboBox
    Friend WithEvents Txtulangi As System.Windows.Forms.TextBox
    Friend WithEvents Txtpassword As System.Windows.Forms.TextBox
    Friend WithEvents Txtnama As System.Windows.Forms.TextBox
    Friend WithEvents Txtuserid As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdsimpan As System.Windows.Forms.Button
    Friend WithEvents cmdedit As System.Windows.Forms.Button
    Friend WithEvents cmdhapus As System.Windows.Forms.Button
    Friend WithEvents cmdbatal As System.Windows.Forms.Button
    Friend WithEvents cmdkeluar As System.Windows.Forms.Button
    Friend WithEvents cmdtambah As System.Windows.Forms.Button
End Class
