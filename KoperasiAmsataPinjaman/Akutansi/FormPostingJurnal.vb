﻿Public Class FormPostingJurnal
    Dim jrec As Integer = 0

    Private Sub cmdposting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdposting.Click
        'cek sudah diposting atau belum
        If cmbakun.Text = "Penjualan" Then
            If cekData("4101") = True Then 'akun penjualan
                MsgBox("Data " & cmbakun.Text & " sudah di POSTING....!!!",
                vbCritical, "Warning")
                Exit Sub
            End If
        Else
            If cekData("5101") = True Then 'akun pembelian
                MsgBox("Data " & cmbakun.Text & " sudah di POSTING....!!!",
                vbCritical, "Warning")
                Exit Sub
            End If
        End If
        baca_data(cmbakun.Text)
    End Sub
    Private Sub FormPostingJurnal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbakun.Items.Clear()
        cmbakun.Items.Add("Penjualan")
        cmbakun.Items.Add("Pembelian")
        dtawal.Value = Now
        dtakhir.Value = Now
        cmbakun.SelectedIndex = 0
    End Sub
    Private Sub baca_data(ByVal nama As String)
        Dim msql As String = ""
        Dim jns As Byte = 0
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        If nama = "Penjualan" Then
            jns = 1 'jenis trx penjualan
            msql = "select * from lapjual where " _
            & "date(tgl_jual)>=date('" _
            & dtawal.Value.ToString("yyyy-MM-dd") & "') and " _
            & "date(tgl_jual)<=date('" _
            & dtakhir.Value.ToString("yyyy-MM-dd") & "') " _
            & " order by no_jual asc"
        ElseIf nama = "Pembelian" Then
            jns = 0 'jns trx pembelian
            msql = "select * from lapbeli where " _
            & "date(tgl_beli)>=date('" _
            & dtawal.Value.ToString("yyyy-MM-dd") & "') and " _
            & "date(tgl_beli)<=date('" _
            & dtakhir.Value.ToString("yyyy-MM-dd") & "') " _
            & " order by no_beli asc"
        Else
            'isi dengan transaksi yang lain, misal retur jual
        End If
        tabel = proses.ExecuteQuery(msql)
        jrec = tabel.Rows.Count
        For i As Integer = 0 To tabel.Rows.Count - 1
            'posting penjualan tunai & pembelian tunai
            post_jurnal(tabel.Rows(i).Item(0), cmbakun.Text, IIf(jns = 1,
            tabel.Rows(i).Item(7), tabel.Rows(i).Item(6)), jns, IIf(jns = 1,
            tabel.Rows(i).Item(8), tabel.Rows(i).Item(7)))
            progress(i, jrec)
        Next i
    End Sub
    Private Sub progress(ByVal x As Integer, ByVal jdata As Integer)
        Dim xx As Integer
        xx = (x + 1) * (100 / jdata)
        ProgressBar1.Value = xx
        LblProsen.Text = Trim(Str(xx)) & " % Completed"
    End Sub
    Private Function cekData(ByVal kode As String) As Boolean
        Dim msql As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim hasil As Boolean = False
        msql = "select kodeakun from vjurnal where kodeakun='" & _
        kode & "' and date(tanggal)>=date('" _
        & dtAwal.Value.ToString("yyyy-MM-dd") & "') and " _
        & "date(tanggal)<=date('" _
        & dtAkhir.Value.ToString("yyyy-MM-dd") & "') " _
        & " order by nojurnal asc"
        tabel = proses.ExecuteQuery(msql)
        If tabel.Rows.Count >= 1 Then
            hasil = True
        Else
            hasil = False
        End If
        Return hasil
    End Function
    Private Sub cmdkeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdkeluar.Click
        Me.Close()
    End Sub
End Class