﻿Public Class FormArusKas
    Dim tahun As Integer
    Private Sub FormArusKas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbbulan.Items.Clear()
        cmbtahun.Items.Clear()
        For i As Integer = 1 To 12
            cmbbulan.Items.Add(i)
        Next
        tahun = Year(Now) '2018
        For j As Integer = (tahun - 2) To tahun
            cmbtahun.Items.Add(j) '2016-2018
        Next
        cmbbulan.SelectedIndex = Month(Now) - 1
        cmbtahun.Text = tahun
        init_arusKas()
    End Sub
    Private Sub init_arusKas()
        Dim jmlKas As Double = 0
        Dim saldoAwal As Double = 0
        Dim saldoAkhir As Double = 0
        lstData.Items.Clear()
        baca_ArusKas(1)
        baca_ArusKas(2)
        baca_ArusKas(3)
        jmlKas = getJumlah(lstData, 2)
        If jmlKas > 0 Then

            Item = Me.lstData.Items.Add("Kenaikan arus kas")
        Else
            Item = Me.lstData.Items.Add("Penurunan arus kas")
        End If
        Item.SubItems.Add(jmlKas)
        saldoAwal = ambilSaldoAwal("Kas")
        Item = Me.lstData.Items.Add("Saldo Awal")
        Item.SubItems.Add(saldoAwal)
        Item = Me.lstData.Items.Add("")
        Item.SubItems.Add("---------------")
        saldoAkhir = saldoAwal + jmlKas
        Item = Me.lstData.Items.Add("Posisi Kas " & Format(Now(), "dd-MM-yyyy"))
        Item.SubItems.Add(saldoAkhir)
    End Sub
    Private Function ambilSaldoAwal(ByVal nama As String) As Double
        Dim msql As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim hasil As Double = 0
        msql = "select SaldoAwal from akun where namaakun='" & _
        nama & "'"
        tabel = proses.ExecuteQuery(msql)
        If tabel.Rows.Count = 1 Then
            hasil = tabel.Rows(0).Item(0)
        End If
        Return hasil
    End Function
    Private Sub baca_ArusKas(ByVal grup As Byte)
        Dim msql As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim saldo As Double = 0
        Dim saldoAwal As Double = 0
        Dim saldoGrup As Double = 0
        Dim ket As String
        If grup = 1 Then
            ket = "Aktivitas Operasional"
        ElseIf grup = 2 Then
            ket = "Aktivitas Investasi"
        Else
            ket = "Aktivitas Pendanaan"
        End If
        msql = "select a.namaakun,k.jumlah,k.jenis,k.grup,a.kodeakun,k.jenisSum from " _
        & "akun_aruskas k,akun a where k.kodeakun=a.kodeakun and k.grup=" _
        & grup & " order by k.grup,k.kodeakun"
        tabel = proses.ExecuteQuery(msql)
        saldo = saldoAwal
        Item = Me.lstData.Items.Add(ket)
        Item.SubItems.Add("")
        Item.SubItems.Add("")
        For i As Integer = 0 To tabel.Rows.Count - 1
            Item = Me.lstData.Items.Add(Space(5) & tabel.Rows(i).Item(0))
            Item.SubItems.Add(getJumlahAK(tabel.Rows(i).Item(4), tabel.Rows(i).Item(5)))
            If tabel.Rows(i).Item(2) = "D" Then
                saldoGrup = saldoGrup + getJumlahAK(tabel.Rows(i).Item(4),
                tabel.Rows(i).Item(5))
            Else

                saldoGrup = saldoGrup - getJumlahAK(tabel.Rows(i).Item(4),
                tabel.Rows(i).Item(5))
            End If
            Item.SubItems.Add("")
        Next i
        Item = Me.lstData.Items.Add("Arus Kas untuk " & ket)
        Item.SubItems.Add("")
        Item.SubItems.Add(saldoGrup)
    End Sub
    Private Function getJumlahAK(ByVal kode As String, ByVal jenis As String) As Double
        Dim msql As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim jml As Double = 0
        If jenis = "D" Then
            msql = "select sum(debit) as jdebit from vjurnal where kodeakun='" & _
            kode & "'"
        Else
            msql = "select sum(kredit) as jkredit from vjurnal where kodeakun='" & _
            kode & "'"
        End If
        tabel = proses.ExecuteQuery(msql)
        If tabel.Rows.Count > 0 Then
            If IsDBNull(tabel.Rows(0).Item(0)) = True Then
                jml = 0
            Else
                jml = tabel.Rows(0).Item(0)
            End If
        Else
            jml = 0
        End If
        Return jml
    End Function

    Private Sub btnexport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnexport.Click
        exportLvToExcel(lstdata)
    End Sub
End Class