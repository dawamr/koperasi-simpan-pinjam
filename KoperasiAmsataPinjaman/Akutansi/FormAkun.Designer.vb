﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAkun
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormAkun))
        Me.dgakun = New System.Windows.Forms.DataGridView()
        Me.btnsesudah = New System.Windows.Forms.Button()
        Me.btnsebelum = New System.Windows.Forms.Button()
        Me.btnakhir = New System.Windows.Forms.Button()
        Me.btnawal = New System.Windows.Forms.Button()
        Me.txtsaldoawal = New System.Windows.Forms.TextBox()
        Me.txtnamaakun = New System.Windows.Forms.TextBox()
        Me.txtkodeakun = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtcari = New System.Windows.Forms.TextBox()
        Me.cbocari = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btntambah = New System.Windows.Forms.Button()
        Me.btnkeluar = New System.Windows.Forms.Button()
        Me.btnbatal = New System.Windows.Forms.Button()
        Me.btnhapus = New System.Windows.Forms.Button()
        Me.btnkoreksi = New System.Windows.Forms.Button()
        Me.btnsimpan = New System.Windows.Forms.Button()
        CType(Me.dgakun, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgakun
        '
        Me.dgakun.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgakun.Location = New System.Drawing.Point(37, 270)
        Me.dgakun.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgakun.Name = "dgakun"
        Me.dgakun.Size = New System.Drawing.Size(743, 247)
        Me.dgakun.TabIndex = 35
        '
        'btnsesudah
        '
        Me.btnsesudah.Location = New System.Drawing.Point(681, 223)
        Me.btnsesudah.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnsesudah.Name = "btnsesudah"
        Me.btnsesudah.Size = New System.Drawing.Size(100, 28)
        Me.btnsesudah.TabIndex = 34
        Me.btnsesudah.Text = "Sesudah"
        Me.btnsesudah.UseVisualStyleBackColor = True
        '
        'btnsebelum
        '
        Me.btnsebelum.Location = New System.Drawing.Point(573, 223)
        Me.btnsebelum.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnsebelum.Name = "btnsebelum"
        Me.btnsebelum.Size = New System.Drawing.Size(100, 28)
        Me.btnsebelum.TabIndex = 33
        Me.btnsebelum.Text = "Sebelum"
        Me.btnsebelum.UseVisualStyleBackColor = True
        '
        'btnakhir
        '
        Me.btnakhir.Location = New System.Drawing.Point(681, 194)
        Me.btnakhir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnakhir.Name = "btnakhir"
        Me.btnakhir.Size = New System.Drawing.Size(100, 28)
        Me.btnakhir.TabIndex = 32
        Me.btnakhir.Text = "Akhir"
        Me.btnakhir.UseVisualStyleBackColor = True
        '
        'btnawal
        '
        Me.btnawal.Location = New System.Drawing.Point(573, 194)
        Me.btnawal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnawal.Name = "btnawal"
        Me.btnawal.Size = New System.Drawing.Size(100, 28)
        Me.btnawal.TabIndex = 31
        Me.btnawal.Text = "Awal"
        Me.btnawal.UseVisualStyleBackColor = True
        '
        'txtsaldoawal
        '
        Me.txtsaldoawal.Location = New System.Drawing.Point(141, 96)
        Me.txtsaldoawal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtsaldoawal.Name = "txtsaldoawal"
        Me.txtsaldoawal.Size = New System.Drawing.Size(183, 23)
        Me.txtsaldoawal.TabIndex = 24
        '
        'txtnamaakun
        '
        Me.txtnamaakun.Location = New System.Drawing.Point(141, 64)
        Me.txtnamaakun.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtnamaakun.Name = "txtnamaakun"
        Me.txtnamaakun.Size = New System.Drawing.Size(301, 23)
        Me.txtnamaakun.TabIndex = 23
        '
        'txtkodeakun
        '
        Me.txtkodeakun.Location = New System.Drawing.Point(141, 32)
        Me.txtkodeakun.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtkodeakun.Name = "txtkodeakun"
        Me.txtkodeakun.Size = New System.Drawing.Size(183, 23)
        Me.txtkodeakun.TabIndex = 22
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(33, 100)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 17)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Saldo Awal"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(33, 68)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 17)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Nama Akun"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(33, 36)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 17)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Kode Akun"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FloralWhite
        Me.GroupBox1.Controls.Add(Me.txtcari)
        Me.GroupBox1.Controls.Add(Me.cbocari)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Location = New System.Drawing.Point(37, 149)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(419, 102)
        Me.GroupBox1.TabIndex = 36
        Me.GroupBox1.TabStop = False
        '
        'txtcari
        '
        Me.txtcari.Location = New System.Drawing.Point(113, 52)
        Me.txtcari.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtcari.Name = "txtcari"
        Me.txtcari.Size = New System.Drawing.Size(277, 23)
        Me.txtcari.TabIndex = 7
        '
        'cbocari
        '
        Me.cbocari.FormattingEnabled = True
        Me.cbocari.Location = New System.Drawing.Point(113, 18)
        Me.cbocari.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbocari.Name = "cbocari"
        Me.cbocari.Size = New System.Drawing.Size(183, 24)
        Me.cbocari.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(13, 52)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 17)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Text Dicari"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(13, 22)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(89, 17)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Berdasarkan"
        '
        'btntambah
        '
        Me.btntambah.Image = CType(resources.GetObject("btntambah.Image"), System.Drawing.Image)
        Me.btntambah.Location = New System.Drawing.Point(573, 32)
        Me.btntambah.Margin = New System.Windows.Forms.Padding(4)
        Me.btntambah.Name = "btntambah"
        Me.btntambah.Size = New System.Drawing.Size(100, 52)
        Me.btntambah.TabIndex = 25
        Me.btntambah.Text = "&Tambah"
        Me.btntambah.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btntambah.UseVisualStyleBackColor = True
        '
        'btnkeluar
        '
        Me.btnkeluar.Image = CType(resources.GetObject("btnkeluar.Image"), System.Drawing.Image)
        Me.btnkeluar.Location = New System.Drawing.Point(681, 133)
        Me.btnkeluar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnkeluar.Name = "btnkeluar"
        Me.btnkeluar.Size = New System.Drawing.Size(100, 52)
        Me.btnkeluar.TabIndex = 30
        Me.btnkeluar.Text = "Kelua&r"
        Me.btnkeluar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnkeluar.UseVisualStyleBackColor = True
        '
        'btnbatal
        '
        Me.btnbatal.Image = CType(resources.GetObject("btnbatal.Image"), System.Drawing.Image)
        Me.btnbatal.Location = New System.Drawing.Point(573, 133)
        Me.btnbatal.Margin = New System.Windows.Forms.Padding(4)
        Me.btnbatal.Name = "btnbatal"
        Me.btnbatal.Size = New System.Drawing.Size(100, 52)
        Me.btnbatal.TabIndex = 29
        Me.btnbatal.Text = "&Batal"
        Me.btnbatal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnbatal.UseVisualStyleBackColor = True
        '
        'btnhapus
        '
        Me.btnhapus.Image = CType(resources.GetObject("btnhapus.Image"), System.Drawing.Image)
        Me.btnhapus.Location = New System.Drawing.Point(681, 82)
        Me.btnhapus.Margin = New System.Windows.Forms.Padding(4)
        Me.btnhapus.Name = "btnhapus"
        Me.btnhapus.Size = New System.Drawing.Size(100, 52)
        Me.btnhapus.TabIndex = 28
        Me.btnhapus.Text = "&Hapus"
        Me.btnhapus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnhapus.UseVisualStyleBackColor = True
        '
        'btnkoreksi
        '
        Me.btnkoreksi.Image = CType(resources.GetObject("btnkoreksi.Image"), System.Drawing.Image)
        Me.btnkoreksi.Location = New System.Drawing.Point(573, 82)
        Me.btnkoreksi.Margin = New System.Windows.Forms.Padding(4)
        Me.btnkoreksi.Name = "btnkoreksi"
        Me.btnkoreksi.Size = New System.Drawing.Size(100, 52)
        Me.btnkoreksi.TabIndex = 27
        Me.btnkoreksi.Text = "&Koreksi"
        Me.btnkoreksi.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnkoreksi.UseVisualStyleBackColor = True
        '
        'btnsimpan
        '
        Me.btnsimpan.Image = CType(resources.GetObject("btnsimpan.Image"), System.Drawing.Image)
        Me.btnsimpan.Location = New System.Drawing.Point(681, 32)
        Me.btnsimpan.Margin = New System.Windows.Forms.Padding(4)
        Me.btnsimpan.Name = "btnsimpan"
        Me.btnsimpan.Size = New System.Drawing.Size(100, 52)
        Me.btnsimpan.TabIndex = 26
        Me.btnsimpan.Text = "&Simpan"
        Me.btnsimpan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnsimpan.UseVisualStyleBackColor = True
        '
        'FormAkun
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(820, 530)
        Me.Controls.Add(Me.btntambah)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgakun)
        Me.Controls.Add(Me.btnsesudah)
        Me.Controls.Add(Me.btnsebelum)
        Me.Controls.Add(Me.btnakhir)
        Me.Controls.Add(Me.btnawal)
        Me.Controls.Add(Me.btnkeluar)
        Me.Controls.Add(Me.btnbatal)
        Me.Controls.Add(Me.btnhapus)
        Me.Controls.Add(Me.btnkoreksi)
        Me.Controls.Add(Me.btnsimpan)
        Me.Controls.Add(Me.txtsaldoawal)
        Me.Controls.Add(Me.txtnamaakun)
        Me.Controls.Add(Me.txtkodeakun)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FormAkun"
        Me.Text = "`"
        CType(Me.dgakun, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgakun As System.Windows.Forms.DataGridView
    Friend WithEvents btnsesudah As System.Windows.Forms.Button
    Friend WithEvents btnsebelum As System.Windows.Forms.Button
    Friend WithEvents btnakhir As System.Windows.Forms.Button
    Friend WithEvents btnawal As System.Windows.Forms.Button
    Friend WithEvents btnkeluar As System.Windows.Forms.Button
    Friend WithEvents btnbatal As System.Windows.Forms.Button
    Friend WithEvents btnhapus As System.Windows.Forms.Button
    Friend WithEvents btnkoreksi As System.Windows.Forms.Button
    Friend WithEvents btnsimpan As System.Windows.Forms.Button
    Friend WithEvents btntambah As System.Windows.Forms.Button
    Friend WithEvents txtsaldoawal As System.Windows.Forms.TextBox
    Friend WithEvents txtnamaakun As System.Windows.Forms.TextBox
    Friend WithEvents txtkodeakun As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtcari As System.Windows.Forms.TextBox
    Friend WithEvents cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label

End Class
