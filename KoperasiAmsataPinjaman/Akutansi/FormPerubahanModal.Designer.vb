﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormPerubahanModal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormPerubahanModal))
        Me.cekbulan = New System.Windows.Forms.CheckBox()
        Me.btncari = New System.Windows.Forms.Button()
        Me.btnexport = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbbulan = New System.Windows.Forms.ComboBox()
        Me.cmbtahun = New System.Windows.Forms.ComboBox()
        Me.cekauto = New System.Windows.Forms.CheckBox()
        Me.btnpos = New System.Windows.Forms.Button()
        Me.lstdata = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'cekbulan
        '
        Me.cekbulan.AutoSize = True
        Me.cekbulan.Location = New System.Drawing.Point(22, 21)
        Me.cekbulan.Name = "cekbulan"
        Me.cekbulan.Size = New System.Drawing.Size(95, 17)
        Me.cekbulan.TabIndex = 0
        Me.cekbulan.Text = "Aktifkan Bulan"
        Me.cekbulan.UseVisualStyleBackColor = True
        '
        'btncari
        '
        Me.btncari.BackColor = System.Drawing.Color.SeaShell
        Me.btncari.Image = CType(resources.GetObject("btncari.Image"), System.Drawing.Image)
        Me.btncari.Location = New System.Drawing.Point(301, 42)
        Me.btncari.Name = "btncari"
        Me.btncari.Size = New System.Drawing.Size(44, 23)
        Me.btncari.TabIndex = 1
        Me.btncari.UseVisualStyleBackColor = False
        '
        'btnexport
        '
        Me.btnexport.BackColor = System.Drawing.Color.SeaShell
        Me.btnexport.Location = New System.Drawing.Point(351, 42)
        Me.btnexport.Name = "btnexport"
        Me.btnexport.Size = New System.Drawing.Size(75, 23)
        Me.btnexport.TabIndex = 2
        Me.btnexport.Text = "Export"
        Me.btnexport.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(157, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Tahun"
        '
        'cmbbulan
        '
        Me.cmbbulan.FormattingEnabled = True
        Me.cmbbulan.Location = New System.Drawing.Point(22, 44)
        Me.cmbbulan.Name = "cmbbulan"
        Me.cmbbulan.Size = New System.Drawing.Size(121, 21)
        Me.cmbbulan.TabIndex = 4
        '
        'cmbtahun
        '
        Me.cmbtahun.FormattingEnabled = True
        Me.cmbtahun.Location = New System.Drawing.Point(160, 44)
        Me.cmbtahun.Name = "cmbtahun"
        Me.cmbtahun.Size = New System.Drawing.Size(121, 21)
        Me.cmbtahun.TabIndex = 5
        '
        'cekauto
        '
        Me.cekauto.AutoSize = True
        Me.cekauto.Location = New System.Drawing.Point(432, 44)
        Me.cekauto.Name = "cekauto"
        Me.cekauto.Size = New System.Drawing.Size(88, 17)
        Me.cekauto.TabIndex = 6
        Me.cekauto.Text = "Auto Refresh"
        Me.cekauto.UseVisualStyleBackColor = True
        '
        'btnpos
        '
        Me.btnpos.BackColor = System.Drawing.Color.SeaShell
        Me.btnpos.Location = New System.Drawing.Point(599, 42)
        Me.btnpos.Name = "btnpos"
        Me.btnpos.Size = New System.Drawing.Size(75, 23)
        Me.btnpos.TabIndex = 7
        Me.btnpos.Text = "Pos"
        Me.btnpos.UseVisualStyleBackColor = False
        '
        'lstdata
        '
        Me.lstdata.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lstdata.Location = New System.Drawing.Point(25, 88)
        Me.lstdata.Name = "lstdata"
        Me.lstdata.Size = New System.Drawing.Size(648, 287)
        Me.lstdata.TabIndex = 8
        Me.lstdata.UseCompatibleStateImageBehavior = False
        Me.lstdata.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Keterangan"
        Me.ColumnHeader1.Width = 250
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Jumlah"
        Me.ColumnHeader2.Width = 250
        '
        'FormPerubahanModal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(737, 456)
        Me.Controls.Add(Me.lstdata)
        Me.Controls.Add(Me.btnpos)
        Me.Controls.Add(Me.cekauto)
        Me.Controls.Add(Me.cmbtahun)
        Me.Controls.Add(Me.cmbbulan)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnexport)
        Me.Controls.Add(Me.btncari)
        Me.Controls.Add(Me.cekbulan)
        Me.ForeColor = System.Drawing.Color.Black
        Me.Name = "FormPerubahanModal"
        Me.Text = "FormPerubahanModal"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cekbulan As System.Windows.Forms.CheckBox
    Friend WithEvents btncari As System.Windows.Forms.Button
    Friend WithEvents btnexport As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbbulan As System.Windows.Forms.ComboBox
    Friend WithEvents cmbtahun As System.Windows.Forms.ComboBox
    Friend WithEvents cekauto As System.Windows.Forms.CheckBox
    Friend WithEvents btnpos As System.Windows.Forms.Button
    Friend WithEvents lstdata As System.Windows.Forms.ListView
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
End Class
