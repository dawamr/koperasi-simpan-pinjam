﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormJurnalUmum
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormJurnalUmum))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtnojurnal = New System.Windows.Forms.TextBox()
        Me.txtnobukti = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Txtdeskripsi = New System.Windows.Forms.TextBox()
        Me.lstjurnal = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnnew = New System.Windows.Forms.Button()
        Me.btncancel = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btnexit = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnlist1 = New System.Windows.Forms.Button()
        Me.btnadd1 = New System.Windows.Forms.Button()
        Me.txtjumlah1 = New System.Windows.Forms.TextBox()
        Me.txtnmakun1 = New System.Windows.Forms.TextBox()
        Me.txtnoakun1 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnlist2 = New System.Windows.Forms.Button()
        Me.btnadd2 = New System.Windows.Forms.Button()
        Me.txtjumlah2 = New System.Windows.Forms.TextBox()
        Me.txtnmakun2 = New System.Windows.Forms.TextBox()
        Me.txtnoakun2 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(26, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "No Jurnal"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tanggal"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(26, 94)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "No Bukti"
        '
        'txtnojurnal
        '
        Me.txtnojurnal.Location = New System.Drawing.Point(94, 33)
        Me.txtnojurnal.Name = "txtnojurnal"
        Me.txtnojurnal.Size = New System.Drawing.Size(88, 20)
        Me.txtnojurnal.TabIndex = 3
        '
        'txtnobukti
        '
        Me.txtnobukti.Location = New System.Drawing.Point(94, 91)
        Me.txtnobukti.Name = "txtnobukti"
        Me.txtnobukti.Size = New System.Drawing.Size(194, 20)
        Me.txtnobukti.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(322, 36)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Deskripsi"
        '
        'Txtdeskripsi
        '
        Me.Txtdeskripsi.Location = New System.Drawing.Point(379, 34)
        Me.Txtdeskripsi.Multiline = True
        Me.Txtdeskripsi.Name = "Txtdeskripsi"
        Me.Txtdeskripsi.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Txtdeskripsi.Size = New System.Drawing.Size(192, 72)
        Me.Txtdeskripsi.TabIndex = 7
        '
        'lstjurnal
        '
        Me.lstjurnal.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lstjurnal.GridLines = True
        Me.lstjurnal.Location = New System.Drawing.Point(29, 278)
        Me.lstjurnal.Name = "lstjurnal"
        Me.lstjurnal.Size = New System.Drawing.Size(561, 182)
        Me.lstjurnal.TabIndex = 10
        Me.lstjurnal.UseCompatibleStateImageBehavior = False
        Me.lstjurnal.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "No Akun"
        Me.ColumnHeader1.Width = 160
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Nama Akun"
        Me.ColumnHeader2.Width = 160
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Debit"
        Me.ColumnHeader3.Width = 100
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Kredit"
        Me.ColumnHeader4.Width = 100
        '
        'btnnew
        '
        Me.btnnew.Location = New System.Drawing.Point(47, 466)
        Me.btnnew.Name = "btnnew"
        Me.btnnew.Size = New System.Drawing.Size(75, 23)
        Me.btnnew.TabIndex = 11
        Me.btnnew.Text = "&New"
        Me.btnnew.UseVisualStyleBackColor = True
        '
        'btncancel
        '
        Me.btncancel.Location = New System.Drawing.Point(347, 466)
        Me.btncancel.Name = "btncancel"
        Me.btncancel.Size = New System.Drawing.Size(75, 23)
        Me.btncancel.TabIndex = 12
        Me.btncancel.Text = "&Cancel"
        Me.btncancel.UseVisualStyleBackColor = True
        '
        'btnsave
        '
        Me.btnsave.Location = New System.Drawing.Point(428, 466)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(75, 23)
        Me.btnsave.TabIndex = 13
        Me.btnsave.Text = "&Save"
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'btnexit
        '
        Me.btnexit.Location = New System.Drawing.Point(509, 466)
        Me.btnexit.Name = "btnexit"
        Me.btnexit.Size = New System.Drawing.Size(75, 23)
        Me.btnexit.TabIndex = 14
        Me.btnexit.Text = "&Exit"
        Me.btnexit.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.SeaShell
        Me.GroupBox1.Controls.Add(Me.btnlist1)
        Me.GroupBox1.Controls.Add(Me.btnadd1)
        Me.GroupBox1.Controls.Add(Me.txtjumlah1)
        Me.GroupBox1.Controls.Add(Me.txtnmakun1)
        Me.GroupBox1.Controls.Add(Me.txtnoakun1)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Location = New System.Drawing.Point(29, 127)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(283, 114)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        '
        'btnlist1
        '
        Me.btnlist1.Image = CType(resources.GetObject("btnlist1.Image"), System.Drawing.Image)
        Me.btnlist1.Location = New System.Drawing.Point(198, 15)
        Me.btnlist1.Name = "btnlist1"
        Me.btnlist1.Size = New System.Drawing.Size(28, 20)
        Me.btnlist1.TabIndex = 15
        Me.btnlist1.UseVisualStyleBackColor = True
        '
        'btnadd1
        '
        Me.btnadd1.Location = New System.Drawing.Point(202, 86)
        Me.btnadd1.Name = "btnadd1"
        Me.btnadd1.Size = New System.Drawing.Size(58, 25)
        Me.btnadd1.TabIndex = 14
        Me.btnadd1.Text = "Add"
        Me.btnadd1.UseVisualStyleBackColor = True
        '
        'txtjumlah1
        '
        Me.txtjumlah1.Location = New System.Drawing.Point(92, 67)
        Me.txtjumlah1.Name = "txtjumlah1"
        Me.txtjumlah1.Size = New System.Drawing.Size(95, 20)
        Me.txtjumlah1.TabIndex = 13
        '
        'txtnmakun1
        '
        Me.txtnmakun1.Location = New System.Drawing.Point(92, 41)
        Me.txtnmakun1.Name = "txtnmakun1"
        Me.txtnmakun1.Size = New System.Drawing.Size(168, 20)
        Me.txtnmakun1.TabIndex = 12
        '
        'txtnoakun1
        '
        Me.txtnoakun1.Location = New System.Drawing.Point(92, 15)
        Me.txtnoakun1.Name = "txtnoakun1"
        Me.txtnoakun1.Size = New System.Drawing.Size(98, 20)
        Me.txtnoakun1.TabIndex = 11
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(16, 70)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(40, 13)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Jumlah"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(16, 44)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(63, 13)
        Me.Label12.TabIndex = 9
        Me.Label12.Text = "Nama Akun"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(16, 18)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(49, 13)
        Me.Label13.TabIndex = 8
        Me.Label13.Text = "No Akun"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.SeaShell
        Me.GroupBox2.Controls.Add(Me.btnlist2)
        Me.GroupBox2.Controls.Add(Me.btnadd2)
        Me.GroupBox2.Controls.Add(Me.txtjumlah2)
        Me.GroupBox2.Controls.Add(Me.txtnmakun2)
        Me.GroupBox2.Controls.Add(Me.txtnoakun2)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Location = New System.Drawing.Point(318, 127)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(283, 114)
        Me.GroupBox2.TabIndex = 16
        Me.GroupBox2.TabStop = False
        '
        'btnlist2
        '
        Me.btnlist2.Image = CType(resources.GetObject("btnlist2.Image"), System.Drawing.Image)
        Me.btnlist2.Location = New System.Drawing.Point(198, 15)
        Me.btnlist2.Name = "btnlist2"
        Me.btnlist2.Size = New System.Drawing.Size(28, 20)
        Me.btnlist2.TabIndex = 15
        Me.btnlist2.UseVisualStyleBackColor = True
        '
        'btnadd2
        '
        Me.btnadd2.Location = New System.Drawing.Point(202, 86)
        Me.btnadd2.Name = "btnadd2"
        Me.btnadd2.Size = New System.Drawing.Size(58, 25)
        Me.btnadd2.TabIndex = 14
        Me.btnadd2.Text = "Add"
        Me.btnadd2.UseVisualStyleBackColor = True
        '
        'txtjumlah2
        '
        Me.txtjumlah2.Location = New System.Drawing.Point(92, 67)
        Me.txtjumlah2.Name = "txtjumlah2"
        Me.txtjumlah2.Size = New System.Drawing.Size(95, 20)
        Me.txtjumlah2.TabIndex = 13
        '
        'txtnmakun2
        '
        Me.txtnmakun2.Location = New System.Drawing.Point(92, 41)
        Me.txtnmakun2.Name = "txtnmakun2"
        Me.txtnmakun2.Size = New System.Drawing.Size(168, 20)
        Me.txtnmakun2.TabIndex = 12
        '
        'txtnoakun2
        '
        Me.txtnoakun2.Location = New System.Drawing.Point(92, 15)
        Me.txtnoakun2.Name = "txtnoakun2"
        Me.txtnoakun2.Size = New System.Drawing.Size(98, 20)
        Me.txtnoakun2.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 70)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Jumlah"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(16, 44)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Nama Akun"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(16, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "No Akun"
        '
        'btndelete
        '
        Me.btndelete.Location = New System.Drawing.Point(503, 249)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(75, 23)
        Me.btndelete.TabIndex = 17
        Me.btndelete.Text = "Delete Item"
        Me.btndelete.UseVisualStyleBackColor = True
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(94, 63)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker1.TabIndex = 18
        '
        'FormJurnalUmum
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(608, 511)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.btndelete)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnexit)
        Me.Controls.Add(Me.btnsave)
        Me.Controls.Add(Me.btncancel)
        Me.Controls.Add(Me.btnnew)
        Me.Controls.Add(Me.lstjurnal)
        Me.Controls.Add(Me.Txtdeskripsi)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtnobukti)
        Me.Controls.Add(Me.txtnojurnal)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FormJurnalUmum"
        Me.Text = "Form Jurnal Umum"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtnojurnal As System.Windows.Forms.TextBox
    Friend WithEvents txtnobukti As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Txtdeskripsi As System.Windows.Forms.TextBox
    Friend WithEvents lstjurnal As System.Windows.Forms.ListView
    Friend WithEvents btnnew As System.Windows.Forms.Button
    Friend WithEvents btncancel As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btnexit As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnlist1 As System.Windows.Forms.Button
    Friend WithEvents btnadd1 As System.Windows.Forms.Button
    Friend WithEvents txtjumlah1 As System.Windows.Forms.TextBox
    Friend WithEvents txtnmakun1 As System.Windows.Forms.TextBox
    Friend WithEvents txtnoakun1 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnlist2 As System.Windows.Forms.Button
    Friend WithEvents btnadd2 As System.Windows.Forms.Button
    Friend WithEvents txtjumlah2 As System.Windows.Forms.TextBox
    Friend WithEvents txtnmakun2 As System.Windows.Forms.TextBox
    Friend WithEvents txtnoakun2 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
End Class
