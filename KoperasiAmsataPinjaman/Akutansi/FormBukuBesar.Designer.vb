﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormBukuBesar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormBukuBesar))
        Me.cekTgl = New System.Windows.Forms.CheckBox()
        Me.dtawal = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtakhir = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbakun = New System.Windows.Forms.ComboBox()
        Me.cmdcari = New System.Windows.Forms.Button()
        Me.cmdexport = New System.Windows.Forms.Button()
        Me.cekauto = New System.Windows.Forms.CheckBox()
        Me.lstdata = New System.Windows.Forms.ListView()
        Me.No = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Tanggal = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Keterangan = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Ref = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Debit = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Kredit = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Saldo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtdebit = New System.Windows.Forms.TextBox()
        Me.txtkredit = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'cekTgl
        '
        Me.cekTgl.AutoSize = True
        Me.cekTgl.Location = New System.Drawing.Point(12, 12)
        Me.cekTgl.Name = "cekTgl"
        Me.cekTgl.Size = New System.Drawing.Size(107, 17)
        Me.cekTgl.TabIndex = 0
        Me.cekTgl.Text = "Aktifkan Tanggal"
        Me.cekTgl.UseVisualStyleBackColor = True
        '
        'dtawal
        '
        Me.dtawal.Location = New System.Drawing.Point(12, 35)
        Me.dtawal.Name = "dtawal"
        Me.dtawal.Size = New System.Drawing.Size(200, 20)
        Me.dtawal.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(218, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(23, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "s/d"
        '
        'dtakhir
        '
        Me.dtakhir.Location = New System.Drawing.Point(249, 36)
        Me.dtakhir.Name = "dtakhir"
        Me.dtakhir.Size = New System.Drawing.Size(200, 20)
        Me.dtakhir.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(452, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nama Akun"
        '
        'cmbakun
        '
        Me.cmbakun.FormattingEnabled = True
        Me.cmbakun.Location = New System.Drawing.Point(455, 35)
        Me.cmbakun.Name = "cmbakun"
        Me.cmbakun.Size = New System.Drawing.Size(121, 21)
        Me.cmbakun.TabIndex = 5
        '
        'cmdcari
        '
        Me.cmdcari.BackColor = System.Drawing.Color.SeaShell
        Me.cmdcari.Image = CType(resources.GetObject("cmdcari.Image"), System.Drawing.Image)
        Me.cmdcari.Location = New System.Drawing.Point(582, 31)
        Me.cmdcari.Name = "cmdcari"
        Me.cmdcari.Size = New System.Drawing.Size(37, 26)
        Me.cmdcari.TabIndex = 6
        Me.cmdcari.UseVisualStyleBackColor = False
        '
        'cmdexport
        '
        Me.cmdexport.BackColor = System.Drawing.Color.SeaShell
        Me.cmdexport.Location = New System.Drawing.Point(625, 30)
        Me.cmdexport.Name = "cmdexport"
        Me.cmdexport.Size = New System.Drawing.Size(75, 26)
        Me.cmdexport.TabIndex = 7
        Me.cmdexport.Text = "Export"
        Me.cmdexport.UseVisualStyleBackColor = False
        '
        'cekauto
        '
        Me.cekauto.AutoSize = True
        Me.cekauto.Location = New System.Drawing.Point(712, 38)
        Me.cekauto.Name = "cekauto"
        Me.cekauto.Size = New System.Drawing.Size(88, 17)
        Me.cekauto.TabIndex = 8
        Me.cekauto.Text = "Auto Refresh"
        Me.cekauto.UseVisualStyleBackColor = True
        '
        'lstdata
        '
        Me.lstdata.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.No, Me.Tanggal, Me.Keterangan, Me.Ref, Me.Debit, Me.Kredit, Me.Saldo})
        Me.lstdata.Location = New System.Drawing.Point(14, 73)
        Me.lstdata.Name = "lstdata"
        Me.lstdata.Size = New System.Drawing.Size(837, 237)
        Me.lstdata.TabIndex = 9
        Me.lstdata.UseCompatibleStateImageBehavior = False
        Me.lstdata.View = System.Windows.Forms.View.Details
        '
        'No
        '
        Me.No.Text = "No"
        '
        'Tanggal
        '
        Me.Tanggal.Text = "Tanggal"
        Me.Tanggal.Width = 100
        '
        'Keterangan
        '
        Me.Keterangan.Text = "Keterangan"
        Me.Keterangan.Width = 200
        '
        'Ref
        '
        Me.Ref.Text = "Ref"
        '
        'Debit
        '
        Me.Debit.Text = "Debit"
        Me.Debit.Width = 100
        '
        'Kredit
        '
        Me.Kredit.Text = "Kredit"
        Me.Kredit.Width = 100
        '
        'Saldo
        '
        Me.Saldo.Text = "Saldo"
        Me.Saldo.Width = 100
        '
        'txtdebit
        '
        Me.txtdebit.Location = New System.Drawing.Point(431, 316)
        Me.txtdebit.Name = "txtdebit"
        Me.txtdebit.Size = New System.Drawing.Size(100, 20)
        Me.txtdebit.TabIndex = 10
        '
        'txtkredit
        '
        Me.txtkredit.Location = New System.Drawing.Point(537, 316)
        Me.txtkredit.Name = "txtkredit"
        Me.txtkredit.Size = New System.Drawing.Size(100, 20)
        Me.txtkredit.TabIndex = 11
        '
        'Timer1
        '
        '
        'FormBukuBesar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1181, 343)
        Me.Controls.Add(Me.txtkredit)
        Me.Controls.Add(Me.txtdebit)
        Me.Controls.Add(Me.lstdata)
        Me.Controls.Add(Me.cekauto)
        Me.Controls.Add(Me.cmdexport)
        Me.Controls.Add(Me.cmdcari)
        Me.Controls.Add(Me.cmbakun)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtakhir)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtawal)
        Me.Controls.Add(Me.cekTgl)
        Me.Name = "FormBukuBesar"
        Me.Text = "  "
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cekTgl As System.Windows.Forms.CheckBox
    Friend WithEvents dtawal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtakhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbakun As System.Windows.Forms.ComboBox
    Friend WithEvents cmdcari As System.Windows.Forms.Button
    Friend WithEvents cmdexport As System.Windows.Forms.Button
    Friend WithEvents cekauto As System.Windows.Forms.CheckBox
    Friend WithEvents lstdata As System.Windows.Forms.ListView
    Friend WithEvents txtdebit As System.Windows.Forms.TextBox
    Friend WithEvents txtkredit As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents No As System.Windows.Forms.ColumnHeader
    Friend WithEvents Tanggal As System.Windows.Forms.ColumnHeader
    Friend WithEvents Keterangan As System.Windows.Forms.ColumnHeader
    Friend WithEvents Ref As System.Windows.Forms.ColumnHeader
    Friend WithEvents Debit As System.Windows.Forms.ColumnHeader
    Friend WithEvents Kredit As System.Windows.Forms.ColumnHeader
    Friend WithEvents Saldo As System.Windows.Forms.ColumnHeader
End Class
