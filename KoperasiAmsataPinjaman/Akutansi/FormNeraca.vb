﻿Public Class FormNeraca
    Dim tahun, bulan As Integer
    Private Sub FormNeraca_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbbulan.Items.Clear()
        For i As Integer = 1 To 12
            cmbbulan.Items.Add(i)
        Next
        tahun = Year(Now)
        For j As Integer = (tahun - 2) To tahun
            cmbtahun.Items.Add(j)
        Next
        cmbbulan.SelectedIndex = Month(Now) - 1
        cmbtahun.Text = tahun
        init_Neraca()
    End Sub
    Private Sub init_Neraca()
        baca_data()
    End Sub
    Private Sub baca_data()
        Dim msql As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim jumlah As Double = 0 'ambil saldoawal di akun
        Dim jumlah1 As Double = 0 'ambil jumlah di jurnal
        Dim tActiva As Double = 0
        Dim tPasiva As Double = 0
        lstData.Items.Clear()
        msql = "select kodeakun,namaakun from akun where kodeakun like '11%' or " & _
        " kodeakun like '12%'" & _
        " or kodeakun like '21%' or kodeakun like '31%' or kodeakun like '41%' " & _
        " or kodeakun like '61%' order by kodeakun asc"
        ' MsgBox(msql)
        tabel = proses.ExecuteQuery(msql)
        For i As Integer = 0 To tabel.Rows.Count - 1
            Item = Me.lstData.Items.Add(tabel.Rows(i).Item(0))
            Item.SubItems.Add(tabel.Rows(i).Item(1))
            'If tabel.Rows(i).Item(0) = "3101" Or tabel.Rows(i).Item(0) = "3103" Or tabel.Rows(i).Item(0) = "1101" Then
            jumlah = getSaldoAwal(tabel.Rows(i).Item(0))
            jumlah1 = getJumlahAK(tabel.Rows(i).Item(0))
            'Else
            'jumlah = getSaldoAwal(tabel.Rows(i).Item(0))
            'jumlah1 = getJumlahAK(tabel.Rows(i).Item(0))
            'jumlah = getJumlahAK(tabel.Rows(i).Item(0))
            'End If
            If (Microsoft.VisualBasic.Left(tabel.Rows(i).Item(0), 1) = "1" Or _
            tabel.Rows(i).Item(0) = "3103") And tabel.Rows(i).Item(0) <> "1203" Then
                Item.SubItems.Add((jumlah + jumlah1))
                Item.SubItems.Add("")
            Else
                Item.SubItems.Add("")
                Item.SubItems.Add((jumlah + jumlah1))
            End If
        Next i
        tActiva = getJumlah(lstData, 2)
        tPasiva = getJumlah(lstData, 3)
        Item = Me.lstData.Items.Add("")
        Item.SubItems.Add("Total")
        Item.SubItems.Add(tActiva)
        Item.SubItems.Add(tPasiva)
    End Sub
    Private Function getJumlahAK(ByVal kode As String) As Double
        Dim msql As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim jml As Double = 0
        If Microsoft.VisualBasic.Left(kode, 1) = "1" Then
            msql = "select (sum(debit)-sum(kredit)) as jTot from vjurnal where kodeakun='" & _
 kode & "'"
        Else
            msql = "select (sum(kredit)-sum(debit)) as jTot from vjurnal where kodeakun='" & _
 kode & "'"
        End If
        tabel = proses.ExecuteQuery(msql)
        If tabel.Rows.Count > 0 Then
            If IsDBNull(tabel.Rows(0).Item(0)) = True Then
                jml = 0
            Else
                jml = tabel.Rows(0).Item(0)
            End If
        Else
            jml = 0
        End If
        Return jml

    End Function

    Private Sub cmdexport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdexport.Click
        exportLvToExcel(lstdata)
    End Sub
End Class