﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormPostingJurnal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormPostingJurnal))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbakun = New System.Windows.Forms.ComboBox()
        Me.dtawal = New System.Windows.Forms.DateTimePicker()
        Me.dtakhir = New System.Windows.Forms.DateTimePicker()
        Me.cmdposting = New System.Windows.Forms.Button()
        Me.cmdkeluar = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblprosen = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(37, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Transaksi"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(37, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Periode"
        '
        'cmbakun
        '
        Me.cmbakun.FormattingEnabled = True
        Me.cmbakun.Location = New System.Drawing.Point(108, 31)
        Me.cmbakun.Name = "cmbakun"
        Me.cmbakun.Size = New System.Drawing.Size(121, 21)
        Me.cmbakun.TabIndex = 2
        '
        'dtawal
        '
        Me.dtawal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtawal.Location = New System.Drawing.Point(108, 75)
        Me.dtawal.Name = "dtawal"
        Me.dtawal.Size = New System.Drawing.Size(151, 20)
        Me.dtawal.TabIndex = 3
        '
        'dtakhir
        '
        Me.dtakhir.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtakhir.Location = New System.Drawing.Point(291, 75)
        Me.dtakhir.Name = "dtakhir"
        Me.dtakhir.Size = New System.Drawing.Size(154, 20)
        Me.dtakhir.TabIndex = 4
        '
        'cmdposting
        '
        Me.cmdposting.BackColor = System.Drawing.Color.SeaShell
        Me.cmdposting.Image = CType(resources.GetObject("cmdposting.Image"), System.Drawing.Image)
        Me.cmdposting.Location = New System.Drawing.Point(118, 122)
        Me.cmdposting.Name = "cmdposting"
        Me.cmdposting.Size = New System.Drawing.Size(141, 33)
        Me.cmdposting.TabIndex = 5
        Me.cmdposting.Text = "Posting Data"
        Me.cmdposting.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.cmdposting.UseVisualStyleBackColor = False
        '
        'cmdkeluar
        '
        Me.cmdkeluar.BackColor = System.Drawing.Color.SeaShell
        Me.cmdkeluar.Image = CType(resources.GetObject("cmdkeluar.Image"), System.Drawing.Image)
        Me.cmdkeluar.Location = New System.Drawing.Point(291, 122)
        Me.cmdkeluar.Name = "cmdkeluar"
        Me.cmdkeluar.Size = New System.Drawing.Size(122, 33)
        Me.cmdkeluar.TabIndex = 6
        Me.cmdkeluar.Text = "Keluar"
        Me.cmdkeluar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.cmdkeluar.UseVisualStyleBackColor = False
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(108, 178)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(337, 23)
        Me.ProgressBar1.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(265, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(23, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "s/d"
        '
        'lblprosen
        '
        Me.lblprosen.AutoSize = True
        Me.lblprosen.Location = New System.Drawing.Point(265, 213)
        Me.lblprosen.Name = "lblprosen"
        Me.lblprosen.Size = New System.Drawing.Size(0, 13)
        Me.lblprosen.TabIndex = 9
        '
        'FormPostingJurnal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(518, 289)
        Me.Controls.Add(Me.lblprosen)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.cmdkeluar)
        Me.Controls.Add(Me.cmdposting)
        Me.Controls.Add(Me.dtakhir)
        Me.Controls.Add(Me.dtawal)
        Me.Controls.Add(Me.cmbakun)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FormPostingJurnal"
        Me.Text = "FormPostingJurnal"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbakun As System.Windows.Forms.ComboBox
    Friend WithEvents dtawal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtakhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmdposting As System.Windows.Forms.Button
    Friend WithEvents cmdkeluar As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblprosen As System.Windows.Forms.Label
End Class
