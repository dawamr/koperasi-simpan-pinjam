﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormCariAkun
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormCariAkun))
        Me.dgakun = New System.Windows.Forms.DataGridView()
        Me.btnselect = New System.Windows.Forms.Button()
        Me.btnexit = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbocari = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtcari = New System.Windows.Forms.TextBox()
        CType(Me.dgakun, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgakun
        '
        Me.dgakun.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgakun.Location = New System.Drawing.Point(11, 117)
        Me.dgakun.Name = "dgakun"
        Me.dgakun.Size = New System.Drawing.Size(458, 237)
        Me.dgakun.TabIndex = 23
        '
        'btnselect
        '
        Me.btnselect.Image = CType(resources.GetObject("btnselect.Image"), System.Drawing.Image)
        Me.btnselect.Location = New System.Drawing.Point(334, 360)
        Me.btnselect.Name = "btnselect"
        Me.btnselect.Size = New System.Drawing.Size(52, 42)
        Me.btnselect.TabIndex = 24
        Me.btnselect.Text = "Select"
        Me.btnselect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnselect.UseVisualStyleBackColor = True
        '
        'btnexit
        '
        Me.btnexit.Image = CType(resources.GetObject("btnexit.Image"), System.Drawing.Image)
        Me.btnexit.Location = New System.Drawing.Point(392, 360)
        Me.btnexit.Name = "btnexit"
        Me.btnexit.Size = New System.Drawing.Size(43, 42)
        Me.btnexit.TabIndex = 25
        Me.btnexit.Text = "Exit"
        Me.btnexit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnexit.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.SeaShell
        Me.GroupBox1.Controls.Add(Me.txtcari)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cbocari)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 19)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(309, 81)
        Me.GroupBox1.TabIndex = 26
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "Text Dicari"
        '
        'cbocari
        '
        Me.cbocari.FormattingEnabled = True
        Me.cbocari.Location = New System.Drawing.Point(92, 16)
        Me.cbocari.Name = "cbocari"
        Me.cbocari.Size = New System.Drawing.Size(138, 21)
        Me.cbocari.TabIndex = 28
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Berdasarkan"
        '
        'txtcari
        '
        Me.txtcari.Location = New System.Drawing.Point(92, 49)
        Me.txtcari.Name = "txtcari"
        Me.txtcari.Size = New System.Drawing.Size(209, 20)
        Me.txtcari.TabIndex = 29
        '
        'FormCariAkun
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(486, 414)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnexit)
        Me.Controls.Add(Me.btnselect)
        Me.Controls.Add(Me.dgakun)
        Me.Name = "FormCariAkun"
        Me.Text = "FormCariAkun"
        CType(Me.dgakun, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgakun As System.Windows.Forms.DataGridView
    Friend WithEvents btnselect As System.Windows.Forms.Button
    Friend WithEvents btnexit As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
