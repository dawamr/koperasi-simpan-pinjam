﻿Public Class FormLabaRugi
    Dim proses As New ClsKoneksi
    Dim LabaBersih As Double = 0
    Dim tahun, bulan As Integer

    Private Sub FormLabaRugi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbbulan.Items.Clear()
        For i As Integer = 1 To 12
            cmbbulan.Items.Add(i)
        Next
        tahun = Year(Now)
        For j As Integer = (tahun - 2) To tahun
            cmbtahun.Items.Add(j)
        Next

        cmbbulan.SelectedIndex = Month(Now) - 1
        cmbtahun.Text = tahun
        init_LR()
    End Sub
    Private Sub init_LR()
        Dim tJual As Double = 0
        Dim tRetur As Double = 0
        Dim tJualBersih As Double = 0
        Dim tPendapatan As Double = 0
        Dim thpp As Double = 0
        Dim tGaji As Double = 0
        Dim tBeban As Double = 0
        Dim tLR As Double = 0
        Dim tPerlengkapan As Double = 0
        Dim tListrik As Double = 0
        Dim tAir As Double = 0
        lstData.Items.Clear()
        bulan = Val(cmbBulan.Text)
        tahun = Val(cmbTahun.Text)
        tJual = getJumlahData("SELECT SUM(totjual) AS xtot FROM lapjual WHERE MONTH(tgl_jual) =" & _
                              bulan & " And YEAR(tgl_jual) = " & tahun)
        tRetur = getJumlahData("SELECT SUM(totretur) AS xtot FROM lapreturjual WHERE Month(tgl_retur) =" & _
                              bulan & " And Year(tgl_retur) = " & tahun)
        tJualBersih = tJual - tRetur
        tPendapatan = tJualBersih
        Item = Me.lstdata.Items.Add("Pendapatan")
        Item.SubItems.Add("")
        Item.SubItems.Add("")
        Item = Me.lstdata.Items.Add("Penjualan Bersih")
        Item.SubItems.Add(tJualBersih)
        Item.SubItems.Add("")
        Item = Me.lstdata.Items.Add("Total Pendapatan")
        Item.SubItems.Add("")
        Item.SubItems.Add(tPendapatan)
        'beban
        
        tGaji = getJumlahData("SELECT SUM(debit) AS xtot FROM vjurnal WHERE MONTH(tanggal) = " & _
                              bulan & " AND YEAR(tanggal)=" & tahun & " and kodeakun='6101' ")
        Item = Me.lstData.Items.Add("Beban Gaji")

        Item.SubItems.Add(tGaji)
        Item.SubItems.Add("")
        tPerlengkapan = getJumlahData("SELECT SUM(debit) AS xtot FROM vjurnal WHERE MONTH(tanggal) = " & _
                              bulan & " AND YEAR(tanggal)=" & tahun & " and kodeakun='6109' ")
        Item = Me.lstdata.Items.Add("Beban Perlengkapan")

        Item.SubItems.Add(tPerlengkapan)
        Item.SubItems.Add("")

        tListrik = getJumlahData("SELECT SUM(debit) AS xtot FROM vjurnal WHERE MONTH(tanggal) = " & _
                              bulan & " AND YEAR(tanggal)=" & tahun & " and kodeakun='6110' ")
        Item = Me.lstdata.Items.Add("Beban Listrik")

        Item.SubItems.Add(tListrik)
        Item.SubItems.Add("")

        tAir = getJumlahData("SELECT SUM(debit) AS xtot FROM vjurnal WHERE MONTH(tanggal) = " & _
                              bulan & " AND YEAR(tanggal)=" & tahun & " and kodeakun='6111' ")
        Item = Me.lstdata.Items.Add("Beban Air")

        Item.SubItems.Add(tAir)
        Item.SubItems.Add("")
        tBeban = thpp + tGaji + tPerlengkapan + tListrik + tAir
        Item = Me.lstdata.Items.Add("Total Beban")
        Item.SubItems.Add("")
        Item.SubItems.Add(tBeban)
        tLR = tPendapatan - tBeban
        Dim pajak As Double = 0
        If tLR > 0 Then
            Item = Me.lstData.Items.Add("Laba sebelum pajak")
            pajak = 0.1 * tLR
            labaBersih = tLR - pajak
        Else
            Item = Me.lstData.Items.Add("Rugi")
            pajak = 0
            labaBersih = tLR
        End If
        Item.SubItems.Add("")
        Item.SubItems.Add(tLR)
        Item = Me.lstData.Items.Add("Pajak")
        Item.SubItems.Add("")
        Item.SubItems.Add(pajak)
        Item = Me.lstData.Items.Add("")
        Item.SubItems.Add("")
        Item.SubItems.Add("------------------")
        If labaBersih > 0 Then
            Item = Me.lstData.Items.Add("Laba Bersih")
        Else
            Item = Me.lstData.Items.Add("Rugi")
        End If
        Item.SubItems.Add("")
        Item.SubItems.Add(labaBersih)
    End Sub
    Private Function getJumlahData(ByVal msql As String) As Double
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim jml As Double = 0
        tabel = proses.ExecuteQuery(msql)
        If tabel.Rows.Count > 0 Then
            If IsDBNull(tabel.Rows(0).Item(0)) = True Then
                jml = 0
            Else
                jml = tabel.Rows(0).Item(0)

            End If
        Else
            jml = 0
        End If
        Return jml
    End Function

    Private Sub btncari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncari.Click
        init_LR()
    End Sub

    Private Sub btnexport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnexport.Click
        exportLvToExcel(lstdata)
    End Sub

    Private Sub btnpos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnpos.Click
        proses.ExecuteNonQuery("update akun set SaldoAwal=SaldoAwal+" & LabaBersih & _
" where KodeAkun='3103'")
        Exit Sub
    End Sub
End Class