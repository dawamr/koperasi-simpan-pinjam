﻿Public Class FormCariAkun
    Dim strsql As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Private Sub FormCariAkun_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshGrid()
        load_cbocari(tabel, cbocari)
        cbocari.SelectedIndex = 0
    End Sub
    Private Sub btnexit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnexit.Click
        Me.Close()
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from akun order by KodeAkun")
            Me.dgAkun.DataSource = tabel
            With Me.dgAkun
                .Columns(0).HeaderText = "Kode Akun"
                .Columns(1).HeaderText = "Nama Akun"
                .Columns(2).HeaderText = "Saldo Awal"
                .Columns(0).Width = 100
                .Columns(1).Width = 300
                .Columns(2).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from akun where " & Trim(namaKolom) & " like ('%" & Trim(sCari) & "%') order by KodeAkun")
            Me.dgAkun.DataSource = tabel
            With Me.dgAkun
                .Columns(0).HeaderText = "Kode Akun"
                .Columns(1).HeaderText = "Nama Akun"
                .Columns(2).HeaderText = "Saldo Awal"
                .Columns(0).Width = 100
                .Columns(1).Width = 285
                .Columns(2).Width = 150
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnselect.Click
        If cekAkun = 1 Then
            FormJurnalUmum.txtnoakun1.Text = dgakun.Rows(dgakun.CurrentCell.RowIndex).Cells(0).Value
            FormJurnalUmum.txtnmakun1.Text = dgakun.Rows(dgakun.CurrentCell.RowIndex).Cells(1).Value
        Else
            FormJurnalUmum.txtnoakun2.Text = dgakun.Rows(dgakun.CurrentCell.RowIndex).Cells(0).Value
            FormJurnalUmum.txtnmakun2.Text = dgakun.Rows(dgakun.CurrentCell.RowIndex).Cells(1).Value
        End If
        Me.Close()
    End Sub
    Private Sub txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        refreshGridCari(cbocari.Text, txtcari.Text)
    End Sub
End Class
