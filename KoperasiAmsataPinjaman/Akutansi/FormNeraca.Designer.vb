﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormNeraca
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormNeraca))
        Me.cekbulan = New System.Windows.Forms.CheckBox()
        Me.cmbbulan = New System.Windows.Forms.ComboBox()
        Me.cmbtahun = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdcari = New System.Windows.Forms.Button()
        Me.cmdexport = New System.Windows.Forms.Button()
        Me.cekauto = New System.Windows.Forms.CheckBox()
        Me.lstdata = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'cekbulan
        '
        Me.cekbulan.AutoSize = True
        Me.cekbulan.Location = New System.Drawing.Point(23, 15)
        Me.cekbulan.Name = "cekbulan"
        Me.cekbulan.Size = New System.Drawing.Size(95, 17)
        Me.cekbulan.TabIndex = 0
        Me.cekbulan.Text = "Aktifkan Bulan"
        Me.cekbulan.UseVisualStyleBackColor = True
        '
        'cmbbulan
        '
        Me.cmbbulan.FormattingEnabled = True
        Me.cmbbulan.Location = New System.Drawing.Point(23, 38)
        Me.cmbbulan.Name = "cmbbulan"
        Me.cmbbulan.Size = New System.Drawing.Size(121, 21)
        Me.cmbbulan.TabIndex = 1
        '
        'cmbtahun
        '
        Me.cmbtahun.FormattingEnabled = True
        Me.cmbtahun.Location = New System.Drawing.Point(150, 38)
        Me.cmbtahun.Name = "cmbtahun"
        Me.cmbtahun.Size = New System.Drawing.Size(121, 21)
        Me.cmbtahun.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(147, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Tahun"
        '
        'cmdcari
        '
        Me.cmdcari.BackColor = System.Drawing.Color.SeaShell
        Me.cmdcari.Image = CType(resources.GetObject("cmdcari.Image"), System.Drawing.Image)
        Me.cmdcari.Location = New System.Drawing.Point(277, 36)
        Me.cmdcari.Name = "cmdcari"
        Me.cmdcari.Size = New System.Drawing.Size(42, 23)
        Me.cmdcari.TabIndex = 4
        Me.cmdcari.UseVisualStyleBackColor = False
        '
        'cmdexport
        '
        Me.cmdexport.BackColor = System.Drawing.Color.SeaShell
        Me.cmdexport.Location = New System.Drawing.Point(325, 36)
        Me.cmdexport.Name = "cmdexport"
        Me.cmdexport.Size = New System.Drawing.Size(75, 23)
        Me.cmdexport.TabIndex = 5
        Me.cmdexport.Text = "Export"
        Me.cmdexport.UseVisualStyleBackColor = False
        '
        'cekauto
        '
        Me.cekauto.AutoSize = True
        Me.cekauto.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.cekauto.Location = New System.Drawing.Point(406, 38)
        Me.cekauto.Name = "cekauto"
        Me.cekauto.Size = New System.Drawing.Size(88, 17)
        Me.cekauto.TabIndex = 6
        Me.cekauto.Text = "Auto Refresh"
        Me.cekauto.UseVisualStyleBackColor = False
        '
        'lstdata
        '
        Me.lstdata.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lstdata.Location = New System.Drawing.Point(26, 75)
        Me.lstdata.Name = "lstdata"
        Me.lstdata.Size = New System.Drawing.Size(628, 348)
        Me.lstdata.TabIndex = 7
        Me.lstdata.UseCompatibleStateImageBehavior = False
        Me.lstdata.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Kode Akun"
        Me.ColumnHeader1.Width = 100
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Nama Akun"
        Me.ColumnHeader2.Width = 200
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Debit"
        Me.ColumnHeader3.Width = 150
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Kredit"
        Me.ColumnHeader4.Width = 150
        '
        'FormNeraca
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(724, 447)
        Me.Controls.Add(Me.lstdata)
        Me.Controls.Add(Me.cekauto)
        Me.Controls.Add(Me.cmdexport)
        Me.Controls.Add(Me.cmdcari)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbtahun)
        Me.Controls.Add(Me.cmbbulan)
        Me.Controls.Add(Me.cekbulan)
        Me.Name = "FormNeraca"
        Me.Text = "FormNeraca"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cekbulan As System.Windows.Forms.CheckBox
    Friend WithEvents cmbbulan As System.Windows.Forms.ComboBox
    Friend WithEvents cmbtahun As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdcari As System.Windows.Forms.Button
    Friend WithEvents cmdexport As System.Windows.Forms.Button
    Friend WithEvents cekauto As System.Windows.Forms.CheckBox
    Friend WithEvents lstdata As System.Windows.Forms.ListView
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
End Class
