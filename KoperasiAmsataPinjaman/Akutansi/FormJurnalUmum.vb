﻿Public Class FormJurnalUmum

    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Private Sub btnexit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnexit.Click
        Me.Close()
    End Sub
    Private Sub btnlist1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnlist1.Click
        cekAkun = 1
        FormCariAkun.showdialog()
    End Sub
    Private Sub FormJurnalUmum_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        setTombol(True)
        setAktif(False)
        clearData()
        DateTimePicker1.Value = Date.Now
    End Sub

    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        setTombol(False)
        setAktif(True)
        clearData()
        txtnojurnal.Text = getID("jurnal", "JU0000", "nojurnal")
    End Sub

    Private Sub btncancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancel.Click
        setAktif(False)
        setTombol(True)
        clearData()
    End Sub
    Private Sub btnlist2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnlist2.Click
        cekAkun = 2
        FormCariAkun.showdialog()
    End Sub
    Private Sub btnadd1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd1.Click
        If txtnoakun1.Text = "" Then
            MsgBox("No Akun Masih Kosong !!!!", vbCritical, "Warning")
            Exit Sub
        End If
        If FindSubItem(lstJurnal, txtnoakun1.Text) = True Then
            MsgBox("No Akun sudah ADA !!!!", vbCritical, "Warning")
            Exit Sub
        End If
        AddtoList(txtnoakun1.Text, txtnmakun1.Text, Val(txtjumlah1.Text), 1)
    End Sub
    Private Sub AddtoList(ByVal xNoAkun As String, ByVal xNamaAkun As String,
                          ByVal xJumlah As Decimal, ByVal xJenis As Byte)
        If xJenis = 1 Then
            Item = Me.lstJurnal.Items.Add(xNoAkun)
            Item.SubItems.Add(xNamaAkun)
            Item.SubItems.Add(xJumlah)  'D
            Item.SubItems.Add(0)        'K
        Else
            Item = Me.lstJurnal.Items.Add(xNoAkun)
            Item.SubItems.Add(xNamaAkun)
            Item.SubItems.Add(0)        'D
            Item.SubItems.Add(xJumlah)  'K
        End If
    End Sub

    Private Sub btnadd2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd2.Click
        If txtnoakun2.Text = "" Then
            MsgBox("No Akun Masih Kosong !!!!", vbCritical, "Warning")
            Exit Sub
        End If
        If FindSubItem(lstJurnal, txtnoakun2.Text) = True Then
            MsgBox("No Akun sudah ADA !!!!", vbCritical, "Warning")
            Exit Sub
        End If
        AddtoList(txtnoakun2.Text, txtnmakun2.Text, Val(txtjumlah2.Text), 2)
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        'lstJurnal.ListItems.Remove(lstJurnal.SelectedItem.Index)
        On Error Resume Next
        If lstJurnal.Items.Count > 0 Then
            lstJurnal.Items.Remove(lstJurnal.SelectedItems(0))
        End If
    End Sub
    Private Sub setTombol(ByVal x As Boolean)
        btnnew.Enabled = x
        btnsave.Enabled = Not x
        btncancel.Enabled = Not x
        btnexit.Enabled = x
        btndelete.Enabled = Not x
        btnadd1.Enabled = Not x
        btnadd2.Enabled = Not x
        btnlist1.Enabled = Not x
        btnlist2.Enabled = Not x
    End Sub

    Private Sub setAktif(ByVal x As Boolean)
        DateTimePicker1.Enabled = x
        txtdeskripsi.Enabled = x
        txtnobukti.Enabled = x
        txtnoakun1.Enabled = x
        txtnoakun2.Enabled = x
        txtjumlah1.Enabled = x
        txtjumlah2.Enabled = x
    End Sub
    Private Sub clearData()
        txtnojurnal.Text = ""
        txtdeskripsi.Text = ""
        txtnobukti.Text = ""
        txtnoakun1.Text = ""
        txtnmakun1.Text = ""
        txtjumlah1.Text = ""
        txtnoakun2.Text = ""
        txtnmakun2.Text = ""
        txtjumlah2.Text = ""
        lstJurnal.Items.Clear()
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim jDebet As Double = 0
        Dim jKredit As Double = 0
        'MsgBox(Format(dtTgl.Value, "yyyy-mm-dd hh:mm:ss"))
        'Exit Sub
        If lstjurnal.Items.Count < 1 Then
            MsgBox("Data TIDAK ADA !!!!", vbExclamation, "Warning")
            Exit Sub
        End If
        jDebet = getJumlah(lstjurnal, 2)    'D
        jKredit = getJumlah(lstjurnal, 3)   'K
        If jDebet <> jKredit Then
            MsgBox("Jumlah Debet dan Kredit TIDAK SAMA !!!!", vbExclamation, "Warning")
            Exit Sub
        End If
        'proses simpan
        Dim msql As String
        Dim i As Integer
        Dim sTgl As String

        sTgl = Format(DateTimePicker1.Value, "yyyy-MM-dd HH:mm:ss")
        msql = "insert into jurnal " _
            & "values('" & txtnojurnal.Text _
            & "','" & sTgl & "','" & _
            txtnobukti.Text & "','" & txtdeskripsi.Text & "')"
        proses.ExecuteNonQuery(msql)
        For i = 0 To lstjurnal.Items.Count - 1
            msql = "insert into detail_jurnal values('" & txtnojurnal.Text & _
            "','" & lstjurnal.Items(i).SubItems(0).Text & _
            "'," & Val(lstjurnal.Items(i).SubItems(2).Text) & _
            "," & Val(lstjurnal.Items(i).SubItems(3).Text) & ")"
            proses.ExecuteNonQuery(msql)
        Next
        'Val(lv.Items(i).SubItems(kolom).Text)
        MsgBox("SUKSES di Simpan.....", vbInformation, "Info")
        setAktif(False)
        setTombol(True)
        clearData()
    End Sub
    Private Sub txtnoakun1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtnoakun1.KeyPress
        Dim tombol As Integer = Asc(e.KeyChar)
        If tombol = 13 Then txtnmakun1.Text = getAkun(txtnoakun1.Text)
    End Sub
    Private Function getAkun(ByVal kd As String) As String
        Dim nama As String = ""
        tabel = proses.ExecuteQuery("select * from akun where KodeAkun like ('%" & Trim(kd) & _
                                    "%') order by KodeAkun")
        If tabel.Rows.Count > 0 Then
            nama = tabel.Rows(0).Item(1).ToString
        End If
        Return nama
    End Function
    Private Sub txtnoakun2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtnoakun2.KeyPress
        Dim tombol As Integer = Asc(e.KeyChar)
        If tombol = 13 Then txtnmakun2.Text = getAkun(txtnoakun2.Text)
    End Sub
End Class