﻿Public Class FormPerubahanModal
    Dim proses As New ClsKoneksi
    Dim tahun, bulan As Integer
    Dim ModalAkhir As Double = 0
    Private Sub FormPerubahanModal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbbulan.Items.Clear()
        cmbtahun.Items.Clear()
        For i As Integer = 1 To 12
            cmbbulan.Items.Add(i)
        Next
        tahun = Year(Now)
        For j As Integer = (tahun - 2) To tahun
            cmbtahun.Items.Add(j)
        Next
        cmbbulan.SelectedIndex = Month(Now) - 1
        cmbtahun.Text = tahun

        init_PerubahanModal()
    End Sub
    Private Sub init_PerubahanModal()
        Dim ModalAwal As Double = 0
        Dim tPrive As Double = 0
        Dim tLR As Double = 0
        lstData.Items.Clear()
        bulan = Val(cmbBulan.Text)
        tahun = Val(cmbTahun.Text)
        ModalAwal = getSaldoAwal("3101") 'ambil modal awal
        tLR = getSaldoAwal("3103") 'ambil LR
        tPrive = getJumlahData("SELECT SUM(debit) AS xtot FROM vjurnal WHERE Month(tanggal) = " & _
            bulan & " AND YEAR(tanggal)=" & tahun & " and kodeakun='3102'")
        Item = Me.lstData.Items.Add("Modal Awal")
        Item.SubItems.Add(ModalAwal)
        Item = Me.lstData.Items.Add("Laba/Rugi")
        Item.SubItems.Add(tLR)
        Item = Me.lstData.Items.Add("")
        Item.SubItems.Add("---------------------")
        Item = Me.lstData.Items.Add("")
        Item.SubItems.Add((ModalAwal + tLR))
        Item = Me.lstData.Items.Add("Prive")
        Item.SubItems.Add(tPrive)
        Item = Me.lstData.Items.Add("")
        Item.SubItems.Add("---------------------")
        ModalAkhir = (ModalAwal + tLR) - tPrive
        Item = Me.lstData.Items.Add("Modal Akhir Per : " & Format(Now, "dd-MM-yyyy"))
        Item.SubItems.Add(ModalAkhir)
    End Sub
    Private Function getJumlahData(ByVal msql As String) As Double
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim jml As Double = 0
        tabel = proses.ExecuteQuery(msql)
        If tabel.Rows.Count > 0 Then
            If IsDBNull(tabel.Rows(0).Item(0)) = True Then
                jml = 0
            Else
                jml = tabel.Rows(0).Item(0)
            End If
        Else
            jml = 0
        End If

        Return jml
    End Function

    Private Sub btnpos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnpos.Click
        proses.ExecuteNonQuery("update akun set SaldoAwal=" & ModalAkhir & _
" where KodeAkun='3101'")
        MsgBox("Perubahan Modal berhasil di Update !!!", MsgBoxStyle.Information, "Info")
        Exit Sub
    End Sub

    Private Sub btnexport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnexport.Click
        exportLvToExcel(lstdata)
    End Sub

    Private Sub btncari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncari.Click
        init_PerubahanModal()
    End Sub
End Class