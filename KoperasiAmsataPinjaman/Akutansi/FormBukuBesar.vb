﻿Public Class FormBukuBesar
    Dim tDebet, tKredit As Double
    Dim counter As Integer
    Private Sub FormBukuBesar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim msql As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        cmbakun.Items.Clear()
        msql = "select * from akun order by kodeakun"
        tabel = proses.ExecuteQuery(msql)
        For i As Integer = 0 To tabel.Rows.Count - 1
            cmbakun.Items.Add(tabel.Rows(i).Item(1)) 'isi combo dg nama akun
        Next i
        dtawal.Value = Now
        dtakhir.Value = Now
        cmbakun.SelectedIndex = 0
        baca_jurnal()
        txtdebit.Top = Me.Height - 60  'auto posisi, set anchor l,r
        txtkredit.Top = Me.Height - 60
    End Sub
    Private Sub baca_jurnal()
        Dim msql As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim saldo As Double = 0
        Dim saldoAwal As Double = 0
        saldoAwal = ambilSaldoAwal(cmbakun.Text)
        lstdata.Items.Clear()
        tDebet = tKredit = 0
        msql = "select * from vjurnal where namaakun='" & _
        cmbakun.Text & "' order by nojurnal asc"
        tabel = proses.ExecuteQuery(msql)
        saldo = saldoAwal
        Item = Me.lstdata.Items.Add("-")
        Item.SubItems.Add(Format(Now, "yyyy-MM-dd"))
        Item.SubItems.Add("Saldo Awal")
        Item.SubItems.Add("-")
        If saldo > 0 Then               'debet
            Item.SubItems.Add(saldo)
            Item.SubItems.Add("0")
            Item.SubItems.Add(saldo)
        Else                            'kredit
            Item.SubItems.Add("0")
            Item.SubItems.Add(saldo)
            Item.SubItems.Add(saldo)
        End If
        For i As Integer = 0 To tabel.Rows.Count - 1
            Item = Me.lstdata.Items.Add(tabel.Rows(i).Item(0))
            Item.SubItems.Add(tabel.Rows(i).Item(1))
            Item.SubItems.Add(tabel.Rows(i).Item(3))
            Item.SubItems.Add(tabel.Rows(i).Item(2))
            Item.SubItems.Add(tabel.Rows(i).Item(6))
            Item.SubItems.Add(tabel.Rows(i).Item(7))
            tDebet = tabel.Rows(i).Item(6)
            tKredit = tabel.Rows(i).Item(7)
            saldo = (saldo + tDebet) - tKredit
            Item.SubItems.Add(saldo)
        Next i
        txtdebit.Text = getJumlah(lstdata, 4)
        txtkredit.Text = getJumlah(lstdata, 5)
    End Sub
    Private Sub baca_data()
        Dim msql As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        lstdata.Items.Clear()
        tDebet = tKredit = 0
        msql = "select * from vjurnal where namaakun='" & _
        cmbakun.Text & "' and " _
        & "date(tanggal)>=date('" _
        & dtawal.Value.ToString("yyyy-MM-dd") & "') and " _
        & "date(tanggal)<=date('" _
        & dtakhir.Value.ToString("yyyy-MM-dd") & "') " _
        & " order by nojurnal asc"
        ' MsgBox(msql)
        tabel = proses.ExecuteQuery(msql)
        For i As Integer = 0 To tabel.Rows.Count - 1
            Item = Me.lstdata.Items.Add(tabel.Rows(i).Item(0))
            Item.SubItems.Add(tabel.Rows(i).Item(1))
            Item.SubItems.Add(tabel.Rows(i).Item(3))
            Item.SubItems.Add(tabel.Rows(i).Item(2))
            Item.SubItems.Add(tabel.Rows(i).Item(6))
            Item.SubItems.Add(tabel.Rows(i).Item(7))
        Next i
        txtdebit.Text = getJumlah(lstdata, 4)
        txtkredit.Text = getJumlah(lstdata, 5)
    End Sub

    Private Sub cmdexport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdexport.Click
        exportLvToExcel(lstdata)
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        counter = counter + Timer1.Interval
        If counter >= 3000 Then      '3 detik
            If cekauto.Checked = True Then
                If cekTgl.Checked = True Then
                    baca_data()
                Else
                    baca_jurnal()
                End If
            End If
            counter = 0
        End If
    End Sub
    Private Function ambilSaldoAwal(ByVal nama As String) As Double
        Dim msql As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim hasil As Double = 0
        lstData.Items.Clear()
        tDebet = tKredit = 0
        msql = "select SaldoAwal from akun where namaakun='" & _
        cmbAkun.Text & "'"
        tabel = proses.ExecuteQuery(msql)
        If tabel.Rows.Count = 1 Then
            hasil = tabel.Rows(0).Item(0)
        End If
        Return hasil
    End Function

    Private Sub cmdcari_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdcari.Click
        If cekTgl.Checked = True Then
            baca_data()
        Else
            baca_jurnal()
        End If
    End Sub
End Class