﻿Public Class FormAkun
    Dim strsql As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Private Sub btnkeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnkeluar.Click
        Me.Close()
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        btntambah.Enabled = x
        btnsimpan.Enabled = Not x
        btnkoreksi.Enabled = x
        btnhapus.Enabled = x
        btnbatal.Enabled = Not x
        btnkeluar.Enabled = x
        btnawal.Enabled = x
        btnakhir.Enabled = x
        btnsebelum.Enabled = x
        btnsesudah.Enabled = x
    End Sub
    Private Sub FormAkun_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        tombol(True)
        aktif(False)
    End Sub
    Private Sub FormAkun_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshGrid()
        baca_data(0)
        load_cbocari(tabel, cbocari)
        cbocari.SelectedIndex = 0
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from akun order by KodeAkun")
            Me.dgAkun.DataSource = tabel
            With Me.dgAkun
                .Columns(0).HeaderText = "Kode Akun"
                .Columns(1).HeaderText = "Nama Akun"
                .Columns(2).HeaderText = "Saldo Awal"
                .Columns(0).Width = 100
                .Columns(1).Width = 285
                .Columns(2).Width = 150
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from akun where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by KodeAkun")
            Me.dgAkun.DataSource = tabel
            With Me.dgAkun
                .Columns(0).HeaderText = "Kode Akun"
                .Columns(1).HeaderText = "Nama Akun"
                .Columns(2).HeaderText = "Saldo Awal"
                .Columns(0).Width = 100
                .Columns(1).Width = 285
                .Columns(2).Width = 150
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub baca_data(ByVal posisi As Integer)
        If posisi = 0 Then
            txtkodeakun.Text = IIf(IsDBNull(dgakun.Rows(0).Cells(0).Value) = True, "-", _
                                   dgakun.Rows(0).Cells(0).Value) 'kodeAkun
            txtnamaakun.Text = IIf(IsDBNull(dgakun.Rows(0).Cells(1).Value) = True, "-", _
                                   dgakun.Rows(0).Cells(1).Value) 'namaAkun
            txtsaldoawal.Text = dgakun.Rows(0).Cells(2).Value 'Saldoawal
        Else
            txtkodeakun.Text = IIf(IsDBNull(dgakun.Rows(dgakun.CurrentCell.RowIndex).Cells(0).Value) = True, _
                 "-", dgakun.Rows(dgakun.CurrentCell.RowIndex).Cells(0).Value)
            txtnamaakun.Text = IIf(IsDBNull(dgakun.Rows(dgakun.CurrentCell.RowIndex).Cells(1).Value) = True, _
                 "-", dgakun.Rows(dgakun.CurrentCell.RowIndex).Cells(1).Value)
            txtsaldoawal.Text = dgakun.Rows(dgakun.CurrentCell.RowIndex).Cells(2).Value
        End If
    End Sub
    Private Sub aktif(ByVal x As Boolean)
        txtkodeakun.Enabled = x
        txtnamaakun.Enabled = x
        txtsaldoawal.Enabled = x
    End Sub

    Private Sub btntambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btntambah.Click
        tombol(False)
        aktif(True)
        kosong()
        pil = 1 'insert
    End Sub
    Private Sub kosong()
        txtkodeakun.Text = ""
        txtnamaakun.Text = ""
        txtsaldoawal.Text = ""
    End Sub

    Private Sub btnbatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbatal.Click
        tombol(True)
        aktif(False)
    End Sub

    Private Sub btnsimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsimpan.Click
        Dim msql As String
        If pil = 1 Then                         'tambah
            msql = "insert into akun " _
                & "values('" & txtkodeakun.Text _
                & "','" & txtnamaakun.Text & "'," & _
                Val(txtsaldoawal.Text) & ")"
        Else                                    'koreksi
            msql = "update akun set namaakun='" & _
            txtnamaakun.Text _
            & "',saldoawal=" & Val(txtsaldoawal.Text) _
            & " where kodeakun='" & txtkodeakun.Text & "'"
        End If
        proses.ExecuteNonQuery(msql)
        tombol(True)
        aktif(False)
        refreshGrid()
        MsgBox("Saved Successfully", vbInformation, "Info")
    End Sub
    Private Sub dgakun_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgakun.CellContentClick
        baca_data(1)
    End Sub

    Private Sub dgakun_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgakun.CellMouseClick
        baca_data(1)
    End Sub

    Private Sub btnhapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnhapus.Click
        Dim psn As Long
        psn = MsgBox("Are you sure you want to delete this data?", vbQuestion + vbYesNo, "Question")
        If psn = vbYes Then
            proses.ExecuteNonQuery("delete from akun where kodeakun='" & _
                                   txtkodeakun.Text & "'")
        End If
        refreshGrid()
    End Sub

    Private Sub btnkoreksi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnkoreksi.Click
        pil = 2
        aktif(True)
        tombol(False)
        txtkodeakun.Enabled = False
        txtnamaakun.Focus()
    End Sub
    Private Sub txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        refreshGridCari(cbocari.Text, txtcari.Text)
    End Sub

    Private Sub btnawal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnawal.Click
        Dim nextRow As DataGridViewRow = dgakun.Rows(0)
        ' Move the Glyph arrow to the next row
        dgakun.CurrentCell = nextRow.Cells(0)
        ' Select the next row
        nextRow.Selected = True
        baca_data(1)
    End Sub

    Private Sub btnakhir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnakhir.Click
        Dim maxRowIndex As Integer = (Me.dgakun.Rows.Count - 1 - 1)
        Dim nextRow As DataGridViewRow = dgakun.Rows(maxRowIndex)
        ' Move the Glyph arrow to the next row
        dgakun.CurrentCell = nextRow.Cells(0)
        ' Select the next row
        nextRow.Selected = True
        baca_data(1)
    End Sub

    Private Sub btnsesudah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsesudah.Click
        Dim maxRowIndex As Integer = (Me.dgakun.Rows.Count - 1 - 1)
        Dim curDataGridViewRow As DataGridViewRow = dgakun.CurrentRow
        Dim curRowIndex As Integer = curDataGridViewRow.Index
        If (curRowIndex >= maxRowIndex) Then
            MsgBox("No More Rows Left")
        Else
            Dim nextRow As DataGridViewRow = dgakun.Rows(curRowIndex + 1)
            dgakun.CurrentCell = nextRow.Cells(0)
            nextRow.Selected = True
        End If
        baca_data(1)
    End Sub

    Private Sub btnsebelum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsebelum.Click
        Dim maxRowIndex As Integer = (Me.dgakun.Rows.Count - 1 - 1)
        Dim curDataGridViewRow As DataGridViewRow = dgakun.CurrentRow
        Dim curRowIndex As Integer = curDataGridViewRow.Index
        If (curRowIndex < 1) Then
            MsgBox("First Record.....")
        Else
            Dim nextRow As DataGridViewRow = dgakun.Rows(curRowIndex - 1)
            dgakun.CurrentCell = nextRow.Cells(0)
            nextRow.Selected = True
        End If
        baca_data(1)
    End Sub
End Class
