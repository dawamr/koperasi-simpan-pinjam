﻿Imports System.Data.Odbc
Imports MySql.Data.MySqlClient
Imports System.Drawing.Printing
Public Class FormPembelian
    Dim proses As New Clskoneksi
    Dim tabel As DataTable
    Dim i As Integer
    Dim WithEvents PD As PrintDocument
    Dim PPD As New PrintPreviewDialog

    Private Sub AddtoList(ByVal xKodeBarang As String, ByVal xNamaBarang As String, ByVal xHarga As Double, ByVal xJumlah As Decimal, ByVal xTotalHarga As Decimal)
        xTotalHarga = (xHarga * xJumlah)
        Item = Me.lstBeli.Items.Add(xKodeBarang)
        Item.SubItems.Add(xNamaBarang)
        Item.SubItems.Add(xHarga)
        Item.SubItems.Add(xJumlah)
        Item.SubItems.Add(xTotalHarga)
    End Sub

    Private Sub btnexit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnexit.Click
        Me.Close()
    End Sub

    Private Sub btnadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd.Click
        If txtKodeBarang.Text = "" Then
            MsgBox("No Barang Masih Kosong !!!", vbCritical, "WARNING")
            Exit Sub
        End If
        Dim cari = FindSubItem(lstBeli, txtKodeBarang.Text)
        If cari = False Then
            AddtoList(txtKodeBarang.Text, txtNamaBarang.Text, txtHarga.Text, txtJumlah.Text, (txtHarga.Text * txtJumlah.Text))
        Else
            cari = True
            MsgBox("Barang sudah dimasukkan, hapus terlebih dulu barang, lalu input kembali !!!", vbCritical, "WARNING")
            Exit Sub
        End If
        Call RumusSubTotal()
        RumusSubJumlah()
        add()
    End Sub

    Private Sub txtKodeBarang_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtKodeBarang.KeyPress
        'If e.KeyChar = Chr(13) Then
        'tabel = proses.ExecuteQuery("select * from barang where kd_brg like ('%" & Trim(txtKodeBarang.Text) & _
        '"%') order by nm_brg")
        'If tabel.Rows.Count > 0 Then
        'txtKodeBarang.Text = dgbarang.Rows(dgBarang.CurrentCell.RowIndex).Cells(0).Value
        'txtNamaBarang.Text = dgBarang.Rows(dgBarang.CurrentCell.RowIndex).Cells(1).Value
        'txtHarga.Text = dgBarang.Rows(dgBarang.CurrentCell.RowIndex).Cells(2).Value
        'End If
        'End If
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        'If lstBeli.Items.Count = 0 Then
        'MsgBox("Masukkan beberapa barang pada keranjang !!!", vbCritical, "WARNING")
        'Exit Sub
        'End If
        'lstBeli.ListItems.Remove(lstBeli.SelectedItem.Index)
        On Error Resume Next
        If lstBeli.Items.Count > 0 Then
            lstBeli.Items.Remove(lstBeli.SelectedItems(0))
        Else
            MsgBox("Check kode barang yg mau dihapus !!!", vbCritical, "WARNING")
        End If
    End Sub

    Private Sub tombol(ByVal x As Boolean)
        btnnew.Enabled = x
        btnsave.Enabled = Not x
        btncancel.Enabled = Not x
        btnexit.Enabled = x
        btndelete.Enabled = Not x
        btnadd.Enabled = Not x
        btncaribrg.Enabled = Not x
        btncarisup.Enabled = Not x
        btnprint.Enabled = Not x
    End Sub

    Private Sub setaktif(ByVal x As Boolean)
        txtidsup.Enabled = x
        txtnamasup.Enabled = x
        txtKodeBarang.Enabled = x
        txtHarga.Enabled = x
        txtJumlah.Enabled = x
        txtTotal.Enabled = x
        txtNamaBarang.Enabled = x
        txtalamat.Enabled = x
        txtnobeli.Enabled = x
        txttotalbeli.Enabled = x
    End Sub

    Private Sub cleardata()
        txtnobeli.Text = ""
        txtidsup.Text = ""
        txtNamaSup.Text = ""
        txtKodeBarang.Text = ""
        txtNamaBarang.Text = ""
        txtHarga.Text = ""
        txtJumlah.Text = ""
        txtTotal.Text = ""
        txtalamat.Text = ""
        txttotalbeli.Text = ""
        txtuangmuka.Text = ""
        txtsisa.Text = ""
        lstBeli.Items.Clear()
    End Sub

    Private Sub add()
        txtKodeBarang.Text = ""
        txtNamaBarang.Text = ""
        txtHarga.Text = ""
        txtJumlah.Text = ""
        Call RumusSubTotal()
    End Sub

    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        tombol(False)
        setaktif(True)
        cleardata()
        txtnobeli.Text = getID("beli", "B0000", "no_beli")
    End Sub

    Private Sub btncancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancel.Click
        setaktif(False)
        tombol(True)
        cleardata()
    End Sub

    Private Sub btncarisup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncarisup.Click
        FormCariSupplier.ShowDialog()
    End Sub

    Private Sub btncaribrg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncaribrg.Click
        FormCariBarang.ShowDialog()
    End Sub

    Private Sub FormPembelian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tombol(True)
        setaktif(False)
        cleardata()
        Timer1.Enabled = True
        'refreshGrid()
    End Sub

    Sub RumusSubTotal()
        Dim Hitung As Integer = 0
        For i As Integer = 0 To lstBeli.Items.Count - 1
            Hitung = Hitung + lstBeli.Items(i).SubItems(4).Text
        Next
        txttotalbeli.Text = Hitung
    End Sub

    Sub RumusSubJumlah()
        Dim Hitung As Integer = 0
        For i As Integer = 0 To lstBeli.Items.Count - 1
            Hitung = Hitung + lstBeli.Items(i).SubItems(3).Text
            txtJumlah.Text = Hitung
        Next
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Jam.Text = Format(Now, "HH:mm:ss")
        'Tanggal.Text = Format(Now, "yyyy-MM-dd")
    End Sub

    Private Sub txtJumlah_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtJumlah.TextChanged
        txtTotal.Text = Val(txtHarga.Text) * Val(txtJumlah.Text)
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If lstBeli.Items.Count = 0 Then
            MsgBox("Masukkan beberapa barang pada keranjang !!!", vbCritical, "INFO")
            Exit Sub
        End If
        If txtidsup.Text = "" And txtnamasup.Text = "" Then
            MsgBox("Jangan lupa data supplier !", vbInformation, "INFO")
            Exit Sub
        End If
        'proses simpan
        Dim msql As String
        Dim i As Integer
        'msql = "Insert into beli values ('" & txtnobeli.Text & "','" & txtidsup.Text & "','" & Format(DateTimePicker1.Value, "yyyy-MM-dd HH:mm:ss") & "')"
        msql = "Insert into beli (no_beli, kd_sup, tgl_beli, total_beli, jml_bayar, sisa_hutang) values ('" & txtnobeli.Text & "','" & txtidsup.Text & "','" & Format(DateTimePicker1.Value, "yyyy-MM-dd") & "','" & txttotalbeli.Text & "','" & txtuangmuka.Text & "','" & txtsisa.Text & "')"
        'msql = "insert into hutang (no_beli, kd_sup, tgl_hutang, total_beli, sisa_hutang) VALUES ('" & txtnobeli.Text & "','" & txtidsup.Text & "','" & Format(DateTimePicker1.Value, "yyyy-MM-dd HH:mm:ss") & "','" & txttotalbeli.Text & "','" & txttotalbeli.Text & "')"
        Dim status As Boolean = proses.ExecuteNonQuery(msql)

        '>> proses jurnal
        'msql = "insert into tbl_jurnal value('" & txtnobeli.Text & "',sysdate(),'Transaksi Pembelian Barang','" & lbtot.Text & "')"
        'proses.ExecuteNonQuery(msql)
        'msql = "insert into tbl_detail_jurnal values ('" & txtnobeli.Text & "', '1107'," & lbtot.Text & ",'0')"
        'proses.ExecuteNonQuery(msql)
        'msql = "insert into tbl_detail_jurnal values ('" & txtnobeli.Text & "', '1101','0'," & lbtot.Text & ")"
        'proses.ExecuteNonQuery(msql)
        'Val(lv.Items(i).SubItems(kolom).Text)
        If status Then
            For i = 0 To lstBeli.Items.Count - 1
                msql = "insert into dbeli values('" & getCode("dbeli", "DB") & "' ,'" & txtnobeli.Text & "','" & lstBeli.Items(i).SubItems(0).Text & "','" & lstBeli.Items(i).SubItems(1).Text & "','" & Val(lstBeli.Items(i).SubItems(2).Text) & "','" & Val(lstBeli.Items(i).SubItems(3).Text) & "', '" & Val(lstBeli.Items(i).SubItems(4).Text) & "')"
                proses.ExecuteNonQuery(msql)
                msql = "update barang set stok =  (stok + '" & lstBeli.Items(i).SubItems(3).Text _
                & "'), harga_beli = '" & Val(lstBeli.Items(i).SubItems(2).Text) & "' where kd_brg = '" & lstBeli.Items(i).SubItems(0).Text & "'"
                proses.ExecuteNonQuery(msql)
            Next

            MsgBox("Transaksi Pembelian Sukses dilakukan", vbInformation, "INFO")
        End If
        setaktif(False)
        tombol(True)
        cleardata()
    End Sub

    Private Sub btnprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnprint.Click
        PPD.Document = PD
        PPD.ShowDialog()
    End Sub

    Private Sub PD_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PD.BeginPrint
        Dim pagesetup As New PageSettings
        pagesetup.PaperSize = New PaperSize("custom", 250, 500)
        PD.DefaultPageSettings = pagesetup

    End Sub

    Private Sub PD_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PD.PrintPage
        Dim f10 As New Font("times new roman", 10, FontStyle.Regular)
        Dim f10b As New Font("times new roman", 10, FontStyle.Bold)
        Dim f14 As New Font("times new roman", 14, FontStyle.Bold)
        Dim f12 As New Font("times new roman", 12, FontStyle.Bold)

        Dim leftmargin As Integer = PD.DefaultPageSettings.Margins.Left
        Dim centermargin As Integer = PD.DefaultPageSettings.PaperSize.Width / 2
        Dim rightmargin As Integer = PD.DefaultPageSettings.PaperSize.Width

        Dim kanan As New StringFormat
        Dim tengah As New StringFormat
        kanan.Alignment = StringAlignment.Far
        tengah.Alignment = StringAlignment.Center

        Dim garis As String
        garis = "......................................................."

        e.Graphics.DrawString("Nota Beli", f14, Brushes.Black, centermargin, 5, tengah)
        e.Graphics.DrawString("Koperasi Amsata", f12, Brushes.Black, centermargin, 10, tengah)
        e.Graphics.DrawString("Jl. blabla", f10, Brushes.Black, centermargin, 25, tengah)
        e.Graphics.DrawString("No Telp : (024) 6557235", f10, Brushes.Black, centermargin, 40, tengah)

        e.Graphics.DrawString("No Beli", f10, Brushes.Black, 0, 60)
        e.Graphics.DrawString(":", f10, Brushes.Black, 65, 60)
        e.Graphics.DrawString(txtnobeli.Text, f10, Brushes.Black, 0, 60)
    End Sub

    Private Sub txttotalbeli_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txttotalbeli.TextChanged
        'RumusSubTotal()
    End Sub

    Private Sub lstBeli_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstBeli.SelectedIndexChanged

    End Sub

    Private Sub txtuangmuka_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtuangmuka.TextChanged
        txtuangmuka.Text = digitsOnly.Replace(txtuangmuka.Text, "")
        If txtuangmuka.Text.Length < 1 Then
            Exit Sub
        End If
        If Double.Parse(txtuangmuka.Text) > Double.Parse(txttotalbeli.Text) Then
            txtuangmuka.Text = Double.Parse(txttotalbeli.Text)
        End If
        txtsisa.Text = Double.Parse(txttotalbeli.Text) - Convert.ToInt32(txtuangmuka.Text)
    End Sub
End Class