﻿Public Class FormBayarHutang
    Dim strsql, query, uuid As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Dim uuidBeli, uuidSup As String

    Private Sub FormBayarHutang_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshGrid()
        kosong()
    End Sub

    Private Sub btncari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncari.Click
        FormCariPembelian.ShowDialog()
    End Sub

    Private Sub txtnoBeli_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtnoBeli.TextChanged
        If txtnoBeli.Text.Length <= 5 Then
            Exit Sub
        End If
        tabel = proses.ExecuteQuery("SELECT * FROM view_beli where no_beli = '" & txtnoBeli.Text & "'")
        If tabel.Rows.Count = 0 Then
            MsgBox("Data Pinajaman Tidak Ditemukan!")
        Else
            Dim index = 0
            uuidBeli = tabel.Rows(0).Item(index)
            index += 1
            uuidSup = tabel.Rows(0).Item(index)
            index += 1
            txtTanggalBayar.Text = tabel.Rows(0).Item(index)
            index += 1
            txtJumlahHutang.Text = tabel.Rows(0).Item(index)
            index += 1
            txtUangMuka.Text = tabel.Rows(0).Item(index)
            index += 1
            txtSisaHutang.Text = tabel.Rows(0).Item(index)
            index += 1
            txtNamaSup.Text = tabel.Rows(0).Item(index)

            txtKodePembayaran.Text = getCode("bayar_hutang", "BP")
            refreshGridCari("no_beli", txtnoBeli.Text)
        End If
        pil = 1 'insert
    End Sub

    Private Sub txtJumlahBayar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtJumlahBayar.TextChanged
        If txtJumlahBayar.Text.Length < 1 Then
            btnsave.Enabled = False
            txtSisaBayar.Text = ""
            Exit Sub
        Else
            btnsave.Enabled = True
        End If
        If Convert.ToInt32(txtJumlahBayar.Text) > Convert.ToInt32(txtJumlahHutang.Text) Then
            txtJumlahBayar.Text = txtJumlahHutang.Text
        End If
        If txtJumlahBayar.Text.Length > txtJumlahHutang.Text.Length Then
            txtJumlahBayar.Text = txtJumlahHutang.Text
        End If
        txtSisaBayar.Text = Convert.ToInt32(txtSisaHutang.Text) - Convert.ToInt32(txtJumlahBayar.Text)
    End Sub

    Sub kosong()
        txtKodePembayaran.Text = ""
        txtnoBeli.Text = ""
        txtNamaSup.Text = ""
        txtJumlahHutang.Text = ""
        txtSisaHutang.Text = ""
        txtJumlahBayar.Text = ""
        txtSisaBayar.Text = ""
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If txtJumlahBayar.Text.Length = 0 Then
            MsgBox("Jumlah bayar tidak boleh kosong")
            Exit Sub
        End If
        query = "insert into bayar_hutang values('" & getCode("bayar_hutang", "BH") & "' ,'" & CDate(txtTanggalBayar.Text).ToString("yyyy-MM-dd") & "', '" & txtnoBeli.Text & "','" & txtJumlahBayar.Text & "')"
        If proses.ExecuteNonQuery(query) Then
            query = "UPDATE beli SET " _
                & "sisa_hutang = '" & txtSisaBayar.Text _
                & "' WHERE no_beli = '" & txtnoBeli.Text & "';"
            If proses.ExecuteNonQuery(query) Then
                MsgBox("Berhasil melakukan pembayaran.")
            End If
        End If
        kosong()
        btnsave.Enabled = False
    End Sub

    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("SELECT * from bayar_hutang ORDER BY no_bayar")
            dgpembelian.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgpembelian
                .Columns(index).HeaderText = "No Bayar"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Bayar"
                index = index + 1
                .Columns(index).HeaderText = "No Beli"
                index = index + 1
                .Columns(index).HeaderText = "Jumlah Bayar"
            End With
            dgpembelian.DefaultCellStyle.Font = New Font("Candara", 12)
            dgpembelian.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgpembelian.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
            Exit Sub
        End Try
    End Sub

    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from bayar_hutang where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by no_bayar")
            dgpembelian.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgpembelian
                .Columns(index).HeaderText = "No Bayar"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Bayar"
                index = index + 1
                .Columns(index).HeaderText = "No Beli"
                index = index + 1
                .Columns(index).HeaderText = "Jumlah Bayar"
            End With
            dgpembelian.DefaultCellStyle.Font = New Font("Candara", 12)
            dgpembelian.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgpembelian.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub txtcari_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtcari.TextChanged
        refreshGridCari(cbocari.SelectedItem.ToString, txtcari.Text)
    End Sub

End Class