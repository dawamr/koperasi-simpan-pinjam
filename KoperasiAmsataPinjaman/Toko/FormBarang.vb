﻿Public Class FormBarang
    Dim strsql As String
    Dim proses As New Clskoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0

    Private Sub FormStock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        refreshGrid()
        tombol(True)
        aktif(False)
        baca_data(0)
        load_cbocari(tabel, cbocari)
        cbocari.SelectedIndex = 0
    End Sub

    Private Sub tombol(ByVal x As Boolean)
        btnadd.Enabled = x
        btnsave.Enabled = Not x
        btnedit.Enabled = x
        btndelete.Enabled = x
        btncancel.Enabled = Not x
        btnexit.Enabled = x
        btnawal.Enabled = x
        btnakhir.Enabled = x
        btnsebelum.Enabled = x
        btnsesudah.Enabled = x
    End Sub

    Private Sub aktif(ByVal x As Boolean)
        txtnmbarang.Enabled = x
        txthargajual.Enabled = x
        txthargabeli.Enabled = x
        txtstock.Enabled = x
        txtstockmin.Enabled = x
    End Sub

    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from barang order by kd_brg")
            Me.dgbarang.DataSource = tabel
            With Me.dgbarang
                .Columns(0).HeaderText = "Kode Barang"
                .Columns(1).HeaderText = "Nama Barang"
                .Columns(2).HeaderText = "Harga Jual"
                .Columns(3).HeaderText = "Harga Beli"
                .Columns(4).HeaderText = "Stok"
                .Columns(5).HeaderText = "Stok Min"
                .Columns(0).Width = 60
                .Columns(1).Width = 130
                .Columns(2).Width = 80
                .Columns(3).Width = 80
                .Columns(4).Width = 100
                .Columns(5).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from barang where " & _
            Trim(namaKolom) & _
            " like ('%" & Trim(sCari) & "%') order by kd_brg")
            Me.dgbarang.DataSource = tabel
            With Me.dgbarang
                .Columns(0).HeaderText = "Kode Barang"
                .Columns(1).HeaderText = "Nama Barang"
                .Columns(2).HeaderText = "Harga Jual"
                .Columns(3).HeaderText = "Harga Beli"
                .Columns(4).HeaderText = "Stok"
                .Columns(5).HeaderText = "Stok Min"
                .Columns(0).Width = 60
                .Columns(1).Width = 130
                .Columns(2).Width = 80
                .Columns(3).Width = 80
                .Columns(4).Width = 100
                .Columns(5).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub

    Private Sub baca_data(ByVal posisi As Integer)
        If posisi = 0 Then
            txtkdbarang.Text = IIf(IsDBNull(dgbarang.Rows(0).Cells(0).Value) = True, "-", _
                                   dgbarang.Rows(0).Cells(0).Value) 'kdbrg
            txtnmbarang.Text = IIf(IsDBNull(dgbarang.Rows(0).Cells(1).Value) = True, "-", _
                                   dgbarang.Rows(0).Cells(1).Value) 'namabrg
            txthargajual.Text = IIf(IsDBNull(dgbarang.Rows(0).Cells(2).Value) = True, "-", _
                                   dgbarang.Rows(0).Cells(2).Value) 'harga jual
            txthargabeli.Text = IIf(IsDBNull(dgbarang.Rows(0).Cells(3).Value) = True, "-", _
                                   dgbarang.Rows(0).Cells(3).Value) 'harga beli
            txtstock.Text = IIf(IsDBNull(dgbarang.Rows(0).Cells(4).Value) = True, "-", _
                                   dgbarang.Rows(0).Cells(4).Value) 'stok
            txtstockmin.Text = dgbarang.Rows(0).Cells(5).Value 'stok minimal
        Else

            txtkdbarang.Text = IIf(IsDBNull(dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(0).Value) = True, _
                 "-", dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(0).Value)
            txtnmbarang.Text = IIf(IsDBNull(dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(1).Value) = True, _
                 "-", dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(1).Value)
            txthargajual.Text = IIf(IsDBNull(dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(2).Value) = True, _
                 "-", dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(2).Value)
            txthargabeli.Text = IIf(IsDBNull(dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(3).Value) = True, _
                 "-", dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(3).Value)
            txthargabeli.Text = IIf(IsDBNull(dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(4).Value) = True, _
                 "-", dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(4).Value)
            txtstockmin.Text = dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(5).Value
        End If
    End Sub

    Private Sub btnexit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnexit.Click
        Me.Close()
    End Sub

    Private Sub btncancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancel.Click
        tombol(True)
        aktif(False)
    End Sub

    Private Sub btnadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd.Click
        tombol(False)
        aktif(True)
        kosong()
        pil = 1 'insert
        txtkdbarang.Text = getCode("barang", "B")
    End Sub

    Private Sub kosong()
        txtkdbarang.Text = ""
        txtnmbarang.Text = ""
        txthargajual.Text = ""
        txthargabeli.Text = ""
        txtstock.Text = ""
        txtstockmin.Text = ""
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim psn As Long
        psn = MsgBox("Yakin menghapus data ini ", vbQuestion + vbYesNo, "Pesan")
        If psn = vbYes Then
            proses.ExecuteNonQuery("delete from barang where kd_brg='" & _
                                   txtkdbarang.Text & "'")
        End If
        refreshGrid()
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim msql As String
        If pil = 1 Then                         'tambah
            msql = "insert into barang (kd_brg, nm_brg, harga, harga_beli, stok, stok_min) VALUES ('" & txtkdbarang.Text & "','" & txtnmbarang.Text & "','" & txthargajual.Text & "','" & txthargabeli.Text & "','" & txtstock.Text & "','" & txtstockmin.Text & "')"

        Else                                    'koreksi
            msql = "update barang set nm_brg='" & _
            txtnmbarang.Text _
            & "',stok='" & txtstock.Text _
            & "',stok_min='" & txtstockmin.Text _
            & "',harga='" & txthargajual.Text _
            & "',harga_beli=" & Val(txthargabeli.Text) _
            & " where kd_brg='" & txtkdbarang.Text & "'"
        End If
        proses.ExecuteNonQuery(msql)
        tombol(True)
        aktif(False)
        refreshGrid()
        MsgBox("Berhasil disimpan!", vbInformation, "Info")
    End Sub

    Private Sub btnedit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnedit.Click
        pil = 2
        aktif(True)
        tombol(False)
        txtkdbarang.Enabled = False
        txtnmbarang.Focus()
    End Sub

    Private Sub txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtcari.TextChanged
        refreshGridCari(cbocari.Text, txtcari.Text)
    End Sub

    Private Sub dgbarang_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgbarang.CellContentClick
        baca_data(1)
    End Sub

    Private Sub dgbarang_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgbarang.CellMouseClick
        baca_data(1)
    End Sub

    Private Sub btnawal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnawal.Click
        Dim nextRow As DataGridViewRow = dgbarang.Rows(0)
        ' Move the Glyph arrow to the next row
        dgbarang.CurrentCell = nextRow.Cells(0)
        ' Select the next row
        nextRow.Selected = True
        baca_data(1)
    End Sub

    Private Sub btnakhir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnakhir.Click
        Dim maxRowIndex As Integer = (Me.dgbarang.Rows.Count - 1 - 1)
        Dim nextRow As DataGridViewRow = dgbarang.Rows(maxRowIndex)
        ' Move the Glyph arrow to the next row
        dgbarang.CurrentCell = nextRow.Cells(0)
        ' Select the next row
        nextRow.Selected = True
        baca_data(1)
    End Sub

    Private Sub btnsebelum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsebelum.Click
        Dim maxRowIndex As Integer = (Me.dgbarang.Rows.Count - 1 - 1)
        Dim curDataGridViewRow As DataGridViewRow = dgbarang.CurrentRow
        Dim curRowIndex As Integer = curDataGridViewRow.Index
        If (curRowIndex < 1) Then
            MsgBox("First Record.....")
        Else
            Dim nextRow As DataGridViewRow = dgbarang.Rows(curRowIndex - 1)
            dgbarang.CurrentCell = nextRow.Cells(0)
            nextRow.Selected = True
        End If
        baca_data(1)
    End Sub

    Private Sub btnsesudah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsesudah.Click
        Dim maxRowIndex As Integer = (Me.dgbarang.Rows.Count - 1 - 1)
        Dim curDataGridViewRow As DataGridViewRow = dgbarang.CurrentRow
        Dim curRowIndex As Integer = curDataGridViewRow.Index
        If (curRowIndex >= maxRowIndex) Then
            MsgBox("No More Rows Left")
        Else
            Dim nextRow As DataGridViewRow = dgbarang.Rows(curRowIndex + 1)
            dgbarang.CurrentCell = nextRow.Cells(0)
            nextRow.Selected = True
        End If
        baca_data(1)
    End Sub

    Private Sub Label9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label9.Click

    End Sub
End Class