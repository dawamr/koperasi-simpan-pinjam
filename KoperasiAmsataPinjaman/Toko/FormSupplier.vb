﻿Public Class FormSupplier
    Dim strsql As String
    Dim proses As New Clskoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Private Sub FormSupplier_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshGrid()
        tombol(True)
        aktif(False)
        baca_data(0)
        load_cbocari(tabel, cbocari)
        cbocari.SelectedIndex = 0
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from supplier order by kd_sup")
            Me.Dgsupplier.DataSource = tabel
            With Me.Dgsupplier
                .Columns(0).HeaderText = "Kd Supplier"
                .Columns(1).HeaderText = "Nama Supplier"
                .Columns(2).HeaderText = "Alamat"
                .Columns(3).HeaderText = "Kota"
                .Columns(4).HeaderText = "Telepon"
                .Columns(0).Width = 60
                .Columns(1).Width = 130
                .Columns(2).Width = 180
                .Columns(3).Width = 80
                .Columns(4).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from supplier where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by kd_sup")
            Me.Dgsupplier.DataSource = tabel
            With Me.Dgsupplier
                .Columns(0).HeaderText = "KD Supplier"
                .Columns(1).HeaderText = "Nama Supplier"
                .Columns(2).HeaderText = "Alamat"
                .Columns(3).HeaderText = "Kota"
                .Columns(4).HeaderText = "Telepon"
                .Columns(0).Width = 60
                .Columns(1).Width = 130
                .Columns(2).Width = 180
                .Columns(3).Width = 80
                .Columns(4).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub baca_data(ByVal posisi As Integer)
        If Dgsupplier.CurrentRow.Index >= Dgsupplier.RowCount - 1 Then
            Exit Sub
        End If
        If posisi = 0 Then
            txtkdsup.Text = IIf(IsDBNull(Dgsupplier.Rows(0).Cells(0).Value) = True, "-", _
                                   Dgsupplier.Rows(0).Cells(0).Value) 'kdsup
            txtnmsup.Text = IIf(IsDBNull(Dgsupplier.Rows(0).Cells(1).Value) = True, "-", _
                                   Dgsupplier.Rows(0).Cells(1).Value) 'namasup
            txtalamat.Text = IIf(IsDBNull(Dgsupplier.Rows(0).Cells(2).Value) = True, "-", _
                                   Dgsupplier.Rows(0).Cells(2).Value) 'alamat
            txtkota.Text = IIf(IsDBNull(Dgsupplier.Rows(0).Cells(3).Value) = True, "-", _
                                   Dgsupplier.Rows(0).Cells(3).Value) 'kota
            txttelpon.Text = Dgsupplier.Rows(0).Cells(4).Value 'notelp

        Else

            txtkdsup.Text = IIf(IsDBNull(Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(0).Value) = True, _
                 "-", Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(0).Value)
            txtnmsup.Text = IIf(IsDBNull(Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(1).Value) = True, _
                 "-", Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(1).Value)
            txtalamat.Text = IIf(IsDBNull(Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(2).Value) = True, _
                 "-", Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(2).Value)
            txtkota.Text = IIf(IsDBNull(Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(3).Value) = True, _
                 "-", Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(3).Value)
            txttelpon.Text = Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(4).Value
        End If
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        btnadd.Enabled = x
        btnsave.Enabled = Not x
        btnedit.Enabled = x
        btndelete.Enabled = x
        btncancel.Enabled = Not x
        btnexit.Enabled = x
    End Sub
    Private Sub aktif(ByVal x As Boolean)
        txtnmsup.Enabled = x
        txtalamat.Enabled = x
        txtkota.Enabled = x
        txttelpon.Enabled = x
    End Sub

    Private Sub btnexit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnexit.Click
        Me.Close()
    End Sub

    Private Sub btnadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd.Click
        tombol(False)
        aktif(True)
        kosong()
        txtkdsup.Text = getCode("supplier", "S")
        pil = 1 'insert
    End Sub
    Private Sub kosong()
        txtkdsup.Text = ""
        txtnmsup.Text = ""
        txtalamat.Text = ""
        txtkota.Text = ""
        txttelpon.Text = ""
    End Sub

    Private Sub btncancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancel.Click
        tombol(True)
        aktif(False)
        kosong()
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim msql As String
        If pil = 1 Then                         'tambah
            msql = "insert into supplier (kd_sup, nm_sup, alamat, kota, phone) VALUES ('" & txtkdsup.Text & "','" & txtnmsup.Text & "','" & txtalamat.Text & "','" & txtkota.Text & "','" & txttelpon.Text & "')"

        Else                                    'koreksi
            msql = "update supplier set nm_sup='" & _
            txtnmsup.Text _
            & "',alamat='" & txtalamat.Text _
            & "',kota='" & txtkota.Text _
            & "',phone=" & Val(txttelpon.Text) _
            & " where kd_sup='" & txtkdsup.Text & "'"
        End If
        proses.ExecuteNonQuery(msql)
        tombol(True)
        aktif(False)
        refreshGrid()
        MsgBox("Berhasil disimpan!", vbInformation, "Info")
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim psn As Long
        psn = MsgBox("Yakin menghapus data ini ", vbQuestion + vbYesNo, "Pesan")
        If psn = vbYes Then
            proses.ExecuteNonQuery("delete from supplier where kd_sup='" & _
                                   txtkdsup.Text & "'")
        End If
        refreshGrid()
    End Sub

    Private Sub btnedit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnedit.Click
        pil = 2
        aktif(True)
        tombol(False)
        txtkdsup.Enabled = False
        txtnmsup.Focus()
    End Sub

    Private Sub txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtcari.TextChanged
        refreshGridCari(cbocari.Text, txtcari.Text)
    End Sub

    Private Sub Dgsupplier_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Dgsupplier.CellContentClick
        baca_data(1)
    End Sub

    Private Sub Dgsupplier_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles Dgsupplier.CellMouseClick
        baca_data(1)
    End Sub

    Private Sub btnawal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnawal.Click
        Dim nextRow As DataGridViewRow = Dgsupplier.Rows(0)
        ' Move the Glyph arrow to the next row
        Dgsupplier.CurrentCell = nextRow.Cells(0)
        ' Select the next row
        nextRow.Selected = True
        baca_data(1)
    End Sub

    Private Sub btnakhir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnakhir.Click
        Dim maxRowIndex As Integer = (Me.Dgsupplier.Rows.Count - 1 - 1)
        Dim nextRow As DataGridViewRow = Dgsupplier.Rows(maxRowIndex)
        ' Move the Glyph arrow to the next row
        Dgsupplier.CurrentCell = nextRow.Cells(0)
        ' Select the next row
        nextRow.Selected = True
        baca_data(1)
    End Sub

    Private Sub btnsebelum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsebelum.Click
        Dim maxRowIndex As Integer = (Me.Dgsupplier.Rows.Count - 1 - 1)
        Dim curDataGridViewRow As DataGridViewRow = Dgsupplier.CurrentRow
        Dim curRowIndex As Integer = curDataGridViewRow.Index
        If (curRowIndex < 1) Then
            MsgBox("First Record.....")
        Else
            Dim nextRow As DataGridViewRow = Dgsupplier.Rows(curRowIndex - 1)
            Dgsupplier.CurrentCell = nextRow.Cells(0)
            nextRow.Selected = True
        End If
        baca_data(1)
    End Sub

    Private Sub btnsesudah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsesudah.Click
        Dim maxRowIndex As Integer = (Me.Dgsupplier.Rows.Count - 1 - 1)
        Dim curDataGridViewRow As DataGridViewRow = Dgsupplier.CurrentRow
        Dim curRowIndex As Integer = curDataGridViewRow.Index
        If (curRowIndex >= maxRowIndex) Then
            MsgBox("No More Rows Left")
        Else
            Dim nextRow As DataGridViewRow = Dgsupplier.Rows(curRowIndex + 1)
            Dgsupplier.CurrentCell = nextRow.Cells(0)
            nextRow.Selected = True
        End If
        baca_data(1)
    End Sub
End Class