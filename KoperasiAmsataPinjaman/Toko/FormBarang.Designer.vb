﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormBarang
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormBarang))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtkdbarang = New System.Windows.Forms.TextBox()
        Me.txtnmbarang = New System.Windows.Forms.TextBox()
        Me.txthargajual = New System.Windows.Forms.TextBox()
        Me.txthargabeli = New System.Windows.Forms.TextBox()
        Me.txtstockmin = New System.Windows.Forms.TextBox()
        Me.txtstock = New System.Windows.Forms.TextBox()
        Me.btnakhir = New System.Windows.Forms.Button()
        Me.btnsesudah = New System.Windows.Forms.Button()
        Me.btnsebelum = New System.Windows.Forms.Button()
        Me.btnawal = New System.Windows.Forms.Button()
        Me.btnexit = New System.Windows.Forms.Button()
        Me.btncancel = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btnedit = New System.Windows.Forms.Button()
        Me.btnadd = New System.Windows.Forms.Button()
        Me.dgbarang = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbocari = New System.Windows.Forms.ComboBox()
        Me.txtcari = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        CType(Me.dgbarang, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(86, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Kode Barang"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(86, 108)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nama Barang"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(86, 146)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Harga Jual"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(86, 183)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Harga Beli"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(86, 218)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(35, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Stock"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(86, 255)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(79, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Stock Minimum"
        '
        'txtkdbarang
        '
        Me.txtkdbarang.Location = New System.Drawing.Point(233, 66)
        Me.txtkdbarang.Name = "txtkdbarang"
        Me.txtkdbarang.Size = New System.Drawing.Size(112, 20)
        Me.txtkdbarang.TabIndex = 7
        '
        'txtnmbarang
        '
        Me.txtnmbarang.Location = New System.Drawing.Point(233, 105)
        Me.txtnmbarang.Name = "txtnmbarang"
        Me.txtnmbarang.Size = New System.Drawing.Size(112, 20)
        Me.txtnmbarang.TabIndex = 8
        '
        'txthargajual
        '
        Me.txthargajual.Location = New System.Drawing.Point(233, 143)
        Me.txthargajual.Name = "txthargajual"
        Me.txthargajual.Size = New System.Drawing.Size(112, 20)
        Me.txthargajual.TabIndex = 9
        '
        'txthargabeli
        '
        Me.txthargabeli.Location = New System.Drawing.Point(233, 180)
        Me.txthargabeli.Name = "txthargabeli"
        Me.txthargabeli.Size = New System.Drawing.Size(112, 20)
        Me.txthargabeli.TabIndex = 10
        '
        'txtstockmin
        '
        Me.txtstockmin.Location = New System.Drawing.Point(233, 252)
        Me.txtstockmin.Name = "txtstockmin"
        Me.txtstockmin.Size = New System.Drawing.Size(112, 20)
        Me.txtstockmin.TabIndex = 11
        '
        'txtstock
        '
        Me.txtstock.Location = New System.Drawing.Point(233, 215)
        Me.txtstock.Name = "txtstock"
        Me.txtstock.Size = New System.Drawing.Size(112, 20)
        Me.txtstock.TabIndex = 12
        '
        'btnakhir
        '
        Me.btnakhir.BackColor = System.Drawing.Color.SteelBlue
        Me.btnakhir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnakhir.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnakhir.Image = CType(resources.GetObject("btnakhir.Image"), System.Drawing.Image)
        Me.btnakhir.Location = New System.Drawing.Point(567, 69)
        Me.btnakhir.Name = "btnakhir"
        Me.btnakhir.Size = New System.Drawing.Size(31, 25)
        Me.btnakhir.TabIndex = 26
        Me.btnakhir.UseVisualStyleBackColor = False
        '
        'btnsesudah
        '
        Me.btnsesudah.BackColor = System.Drawing.Color.SteelBlue
        Me.btnsesudah.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsesudah.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnsesudah.Image = CType(resources.GetObject("btnsesudah.Image"), System.Drawing.Image)
        Me.btnsesudah.Location = New System.Drawing.Point(540, 69)
        Me.btnsesudah.Name = "btnsesudah"
        Me.btnsesudah.Size = New System.Drawing.Size(31, 25)
        Me.btnsesudah.TabIndex = 25
        Me.btnsesudah.UseVisualStyleBackColor = False
        '
        'btnsebelum
        '
        Me.btnsebelum.BackColor = System.Drawing.Color.SteelBlue
        Me.btnsebelum.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsebelum.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnsebelum.Image = CType(resources.GetObject("btnsebelum.Image"), System.Drawing.Image)
        Me.btnsebelum.Location = New System.Drawing.Point(512, 69)
        Me.btnsebelum.Name = "btnsebelum"
        Me.btnsebelum.Size = New System.Drawing.Size(31, 25)
        Me.btnsebelum.TabIndex = 24
        Me.btnsebelum.UseVisualStyleBackColor = False
        '
        'btnawal
        '
        Me.btnawal.BackColor = System.Drawing.Color.SteelBlue
        Me.btnawal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnawal.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnawal.Image = CType(resources.GetObject("btnawal.Image"), System.Drawing.Image)
        Me.btnawal.Location = New System.Drawing.Point(483, 69)
        Me.btnawal.Name = "btnawal"
        Me.btnawal.Size = New System.Drawing.Size(35, 25)
        Me.btnawal.TabIndex = 23
        Me.btnawal.UseVisualStyleBackColor = False
        '
        'btnexit
        '
        Me.btnexit.BackColor = System.Drawing.Color.SteelBlue
        Me.btnexit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnexit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnexit.Image = CType(resources.GetObject("btnexit.Image"), System.Drawing.Image)
        Me.btnexit.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnexit.Location = New System.Drawing.Point(346, 327)
        Me.btnexit.Name = "btnexit"
        Me.btnexit.Size = New System.Drawing.Size(58, 51)
        Me.btnexit.TabIndex = 22
        Me.btnexit.Text = "&Exit"
        Me.btnexit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnexit.UseVisualStyleBackColor = False
        '
        'btncancel
        '
        Me.btncancel.BackColor = System.Drawing.Color.SteelBlue
        Me.btncancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btncancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btncancel.Image = CType(resources.GetObject("btncancel.Image"), System.Drawing.Image)
        Me.btncancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btncancel.Location = New System.Drawing.Point(289, 327)
        Me.btncancel.Name = "btncancel"
        Me.btncancel.Size = New System.Drawing.Size(58, 51)
        Me.btncancel.TabIndex = 21
        Me.btncancel.Text = "&Cancel"
        Me.btncancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btncancel.UseVisualStyleBackColor = False
        '
        'btndelete
        '
        Me.btndelete.BackColor = System.Drawing.Color.SteelBlue
        Me.btndelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btndelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btndelete.Image = CType(resources.GetObject("btndelete.Image"), System.Drawing.Image)
        Me.btndelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btndelete.Location = New System.Drawing.Point(233, 327)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(58, 51)
        Me.btndelete.TabIndex = 20
        Me.btndelete.Text = "&Delete"
        Me.btndelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btndelete.UseVisualStyleBackColor = False
        '
        'btnsave
        '
        Me.btnsave.BackColor = System.Drawing.Color.SteelBlue
        Me.btnsave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsave.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnsave.Image = CType(resources.GetObject("btnsave.Image"), System.Drawing.Image)
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnsave.Location = New System.Drawing.Point(121, 327)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(58, 51)
        Me.btnsave.TabIndex = 19
        Me.btnsave.Text = "&Save"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnsave.UseVisualStyleBackColor = False
        '
        'btnedit
        '
        Me.btnedit.BackColor = System.Drawing.Color.SteelBlue
        Me.btnedit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnedit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnedit.Image = CType(resources.GetObject("btnedit.Image"), System.Drawing.Image)
        Me.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnedit.Location = New System.Drawing.Point(176, 327)
        Me.btnedit.Name = "btnedit"
        Me.btnedit.Size = New System.Drawing.Size(59, 51)
        Me.btnedit.TabIndex = 18
        Me.btnedit.Text = "&Edit"
        Me.btnedit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnedit.UseVisualStyleBackColor = False
        '
        'btnadd
        '
        Me.btnadd.BackColor = System.Drawing.Color.SteelBlue
        Me.btnadd.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnadd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnadd.Image = CType(resources.GetObject("btnadd.Image"), System.Drawing.Image)
        Me.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnadd.Location = New System.Drawing.Point(64, 327)
        Me.btnadd.Name = "btnadd"
        Me.btnadd.Size = New System.Drawing.Size(58, 51)
        Me.btnadd.TabIndex = 17
        Me.btnadd.Text = "&Add"
        Me.btnadd.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnadd.UseVisualStyleBackColor = False
        '
        'dgbarang
        '
        Me.dgbarang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgbarang.Location = New System.Drawing.Point(483, 105)
        Me.dgbarang.Name = "dgbarang"
        Me.dgbarang.Size = New System.Drawing.Size(594, 201)
        Me.dgbarang.TabIndex = 27
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.cbocari)
        Me.Panel1.Controls.Add(Me.txtcari)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Location = New System.Drawing.Point(483, 312)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(594, 66)
        Me.Panel1.TabIndex = 28
        '
        'cbocari
        '
        Me.cbocari.Cursor = System.Windows.Forms.Cursors.Default
        Me.cbocari.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbocari.FormattingEnabled = True
        Me.cbocari.Location = New System.Drawing.Point(99, 8)
        Me.cbocari.Name = "cbocari"
        Me.cbocari.Size = New System.Drawing.Size(473, 21)
        Me.cbocari.TabIndex = 7
        Me.cbocari.Text = "--Pilih--"
        '
        'txtcari
        '
        Me.txtcari.Location = New System.Drawing.Point(99, 35)
        Me.txtcari.Name = "txtcari"
        Me.txtcari.Size = New System.Drawing.Size(473, 20)
        Me.txtcari.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(25, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Cari"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 11)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 13)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Berdasarkan"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(380, 18)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(188, 33)
        Me.Label9.TabIndex = 54
        Me.Label9.Text = "Data Barang"
        '
        'FormBarang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1097, 468)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.dgbarang)
        Me.Controls.Add(Me.btnakhir)
        Me.Controls.Add(Me.btnsesudah)
        Me.Controls.Add(Me.btnsebelum)
        Me.Controls.Add(Me.btnawal)
        Me.Controls.Add(Me.btnexit)
        Me.Controls.Add(Me.btncancel)
        Me.Controls.Add(Me.btndelete)
        Me.Controls.Add(Me.btnsave)
        Me.Controls.Add(Me.btnedit)
        Me.Controls.Add(Me.btnadd)
        Me.Controls.Add(Me.txtstock)
        Me.Controls.Add(Me.txtstockmin)
        Me.Controls.Add(Me.txthargabeli)
        Me.Controls.Add(Me.txthargajual)
        Me.Controls.Add(Me.txtnmbarang)
        Me.Controls.Add(Me.txtkdbarang)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FormBarang"
        Me.Text = "Data Stock"
        CType(Me.dgbarang, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtkdbarang As System.Windows.Forms.TextBox
    Friend WithEvents txtnmbarang As System.Windows.Forms.TextBox
    Friend WithEvents txthargajual As System.Windows.Forms.TextBox
    Friend WithEvents txthargabeli As System.Windows.Forms.TextBox
    Friend WithEvents txtstockmin As System.Windows.Forms.TextBox
    Friend WithEvents txtstock As System.Windows.Forms.TextBox
    Friend WithEvents btnakhir As System.Windows.Forms.Button
    Friend WithEvents btnsesudah As System.Windows.Forms.Button
    Friend WithEvents btnsebelum As System.Windows.Forms.Button
    Friend WithEvents btnawal As System.Windows.Forms.Button
    Friend WithEvents btnexit As System.Windows.Forms.Button
    Friend WithEvents btncancel As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btnedit As System.Windows.Forms.Button
    Friend WithEvents btnadd As System.Windows.Forms.Button
    Friend WithEvents dgbarang As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
End Class
