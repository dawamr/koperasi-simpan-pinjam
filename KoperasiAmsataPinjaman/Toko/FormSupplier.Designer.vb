﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormSupplier
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormSupplier))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbocari = New System.Windows.Forms.ComboBox()
        Me.txtcari = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Dgsupplier = New System.Windows.Forms.DataGridView()
        Me.btnakhir = New System.Windows.Forms.Button()
        Me.btnsesudah = New System.Windows.Forms.Button()
        Me.btnsebelum = New System.Windows.Forms.Button()
        Me.btnawal = New System.Windows.Forms.Button()
        Me.btnexit = New System.Windows.Forms.Button()
        Me.btncancel = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btnedit = New System.Windows.Forms.Button()
        Me.btnadd = New System.Windows.Forms.Button()
        Me.txttelpon = New System.Windows.Forms.TextBox()
        Me.txtkota = New System.Windows.Forms.TextBox()
        Me.txtalamat = New System.Windows.Forms.TextBox()
        Me.txtnmsup = New System.Windows.Forms.TextBox()
        Me.txtkdsup = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.Dgsupplier, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.cbocari)
        Me.Panel1.Controls.Add(Me.txtcari)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Location = New System.Drawing.Point(461, 302)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(594, 66)
        Me.Panel1.TabIndex = 52
        '
        'cbocari
        '
        Me.cbocari.Cursor = System.Windows.Forms.Cursors.Default
        Me.cbocari.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbocari.FormattingEnabled = True
        Me.cbocari.Location = New System.Drawing.Point(100, 8)
        Me.cbocari.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cbocari.Name = "cbocari"
        Me.cbocari.Size = New System.Drawing.Size(473, 21)
        Me.cbocari.TabIndex = 7
        Me.cbocari.Text = "--Pilih--"
        '
        'txtcari
        '
        Me.txtcari.Location = New System.Drawing.Point(100, 35)
        Me.txtcari.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtcari.Name = "txtcari"
        Me.txtcari.Size = New System.Drawing.Size(473, 20)
        Me.txtcari.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 38)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(25, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Cari"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 11)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 13)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Berdasarkan"
        '
        'Dgsupplier
        '
        Me.Dgsupplier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgsupplier.Location = New System.Drawing.Point(461, 95)
        Me.Dgsupplier.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Dgsupplier.Name = "Dgsupplier"
        Me.Dgsupplier.Size = New System.Drawing.Size(594, 201)
        Me.Dgsupplier.TabIndex = 51
        '
        'btnakhir
        '
        Me.btnakhir.BackColor = System.Drawing.Color.SteelBlue
        Me.btnakhir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnakhir.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnakhir.Image = CType(resources.GetObject("btnakhir.Image"), System.Drawing.Image)
        Me.btnakhir.Location = New System.Drawing.Point(545, 59)
        Me.btnakhir.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnakhir.Name = "btnakhir"
        Me.btnakhir.Size = New System.Drawing.Size(31, 25)
        Me.btnakhir.TabIndex = 50
        Me.btnakhir.UseVisualStyleBackColor = False
        '
        'btnsesudah
        '
        Me.btnsesudah.BackColor = System.Drawing.Color.SteelBlue
        Me.btnsesudah.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsesudah.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnsesudah.Image = CType(resources.GetObject("btnsesudah.Image"), System.Drawing.Image)
        Me.btnsesudah.Location = New System.Drawing.Point(518, 59)
        Me.btnsesudah.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnsesudah.Name = "btnsesudah"
        Me.btnsesudah.Size = New System.Drawing.Size(31, 25)
        Me.btnsesudah.TabIndex = 49
        Me.btnsesudah.UseVisualStyleBackColor = False
        '
        'btnsebelum
        '
        Me.btnsebelum.BackColor = System.Drawing.Color.SteelBlue
        Me.btnsebelum.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsebelum.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnsebelum.Image = CType(resources.GetObject("btnsebelum.Image"), System.Drawing.Image)
        Me.btnsebelum.Location = New System.Drawing.Point(490, 59)
        Me.btnsebelum.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnsebelum.Name = "btnsebelum"
        Me.btnsebelum.Size = New System.Drawing.Size(31, 25)
        Me.btnsebelum.TabIndex = 48
        Me.btnsebelum.UseVisualStyleBackColor = False
        '
        'btnawal
        '
        Me.btnawal.BackColor = System.Drawing.Color.SteelBlue
        Me.btnawal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnawal.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnawal.Image = CType(resources.GetObject("btnawal.Image"), System.Drawing.Image)
        Me.btnawal.Location = New System.Drawing.Point(461, 59)
        Me.btnawal.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnawal.Name = "btnawal"
        Me.btnawal.Size = New System.Drawing.Size(35, 25)
        Me.btnawal.TabIndex = 47
        Me.btnawal.UseVisualStyleBackColor = False
        '
        'btnexit
        '
        Me.btnexit.BackColor = System.Drawing.Color.SteelBlue
        Me.btnexit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnexit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnexit.Image = CType(resources.GetObject("btnexit.Image"), System.Drawing.Image)
        Me.btnexit.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnexit.Location = New System.Drawing.Point(324, 317)
        Me.btnexit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnexit.Name = "btnexit"
        Me.btnexit.Size = New System.Drawing.Size(58, 51)
        Me.btnexit.TabIndex = 46
        Me.btnexit.Text = "&Exit"
        Me.btnexit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnexit.UseVisualStyleBackColor = False
        '
        'btncancel
        '
        Me.btncancel.BackColor = System.Drawing.Color.SteelBlue
        Me.btncancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btncancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btncancel.Image = CType(resources.GetObject("btncancel.Image"), System.Drawing.Image)
        Me.btncancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btncancel.Location = New System.Drawing.Point(268, 317)
        Me.btncancel.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btncancel.Name = "btncancel"
        Me.btncancel.Size = New System.Drawing.Size(58, 51)
        Me.btncancel.TabIndex = 45
        Me.btncancel.Text = "&Cancel"
        Me.btncancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btncancel.UseVisualStyleBackColor = False
        '
        'btndelete
        '
        Me.btndelete.BackColor = System.Drawing.Color.SteelBlue
        Me.btndelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btndelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btndelete.Image = CType(resources.GetObject("btndelete.Image"), System.Drawing.Image)
        Me.btndelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btndelete.Location = New System.Drawing.Point(211, 317)
        Me.btndelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(58, 51)
        Me.btndelete.TabIndex = 44
        Me.btndelete.Text = "&Delete"
        Me.btndelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btndelete.UseVisualStyleBackColor = False
        '
        'btnsave
        '
        Me.btnsave.BackColor = System.Drawing.Color.SteelBlue
        Me.btnsave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsave.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnsave.Image = CType(resources.GetObject("btnsave.Image"), System.Drawing.Image)
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnsave.Location = New System.Drawing.Point(100, 317)
        Me.btnsave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(58, 51)
        Me.btnsave.TabIndex = 43
        Me.btnsave.Text = "&Save"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnsave.UseVisualStyleBackColor = False
        '
        'btnedit
        '
        Me.btnedit.BackColor = System.Drawing.Color.SteelBlue
        Me.btnedit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnedit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnedit.Image = CType(resources.GetObject("btnedit.Image"), System.Drawing.Image)
        Me.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnedit.Location = New System.Drawing.Point(154, 317)
        Me.btnedit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnedit.Name = "btnedit"
        Me.btnedit.Size = New System.Drawing.Size(59, 51)
        Me.btnedit.TabIndex = 42
        Me.btnedit.Text = "&Edit"
        Me.btnedit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnedit.UseVisualStyleBackColor = False
        '
        'btnadd
        '
        Me.btnadd.BackColor = System.Drawing.Color.SteelBlue
        Me.btnadd.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnadd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnadd.Image = CType(resources.GetObject("btnadd.Image"), System.Drawing.Image)
        Me.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnadd.Location = New System.Drawing.Point(42, 317)
        Me.btnadd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnadd.Name = "btnadd"
        Me.btnadd.Size = New System.Drawing.Size(58, 51)
        Me.btnadd.TabIndex = 41
        Me.btnadd.Text = "&Add"
        Me.btnadd.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnadd.UseVisualStyleBackColor = False
        '
        'txttelpon
        '
        Me.txttelpon.Location = New System.Drawing.Point(212, 242)
        Me.txttelpon.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txttelpon.Name = "txttelpon"
        Me.txttelpon.Size = New System.Drawing.Size(112, 20)
        Me.txttelpon.TabIndex = 40
        '
        'txtkota
        '
        Me.txtkota.Location = New System.Drawing.Point(212, 207)
        Me.txtkota.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtkota.Name = "txtkota"
        Me.txtkota.Size = New System.Drawing.Size(112, 20)
        Me.txtkota.TabIndex = 38
        '
        'txtalamat
        '
        Me.txtalamat.Location = New System.Drawing.Point(212, 170)
        Me.txtalamat.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtalamat.Name = "txtalamat"
        Me.txtalamat.Size = New System.Drawing.Size(112, 20)
        Me.txtalamat.TabIndex = 37
        '
        'txtnmsup
        '
        Me.txtnmsup.Location = New System.Drawing.Point(212, 132)
        Me.txtnmsup.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtnmsup.Name = "txtnmsup"
        Me.txtnmsup.Size = New System.Drawing.Size(112, 20)
        Me.txtnmsup.TabIndex = 36
        '
        'txtkdsup
        '
        Me.txtkdsup.Location = New System.Drawing.Point(212, 93)
        Me.txtkdsup.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtkdsup.Name = "txtkdsup"
        Me.txtkdsup.Size = New System.Drawing.Size(112, 20)
        Me.txtkdsup.TabIndex = 35
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(66, 245)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 13)
        Me.Label6.TabIndex = 33
        Me.Label6.Text = "Telepon"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(66, 210)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 13)
        Me.Label5.TabIndex = 32
        Me.Label5.Text = "Kota"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(66, 173)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Alamat"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(66, 135)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Nama Supplier"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(66, 96)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Kode Supplier"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(418, 9)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(204, 33)
        Me.Label7.TabIndex = 53
        Me.Label7.Text = "Data Supplier"
        '
        'FormSupplier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1095, 425)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Dgsupplier)
        Me.Controls.Add(Me.btnakhir)
        Me.Controls.Add(Me.btnsesudah)
        Me.Controls.Add(Me.btnsebelum)
        Me.Controls.Add(Me.btnawal)
        Me.Controls.Add(Me.btnexit)
        Me.Controls.Add(Me.btncancel)
        Me.Controls.Add(Me.btndelete)
        Me.Controls.Add(Me.btnsave)
        Me.Controls.Add(Me.btnedit)
        Me.Controls.Add(Me.btnadd)
        Me.Controls.Add(Me.txttelpon)
        Me.Controls.Add(Me.txtkota)
        Me.Controls.Add(Me.txtalamat)
        Me.Controls.Add(Me.txtnmsup)
        Me.Controls.Add(Me.txtkdsup)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "FormSupplier"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Dgsupplier, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Dgsupplier As System.Windows.Forms.DataGridView
    Friend WithEvents btnakhir As System.Windows.Forms.Button
    Friend WithEvents btnsesudah As System.Windows.Forms.Button
    Friend WithEvents btnsebelum As System.Windows.Forms.Button
    Friend WithEvents btnawal As System.Windows.Forms.Button
    Friend WithEvents btnexit As System.Windows.Forms.Button
    Friend WithEvents btncancel As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btnedit As System.Windows.Forms.Button
    Friend WithEvents btnadd As System.Windows.Forms.Button
    Friend WithEvents txttelpon As System.Windows.Forms.TextBox
    Friend WithEvents txtkota As System.Windows.Forms.TextBox
    Friend WithEvents txtalamat As System.Windows.Forms.TextBox
    Friend WithEvents txtnmsup As System.Windows.Forms.TextBox
    Friend WithEvents txtkdsup As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
