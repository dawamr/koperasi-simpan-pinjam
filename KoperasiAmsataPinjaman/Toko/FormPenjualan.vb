﻿Public Class Form_Penjualan
    Dim proses As New Clskoneksi
    Dim tabel As DataTable
    Dim i, listIndex, stock, harga_beli As Integer
    
    Private Sub btncarianggota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncarianggota.Click
        FormAnggota.Close()
        FormAnggota.StartPosition = FormStartPosition.CenterScreen
        FormAnggota.Show()
        FormAnggota.cmdkeluar.Visible = True
        FormAnggota.cmdpilih.Visible = True
        FormAnggota.cmdbatal.Visible = False
        FormAnggota.cmdtambah.Visible = False
        FormAnggota.cmdedit.Visible = False
        FormAnggota.cmdSimpan.Visible = False
    End Sub
    Sub aktif()
        txtJumlah.Enabled = True
    End Sub
    Private Sub AddtoList(ByVal xKodeBarang As String, ByVal xNamaBarang As String, ByVal xHarga As Double, ByVal xJumlah As Decimal, ByVal xTotalHarga As Decimal, ByVal xHargaBeli As Integer)
        xTotalHarga = (xHarga * xJumlah)
        Item = Me.lstJual.Items.Add(xKodeBarang)
        Item.SubItems.Add(xNamaBarang)
        Item.SubItems.Add(xHarga)
        Item.SubItems.Add(xJumlah)
        Item.SubItems.Add(xTotalHarga)
        Item.SubItems.Add(xHargaBeli)

    End Sub

    Function jikaKredit(ByVal totalnya As Integer, ByVal bayar As Integer) As Integer
        Return totalnya - bayar
    End Function
    Function jikaTunai(ByVal totalnya As Integer, ByVal bayar As Integer) As Integer
        Return bayar - totalnya
    End Function

    Sub RumusSubTotal()
        Dim Hitung As Integer = 0
        For i As Integer = 0 To lstJual.Items.Count - 1
            Hitung = Hitung + lstJual.Items(i).SubItems(4).Text
        Next
        txttotaljual.Text = Hitung
    End Sub

    Sub RumusSubJumlah()
        Dim Hitung As Integer = 0
        For i As Integer = 0 To lstJual.Items.Count - 1
            Hitung = Hitung + lstJual.Items(i).SubItems(3).Text
            txtJumlah.Text = Hitung
        Next
    End Sub

    Private Sub add()
        txtKodeBarang.Text = ""
        txtNamaBarang.Text = ""
        txtHarga.Text = ""
        txtJumlah.Text = ""
        txtTotalHargaBarang.Text = ""
        Call RumusSubTotal()
    End Sub

    Private Sub tombol(ByVal x As Boolean)
        btnadd.Enabled = x
        btnsave.Enabled = Not x
        btncancel.Enabled = Not x
        btnexit.Enabled = x
        btndelete.Enabled = Not x
        btnadd.Enabled = Not x
        btncaribrg.Enabled = Not x
        btncarianggota.Enabled = Not x
        btnprint.Enabled = Not x
    End Sub
    Private Sub setaktif(ByVal x As Boolean)
        txtUUIDanggota.Enabled = x
        txtNamaAnggota.Enabled = x
        txtKodeBarang.Enabled = x
        txtHarga.Enabled = x
        txtJumlah.Enabled = x
        txtTotalHargaBarang.Enabled = x
        txtNamaBarang.Enabled = x
        txttotaljual.Enabled = x
    End Sub

    Private Sub cleardata()
        txtNojual.Text = ""
        txtUUIDanggota.Text = ""
        txtNamaAnggota.Text = ""
        txtKodeBarang.Text = ""
        txtNamaBarang.Text = ""
        txtHarga.Text = ""
        txtJumlah.Text = ""
        txtTotalHargaBarang.Text = ""
        txttotaljual.Text = ""
        txtBayar.Text = ""
        txtsisa.Text = ""
        lstJual.Items.Clear()
    End Sub

    Private Sub txtUUIDanggota_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUUIDanggota.TextChanged
        Dim data_anggota As DataTable = getData("SELECT nama FROM ms_anggota where uuid = '" & txtUUIDanggota.Text & "'")
        txtNamaAnggota.Text = data_anggota.Rows(0).Item(0)
        txtNojual.Text = getCode("jual", "PJ")
    End Sub

    Private Sub btncaribrg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncaribrg.Click
        FormCariBarang.Show()
    End Sub

    Private Sub txtKodeBarang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKodeBarang.TextChanged
        If txtKodeBarang.Text.Length < 3 Then
            Exit Sub
        End If
        Dim data_barang As DataTable = getData("SELECT nm_brg, harga, stok, harga_beli FROM barang where kd_brg = '" & txtKodeBarang.Text & "'")
        txtNamaBarang.Text = data_barang.Rows(0).Item(0)
        txtHarga.Text = data_barang.Rows(0).Item(1)
        stock = data_barang.Rows(0).Item(2)
        harga_beli = data_barang.Rows(0).Item(3)
        txtJumlah.Enabled = True
        txtJumlah.Text = ""
        txtTotalHargaBarang.Text = ""
    End Sub

    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtJumlah.TextChanged
        If txtJumlah.Text.Length = 0 Then
            Exit Sub
        End If
        If Convert.ToInt32(txtJumlah.Text) > stock Then
            txtJumlah.Text = stock
        End If
        txtTotalHargaBarang.Text = Convert.ToInt32(txtHarga.Text) * Convert.ToInt32(txtJumlah.Text)
    End Sub

    Private Sub btnadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd.Click
        txtNojual.Text = getCode("jual", "PJ")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If txtKodeBarang.Text = "" Then
            MsgBox("No Barang Masih Kosong !!!", vbCritical, "WARNING")
            Exit Sub
        End If
        Dim cari = FindSubItem(lstJual, txtKodeBarang.Text)
        If cari = False Then
            AddtoList(txtKodeBarang.Text, txtNamaBarang.Text, txtHarga.Text, txtJumlah.Text, (txtHarga.Text * txtJumlah.Text), harga_beli)
        Else
            cari = True
            MsgBox("Barang sudah dimasukkan, hapus terlebih dulu barang, lalu input kembali !!!", vbCritical, "WARNING")
            Exit Sub
        End If
        Call RumusSubTotal()
        RumusSubJumlah()
        add()
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        lstJual.Items.RemoveAt(listIndex)
        Call RumusSubTotal()
        RumusSubJumlah()
        add()
    End Sub

    Private Sub lstJual_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstJual.Click
        listIndex = 0
        listIndex = lstJual.FocusedItem.Index
        txtKodeBarang.Text = lstJual.FocusedItem.SubItems(0).Text
        txtNamaBarang.Text = lstJual.FocusedItem.SubItems(1).Text
        txtHarga.Text = lstJual.FocusedItem.SubItems(2).Text
    End Sub


    Private Sub lstJual_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstJual.SelectedIndexChanged
        listIndex = 0
        listIndex = lstJual.FocusedItem.Index
        txtKodeBarang.Text = lstJual.FocusedItem.SubItems(0).Text
        txtNamaBarang.Text = lstJual.FocusedItem.SubItems(1).Text
        txtHarga.Text = lstJual.FocusedItem.SubItems(2).Text
    End Sub

    Private Sub TextBox8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBayar.TextChanged
        txtBayar.Text = digitsOnly.Replace(txtBayar.Text, "")
        If rdBtn1.Checked = False And rdBtn2.Checked = False Then
            MsgBox("Belum memilih pembayaran")
            Exit Sub
        End If
        If txtBayar.Text.Length < 1 Then
            Exit Sub
        End If
        If rdBtn1.Checked Then
            txtSisa.Text = jikaTunai(txttotaljual.Text, txtBayar.Text)
        End If
        If rdBtn2.Checked Then
            txtSisa.Text = jikaKredit(txttotaljual.Text, txtBayar.Text)
        End If
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If lstJual.Items.Count = 0 Then
            MsgBox("Masukkan beberapa barang pada keranjang !!!", vbCritical, "INFO")
            Exit Sub
        End If
        If txtUUIDanggota.Text = "" Or txtNamaAnggota.Text = "" Then
            MsgBox("Jangan lupa data anggota !", vbInformation, "INFO")
            Exit Sub
        End If

        Dim msql, uuidJual, rdBtnChecked As String
        Dim i As Integer
        If rdBtn1.Checked Then
            rdBtnChecked = "Tunai"
        End If
        If rdBtn2.Checked Then
            rdBtnChecked = "Kredit"
        End If
        uuidJual = UUID4()
        'msql = "Insert into beli values ('" & txtnobeli.Text & "','" & txtidsup.Text & "','" & Format(DateTimePicker1.Value, "yyyy-MM-dd HH:mm:ss") & "')"
        msql = "Insert into jual values ('" & uuidJual & "', '" & txtUUIDanggota.Text & "', '" & txtNojual.Text & "','" & Format(DateTimePicker1.Value, "yyyy-MM-dd") & "','" & rdBtnChecked & "', '" & txttotaljual.Text & "','" & txtBayar.Text & "','" & txtSisa.Text & "')"
        'msql = "insert into hutang (no_beli, kd_sup, tgl_hutang, total_beli, sisa_hutang) VALUES ('" & txtnobeli.Text & "','" & txtidsup.Text & "','" & Format(DateTimePicker1.Value, "yyyy-MM-dd HH:mm:ss") & "','" & txttotalbeli.Text & "','" & txttotalbeli.Text & "')"
        Dim status As Boolean = proses.ExecuteNonQuery(msql)

        '>> proses jurnal
        'msql = "insert into tbl_jurnal value('" & txtnobeli.Text & "',sysdate(),'Transaksi Pembelian Barang','" & lbtot.Text & "')"
        'proses.ExecuteNonQuery(msql)
        'msql = "insert into tbl_detail_jurnal values ('" & txtnobeli.Text & "', '1107'," & lbtot.Text & ",'0')"
        'proses.ExecuteNonQuery(msql)
        'msql = "insert into tbl_detail_jurnal values ('" & txtnobeli.Text & "', '1101','0'," & lbtot.Text & ")"
        'proses.ExecuteNonQuery(msql)
        'Val(lv.Items(i).SubItems(kolom).Text)
        If status Then
            For i = 0 To lstJual.Items.Count - 1
                msql = "insert into djual values('" & UUID4() & "' ,'" & uuidJual & "','" & lstJual.Items(i).SubItems(0).Text & "','" & lstJual.Items(i).SubItems(1).Text & "','" & Val(lstJual.Items(i).SubItems(5).Text) & "','" & Val(lstJual.Items(i).SubItems(2).Text) & "', '" & Val(lstJual.Items(i).SubItems(3).Text) & "', '" & Val(lstJual.Items(i).SubItems(4).Text) & "')"
                proses.ExecuteNonQuery(msql)
                msql = "update barang set stok =  (stok - '" & lstJual.Items(i).SubItems(3).Text _
                & "') where kd_brg = '" & lstJual.Items(i).SubItems(0).Text & "'"
                proses.ExecuteNonQuery(msql)
            Next

            MsgBox("Transaksi Penjualan Sukses dilakukan", vbInformation, "INFO")
        End If
        setaktif(False)
        tombol(True)
        cleardata()
    End Sub

    Private Sub btncancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancel.Click
        setaktif(False)
        tombol(True)
        cleardata()
    End Sub
End Class