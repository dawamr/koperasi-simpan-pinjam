﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormBayarPiutang
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormBayarPiutang))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtNamaAnggota = New System.Windows.Forms.TextBox()
        Me.txtKodePembayaran = New System.Windows.Forms.TextBox()
        Me.txtSisaPiutang = New System.Windows.Forms.TextBox()
        Me.txtJumlahBayar = New System.Windows.Forms.TextBox()
        Me.txtSisaBayar = New System.Windows.Forms.TextBox()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btnexit = New System.Windows.Forms.Button()
        Me.btncari = New System.Windows.Forms.Button()
        Me.btnprint = New System.Windows.Forms.Button()
        Me.txtTanggalBayar = New System.Windows.Forms.DateTimePicker()
        Me.txtUUIDpenjualan = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtJumlahPiutang = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(112, 116)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 17)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "UUID Penjualan"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(112, 148)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 17)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "Tanggal Bayar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(112, 187)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 17)
        Me.Label3.TabIndex = 41
        Me.Label3.Text = "Nama Anggota"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(112, 227)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(105, 17)
        Me.Label4.TabIndex = 42
        Me.Label4.Text = "Jumlah Piutang"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(112, 264)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 17)
        Me.Label5.TabIndex = 43
        Me.Label5.Text = "Sisa Piutang"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(112, 301)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 17)
        Me.Label6.TabIndex = 44
        Me.Label6.Text = "Jumlah Bayar"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(112, 340)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(76, 17)
        Me.Label7.TabIndex = 45
        Me.Label7.Text = "Sisa Bayar"
        '
        'txtNamaAnggota
        '
        Me.txtNamaAnggota.Enabled = False
        Me.txtNamaAnggota.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNamaAnggota.Location = New System.Drawing.Point(256, 184)
        Me.txtNamaAnggota.Name = "txtNamaAnggota"
        Me.txtNamaAnggota.Size = New System.Drawing.Size(217, 23)
        Me.txtNamaAnggota.TabIndex = 48
        '
        'txtKodePembayaran
        '
        Me.txtKodePembayaran.Enabled = False
        Me.txtKodePembayaran.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKodePembayaran.Location = New System.Drawing.Point(256, 78)
        Me.txtKodePembayaran.Name = "txtKodePembayaran"
        Me.txtKodePembayaran.Size = New System.Drawing.Size(217, 23)
        Me.txtKodePembayaran.TabIndex = 93
        '
        'txtSisaPiutang
        '
        Me.txtSisaPiutang.Enabled = False
        Me.txtSisaPiutang.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSisaPiutang.Location = New System.Drawing.Point(256, 261)
        Me.txtSisaPiutang.Name = "txtSisaPiutang"
        Me.txtSisaPiutang.Size = New System.Drawing.Size(217, 23)
        Me.txtSisaPiutang.TabIndex = 50
        '
        'txtJumlahBayar
        '
        Me.txtJumlahBayar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJumlahBayar.Location = New System.Drawing.Point(256, 298)
        Me.txtJumlahBayar.Name = "txtJumlahBayar"
        Me.txtJumlahBayar.Size = New System.Drawing.Size(217, 23)
        Me.txtJumlahBayar.TabIndex = 51
        '
        'txtSisaBayar
        '
        Me.txtSisaBayar.Enabled = False
        Me.txtSisaBayar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSisaBayar.Location = New System.Drawing.Point(256, 337)
        Me.txtSisaBayar.Name = "txtSisaBayar"
        Me.txtSisaBayar.Size = New System.Drawing.Size(217, 23)
        Me.txtSisaBayar.TabIndex = 52
        '
        'btnsave
        '
        Me.btnsave.BackColor = System.Drawing.Color.SteelBlue
        Me.btnsave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsave.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnsave.Image = CType(resources.GetObject("btnsave.Image"), System.Drawing.Image)
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnsave.Location = New System.Drawing.Point(348, 395)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(58, 51)
        Me.btnsave.TabIndex = 53
        Me.btnsave.Text = "&Save"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnsave.UseVisualStyleBackColor = False
        '
        'btnexit
        '
        Me.btnexit.BackColor = System.Drawing.Color.SteelBlue
        Me.btnexit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnexit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnexit.Image = CType(resources.GetObject("btnexit.Image"), System.Drawing.Image)
        Me.btnexit.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnexit.Location = New System.Drawing.Point(462, 395)
        Me.btnexit.Name = "btnexit"
        Me.btnexit.Size = New System.Drawing.Size(58, 51)
        Me.btnexit.TabIndex = 54
        Me.btnexit.Text = "&Exit"
        Me.btnexit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnexit.UseVisualStyleBackColor = False
        '
        'btncari
        '
        Me.btncari.BackColor = System.Drawing.Color.SteelBlue
        Me.btncari.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btncari.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btncari.Image = CType(resources.GetObject("btncari.Image"), System.Drawing.Image)
        Me.btncari.Location = New System.Drawing.Point(488, 113)
        Me.btncari.Name = "btncari"
        Me.btncari.Size = New System.Drawing.Size(51, 24)
        Me.btncari.TabIndex = 55
        Me.btncari.UseVisualStyleBackColor = False
        '
        'btnprint
        '
        Me.btnprint.BackColor = System.Drawing.Color.SteelBlue
        Me.btnprint.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnprint.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnprint.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnprint.Location = New System.Drawing.Point(405, 395)
        Me.btnprint.Name = "btnprint"
        Me.btnprint.Size = New System.Drawing.Size(58, 51)
        Me.btnprint.TabIndex = 89
        Me.btnprint.Text = "&Print"
        Me.btnprint.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnprint.UseVisualStyleBackColor = False
        '
        'txtTanggalBayar
        '
        Me.txtTanggalBayar.Enabled = False
        Me.txtTanggalBayar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTanggalBayar.Location = New System.Drawing.Point(256, 148)
        Me.txtTanggalBayar.Name = "txtTanggalBayar"
        Me.txtTanggalBayar.Size = New System.Drawing.Size(217, 23)
        Me.txtTanggalBayar.TabIndex = 90
        '
        'txtUUIDpenjualan
        '
        Me.txtUUIDpenjualan.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUUIDpenjualan.Location = New System.Drawing.Point(256, 113)
        Me.txtUUIDpenjualan.Name = "txtUUIDpenjualan"
        Me.txtUUIDpenjualan.Size = New System.Drawing.Size(217, 23)
        Me.txtUUIDpenjualan.TabIndex = 91
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(112, 81)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(125, 17)
        Me.Label8.TabIndex = 92
        Me.Label8.Text = "Kode Pembayaran"
        '
        'txtJumlahPiutang
        '
        Me.txtJumlahPiutang.Enabled = False
        Me.txtJumlahPiutang.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJumlahPiutang.Location = New System.Drawing.Point(256, 224)
        Me.txtJumlahPiutang.Name = "txtJumlahPiutang"
        Me.txtJumlahPiutang.Size = New System.Drawing.Size(217, 23)
        Me.txtJumlahPiutang.TabIndex = 49
        '
        'FormBayarPiutang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(641, 511)
        Me.Controls.Add(Me.txtKodePembayaran)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtUUIDpenjualan)
        Me.Controls.Add(Me.txtTanggalBayar)
        Me.Controls.Add(Me.btnprint)
        Me.Controls.Add(Me.btncari)
        Me.Controls.Add(Me.btnexit)
        Me.Controls.Add(Me.btnsave)
        Me.Controls.Add(Me.txtSisaBayar)
        Me.Controls.Add(Me.txtJumlahBayar)
        Me.Controls.Add(Me.txtSisaPiutang)
        Me.Controls.Add(Me.txtJumlahPiutang)
        Me.Controls.Add(Me.txtNamaAnggota)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FormBayarPiutang"
        Me.Text = "FormBayarAngsuranToko"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtNamaAnggota As System.Windows.Forms.TextBox
    Friend WithEvents txtKodePembayaran As System.Windows.Forms.TextBox
    Friend WithEvents txtSisaPiutang As System.Windows.Forms.TextBox
    Friend WithEvents txtJumlahBayar As System.Windows.Forms.TextBox
    Friend WithEvents txtSisaBayar As System.Windows.Forms.TextBox
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btnexit As System.Windows.Forms.Button
    Friend WithEvents btncari As System.Windows.Forms.Button
    Friend WithEvents btnprint As System.Windows.Forms.Button
    Friend WithEvents txtTanggalBayar As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtUUIDpenjualan As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtJumlahPiutang As System.Windows.Forms.TextBox
End Class
