﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormLaporanBayarPiutang
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormLaporanBayarPiutang))
        Me.lstBeli = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.cekauto = New System.Windows.Forms.CheckBox()
        Me.btnexport = New System.Windows.Forms.Button()
        Me.btncari = New System.Windows.Forms.Button()
        Me.cekaktif = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'lstBeli
        '
        Me.lstBeli.BackColor = System.Drawing.Color.Lavender
        Me.lstBeli.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7})
        Me.lstBeli.GridLines = True
        Me.lstBeli.Location = New System.Drawing.Point(26, 120)
        Me.lstBeli.Name = "lstBeli"
        Me.lstBeli.Size = New System.Drawing.Size(932, 133)
        Me.lstBeli.TabIndex = 120
        Me.lstBeli.UseCompatibleStateImageBehavior = False
        Me.lstBeli.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "No Bayar"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Tanggal Bayar"
        Me.ColumnHeader2.Width = 120
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "No Jual"
        Me.ColumnHeader3.Width = 120
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Nama Anggota"
        Me.ColumnHeader4.Width = 300
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Jumlah Piutang"
        Me.ColumnHeader5.Width = 120
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Jumlah Bayar"
        Me.ColumnHeader6.Width = 100
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Sisa Piutang"
        Me.ColumnHeader7.Width = 100
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(805, 259)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(153, 20)
        Me.TextBox1.TabIndex = 119
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(120, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(26, 13)
        Me.Label2.TabIndex = 118
        Me.Label2.Text = "s/d"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(154, 82)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(89, 20)
        Me.DateTimePicker2.TabIndex = 117
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(26, 82)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(89, 20)
        Me.DateTimePicker1.TabIndex = 116
        '
        'cekauto
        '
        Me.cekauto.AutoSize = True
        Me.cekauto.Location = New System.Drawing.Point(409, 79)
        Me.cekauto.Name = "cekauto"
        Me.cekauto.Size = New System.Drawing.Size(88, 17)
        Me.cekauto.TabIndex = 115
        Me.cekauto.Text = "Auto Refresh"
        Me.cekauto.UseVisualStyleBackColor = True
        '
        'btnexport
        '
        Me.btnexport.BackColor = System.Drawing.Color.SteelBlue
        Me.btnexport.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnexport.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnexport.Location = New System.Drawing.Point(309, 73)
        Me.btnexport.Name = "btnexport"
        Me.btnexport.Size = New System.Drawing.Size(78, 31)
        Me.btnexport.TabIndex = 114
        Me.btnexport.Text = "&Export"
        Me.btnexport.UseVisualStyleBackColor = False
        '
        'btncari
        '
        Me.btncari.BackColor = System.Drawing.Color.SteelBlue
        Me.btncari.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btncari.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btncari.Image = CType(resources.GetObject("btncari.Image"), System.Drawing.Image)
        Me.btncari.Location = New System.Drawing.Point(261, 73)
        Me.btncari.Name = "btncari"
        Me.btncari.Size = New System.Drawing.Size(41, 31)
        Me.btncari.TabIndex = 113
        Me.btncari.UseVisualStyleBackColor = False
        '
        'cekaktif
        '
        Me.cekaktif.AutoSize = True
        Me.cekaktif.Location = New System.Drawing.Point(26, 46)
        Me.cekaktif.Name = "cekaktif"
        Me.cekaktif.Size = New System.Drawing.Size(107, 17)
        Me.cekaktif.TabIndex = 112
        Me.cekaktif.Text = "Aktifkan Tanggal"
        Me.cekaktif.UseVisualStyleBackColor = True
        '
        'FormLaporanBayarPiutang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1069, 440)
        Me.Controls.Add(Me.lstBeli)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DateTimePicker2)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.cekauto)
        Me.Controls.Add(Me.btnexport)
        Me.Controls.Add(Me.btncari)
        Me.Controls.Add(Me.cekaktif)
        Me.Name = "FormLaporanBayarPiutang"
        Me.Text = "FormLaporanBayarPiutang"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstBeli As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents cekauto As System.Windows.Forms.CheckBox
    Friend WithEvents btnexport As System.Windows.Forms.Button
    Friend WithEvents btncari As System.Windows.Forms.Button
    Friend WithEvents cekaktif As System.Windows.Forms.CheckBox
End Class
