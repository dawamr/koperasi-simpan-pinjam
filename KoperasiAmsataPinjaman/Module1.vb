﻿Imports MySql.Data.MySqlClient.MySqlConnection
Imports System.Diagnostics
Imports System.Data
Imports System.IO
Imports System.Text.RegularExpressions

Module Connection
    '-- MySQL Connection
    Public connDB As New MySql.Data.MySqlClient.MySqlConnection
    Public comDB As New MySql.Data.MySqlClient.MySqlCommand
    Public rdDB As MySql.Data.MySqlClient.MySqlDataReader
    Public myError As MySql.Data.MySqlClient.MySqlError
    Public SQL As String
    Public Item As ListViewItem
    Public cekAkun As Byte
    Public cekanggota As Byte
    Public cekpengajuan As Byte
    Public actLogin As String
    Public hak_user As Byte
    Public dialogPrint As String
    Public digitsOnly As Regex = New Regex("[^\d]")

    Public Sub conecDB()
        Dim strServer As String = "localhost"
        Dim strDbase As String = "koperasi" 'Database name
        Dim strUser As String = "root" 'Database user
        Dim strPass As String = "" 'Database password

        'MySQL Connection String
        If connDB.State <> ConnectionState.Open Then connDB.ConnectionString =
        "server=" & strServer.Trim & ";database=" & strDbase.Trim & ";user=" & strUser.Trim &
        ";password=" & strPass & ";Convert Zero Datetime=True;"
        If connDB.State <> ConnectionState.Open Then connDB.Open()
    End Sub
    Public Sub closeDB()
        If connDB.State <> ConnectionState.Closed Then connDB.Close()
    End Sub
    Public Sub initCMD()
        With comDB
            .Connection = connDB
            .CommandType = CommandType.Text
            .CommandTimeout = 0
        End With
    End Sub
    Public Sub load_cbocari(ByVal DT As DataTable, ByVal cbocari As ComboBox)
        Dim name(DT.Columns.Count) As String
        Dim i As Integer = 0
        For Each column As DataColumn In DT.Columns
            name(i) = column.ColumnName
            cbocari.Items.Add(name(i))
            i += 1 'i=i+1 / i++
        Next
    End Sub
    Public Function FindSubItem(ByVal lv As ListView, ByVal SearchString As String) As Boolean
        Dim founditem As ListViewItem = lv.FindItemWithText(SearchString)
        Dim cek As Boolean = False
        If Not (founditem Is Nothing) Then
            'MessageBox.Show("Data sudah ada !!", "Duplicate", MessageBoxButtons.OK, MessageBoxIcon.Error)
            cek = True
        Else
            cek = False
        End If
        Return cek
    End Function
    Public Function getID(ByVal nmtabel As String, ByVal prefix As String, ByVal urut As String) As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim jrec As Long
        Dim p As Integer
        Dim hasil As String
        Dim xkode As String
        tabel = proses.ExecuteQuery("select * from " & nmtabel & " order by " & urut)
        jrec = tabel.Rows.Count
        If jrec < 1 Then
            hasil = Left(prefix, 5) & "1" '6 digit

        Else
            'xkode = Right(Trim(tabel.Rows(jrec - 1).Item(0)), 4)
            xkode = Mid(Trim(tabel.Rows(jrec - 1).Item(0)), 3, 4) 'JU0001
            p = Len(Trim(Str(Val(xkode) + 1)))
            hasil = Left(prefix, 6 - p) & _
            Trim(Str(Val(xkode) + 1))
            'di tabel jurnal & detailjurnal, nojurnal length 30
        End If
        ' MsgBox(hasil)
        Return hasil
    End Function

    Public Function getCode(ByVal nmtabel As String, ByVal prefix As String) As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim num As Integer
        Dim hasil As String
        tabel = proses.ExecuteQuery("select count(*) from " & nmtabel & "")
        num = Convert.ToInt32(tabel.Rows(0).Item(0)) + 1

        If num.ToString.Length = 1 Then
            hasil = prefix & "0000" & num
        ElseIf num.ToString.Length = 2 Then
            hasil = prefix & "000" & num
        ElseIf num.ToString.Length = 3 Then
            hasil = prefix & "00" & num
        ElseIf num.ToString.Length = 4 Then
            hasil = prefix & "0" & num
        Else
            hasil = prefix & num.ToString
        End If
        ' MsgBox(hasil)
        Return hasil
    End Function
    Public Function getData(ByVal query As String) As DataTable
        Dim proses As New ClsKoneksi
        Dim result As DataTable
        result = proses.ExecuteQuery(query)

        If result.Rows.Count > 0 Then
            Return result
        Else
            Return result
        End If

    End Function
    Public Function checkExits(ByVal table As String, ByVal column As String, ByVal txt As String) As Boolean
        Dim proses As New ClsKoneksi
        Dim result As DataTable
        result = proses.ExecuteQuery("SELECT COUNT(*) FROM " & table & " WHERE " & column & " = '" & txt & "'")

        If Convert.ToInt32(result.Rows(0).Item(0)) > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function UUID4() As String
        Dim myuuid As Guid = Guid.NewGuid()
        Dim myuuidAsString As String = myuuid.ToString()

        Return myuuidAsString
    End Function

    Public Function pembualatanNominal(ByVal nominal As String) As Integer
        Dim angka, akhir As Integer
        If nominal.Length = 0 Then
            Return 0
            Exit Function
        ElseIf nominal.Length = 1 Then
            nominal = "00" & nominal
        ElseIf nominal.Length = 2 Then
            nominal = "0" & nominal
        End If
        angka = Convert.ToInt32(nominal.Substring(nominal.Length - 3))
        If angka < 500 Then
            akhir = 500 - angka
            akhir = akhir + Convert.ToInt32(nominal)
        Else
            akhir = 1000 - angka
            akhir = akhir + Convert.ToInt32(nominal)
        End If

        Return akhir
    End Function

    Public Function ConfigSimpananWajib(ByVal jabatan As String) As Integer
        Dim sw As Integer

        If jabatan = "Senior Staf" Then
            sw = 10000
        ElseIf jabatan = "Supervisor" Then
            sw = 3000
        ElseIf jabatan = "Karyawan" Then
            sw = 2000
        Else
            sw = 0
        End If

        Return sw
    End Function

    Public Function ConfigSimpananPokok() As Integer
        Dim sp As String = 10000

        Return sp
    End Function
    Public Function getJumlah(ByVal lv As ListView, ByVal kolom As Byte) As Double
        Dim xJumlah As Double = 0
        Dim i As Integer
        'MsgBox(lv.Items.Count)
        For i = 0 To lv.Items.Count - 1
            xJumlah = xJumlah + Val(lv.Items(i).SubItems(kolom).Text)
        Next
        Return xJumlah
    End Function
    Public Sub post_jurnal(ByVal nobukti As String, _
    ByVal uraian As String, ByVal jumlah As Double, ByVal jenis As Byte, _
    Optional ByVal kredit As Boolean = True, Optional ByVal um As Double = 0)
        Dim nojurnal As String
        Dim proses As New ClsKoneksi
        'pembelian
        nojurnal = getID("jurnal", "JU000000", "nojurnal")
        If jenis = 0 Then
            If kredit = True Then
                proses.ExecuteNonQuery("insert into jurnal values ('" & _
             nojurnal & "','" & Format(Now, "yyyy-mm-dd") & "','" & _
             nobukti & "', '" & uraian & "')")
                'simpan di detail
                proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
             nojurnal & "', '5101', " & jumlah & ",0)")
                proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
                nojurnal & "', '1101',0," & um & ")")
                proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
               nojurnal & "', '2101',0," & jumlah - um & ")")
            Else
                proses.ExecuteNonQuery("insert into jurnal values ('" & _
                nojurnal & "','" & Format(Now, "yyyy-mm-dd") & "','" & _
                nobukti & "', '" & uraian & "')")
                'simpan di detail
                proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
                nojurnal & "', '5101', " & jumlah & ",0)")
                proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
                nojurnal & "', '1101',0," & jumlah & ")")
            End If

        ElseIf jenis = 1 Then
            If kredit = True Then
                'penjualan
                proses.ExecuteNonQuery("insert into jurnal values ('" & _
                nojurnal & "','" & Format(Now, "yyyy-mm-dd") & "','" & _
                nobukti & "', '" & uraian & "')")
                'simpan di detail
                proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
                nojurnal & "', '1101', " & um & ",0)")
                proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
                nojurnal & "', '4101',0," & jumlah & ")")
                proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
                nojurnal & "', '1102'," & jumlah - um & ",0)")
            Else
                proses.ExecuteNonQuery("insert into jurnal values ('" & _
               nojurnal & "','" & Format(Now, "yyyy-mm-dd") & "','" & _
               nobukti & "', '" & uraian & "')")
                'simpan di detail
                proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
                nojurnal & "', '1101', " & jumlah & ",0)")
                proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
                nojurnal & "', '4101',0," & jumlah & ")")
            End If
        ElseIf jenis = 2 Then
            'penjualan jasa
            proses.ExecuteNonQuery("insert into jurnal values ('" & _
            nojurnal & "','" & Format(Now, "yyyy-mm-dd") & "','" & _
            nobukti & "', '" & uraian & "')")
            proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
            nojurnal & "', '1101', " & jumlah & ",0)")
            proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
            nojurnal & "', '4102',0," & jumlah & ")")
        ElseIf jenis = 3 Then
            'post R/L
            proses.ExecuteNonQuery("insert into jurnal values ('" & _
             nojurnal & "','" & Format(Now, "yyyy-mm-dd") & "','" & _
             nobukti & "', '" & uraian & "')")
            proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
            nojurnal & "', '6101', " & jumlah & ",0)")
            proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
            nojurnal & "', '3101',0," & jumlah & ")")
        ElseIf jenis = 4 Then
            'post piutang
            proses.ExecuteNonQuery("insert into jurnal values ('" & _
             nojurnal & "','" & Format(Now, "yyyy-mm-dd") & "','" & _
             nobukti & "', '" & uraian & "')")
            proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
            nojurnal & "', '1101', " & jumlah & ",0)")
            proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
            nojurnal & "', '1102',0," & jumlah & ")")
        ElseIf jenis = 5 Then
            'post hutang
            proses.ExecuteNonQuery("insert into jurnal values ('" & _
             nojurnal & "','" & Format(Now, "yyyy-mm-dd") & "','" & _
             nobukti & "', '" & uraian & "')")
            proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
            nojurnal & "', '1101',0," & jumlah & ")")
            proses.ExecuteNonQuery("insert into detail_jurnal values ('" & _
            nojurnal & "', '2101'," & jumlah & ",0)")

        End If
    End Sub
    Public Sub exportLvToExcel(ByVal objlv As ListView)
        Try
            Dim objExcel As New Microsoft.Office.Interop.Excel.Application
            Dim bkWorkBook As Microsoft.Office.Interop.Excel.Workbook
            Dim shWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
            Dim i, j As Integer
            objExcel = New Microsoft.Office.Interop.Excel.Application
            bkWorkBook = objExcel.Workbooks.Add
            shWorkSheet = CType(bkWorkBook.ActiveSheet, Microsoft.Office.Interop.Excel.Worksheet)
            For i = 0 To objlv.Columns.Count - 1
                shWorkSheet.Cells(1, i + 1) = objlv.Columns(i).Text
            Next
            For i = 0 To objlv.Items.Count - 1
                For j = 0 To objlv.Items(i).SubItems.Count - 1
                    shWorkSheet.Cells(i + 2, j + 1) = objlv.Items(i).SubItems(j).Text
                Next
            Next
            objExcel.Visible = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Function getSaldoAwal(ByVal kode As String) As Double
        Dim msql As String
        Dim proses As New ClsKoneksi
        Dim tabel As DataTable
        Dim hasil As Double = 0

        msql = "select SaldoAwal from akun where KodeAkun='" & _
        kode & "'"
        tabel = proses.ExecuteQuery(msql)
        If tabel.Rows.Count = 1 Then
            hasil = tabel.Rows(0).Item(0)
        End If

        Return hasil
    End Function

End Module

