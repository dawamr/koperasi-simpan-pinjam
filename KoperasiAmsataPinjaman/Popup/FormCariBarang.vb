﻿Public Class FormCariBarang
    Dim srtsql As String
    Dim proses As New Clskoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0

    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from barang order by kd_brg")
            Me.dgbarang.DataSource = tabel
            With Me.dgbarang
                .Columns(0).HeaderText = "Kode Barang"
                .Columns(1).HeaderText = "Nama Barang"
                .Columns(2).HeaderText = "Harga Jual"
                .Columns(3).HeaderText = "Harga Beli"
                .Columns(4).HeaderText = "Stok"
                .Columns(5).HeaderText = "Stok Min"
                .Columns(0).Width = 60
                .Columns(1).Width = 130
                .Columns(2).Width = 80
                .Columns(3).Width = 80
                .Columns(4).Width = 100
                .Columns(5).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub

    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from barang where " & _
            Trim(namaKolom) & _
            " like ('%" & Trim(sCari) & "%') order by kd_brg")
            Me.dgbarang.DataSource = tabel
            With Me.dgbarang
                .Columns(0).HeaderText = "Kode Barang"
                .Columns(1).HeaderText = "Nama Barang"
                .Columns(2).HeaderText = "Harga Jual"
                .Columns(3).HeaderText = "Harga Beli"
                .Columns(4).HeaderText = "Stok"
                .Columns(5).HeaderText = "Stok Min"
                .Columns(0).Width = 60
                .Columns(1).Width = 130
                .Columns(2).Width = 80
                .Columns(3).Width = 80
                .Columns(4).Width = 100
                .Columns(5).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub

    Private Sub txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtcari.TextChanged
        refreshGridCari(cbocari.Text, txtcari.Text)
    End Sub

    Private Sub btnadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd.Click
        FormPembelian.txtKodeBarang.Text = dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(0).Value
        FormPembelian.txtNamaBarang.Text = dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(1).Value
        FormPembelian.txtHarga.Text = dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(3).Value
        Form_Penjualan.txtKodeBarang.Text = dgbarang.Rows(dgbarang.CurrentCell.RowIndex).Cells(0).Value
        Me.Close()
    End Sub

    Private Sub FormCariBarang_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshGrid()
        load_cbocari(tabel, cbocari)
        cbocari.SelectedIndex = 0
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class