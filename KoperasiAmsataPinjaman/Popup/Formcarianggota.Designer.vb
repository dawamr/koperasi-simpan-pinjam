﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormCariAnggota
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Txtcari = New System.Windows.Forms.TextBox()
        Me.Cbocari = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdkeluar = New System.Windows.Forms.Button()
        Me.cmdpilih = New System.Windows.Forms.Button()
        Me.Dganggota = New System.Windows.Forms.DataGridView()
        Me.GroupBox1.SuspendLayout()
        CType(Me.Dganggota, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Txtcari)
        Me.GroupBox1.Controls.Add(Me.Cbocari)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(41, 27)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(340, 76)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'Txtcari
        '
        Me.Txtcari.Location = New System.Drawing.Point(90, 45)
        Me.Txtcari.Name = "Txtcari"
        Me.Txtcari.Size = New System.Drawing.Size(226, 20)
        Me.Txtcari.TabIndex = 3
        '
        'Cbocari
        '
        Me.Cbocari.FormattingEnabled = True
        Me.Cbocari.Location = New System.Drawing.Point(89, 13)
        Me.Cbocari.Name = "Cbocari"
        Me.Cbocari.Size = New System.Drawing.Size(143, 21)
        Me.Cbocari.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Text Dicari"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Berdasarkan"
        '
        'cmdkeluar
        '
        Me.cmdkeluar.Location = New System.Drawing.Point(620, 423)
        Me.cmdkeluar.Name = "cmdkeluar"
        Me.cmdkeluar.Size = New System.Drawing.Size(76, 28)
        Me.cmdkeluar.TabIndex = 5
        Me.cmdkeluar.Text = "Keluar"
        Me.cmdkeluar.UseVisualStyleBackColor = True
        '
        'cmdpilih
        '
        Me.cmdpilih.Location = New System.Drawing.Point(531, 423)
        Me.cmdpilih.Name = "cmdpilih"
        Me.cmdpilih.Size = New System.Drawing.Size(83, 29)
        Me.cmdpilih.TabIndex = 4
        Me.cmdpilih.Text = "Pilih"
        Me.cmdpilih.UseVisualStyleBackColor = True
        '
        'Dganggota
        '
        Me.Dganggota.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dganggota.Location = New System.Drawing.Point(44, 127)
        Me.Dganggota.Name = "Dganggota"
        Me.Dganggota.Size = New System.Drawing.Size(697, 277)
        Me.Dganggota.TabIndex = 6
        '
        'Formcarianggota
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 464)
        Me.Controls.Add(Me.Dganggota)
        Me.Controls.Add(Me.cmdkeluar)
        Me.Controls.Add(Me.cmdpilih)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Formcarianggota"
        Me.Text = "Formcarianggota"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.Dganggota, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdkeluar As System.Windows.Forms.Button
    Friend WithEvents cmdpilih As System.Windows.Forms.Button
    Friend WithEvents Dganggota As System.Windows.Forms.DataGridView
End Class
