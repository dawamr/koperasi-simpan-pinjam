﻿Public Class FormCariPenjualan

    Dim strsql, uuid As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Public noBeli As String

    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("SELECT beli.*, supplier.nm_sup, supplier.phone FROM `beli` JOIN supplier ON beli.kd_sup = supplier.kd_sup ORDER BY no_beli")
            dgpenjualan.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgpenjualan
                .Columns(index).HeaderText = "No Beli"
                index = index + 1
                .Columns(index).HeaderText = "Kode Supllier"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Beli"
                index = index + 1
                .Columns(index).HeaderText = "Jumlah Hutang"
                index = index + 1
                .Columns(index).HeaderText = "Sisa Hutang"
                index = index + 1
                .Columns(index).HeaderText = "Nama Supplier"
                index = index + 1
                .Columns(index).HeaderText = "Kontak Supplier"

            End With
            dgpenjualan.DefaultCellStyle.Font = New Font("Candara", 12)
            dgpenjualan.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgpenjualan.ReadOnly = True
            dgpenjualan.AutoSizeColumnsMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
            Exit Sub
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from view_pinjaman where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by IDpengajuan")
            dgpenjualan.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgpenjualan
                .Columns(index).HeaderText = "No Beli"
                index = index + 1
                .Columns(index).HeaderText = "Kode Supllier"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Beli"
                index = index + 1
                .Columns(index).HeaderText = "Jumlah Hutang"
                index = index + 1
                .Columns(index).HeaderText = "Sisa Hutang"
                index = index + 1
                .Columns(index).HeaderText = "Nama Supplier"
                index = index + 1
                .Columns(index).HeaderText = "Kontak Supplier"

            End With
            dgpenjualan.DefaultCellStyle.Font = New Font("Candara", 12)
            dgpenjualan.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgpenjualan.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub

    Private Sub baca_data()
        Dim index As Integer = 0
        If dgpenjualan.RowCount > 0 Then
            With dgpenjualan
                Dim baris As Integer = .CurrentRow.Index
                If baris >= dgpenjualan.RowCount - 1 Then
                    Exit Sub
                End If
                noBeli = CStr(.Item(index, baris).Value)
            End With
        End If
    End Sub

    Private Sub btnadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd.Click
        If noBeli = "" Then
            MsgBox("Anda belum memilih Pembelian!")
            Exit Sub
        End If
        FormBayarPiutang.txtUUIDpenjualan.Text = noBeli
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FormCariPembelian_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        refreshGrid()
        load_cbocari(tabel, cbocari)
        cbocari.SelectedIndex = 0
    End Sub

    Private Sub dgpembelian_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgpenjualan.CellClick
        baca_data()
    End Sub

    Private Sub txtcari_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtcari.TextChanged
        refreshGridCari(cbocari.SelectedItem.ToString, txtcari.Text)
    End Sub

    Private Sub dgpembelian_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgpenjualan.CellContentClick
        baca_data()
    End Sub
End Class