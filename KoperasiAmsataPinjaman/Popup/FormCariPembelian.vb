﻿Public Class FormCariPembelian
    Dim strsql, uuid As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Public noBeli As String

    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("SELECT * from view_beli ORDER BY no_beli")
            dgpembelian.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgpembelian
                .Columns(index).HeaderText = "No Beli"
                index = index + 1
                .Columns(index).HeaderText = "Kode Supllier"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Beli"
                index = index + 1
                .Columns(index).HeaderText = "Jumlah Beli"
                index = index + 1
                .Columns(index).HeaderText = "Uang Muka"
                index = index + 1
                .Columns(index).HeaderText = "Sisa Hutang"
                index = index + 1
                .Columns(index).HeaderText = "Nama Supplier"
                index = index + 1
                .Columns(index).HeaderText = "Kontak Supplier"
               
            End With
            dgpembelian.DefaultCellStyle.Font = New Font("Candara", 12)
            dgpembelian.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgpembelian.ReadOnly = True
            dgpembelian.AutoSizeColumnsMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
            Exit Sub
        End Try
    End Sub

    Private Sub baca_data()
        Dim index As Integer = 0
        If dgpembelian.RowCount > 0 Then
            With dgpembelian
                Dim baris As Integer = .CurrentRow.Index
                If baris >= dgpembelian.RowCount - 1 Then
                    Exit Sub
                End If
                noBeli = CStr(.Item(index, baris).Value)
                'index += 1
                'txtUUIDanggota.Text = CStr(.Item(index, baris).Value)
                'index += 1
                'txtIDpengajuan.Text = CStr(.Item(index, baris).Value)
                'index += 1
                ''tanggal
                'index += 1
                'txtNominal.Text = CStr(.Item(index, baris).Value)
                'index += 1
                ''created
                'index += 1
                ''updated
                'index += 1
                'txtidanggota.Text = CStr(.Item(index, baris).Value)
                'index += 1
                'Txtnamaanggota.Text = CStr(.Item(index, baris).Value)
                'index += 1
                'Txtjabatan.Text = CStr(.Item(index, baris).Value)
                'dst
            End With
        End If
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from view_pinjaman where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by IDpengajuan")
            dgpembelian.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgpembelian
                .Columns(index).HeaderText = "No Beli"
                index = index + 1
                .Columns(index).HeaderText = "Kode Supllier"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Beli"
                index = index + 1
                .Columns(index).HeaderText = "Jumlah Hutang"
                index = index + 1
                .Columns(index).HeaderText = "Sisa Hutang"
                index = index + 1
                .Columns(index).HeaderText = "Nama Supplier"
                index = index + 1
                .Columns(index).HeaderText = "Kontak Supplier"

            End With
            dgpembelian.DefaultCellStyle.Font = New Font("Candara", 12)
            dgpembelian.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgpembelian.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub btnadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd.Click
        If noBeli = "" Then
            MsgBox("Anda belum memilih Pembelian!")
            Exit Sub
        End If
        FormBayarHutang.txtnoBeli.Text = noBeli
        Me.Close()
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FormCariPembelian_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        refreshGrid()
        load_cbocari(tabel, cbocari)
        cbocari.SelectedIndex = 0
    End Sub

    Private Sub dgpembelian_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgpembelian.CellClick
        baca_data()
    End Sub

    Private Sub txtcari_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtcari.TextChanged
        refreshGridCari(cbocari.SelectedItem.ToString, txtcari.Text)
    End Sub
End Class