﻿Public Class FormCariAnggota
    Dim strsql As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Private Sub FormCariAnggota_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshGrid()
        load_cbocari(tabel, cbocari)
        cbocari.SelectedIndex = 0
    End Sub
    Private Sub cmdkeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdkeluar.Click
        Me.Close()
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from anggota order by id_anggota")
            Me.dganggota.DataSource = tabel
            With Me.dganggota
                .Columns(0).HeaderText = "Id Anggota"
                .Columns(1).HeaderText = "Nama Anggota"
                .Columns(2).HeaderText = "Jabatan Anggota"
                .Columns(3).HeaderText = "Alamat Anggota"
                .Columns(4).HeaderText = "NoTelp Anggota"
                .Columns(0).Width = 100
                .Columns(1).Width = 300
                .Columns(2).Width = 100
                .Columns(3).Width = 300
                .Columns(4).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from anggota where " & Trim(namaKolom) & " like ('%" & Trim(sCari) & "%') order by id_anggota")
            Me.dganggota.DataSource = tabel
            With Me.dganggota
                .Columns(0).HeaderText = "Id Anggota"
                .Columns(1).HeaderText = "Nama Anggota"
                .Columns(2).HeaderText = "Jabatan Anggota"
                .Columns(3).HeaderText = "Alamat Anggota"
                .Columns(4).HeaderText = "NoTelp Anggota"
                .Columns(0).Width = 100
                .Columns(1).Width = 300
                .Columns(2).Width = 100
                .Columns(3).Width = 300
                .Columns(4).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub cmdpilih_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdpilih.Click
        If cekanggota = 1 Then
            Formsimpsukarela.Txtidanggota.Text = Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(0).Value
            FormSimpsukarela.Txtnamaanggota.Text = Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(1).Value
            Formpengajuanpinjm.Txtidanggota.Text = Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(0).Value
            Formpengajuanpinjm.Txtnamaanggota.Text = Dganggota.Rows(Dganggota.CurrentCell.RowIndex).Cells(1).Value
        End If
        Me.Close()
    End Sub
    Private Sub txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Txtcari.TextChanged
        refreshGridCari(Cbocari.Text, Txtcari.Text)
    End Sub
End Class