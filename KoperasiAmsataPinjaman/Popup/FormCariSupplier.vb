﻿Public Class FormCariSupplier
    Dim strsql As String
    Dim proses As New Clskoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0

    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from supplier order by kd_sup")
            Me.Dgsupplier.DataSource = tabel
            With Me.Dgsupplier
                .Columns(0).HeaderText = "Kd Supplier"
                .Columns(1).HeaderText = "Nama Supplier"
                .Columns(2).HeaderText = "Alamat"
                .Columns(3).HeaderText = "Kota"
                .Columns(4).HeaderText = "Telepon"
                .Columns(0).Width = 60
                .Columns(1).Width = 130
                .Columns(2).Width = 180
                .Columns(3).Width = 80
                .Columns(4).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub

    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from supplier where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by kd_sup")
            Me.Dgsupplier.DataSource = tabel
            With Me.Dgsupplier
                .Columns(0).HeaderText = "KD Supplier"
                .Columns(1).HeaderText = "Nama Supplier"
                .Columns(2).HeaderText = "Alamat"
                .Columns(3).HeaderText = "Kota"
                .Columns(4).HeaderText = "Telepon"
                .Columns(0).Width = 60
                .Columns(1).Width = 130
                .Columns(2).Width = 180
                .Columns(3).Width = 80
                .Columns(4).Width = 100
            End With
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub

    Private Sub FormCariSupplier_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshGrid()
        load_cbocari(tabel, cbocari)
        cbocari.SelectedIndex = 0
    End Sub

    Private Sub txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtcari.TextChanged
        refreshGridCari(cbocari.Text, txtcari.Text)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub btnpilih_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnpilih.Click
        FormPembelian.txtidsup.Text = Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(0).Value
        FormPembelian.txtnamasup.Text = Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(1).Value
        FormPembelian.txtalamat.Text = Dgsupplier.Rows(Dgsupplier.CurrentCell.RowIndex).Cells(2).Value
        Me.Close()
    End Sub
End Class