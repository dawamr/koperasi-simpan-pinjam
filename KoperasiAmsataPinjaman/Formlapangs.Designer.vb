﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Formlapangs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Formlapangs))
        Me.Lstdata = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Txtkredit = New System.Windows.Forms.TextBox()
        Me.Txtdebet = New System.Windows.Forms.TextBox()
        Me.Cekauto = New System.Windows.Forms.CheckBox()
        Me.Btnexport = New System.Windows.Forms.Button()
        Me.Btncari = New System.Windows.Forms.Button()
        Me.Cmbakun = New System.Windows.Forms.ComboBox()
        Me.Dtakhir = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Dtawal = New System.Windows.Forms.DateTimePicker()
        Me.Cektgl = New System.Windows.Forms.CheckBox()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Lstdata
        '
        Me.Lstdata.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader10, Me.ColumnHeader11, Me.ColumnHeader12, Me.ColumnHeader13, Me.ColumnHeader2})
        Me.Lstdata.Location = New System.Drawing.Point(5, 77)
        Me.Lstdata.Name = "Lstdata"
        Me.Lstdata.Size = New System.Drawing.Size(834, 356)
        Me.Lstdata.TabIndex = 22
        Me.Lstdata.UseCompatibleStateImageBehavior = False
        Me.Lstdata.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "No"
        Me.ColumnHeader1.Width = 34
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "ID Angsuran"
        Me.ColumnHeader7.Width = 93
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "ID Anggota"
        Me.ColumnHeader8.Width = 86
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Nama Anggota"
        Me.ColumnHeader9.Width = 160
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Angsuran Ke"
        Me.ColumnHeader10.Width = 76
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Tgl Bayar"
        Me.ColumnHeader11.Width = 126
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "Nominal Angsuran"
        Me.ColumnHeader12.Width = 137
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Total Bayar"
        Me.ColumnHeader13.Width = 157
        '
        'Txtkredit
        '
        Me.Txtkredit.Location = New System.Drawing.Point(713, 439)
        Me.Txtkredit.Name = "Txtkredit"
        Me.Txtkredit.Size = New System.Drawing.Size(123, 20)
        Me.Txtkredit.TabIndex = 21
        '
        'Txtdebet
        '
        Me.Txtdebet.Location = New System.Drawing.Point(584, 439)
        Me.Txtdebet.Name = "Txtdebet"
        Me.Txtdebet.Size = New System.Drawing.Size(123, 20)
        Me.Txtdebet.TabIndex = 20
        '
        'Cekauto
        '
        Me.Cekauto.AutoSize = True
        Me.Cekauto.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Cekauto.Location = New System.Drawing.Point(620, 48)
        Me.Cekauto.Name = "Cekauto"
        Me.Cekauto.Size = New System.Drawing.Size(88, 17)
        Me.Cekauto.TabIndex = 19
        Me.Cekauto.Text = "Auto Refresh"
        Me.Cekauto.UseVisualStyleBackColor = False
        '
        'Btnexport
        '
        Me.Btnexport.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Btnexport.Location = New System.Drawing.Point(532, 39)
        Me.Btnexport.Name = "Btnexport"
        Me.Btnexport.Size = New System.Drawing.Size(73, 32)
        Me.Btnexport.TabIndex = 18
        Me.Btnexport.Text = "Export"
        Me.Btnexport.UseVisualStyleBackColor = True
        '
        'Btncari
        '
        Me.Btncari.Image = CType(resources.GetObject("Btncari.Image"), System.Drawing.Image)
        Me.Btncari.Location = New System.Drawing.Point(488, 39)
        Me.Btncari.Name = "Btncari"
        Me.Btncari.Size = New System.Drawing.Size(35, 32)
        Me.Btncari.TabIndex = 17
        Me.Btncari.UseVisualStyleBackColor = True
        '
        'Cmbakun
        '
        Me.Cmbakun.FormattingEnabled = True
        Me.Cmbakun.Location = New System.Drawing.Point(371, 46)
        Me.Cmbakun.Name = "Cmbakun"
        Me.Cmbakun.Size = New System.Drawing.Size(111, 21)
        Me.Cmbakun.TabIndex = 16
        '
        'Dtakhir
        '
        Me.Dtakhir.Location = New System.Drawing.Point(199, 46)
        Me.Dtakhir.Name = "Dtakhir"
        Me.Dtakhir.Size = New System.Drawing.Size(154, 20)
        Me.Dtakhir.TabIndex = 15
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(170, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(23, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "s/d"
        '
        'Dtawal
        '
        Me.Dtawal.Location = New System.Drawing.Point(5, 46)
        Me.Dtawal.Name = "Dtawal"
        Me.Dtawal.Size = New System.Drawing.Size(159, 20)
        Me.Dtawal.TabIndex = 13
        '
        'Cektgl
        '
        Me.Cektgl.AutoSize = True
        Me.Cektgl.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Cektgl.Location = New System.Drawing.Point(10, 23)
        Me.Cektgl.Name = "Cektgl"
        Me.Cektgl.Size = New System.Drawing.Size(107, 17)
        Me.Cektgl.TabIndex = 12
        Me.Cektgl.Text = "Aktifkan Tanggal"
        Me.Cektgl.UseVisualStyleBackColor = False
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Sisa Bayar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(370, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Nama Anggota"
        '
        'Formlapangs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(845, 482)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Lstdata)
        Me.Controls.Add(Me.Txtkredit)
        Me.Controls.Add(Me.Txtdebet)
        Me.Controls.Add(Me.Cekauto)
        Me.Controls.Add(Me.Btnexport)
        Me.Controls.Add(Me.Btncari)
        Me.Controls.Add(Me.Cmbakun)
        Me.Controls.Add(Me.Dtakhir)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Dtawal)
        Me.Controls.Add(Me.Cektgl)
        Me.Name = "Formlapangs"
        Me.Text = "Laporan Periodik Angsuran"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Lstdata As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Txtkredit As System.Windows.Forms.TextBox
    Friend WithEvents Txtdebet As System.Windows.Forms.TextBox
    Friend WithEvents Cekauto As System.Windows.Forms.CheckBox
    Friend WithEvents Btnexport As System.Windows.Forms.Button
    Friend WithEvents Btncari As System.Windows.Forms.Button
    Friend WithEvents Cmbakun As System.Windows.Forms.ComboBox
    Friend WithEvents Dtakhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Dtawal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Cektgl As System.Windows.Forms.CheckBox
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader13 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
