﻿Public Class Pengawas

    Private Sub StSimpanPinjam_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PanelMenuItem()
    End Sub

    Sub PanelMenuOff()
        panelMenu1.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu2.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu3.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu4.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu5.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu6.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu7.BackColor = Color.FromArgb(10, 110, 100)
    End Sub

    Sub PanelMenuItem()
        Dim akses As String = FormLogin.authPengguna.Rows(0).Item("akses")
        txtMenuPanel.Text = akses
        If akses = "Kepala" Then
            btnMenu1.Text = "Anggota"
            btnMenu2.Text = "Simpanan Pokok"
            btnMenu3.Text = "Simpanan Sukarela"
            btnMenu4.Text = "Simpanan Wajib"
            btnMenu5.Text = "Pengajuan Pinjaman"
            btnMenu6.Text = "Pinjaman"
            btnMenu7.Text = "Angsuran"
            btnMenu8.Text = "Rekap"
        End If
    End Sub

End Class