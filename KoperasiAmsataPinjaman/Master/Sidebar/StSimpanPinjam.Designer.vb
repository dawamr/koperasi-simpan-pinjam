﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StSimpanPinjam
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.panelMenu7 = New System.Windows.Forms.Panel()
        Me.btnMenu7 = New System.Windows.Forms.Button()
        Me.txtMenuPanel = New System.Windows.Forms.Label()
        Me.panelMenu1 = New System.Windows.Forms.Panel()
        Me.btnMenu1 = New System.Windows.Forms.Button()
        Me.panelMenu6 = New System.Windows.Forms.Panel()
        Me.btnMenu6 = New System.Windows.Forms.Button()
        Me.panelMenu5 = New System.Windows.Forms.Panel()
        Me.btnMenu5 = New System.Windows.Forms.Button()
        Me.panelMenu4 = New System.Windows.Forms.Panel()
        Me.btnMenu4 = New System.Windows.Forms.Button()
        Me.panelMenu3 = New System.Windows.Forms.Panel()
        Me.btnMenu3 = New System.Windows.Forms.Button()
        Me.panelMenu2 = New System.Windows.Forms.Panel()
        Me.btnMenu2 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.panelMenu8 = New System.Windows.Forms.Panel()
        Me.btnMenu8 = New System.Windows.Forms.Button()
        Me.Panel2.SuspendLayout()
        Me.panelMenu7.SuspendLayout()
        Me.panelMenu1.SuspendLayout()
        Me.panelMenu6.SuspendLayout()
        Me.panelMenu5.SuspendLayout()
        Me.panelMenu4.SuspendLayout()
        Me.panelMenu3.SuspendLayout()
        Me.panelMenu2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelMenu8.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel2.Controls.Add(Me.panelMenu8)
        Me.Panel2.Controls.Add(Me.panelMenu7)
        Me.Panel2.Controls.Add(Me.txtMenuPanel)
        Me.Panel2.Controls.Add(Me.panelMenu1)
        Me.Panel2.Controls.Add(Me.panelMenu6)
        Me.Panel2.Controls.Add(Me.panelMenu5)
        Me.Panel2.Controls.Add(Me.panelMenu4)
        Me.Panel2.Controls.Add(Me.panelMenu3)
        Me.Panel2.Controls.Add(Me.panelMenu2)
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.ForeColor = System.Drawing.Color.White
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(180, 543)
        Me.Panel2.TabIndex = 4
        '
        'panelMenu7
        '
        Me.panelMenu7.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenu7.Controls.Add(Me.btnMenu7)
        Me.panelMenu7.Location = New System.Drawing.Point(1, 413)
        Me.panelMenu7.Name = "panelMenu7"
        Me.panelMenu7.Size = New System.Drawing.Size(177, 39)
        Me.panelMenu7.TabIndex = 7
        '
        'btnMenu7
        '
        Me.btnMenu7.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenu7.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenu7.FlatAppearance.BorderSize = 0
        Me.btnMenu7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenu7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenu7.ForeColor = System.Drawing.Color.White
        Me.btnMenu7.Location = New System.Drawing.Point(5, 0)
        Me.btnMenu7.Name = "btnMenu7"
        Me.btnMenu7.Size = New System.Drawing.Size(172, 39)
        Me.btnMenu7.TabIndex = 0
        Me.btnMenu7.Text = "-"
        Me.btnMenu7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenu7.UseVisualStyleBackColor = False
        '
        'txtMenuPanel
        '
        Me.txtMenuPanel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.txtMenuPanel.Location = New System.Drawing.Point(0, 126)
        Me.txtMenuPanel.Name = "txtMenuPanel"
        Me.txtMenuPanel.Size = New System.Drawing.Size(180, 31)
        Me.txtMenuPanel.TabIndex = 6
        Me.txtMenuPanel.Text = "-"
        Me.txtMenuPanel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'panelMenu1
        '
        Me.panelMenu1.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenu1.Controls.Add(Me.btnMenu1)
        Me.panelMenu1.Location = New System.Drawing.Point(1, 173)
        Me.panelMenu1.Name = "panelMenu1"
        Me.panelMenu1.Size = New System.Drawing.Size(177, 39)
        Me.panelMenu1.TabIndex = 2
        '
        'btnMenu1
        '
        Me.btnMenu1.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenu1.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenu1.FlatAppearance.BorderSize = 0
        Me.btnMenu1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenu1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenu1.ForeColor = System.Drawing.Color.White
        Me.btnMenu1.Location = New System.Drawing.Point(5, 0)
        Me.btnMenu1.Name = "btnMenu1"
        Me.btnMenu1.Size = New System.Drawing.Size(172, 39)
        Me.btnMenu1.TabIndex = 0
        Me.btnMenu1.Text = "-"
        Me.btnMenu1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenu1.UseVisualStyleBackColor = False
        '
        'panelMenu6
        '
        Me.panelMenu6.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenu6.Controls.Add(Me.btnMenu6)
        Me.panelMenu6.Location = New System.Drawing.Point(1, 373)
        Me.panelMenu6.Name = "panelMenu6"
        Me.panelMenu6.Size = New System.Drawing.Size(177, 39)
        Me.panelMenu6.TabIndex = 5
        '
        'btnMenu6
        '
        Me.btnMenu6.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenu6.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenu6.FlatAppearance.BorderSize = 0
        Me.btnMenu6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenu6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenu6.ForeColor = System.Drawing.Color.White
        Me.btnMenu6.Location = New System.Drawing.Point(5, 0)
        Me.btnMenu6.Name = "btnMenu6"
        Me.btnMenu6.Size = New System.Drawing.Size(172, 39)
        Me.btnMenu6.TabIndex = 0
        Me.btnMenu6.Text = "-"
        Me.btnMenu6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenu6.UseVisualStyleBackColor = False
        '
        'panelMenu5
        '
        Me.panelMenu5.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenu5.Controls.Add(Me.btnMenu5)
        Me.panelMenu5.Location = New System.Drawing.Point(1, 333)
        Me.panelMenu5.Name = "panelMenu5"
        Me.panelMenu5.Size = New System.Drawing.Size(177, 39)
        Me.panelMenu5.TabIndex = 4
        '
        'btnMenu5
        '
        Me.btnMenu5.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenu5.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenu5.FlatAppearance.BorderSize = 0
        Me.btnMenu5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenu5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenu5.ForeColor = System.Drawing.Color.White
        Me.btnMenu5.Location = New System.Drawing.Point(5, 0)
        Me.btnMenu5.Name = "btnMenu5"
        Me.btnMenu5.Size = New System.Drawing.Size(172, 39)
        Me.btnMenu5.TabIndex = 0
        Me.btnMenu5.Text = "-"
        Me.btnMenu5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenu5.UseVisualStyleBackColor = False
        '
        'panelMenu4
        '
        Me.panelMenu4.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenu4.Controls.Add(Me.btnMenu4)
        Me.panelMenu4.Location = New System.Drawing.Point(1, 293)
        Me.panelMenu4.Name = "panelMenu4"
        Me.panelMenu4.Size = New System.Drawing.Size(177, 39)
        Me.panelMenu4.TabIndex = 3
        '
        'btnMenu4
        '
        Me.btnMenu4.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenu4.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenu4.FlatAppearance.BorderSize = 0
        Me.btnMenu4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenu4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenu4.ForeColor = System.Drawing.Color.White
        Me.btnMenu4.Location = New System.Drawing.Point(5, 0)
        Me.btnMenu4.Name = "btnMenu4"
        Me.btnMenu4.Size = New System.Drawing.Size(172, 39)
        Me.btnMenu4.TabIndex = 0
        Me.btnMenu4.Text = "-"
        Me.btnMenu4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenu4.UseVisualStyleBackColor = False
        '
        'panelMenu3
        '
        Me.panelMenu3.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenu3.Controls.Add(Me.btnMenu3)
        Me.panelMenu3.Location = New System.Drawing.Point(1, 253)
        Me.panelMenu3.Name = "panelMenu3"
        Me.panelMenu3.Size = New System.Drawing.Size(177, 39)
        Me.panelMenu3.TabIndex = 2
        '
        'btnMenu3
        '
        Me.btnMenu3.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenu3.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenu3.FlatAppearance.BorderSize = 0
        Me.btnMenu3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenu3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenu3.ForeColor = System.Drawing.Color.White
        Me.btnMenu3.Location = New System.Drawing.Point(5, 0)
        Me.btnMenu3.Name = "btnMenu3"
        Me.btnMenu3.Size = New System.Drawing.Size(172, 39)
        Me.btnMenu3.TabIndex = 0
        Me.btnMenu3.Text = "-"
        Me.btnMenu3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenu3.UseVisualStyleBackColor = False
        '
        'panelMenu2
        '
        Me.panelMenu2.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenu2.Controls.Add(Me.btnMenu2)
        Me.panelMenu2.Location = New System.Drawing.Point(1, 213)
        Me.panelMenu2.Name = "panelMenu2"
        Me.panelMenu2.Size = New System.Drawing.Size(177, 39)
        Me.panelMenu2.TabIndex = 1
        '
        'btnMenu2
        '
        Me.btnMenu2.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenu2.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenu2.FlatAppearance.BorderSize = 0
        Me.btnMenu2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenu2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenu2.ForeColor = System.Drawing.Color.White
        Me.btnMenu2.Location = New System.Drawing.Point(5, 0)
        Me.btnMenu2.Name = "btnMenu2"
        Me.btnMenu2.Size = New System.Drawing.Size(172, 39)
        Me.btnMenu2.TabIndex = 0
        Me.btnMenu2.Text = "-"
        Me.btnMenu2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenu2.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(36, 16)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(108, 108)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'panelMenu8
        '
        Me.panelMenu8.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenu8.Controls.Add(Me.btnMenu8)
        Me.panelMenu8.Location = New System.Drawing.Point(1, 453)
        Me.panelMenu8.Name = "panelMenu8"
        Me.panelMenu8.Size = New System.Drawing.Size(177, 39)
        Me.panelMenu8.TabIndex = 8
        '
        'btnMenu8
        '
        Me.btnMenu8.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenu8.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenu8.FlatAppearance.BorderSize = 0
        Me.btnMenu8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenu8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenu8.ForeColor = System.Drawing.Color.White
        Me.btnMenu8.Location = New System.Drawing.Point(5, 0)
        Me.btnMenu8.Name = "btnMenu8"
        Me.btnMenu8.Size = New System.Drawing.Size(172, 39)
        Me.btnMenu8.TabIndex = 0
        Me.btnMenu8.Text = "-"
        Me.btnMenu8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenu8.UseVisualStyleBackColor = False
        '
        'StSimpanPinjam
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(180, 543)
        Me.Controls.Add(Me.Panel2)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "StSimpanPinjam"
        Me.Text = "StSimpanPinjam"
        Me.Panel2.ResumeLayout(False)
        Me.panelMenu7.ResumeLayout(False)
        Me.panelMenu1.ResumeLayout(False)
        Me.panelMenu6.ResumeLayout(False)
        Me.panelMenu5.ResumeLayout(False)
        Me.panelMenu4.ResumeLayout(False)
        Me.panelMenu3.ResumeLayout(False)
        Me.panelMenu2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelMenu8.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents panelMenu7 As System.Windows.Forms.Panel
    Friend WithEvents btnMenu7 As System.Windows.Forms.Button
    Friend WithEvents txtMenuPanel As System.Windows.Forms.Label
    Friend WithEvents panelMenu1 As System.Windows.Forms.Panel
    Friend WithEvents btnMenu1 As System.Windows.Forms.Button
    Friend WithEvents panelMenu6 As System.Windows.Forms.Panel
    Friend WithEvents btnMenu6 As System.Windows.Forms.Button
    Friend WithEvents panelMenu5 As System.Windows.Forms.Panel
    Friend WithEvents btnMenu5 As System.Windows.Forms.Button
    Friend WithEvents panelMenu4 As System.Windows.Forms.Panel
    Friend WithEvents btnMenu4 As System.Windows.Forms.Button
    Friend WithEvents panelMenu3 As System.Windows.Forms.Panel
    Friend WithEvents btnMenu3 As System.Windows.Forms.Button
    Friend WithEvents panelMenu2 As System.Windows.Forms.Panel
    Friend WithEvents btnMenu2 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents panelMenu8 As System.Windows.Forms.Panel
    Friend WithEvents btnMenu8 As System.Windows.Forms.Button
End Class
