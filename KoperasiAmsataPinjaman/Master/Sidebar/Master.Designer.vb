﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Master
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnMenuSimapanWajib = New System.Windows.Forms.Button()
        Me.panelMenuBarang = New System.Windows.Forms.Panel()
        Me.btnMenuBarang = New System.Windows.Forms.Button()
        Me.panelMenuSHU = New System.Windows.Forms.Panel()
        Me.btnMenuSHU = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PanelMenuAll = New System.Windows.Forms.Panel()
        Me.panelMenuLaporan = New System.Windows.Forms.Panel()
        Me.btnMenuLaporan = New System.Windows.Forms.Button()
        Me.panelMenuAngsuran = New System.Windows.Forms.Panel()
        Me.btnMenuAngsuran = New System.Windows.Forms.Button()
        Me.panelMenuPinjaman = New System.Windows.Forms.Panel()
        Me.btnMenuPinjaman = New System.Windows.Forms.Button()
        Me.panelMenuPengajuanPinjaman = New System.Windows.Forms.Panel()
        Me.btnMenuPengajuanPinjaman = New System.Windows.Forms.Button()
        Me.panelMenuSimpananSukarela = New System.Windows.Forms.Panel()
        Me.btnMenuSimpananSukarela = New System.Windows.Forms.Button()
        Me.panelMenuAkutansi = New System.Windows.Forms.Panel()
        Me.btnMenuAkutansi = New System.Windows.Forms.Button()
        Me.panelMenuBayarPiutang = New System.Windows.Forms.Panel()
        Me.btnMenuBayarPiutang = New System.Windows.Forms.Button()
        Me.panelMenuPenjualan = New System.Windows.Forms.Panel()
        Me.btnMenuPenjualan = New System.Windows.Forms.Button()
        Me.panelMenuBayarHut = New System.Windows.Forms.Panel()
        Me.btnMenuBayarHutang = New System.Windows.Forms.Button()
        Me.panelMenuReturPem = New System.Windows.Forms.Panel()
        Me.btnMenuReturPemb = New System.Windows.Forms.Button()
        Me.panelMenuPembelian = New System.Windows.Forms.Panel()
        Me.btnMenuPembelian = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.panelMenuSupplier = New System.Windows.Forms.Panel()
        Me.btnMenuSupplier = New System.Windows.Forms.Button()
        Me.panelMenuUser = New System.Windows.Forms.Panel()
        Me.btnMenuUser = New System.Windows.Forms.Button()
        Me.panelMenuFinancial = New System.Windows.Forms.Panel()
        Me.btnMenuFinancial = New System.Windows.Forms.Button()
        Me.panelMenuPenutupanAkun = New System.Windows.Forms.Panel()
        Me.btnMenuPenutupanAkun = New System.Windows.Forms.Button()
        Me.panelMenuSimpWajib = New System.Windows.Forms.Panel()
        Me.panelMenuSimpPokok = New System.Windows.Forms.Panel()
        Me.btnMenuSimpananPokok = New System.Windows.Forms.Button()
        Me.panelMenuAnggota = New System.Windows.Forms.Panel()
        Me.btnMenuAnggota = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.txtMenuPanel = New System.Windows.Forms.Label()
        Me.panelMenuBarang.SuspendLayout()
        Me.panelMenuSHU.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.PanelMenuAll.SuspendLayout()
        Me.panelMenuLaporan.SuspendLayout()
        Me.panelMenuAngsuran.SuspendLayout()
        Me.panelMenuPinjaman.SuspendLayout()
        Me.panelMenuPengajuanPinjaman.SuspendLayout()
        Me.panelMenuSimpananSukarela.SuspendLayout()
        Me.panelMenuAkutansi.SuspendLayout()
        Me.panelMenuBayarPiutang.SuspendLayout()
        Me.panelMenuPenjualan.SuspendLayout()
        Me.panelMenuBayarHut.SuspendLayout()
        Me.panelMenuReturPem.SuspendLayout()
        Me.panelMenuPembelian.SuspendLayout()
        Me.panelMenuSupplier.SuspendLayout()
        Me.panelMenuUser.SuspendLayout()
        Me.panelMenuFinancial.SuspendLayout()
        Me.panelMenuPenutupanAkun.SuspendLayout()
        Me.panelMenuSimpWajib.SuspendLayout()
        Me.panelMenuSimpPokok.SuspendLayout()
        Me.panelMenuAnggota.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnMenuSimapanWajib
        '
        Me.btnMenuSimapanWajib.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuSimapanWajib.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuSimapanWajib.FlatAppearance.BorderSize = 0
        Me.btnMenuSimapanWajib.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuSimapanWajib.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuSimapanWajib.ForeColor = System.Drawing.Color.White
        Me.btnMenuSimapanWajib.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuSimapanWajib.Name = "btnMenuSimapanWajib"
        Me.btnMenuSimapanWajib.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuSimapanWajib.TabIndex = 0
        Me.btnMenuSimapanWajib.Text = "Simpanan Wajib"
        Me.btnMenuSimapanWajib.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuSimapanWajib.UseVisualStyleBackColor = False
        '
        'panelMenuBarang
        '
        Me.panelMenuBarang.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuBarang.Controls.Add(Me.btnMenuBarang)
        Me.panelMenuBarang.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuBarang.Location = New System.Drawing.Point(0, 273)
        Me.panelMenuBarang.Name = "panelMenuBarang"
        Me.panelMenuBarang.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuBarang.TabIndex = 8
        '
        'btnMenuBarang
        '
        Me.btnMenuBarang.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuBarang.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuBarang.FlatAppearance.BorderSize = 0
        Me.btnMenuBarang.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuBarang.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuBarang.ForeColor = System.Drawing.Color.White
        Me.btnMenuBarang.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuBarang.Name = "btnMenuBarang"
        Me.btnMenuBarang.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuBarang.TabIndex = 0
        Me.btnMenuBarang.Text = "Barang"
        Me.btnMenuBarang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuBarang.UseVisualStyleBackColor = False
        '
        'panelMenuSHU
        '
        Me.panelMenuSHU.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuSHU.Controls.Add(Me.btnMenuSHU)
        Me.panelMenuSHU.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuSHU.Location = New System.Drawing.Point(0, 156)
        Me.panelMenuSHU.Name = "panelMenuSHU"
        Me.panelMenuSHU.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuSHU.TabIndex = 5
        '
        'btnMenuSHU
        '
        Me.btnMenuSHU.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuSHU.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuSHU.FlatAppearance.BorderSize = 0
        Me.btnMenuSHU.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuSHU.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuSHU.ForeColor = System.Drawing.Color.White
        Me.btnMenuSHU.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuSHU.Name = "btnMenuSHU"
        Me.btnMenuSHU.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuSHU.TabIndex = 0
        Me.btnMenuSHU.Text = "SHU"
        Me.btnMenuSHU.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuSHU.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel2.Controls.Add(Me.PanelMenuAll)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.ForeColor = System.Drawing.Color.White
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(180, 543)
        Me.Panel2.TabIndex = 6
        '
        'PanelMenuAll
        '
        Me.PanelMenuAll.AutoScroll = True
        Me.PanelMenuAll.AutoSize = True
        Me.PanelMenuAll.Controls.Add(Me.panelMenuLaporan)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuAngsuran)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuPinjaman)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuPengajuanPinjaman)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuSimpananSukarela)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuAkutansi)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuBayarPiutang)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuPenjualan)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuBayarHut)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuReturPem)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuPembelian)
        Me.PanelMenuAll.Controls.Add(Me.Panel1)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuSupplier)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuBarang)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuUser)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuFinancial)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuSHU)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuPenutupanAkun)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuSimpWajib)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuSimpPokok)
        Me.PanelMenuAll.Controls.Add(Me.panelMenuAnggota)
        Me.PanelMenuAll.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PanelMenuAll.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelMenuAll.Location = New System.Drawing.Point(0, 167)
        Me.PanelMenuAll.Name = "PanelMenuAll"
        Me.PanelMenuAll.Size = New System.Drawing.Size(180, 376)
        Me.PanelMenuAll.TabIndex = 10
        '
        'panelMenuLaporan
        '
        Me.panelMenuLaporan.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuLaporan.Controls.Add(Me.btnMenuLaporan)
        Me.panelMenuLaporan.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuLaporan.Location = New System.Drawing.Point(0, 741)
        Me.panelMenuLaporan.Name = "panelMenuLaporan"
        Me.panelMenuLaporan.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuLaporan.TabIndex = 14
        '
        'btnMenuLaporan
        '
        Me.btnMenuLaporan.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuLaporan.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuLaporan.FlatAppearance.BorderSize = 0
        Me.btnMenuLaporan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuLaporan.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuLaporan.ForeColor = System.Drawing.Color.White
        Me.btnMenuLaporan.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuLaporan.Name = "btnMenuLaporan"
        Me.btnMenuLaporan.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuLaporan.TabIndex = 0
        Me.btnMenuLaporan.Text = "Laporan"
        Me.btnMenuLaporan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuLaporan.UseVisualStyleBackColor = False
        '
        'panelMenuAngsuran
        '
        Me.panelMenuAngsuran.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuAngsuran.Controls.Add(Me.btnMenuAngsuran)
        Me.panelMenuAngsuran.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuAngsuran.Location = New System.Drawing.Point(0, 702)
        Me.panelMenuAngsuran.Name = "panelMenuAngsuran"
        Me.panelMenuAngsuran.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuAngsuran.TabIndex = 21
        '
        'btnMenuAngsuran
        '
        Me.btnMenuAngsuran.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuAngsuran.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuAngsuran.FlatAppearance.BorderSize = 0
        Me.btnMenuAngsuran.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuAngsuran.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuAngsuran.ForeColor = System.Drawing.Color.White
        Me.btnMenuAngsuran.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuAngsuran.Name = "btnMenuAngsuran"
        Me.btnMenuAngsuran.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuAngsuran.TabIndex = 0
        Me.btnMenuAngsuran.Text = "Angsuran"
        Me.btnMenuAngsuran.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuAngsuran.UseVisualStyleBackColor = False
        '
        'panelMenuPinjaman
        '
        Me.panelMenuPinjaman.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuPinjaman.Controls.Add(Me.btnMenuPinjaman)
        Me.panelMenuPinjaman.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuPinjaman.Location = New System.Drawing.Point(0, 663)
        Me.panelMenuPinjaman.Name = "panelMenuPinjaman"
        Me.panelMenuPinjaman.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuPinjaman.TabIndex = 20
        '
        'btnMenuPinjaman
        '
        Me.btnMenuPinjaman.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuPinjaman.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuPinjaman.FlatAppearance.BorderSize = 0
        Me.btnMenuPinjaman.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuPinjaman.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuPinjaman.ForeColor = System.Drawing.Color.White
        Me.btnMenuPinjaman.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuPinjaman.Name = "btnMenuPinjaman"
        Me.btnMenuPinjaman.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuPinjaman.TabIndex = 0
        Me.btnMenuPinjaman.Text = "Pinjaman"
        Me.btnMenuPinjaman.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuPinjaman.UseVisualStyleBackColor = False
        '
        'panelMenuPengajuanPinjaman
        '
        Me.panelMenuPengajuanPinjaman.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuPengajuanPinjaman.Controls.Add(Me.btnMenuPengajuanPinjaman)
        Me.panelMenuPengajuanPinjaman.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuPengajuanPinjaman.Location = New System.Drawing.Point(0, 624)
        Me.panelMenuPengajuanPinjaman.Name = "panelMenuPengajuanPinjaman"
        Me.panelMenuPengajuanPinjaman.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuPengajuanPinjaman.TabIndex = 19
        '
        'btnMenuPengajuanPinjaman
        '
        Me.btnMenuPengajuanPinjaman.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuPengajuanPinjaman.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuPengajuanPinjaman.FlatAppearance.BorderSize = 0
        Me.btnMenuPengajuanPinjaman.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuPengajuanPinjaman.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuPengajuanPinjaman.ForeColor = System.Drawing.Color.White
        Me.btnMenuPengajuanPinjaman.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuPengajuanPinjaman.Name = "btnMenuPengajuanPinjaman"
        Me.btnMenuPengajuanPinjaman.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuPengajuanPinjaman.TabIndex = 0
        Me.btnMenuPengajuanPinjaman.Text = "Pengajuan Pinjaman"
        Me.btnMenuPengajuanPinjaman.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuPengajuanPinjaman.UseVisualStyleBackColor = False
        '
        'panelMenuSimpananSukarela
        '
        Me.panelMenuSimpananSukarela.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuSimpananSukarela.Controls.Add(Me.btnMenuSimpananSukarela)
        Me.panelMenuSimpananSukarela.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuSimpananSukarela.Location = New System.Drawing.Point(0, 585)
        Me.panelMenuSimpananSukarela.Name = "panelMenuSimpananSukarela"
        Me.panelMenuSimpananSukarela.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuSimpananSukarela.TabIndex = 18
        '
        'btnMenuSimpananSukarela
        '
        Me.btnMenuSimpananSukarela.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuSimpananSukarela.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuSimpananSukarela.FlatAppearance.BorderSize = 0
        Me.btnMenuSimpananSukarela.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuSimpananSukarela.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuSimpananSukarela.ForeColor = System.Drawing.Color.White
        Me.btnMenuSimpananSukarela.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuSimpananSukarela.Name = "btnMenuSimpananSukarela"
        Me.btnMenuSimpananSukarela.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuSimpananSukarela.TabIndex = 0
        Me.btnMenuSimpananSukarela.Text = "SimpananSukarela"
        Me.btnMenuSimpananSukarela.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuSimpananSukarela.UseVisualStyleBackColor = False
        '
        'panelMenuAkutansi
        '
        Me.panelMenuAkutansi.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuAkutansi.Controls.Add(Me.btnMenuAkutansi)
        Me.panelMenuAkutansi.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuAkutansi.Location = New System.Drawing.Point(0, 546)
        Me.panelMenuAkutansi.Name = "panelMenuAkutansi"
        Me.panelMenuAkutansi.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuAkutansi.TabIndex = 17
        '
        'btnMenuAkutansi
        '
        Me.btnMenuAkutansi.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuAkutansi.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuAkutansi.FlatAppearance.BorderSize = 0
        Me.btnMenuAkutansi.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuAkutansi.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuAkutansi.ForeColor = System.Drawing.Color.White
        Me.btnMenuAkutansi.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuAkutansi.Name = "btnMenuAkutansi"
        Me.btnMenuAkutansi.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuAkutansi.TabIndex = 0
        Me.btnMenuAkutansi.Text = "Akun"
        Me.btnMenuAkutansi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuAkutansi.UseVisualStyleBackColor = False
        '
        'panelMenuBayarPiutang
        '
        Me.panelMenuBayarPiutang.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuBayarPiutang.Controls.Add(Me.btnMenuBayarPiutang)
        Me.panelMenuBayarPiutang.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuBayarPiutang.Location = New System.Drawing.Point(0, 507)
        Me.panelMenuBayarPiutang.Name = "panelMenuBayarPiutang"
        Me.panelMenuBayarPiutang.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuBayarPiutang.TabIndex = 16
        '
        'btnMenuBayarPiutang
        '
        Me.btnMenuBayarPiutang.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuBayarPiutang.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuBayarPiutang.FlatAppearance.BorderSize = 0
        Me.btnMenuBayarPiutang.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuBayarPiutang.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuBayarPiutang.ForeColor = System.Drawing.Color.White
        Me.btnMenuBayarPiutang.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuBayarPiutang.Name = "btnMenuBayarPiutang"
        Me.btnMenuBayarPiutang.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuBayarPiutang.TabIndex = 0
        Me.btnMenuBayarPiutang.Text = "Bayar Piutang"
        Me.btnMenuBayarPiutang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuBayarPiutang.UseVisualStyleBackColor = False
        '
        'panelMenuPenjualan
        '
        Me.panelMenuPenjualan.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuPenjualan.Controls.Add(Me.btnMenuPenjualan)
        Me.panelMenuPenjualan.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuPenjualan.Location = New System.Drawing.Point(0, 468)
        Me.panelMenuPenjualan.Name = "panelMenuPenjualan"
        Me.panelMenuPenjualan.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuPenjualan.TabIndex = 15
        '
        'btnMenuPenjualan
        '
        Me.btnMenuPenjualan.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuPenjualan.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuPenjualan.FlatAppearance.BorderSize = 0
        Me.btnMenuPenjualan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuPenjualan.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuPenjualan.ForeColor = System.Drawing.Color.White
        Me.btnMenuPenjualan.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuPenjualan.Name = "btnMenuPenjualan"
        Me.btnMenuPenjualan.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuPenjualan.TabIndex = 0
        Me.btnMenuPenjualan.Text = "Penjualan"
        Me.btnMenuPenjualan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuPenjualan.UseVisualStyleBackColor = False
        '
        'panelMenuBayarHut
        '
        Me.panelMenuBayarHut.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuBayarHut.Controls.Add(Me.btnMenuBayarHutang)
        Me.panelMenuBayarHut.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuBayarHut.Location = New System.Drawing.Point(0, 429)
        Me.panelMenuBayarHut.Name = "panelMenuBayarHut"
        Me.panelMenuBayarHut.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuBayarHut.TabIndex = 13
        '
        'btnMenuBayarHutang
        '
        Me.btnMenuBayarHutang.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuBayarHutang.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuBayarHutang.FlatAppearance.BorderSize = 0
        Me.btnMenuBayarHutang.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuBayarHutang.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuBayarHutang.ForeColor = System.Drawing.Color.White
        Me.btnMenuBayarHutang.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuBayarHutang.Name = "btnMenuBayarHutang"
        Me.btnMenuBayarHutang.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuBayarHutang.TabIndex = 0
        Me.btnMenuBayarHutang.Text = "Bayar Hutang"
        Me.btnMenuBayarHutang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuBayarHutang.UseVisualStyleBackColor = False
        '
        'panelMenuReturPem
        '
        Me.panelMenuReturPem.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuReturPem.Controls.Add(Me.btnMenuReturPemb)
        Me.panelMenuReturPem.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuReturPem.Location = New System.Drawing.Point(0, 390)
        Me.panelMenuReturPem.Name = "panelMenuReturPem"
        Me.panelMenuReturPem.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuReturPem.TabIndex = 12
        '
        'btnMenuReturPemb
        '
        Me.btnMenuReturPemb.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuReturPemb.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuReturPemb.FlatAppearance.BorderSize = 0
        Me.btnMenuReturPemb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuReturPemb.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuReturPemb.ForeColor = System.Drawing.Color.White
        Me.btnMenuReturPemb.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuReturPemb.Name = "btnMenuReturPemb"
        Me.btnMenuReturPemb.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuReturPemb.TabIndex = 0
        Me.btnMenuReturPemb.Text = "Retur Pembelian"
        Me.btnMenuReturPemb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuReturPemb.UseVisualStyleBackColor = False
        '
        'panelMenuPembelian
        '
        Me.panelMenuPembelian.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuPembelian.Controls.Add(Me.btnMenuPembelian)
        Me.panelMenuPembelian.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuPembelian.Location = New System.Drawing.Point(0, 351)
        Me.panelMenuPembelian.Name = "panelMenuPembelian"
        Me.panelMenuPembelian.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuPembelian.TabIndex = 11
        '
        'btnMenuPembelian
        '
        Me.btnMenuPembelian.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuPembelian.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuPembelian.FlatAppearance.BorderSize = 0
        Me.btnMenuPembelian.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuPembelian.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuPembelian.ForeColor = System.Drawing.Color.White
        Me.btnMenuPembelian.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuPembelian.Name = "btnMenuPembelian"
        Me.btnMenuPembelian.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuPembelian.TabIndex = 0
        Me.btnMenuPembelian.Text = "Pembelian"
        Me.btnMenuPembelian.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuPembelian.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 780)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(163, 32)
        Me.Panel1.TabIndex = 10
        '
        'panelMenuSupplier
        '
        Me.panelMenuSupplier.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuSupplier.Controls.Add(Me.btnMenuSupplier)
        Me.panelMenuSupplier.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuSupplier.Location = New System.Drawing.Point(0, 312)
        Me.panelMenuSupplier.Name = "panelMenuSupplier"
        Me.panelMenuSupplier.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuSupplier.TabIndex = 9
        '
        'btnMenuSupplier
        '
        Me.btnMenuSupplier.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuSupplier.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuSupplier.FlatAppearance.BorderSize = 0
        Me.btnMenuSupplier.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuSupplier.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuSupplier.ForeColor = System.Drawing.Color.White
        Me.btnMenuSupplier.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuSupplier.Name = "btnMenuSupplier"
        Me.btnMenuSupplier.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuSupplier.TabIndex = 0
        Me.btnMenuSupplier.Text = "Supplier"
        Me.btnMenuSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuSupplier.UseVisualStyleBackColor = False
        '
        'panelMenuUser
        '
        Me.panelMenuUser.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuUser.Controls.Add(Me.btnMenuUser)
        Me.panelMenuUser.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuUser.Location = New System.Drawing.Point(0, 234)
        Me.panelMenuUser.Name = "panelMenuUser"
        Me.panelMenuUser.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuUser.TabIndex = 7
        '
        'btnMenuUser
        '
        Me.btnMenuUser.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuUser.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuUser.FlatAppearance.BorderSize = 0
        Me.btnMenuUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuUser.ForeColor = System.Drawing.Color.White
        Me.btnMenuUser.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuUser.Name = "btnMenuUser"
        Me.btnMenuUser.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuUser.TabIndex = 0
        Me.btnMenuUser.Text = "User"
        Me.btnMenuUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuUser.UseVisualStyleBackColor = False
        '
        'panelMenuFinancial
        '
        Me.panelMenuFinancial.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuFinancial.Controls.Add(Me.btnMenuFinancial)
        Me.panelMenuFinancial.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuFinancial.Location = New System.Drawing.Point(0, 195)
        Me.panelMenuFinancial.Name = "panelMenuFinancial"
        Me.panelMenuFinancial.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuFinancial.TabIndex = 3
        '
        'btnMenuFinancial
        '
        Me.btnMenuFinancial.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuFinancial.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuFinancial.FlatAppearance.BorderSize = 0
        Me.btnMenuFinancial.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuFinancial.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuFinancial.ForeColor = System.Drawing.Color.White
        Me.btnMenuFinancial.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuFinancial.Name = "btnMenuFinancial"
        Me.btnMenuFinancial.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuFinancial.TabIndex = 0
        Me.btnMenuFinancial.Text = "Financial Statment"
        Me.btnMenuFinancial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuFinancial.UseVisualStyleBackColor = False
        '
        'panelMenuPenutupanAkun
        '
        Me.panelMenuPenutupanAkun.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuPenutupanAkun.Controls.Add(Me.btnMenuPenutupanAkun)
        Me.panelMenuPenutupanAkun.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuPenutupanAkun.Location = New System.Drawing.Point(0, 117)
        Me.panelMenuPenutupanAkun.Name = "panelMenuPenutupanAkun"
        Me.panelMenuPenutupanAkun.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuPenutupanAkun.TabIndex = 4
        '
        'btnMenuPenutupanAkun
        '
        Me.btnMenuPenutupanAkun.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuPenutupanAkun.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuPenutupanAkun.FlatAppearance.BorderSize = 0
        Me.btnMenuPenutupanAkun.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuPenutupanAkun.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuPenutupanAkun.ForeColor = System.Drawing.Color.White
        Me.btnMenuPenutupanAkun.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuPenutupanAkun.Name = "btnMenuPenutupanAkun"
        Me.btnMenuPenutupanAkun.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuPenutupanAkun.TabIndex = 0
        Me.btnMenuPenutupanAkun.Text = "Penutupan Akun"
        Me.btnMenuPenutupanAkun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuPenutupanAkun.UseVisualStyleBackColor = False
        '
        'panelMenuSimpWajib
        '
        Me.panelMenuSimpWajib.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuSimpWajib.Controls.Add(Me.btnMenuSimapanWajib)
        Me.panelMenuSimpWajib.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuSimpWajib.Location = New System.Drawing.Point(0, 78)
        Me.panelMenuSimpWajib.Name = "panelMenuSimpWajib"
        Me.panelMenuSimpWajib.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuSimpWajib.TabIndex = 2
        '
        'panelMenuSimpPokok
        '
        Me.panelMenuSimpPokok.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuSimpPokok.Controls.Add(Me.btnMenuSimpananPokok)
        Me.panelMenuSimpPokok.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuSimpPokok.Location = New System.Drawing.Point(0, 39)
        Me.panelMenuSimpPokok.Name = "panelMenuSimpPokok"
        Me.panelMenuSimpPokok.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuSimpPokok.TabIndex = 1
        '
        'btnMenuSimpananPokok
        '
        Me.btnMenuSimpananPokok.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuSimpananPokok.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuSimpananPokok.FlatAppearance.BorderSize = 0
        Me.btnMenuSimpananPokok.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuSimpananPokok.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuSimpananPokok.ForeColor = System.Drawing.Color.White
        Me.btnMenuSimpananPokok.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuSimpananPokok.Name = "btnMenuSimpananPokok"
        Me.btnMenuSimpananPokok.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuSimpananPokok.TabIndex = 0
        Me.btnMenuSimpananPokok.Text = "Simpanan Pokok"
        Me.btnMenuSimpananPokok.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuSimpananPokok.UseVisualStyleBackColor = False
        '
        'panelMenuAnggota
        '
        Me.panelMenuAnggota.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.panelMenuAnggota.Controls.Add(Me.btnMenuAnggota)
        Me.panelMenuAnggota.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelMenuAnggota.Location = New System.Drawing.Point(0, 0)
        Me.panelMenuAnggota.Name = "panelMenuAnggota"
        Me.panelMenuAnggota.Size = New System.Drawing.Size(163, 39)
        Me.panelMenuAnggota.TabIndex = 2
        '
        'btnMenuAnggota
        '
        Me.btnMenuAnggota.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btnMenuAnggota.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnMenuAnggota.FlatAppearance.BorderSize = 0
        Me.btnMenuAnggota.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenuAnggota.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnMenuAnggota.ForeColor = System.Drawing.Color.White
        Me.btnMenuAnggota.Location = New System.Drawing.Point(-14, 0)
        Me.btnMenuAnggota.Name = "btnMenuAnggota"
        Me.btnMenuAnggota.Size = New System.Drawing.Size(177, 39)
        Me.btnMenuAnggota.TabIndex = 0
        Me.btnMenuAnggota.Text = "Anggota"
        Me.btnMenuAnggota.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMenuAnggota.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.PictureBox1)
        Me.Panel3.Controls.Add(Me.txtMenuPanel)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(180, 167)
        Me.Panel3.TabIndex = 9
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(36, 13)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(108, 108)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'txtMenuPanel
        '
        Me.txtMenuPanel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.txtMenuPanel.Location = New System.Drawing.Point(0, 128)
        Me.txtMenuPanel.Name = "txtMenuPanel"
        Me.txtMenuPanel.Size = New System.Drawing.Size(180, 31)
        Me.txtMenuPanel.TabIndex = 6
        Me.txtMenuPanel.Text = "-"
        Me.txtMenuPanel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Master
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(180, 543)
        Me.Controls.Add(Me.Panel2)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Master"
        Me.Text = "Administrasi"
        Me.panelMenuBarang.ResumeLayout(False)
        Me.panelMenuSHU.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.PanelMenuAll.ResumeLayout(False)
        Me.panelMenuLaporan.ResumeLayout(False)
        Me.panelMenuAngsuran.ResumeLayout(False)
        Me.panelMenuPinjaman.ResumeLayout(False)
        Me.panelMenuPengajuanPinjaman.ResumeLayout(False)
        Me.panelMenuSimpananSukarela.ResumeLayout(False)
        Me.panelMenuAkutansi.ResumeLayout(False)
        Me.panelMenuBayarPiutang.ResumeLayout(False)
        Me.panelMenuPenjualan.ResumeLayout(False)
        Me.panelMenuBayarHut.ResumeLayout(False)
        Me.panelMenuReturPem.ResumeLayout(False)
        Me.panelMenuPembelian.ResumeLayout(False)
        Me.panelMenuSupplier.ResumeLayout(False)
        Me.panelMenuUser.ResumeLayout(False)
        Me.panelMenuFinancial.ResumeLayout(False)
        Me.panelMenuPenutupanAkun.ResumeLayout(False)
        Me.panelMenuSimpWajib.ResumeLayout(False)
        Me.panelMenuSimpPokok.ResumeLayout(False)
        Me.panelMenuAnggota.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnMenuSimapanWajib As System.Windows.Forms.Button
    Friend WithEvents panelMenuBarang As System.Windows.Forms.Panel
    Friend WithEvents btnMenuBarang As System.Windows.Forms.Button
    Friend WithEvents panelMenuSHU As System.Windows.Forms.Panel
    Friend WithEvents btnMenuSHU As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents panelMenuUser As System.Windows.Forms.Panel
    Friend WithEvents btnMenuUser As System.Windows.Forms.Button
    Friend WithEvents txtMenuPanel As System.Windows.Forms.Label
    Friend WithEvents panelMenuAnggota As System.Windows.Forms.Panel
    Friend WithEvents btnMenuAnggota As System.Windows.Forms.Button
    Friend WithEvents panelMenuPenutupanAkun As System.Windows.Forms.Panel
    Friend WithEvents btnMenuPenutupanAkun As System.Windows.Forms.Button
    Friend WithEvents panelMenuFinancial As System.Windows.Forms.Panel
    Friend WithEvents btnMenuFinancial As System.Windows.Forms.Button
    Friend WithEvents panelMenuSimpWajib As System.Windows.Forms.Panel
    Friend WithEvents panelMenuSimpPokok As System.Windows.Forms.Panel
    Friend WithEvents btnMenuSimpananPokok As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PanelMenuAll As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents panelMenuSupplier As System.Windows.Forms.Panel
    Friend WithEvents btnMenuSupplier As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents panelMenuReturPem As System.Windows.Forms.Panel
    Friend WithEvents btnMenuReturPemb As System.Windows.Forms.Button
    Friend WithEvents panelMenuPembelian As System.Windows.Forms.Panel
    Friend WithEvents btnMenuPembelian As System.Windows.Forms.Button
    Friend WithEvents panelMenuBayarPiutang As System.Windows.Forms.Panel
    Friend WithEvents btnMenuBayarPiutang As System.Windows.Forms.Button
    Friend WithEvents panelMenuPenjualan As System.Windows.Forms.Panel
    Friend WithEvents btnMenuPenjualan As System.Windows.Forms.Button
    Friend WithEvents panelMenuLaporan As System.Windows.Forms.Panel
    Friend WithEvents btnMenuLaporan As System.Windows.Forms.Button
    Friend WithEvents panelMenuBayarHut As System.Windows.Forms.Panel
    Friend WithEvents btnMenuBayarHutang As System.Windows.Forms.Button
    Friend WithEvents panelMenuAkutansi As System.Windows.Forms.Panel
    Friend WithEvents btnMenuAkutansi As System.Windows.Forms.Button
    Friend WithEvents panelMenuSimpananSukarela As System.Windows.Forms.Panel
    Friend WithEvents btnMenuSimpananSukarela As System.Windows.Forms.Button
    Friend WithEvents panelMenuPengajuanPinjaman As System.Windows.Forms.Panel
    Friend WithEvents btnMenuPengajuanPinjaman As System.Windows.Forms.Button
    Friend WithEvents panelMenuAngsuran As System.Windows.Forms.Panel
    Friend WithEvents btnMenuAngsuran As System.Windows.Forms.Button
    Friend WithEvents panelMenuPinjaman As System.Windows.Forms.Panel
    Friend WithEvents btnMenuPinjaman As System.Windows.Forms.Button
End Class
