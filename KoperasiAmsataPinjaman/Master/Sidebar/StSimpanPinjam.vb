﻿Public Class StSimpanPinjam

    Private Sub StSimpanPinjam_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PanelMenuItem()
    End Sub

    Sub PanelMenuOff()
        panelMenu1.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu2.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu3.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu4.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu5.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu6.BackColor = Color.FromArgb(10, 110, 100)
        panelMenu7.BackColor = Color.FromArgb(10, 110, 100)
    End Sub

    Sub PanelMenuItem()
        Dim akses As String = FormLogin.authPengguna.Rows(0).Item("akses")
        txtMenuPanel.Text = akses
        If akses = "Administrasi" Then
            btnMenu1.Text = "Anggota"
            btnMenu2.Text = "Simpanan Pokok"
            btnMenu3.Text = "Simpanan Sukarela"
            btnMenu4.Text = "Simpanan Wajib"
            btnMenu2.Text = "Financial Statment"
            btnMenu2.Text = "Penutupan Akun"
        End If
        If akses = "Kepala" Then

        End If
        If akses = "Pengawas" Then

        End If
        If akses = "Staf Pembelian" Then

        End If
        If akses = "Staf Penjualan" Then

            btnMenu3.Text = "Simpanan Sukarela"
            btnMenu4.Text = "Simpanan Wajib"
            btnMenu5.Text = "Pengajuan Pinjaman"
            btnMenu6.Text = "Pinjaman"
            btnMenu7.Text = "Angsuran"
            btnMenu8.Text = "Rekap"
        End If
    End Sub

    Private Sub btnMenu1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenu1.Click
        PanelMenuOff()
        panelMenu1.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormAnggota.TopLevel = False
        FormAnggota.TopMost = True
        FormAnggota.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormAnggota)
        FormAnggota.Show()
    End Sub

    Private Sub btnMenu2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenu2.Click
        PanelMenuOff()
        panelMenu2.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormSimpananPokok.TopLevel = False
        FormSimpananPokok.TopMost = True
        FormSimpananPokok.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormSimpananPokok)
        FormSimpananPokok.Show()
    End Sub

    Private Sub btnMenu3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenu3.Click
        PanelMenuOff()
        panelMenu3.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormSimpsukarela.TopLevel = False
        FormSimpsukarela.TopMost = True
        FormSimpsukarela.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormSimpsukarela)
        FormSimpsukarela.Show()
    End Sub

    Private Sub btnMenu4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenu4.Click
        PanelMenuOff()
        panelMenu4.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormSimpananWajib.TopLevel = False
        FormSimpananWajib.TopMost = True
        FormSimpananWajib.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormSimpananWajib)
        FormSimpananWajib.Show()
    End Sub

    Private Sub btnMenu5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenu5.Click
        PanelMenuOff()
        panelMenu5.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        Formpengajuanpinjm.TopLevel = False
        Formpengajuanpinjm.TopMost = True
        Formpengajuanpinjm.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(Formpengajuanpinjm)
        Formpengajuanpinjm.Show()
    End Sub
    Private Sub btnMenu6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenu6.Click
        PanelMenuOff()
        panelMenu6.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormPinjaman.TopLevel = False
        FormPinjaman.TopMost = True
        FormPinjaman.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormPinjaman)
        FormPinjaman.Show()
    End Sub

    Private Sub btnMenu7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenu7.Click
        PanelMenuOff()
        panelMenu7.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormAngsuran.TopLevel = False
        FormAngsuran.TopMost = True
        FormAngsuran.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormAngsuran)
        FormAngsuran.Show()
    End Sub

    Private Sub btnMenu8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenu8.Click
        PanelMenuOff()
        panelMenu8.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormAkun.TopLevel = False
        FormAkun.TopMost = True
        FormAkun.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormAkun)
        FormAkun.Show()
    End Sub

    Private Sub Panel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel2.Paint

    End Sub
End Class