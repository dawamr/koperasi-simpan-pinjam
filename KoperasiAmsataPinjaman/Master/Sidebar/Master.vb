﻿Public Class Master
    Private Sub StSimpanPinjam_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PanelMenuItem()
    End Sub

    Sub PanelMenuOff()
        panelMenuAnggota.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuSimpPokok.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuSimpWajib.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuFinancial.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuPenutupanAkun.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuSHU.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuUser.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuAnggota.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuSimpPokok.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuSimpWajib.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuFinancial.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuPembelian.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuAkutansi.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuSHU.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuFinancial.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuUser.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuBarang.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuSupplier.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuPembelian.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuReturPem.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuBayarHut.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuLaporan.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuPenjualan.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuBayarPiutang.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuSimpananSukarela.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuPengajuanPinjaman.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuPinjaman.BackColor = Color.FromArgb(10, 110, 100)
        panelMenuAngsuran.BackColor = Color.FromArgb(10, 110, 100)
    End Sub

    Sub PanelMenuVisible(ByVal x As Boolean)
        panelMenuAnggota.Visible = x
        panelMenuSimpPokok.Visible = x
        panelMenuSimpWajib.Visible = x
        panelMenuFinancial.Visible = x
        panelMenuPenutupanAkun.Visible = x
        panelMenuSHU.Visible = x
        panelMenuUser.Visible = x
        panelMenuAnggota.Visible = x
        panelMenuSimpPokok.Visible = x
        panelMenuSimpWajib.Visible = x
        panelMenuFinancial.Visible = x
        panelMenuPembelian.Visible = x
        panelMenuAkutansi.Visible = x
        panelMenuSHU.Visible = x
        panelMenuFinancial.Visible = x
        panelMenuUser.Visible = x
        panelMenuBarang.Visible = x
        panelMenuSupplier.Visible = x
        panelMenuPembelian.Visible = x
        panelMenuReturPem.Visible = x
        panelMenuBayarHut.Visible = x
        panelMenuLaporan.Visible = x
        panelMenuPenjualan.Visible = x
        panelMenuBayarPiutang.Visible = x
        panelMenuSimpananSukarela.Visible = x
        panelMenuPengajuanPinjaman.Visible = x
        panelMenuPinjaman.Visible = x
        panelMenuAngsuran.Visible = x

    End Sub

    Sub PanelMenuItem()
        Dim akses As String = FormLogin.authPengguna.Rows(0).Item("akses")
        'Dim akses As String = "Administrasi"
        txtMenuPanel.Text = akses
        PanelMenuVisible(False)
        PanelMenuAll.AutoScroll = True
        PanelMenuOff()
        If akses = "Administrasi" Then
            panelMenuAnggota.Visible = True
            panelMenuSimpPokok.Visible = True
            panelMenuSimpWajib.Visible = True
            panelMenuFinancial.Visible = True
            panelMenuAkutansi.Visible = True
        End If
        If akses = "Kepala" Then
            panelMenuSHU.Visible = True
            panelMenuFinancial.Visible = True
        End If
        If akses = "Pengawas" Then
            panelMenuUser.Visible = True
        End If
        If akses = "Staf Simpan Pinjam" Then
            panelMenuSimpananSukarela.Visible = True
            panelMenuPengajuanPinjaman.Visible = True
            panelMenuPinjaman.Visible = True
            panelMenuAngsuran.Visible = True
            panelMenuLaporan.Visible = True
        End If
        If akses = "Staf Pembelian" Then
            panelMenuBarang.Visible = True
            panelMenuSupplier.Visible = True
            panelMenuPembelian.Visible = True
            panelMenuReturPem.Visible = True
            panelMenuBayarHut.Visible = True
            panelMenuLaporan.Visible = True
        End If
        If akses = "Staf Penjualan" Then
            panelMenuPenjualan.Visible = True
            panelMenuBayarPiutang.Visible = True
            panelMenuLaporan.Visible = True
        End If
    End Sub

    Private Sub btnMenuAnggota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PanelMenuOff()
        panelMenuAnggota.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormAnggota.TopLevel = False
        FormAnggota.TopMost = True
        FormAnggota.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormAnggota)
        FormAnggota.Show()
    End Sub

    Private Sub btnMenuSimpananPokok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PanelMenuOff()
        panelMenuSimpPokok.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormSimpananPokok.TopLevel = False
        FormSimpananPokok.TopMost = True
        FormSimpananPokok.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormSimpananPokok)
        FormSimpananPokok.Show()
    End Sub

    Private Sub btnMenuSimpananSukarela_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PanelMenuOff()
        panelMenuSimpananSukarela.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormSimpsukarela.TopLevel = False
        FormSimpsukarela.TopMost = True
        FormSimpsukarela.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormSimpsukarela)
        FormSimpsukarela.Show()
    End Sub
    Private Sub btnMenuSimapanWajib_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PanelMenuOff()
        panelMenuSimpWajib.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormSimpananWajib.TopLevel = False
        FormSimpananWajib.TopMost = True
        FormSimpananWajib.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormSimpananWajib)
        FormSimpananWajib.Show()
    End Sub
    Private Sub btnMenuPengajuanPinjaman_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PanelMenuOff()
        panelMenuPengajuanPinjaman.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        Formpengajuanpinjm.TopLevel = False
        Formpengajuanpinjm.TopMost = True
        Formpengajuanpinjm.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(Formpengajuanpinjm)
        Formpengajuanpinjm.Show()
    End Sub
    Private Sub btnMenuPinjaman_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuPinjaman.Click
        PanelMenuOff()
        panelMenuPinjaman.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormPinjaman.TopLevel = False
        FormPinjaman.TopMost = True
        FormPinjaman.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormPinjaman)
        FormPinjaman.Show()
    End Sub

    Private Sub btnMenuAngsuran_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuAngsuran.Click
        PanelMenuOff()
        panelMenuAngsuran.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormAngsuran.TopLevel = False
        FormAngsuran.TopMost = True
        FormAngsuran.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormAngsuran)
        FormAngsuran.Show()
    End Sub


    Private Sub btnMenuUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuUser.Click
        PanelMenuOff()
        panelMenuUser.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormUser2.TopLevel = False
        FormUser2.TopMost = True
        FormUser2.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormUser2)
        FormUser2.Show()
    End Sub

    Private Sub btnMenuSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuSupplier.Click
        PanelMenuOff()
        panelMenuSupplier.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormSupplier.TopLevel = False
        FormSupplier.TopMost = True
        FormSupplier.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormSupplier)
        FormSupplier.Show()
    End Sub

    Private Sub btnMenuBarang_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuBarang.Click
        PanelMenuOff()
        panelMenuBarang.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormBarang.TopLevel = False
        FormBarang.TopMost = True
        FormBarang.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormBarang)
        FormBarang.Show()
    End Sub

    Private Sub btnMenuPembelian_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuPembelian.Click
        PanelMenuOff()
        panelMenuPembelian.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormPembelian.TopLevel = False
        FormPembelian.TopMost = True
        FormPembelian.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormPembelian)
        FormPembelian.Show()
    End Sub

    Private Sub btnMenuReturPemb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuReturPemb.Click
        PanelMenuOff()
        panelMenuReturPem.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormReturPembelian.TopLevel = False
        FormReturPembelian.TopMost = True
        FormReturPembelian.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormReturPembelian)
        FormReturPembelian.Show()
    End Sub

    Private Sub btnMenuBayarHutang_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuBayarHutang.Click
        PanelMenuOff()
        panelMenuBayarHut.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormBayarHutang.TopLevel = False
        FormBayarHutang.TopMost = True
        FormBayarHutang.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormBayarHutang)
        FormBayarHutang.Show()
    End Sub

    Private Sub btnMenuPenjualan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuPenjualan.Click
        PanelMenuOff()
        panelMenuPenjualan.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        Form_Penjualan.TopLevel = False
        Form_Penjualan.TopMost = True
        Form_Penjualan.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(Form_Penjualan)
        Form_Penjualan.Show()
    End Sub

    Private Sub btnMenuBayarPiutang_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuBayarPiutang.Click
        PanelMenuOff()
        panelMenuBayarPiutang.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormBayarPiutang.TopLevel = False
        FormBayarPiutang.TopMost = True
        FormBayarPiutang.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormBayarPiutang)
        FormBayarPiutang.Show()
    End Sub

    Private Sub btnMenuSimpananSukarela_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuSimpananSukarela.Click
        PanelMenuOff()
        panelMenuSimpananSukarela.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormSimpsukarela.TopLevel = False
        FormSimpsukarela.TopMost = True
        FormSimpsukarela.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormSimpsukarela)
        FormSimpsukarela.Show()
    End Sub

    Private Sub btnMenuPengajuanPinjaman_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuPengajuanPinjaman.Click
        PanelMenuOff()
        panelMenuPengajuanPinjaman.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        Formpengajuanpinjm.TopLevel = False
        Formpengajuanpinjm.TopMost = True
        Formpengajuanpinjm.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(Formpengajuanpinjm)
        Formpengajuanpinjm.Show()
    End Sub

    Private Sub btnMenuAnggota_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuAnggota.Click
        PanelMenuOff()
        panelMenuAnggota.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormAnggota.TopLevel = False
        FormAnggota.TopMost = True
        FormAnggota.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormAnggota)
        FormAnggota.Show()
    End Sub

    Private Sub btnMenuSimpananPokok_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuSimpananPokok.Click
        PanelMenuOff()
        panelMenuSimpPokok.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormSimpananPokok.TopLevel = False
        FormSimpananPokok.TopMost = True
        FormSimpananPokok.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormSimpananPokok)
        FormSimpananPokok.Show()
    End Sub

    Private Sub btnMenuSimapanWajib_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuSimapanWajib.Click
        PanelMenuOff()
        panelMenuSimpWajib.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormSimpananWajib.TopLevel = False
        FormSimpananWajib.TopMost = True
        FormSimpananWajib.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormSimpananWajib)
        FormSimpananWajib.Show()
    End Sub

    Private Sub btnMenuPenutupanAkun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuPenutupanAkun.Click

    End Sub

    Private Sub btnMenuAkutansi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuAkutansi.Click
        PanelMenuOff()
        panelMenuAkutansi.BackColor = Color.FromArgb(0, 245, 215)
        FrmMaster.panelBody.Controls.Clear()
        FormAkun.TopLevel = False
        FormAkun.TopMost = True
        FormAkun.Dock = DockStyle.Fill
        FrmMaster.panelBody.Controls.Add(FormAkun)
        FormAkun.Show()
    End Sub

    Private Sub btnMenuSHU_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenuSHU.Click

    End Sub
End Class