﻿Public Class Formpengajuanpinjm
    Dim strsql, uuid As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Public uuidPengajuan As String = ""
    Private Sub Formpengajuanpinjm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tombol(True)
        aktif(False)
        refreshGrid()
        load_cbocari(tabel, Cbocari)
        Cbocari.SelectedIndex = 0

        cmdsimpan.Location = New Point(cmdedit.Location)
        cmdbatal.Location = New Point(cmdtambah.Location)
        cmdpilih.Location = New Point(cmdedit.Location)
        cmdkeluar.Location = New Point(cmdtambah.Location)

        txtUUIDanggota.Enabled = True
        txtIDpengajuan.Enabled = False
        txtidanggota.Enabled = False
        Txtnamaanggota.Enabled = False
        Txtjabatan.Enabled = False

        cmdedit.Enabled = False
    End Sub
    Private Sub clearData()
        txtIDpengajuan.Text = ""
        txtUUIDanggota.Text = ""
        txtidanggota.Text = ""
        Txtnamaanggota.Text = ""
        Txtjabatan.Text = ""
        txtNominal.Text = ""
        txtBungaAngsuran.Text = ""
        txtNominalAngsuran.Text = ""
        txtTotalAngsuran.Text = ""
        cmbStatusPengajuan.Text = ""
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        cmdtambah.Visible = x
        cmdSimpan.Visible = Not x
        cmdedit.Visible = x
        cmdbatal.Visible = Not x
    End Sub
    Private Sub aktif(ByVal x As Boolean)
        txtNominal.Enabled = x
    End Sub

    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("SELECT * FROM view_pengajuan_pinjaman ORDER BY IDpengajuan")
            dgpengajuan.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgpengajuan
                .Columns(index).HeaderText = "UUID"
                index = index + 1
                .Columns(index).HeaderText = "ID Pengajuan"
                index = index + 1
                .Columns(index).HeaderText = "UUID Anggota"
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Bayar"
                index = index + 1
                .Columns(index).HeaderText = "Nominal"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "ID Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Nama Anggota"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
            End With
            dgpengajuan.DefaultCellStyle.Font = New Font("Candara", 12)
            dgpengajuan.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgpengajuan.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
            Exit Sub
        End Try
    End Sub
    Private Sub baca_data()
        Dim index As Integer = 0
        If dgpengajuan.RowCount > 0 Then
            With dgpengajuan
                Dim baris As Integer = .CurrentRow.Index
                If baris >= dgpengajuan.RowCount - 1 Then
                    Exit Sub
                End If
                uuidPengajuan = CStr(.Item(index, baris).Value)
                'index += 1
                'txtUUIDanggota.Text = CStr(.Item(index, baris).Value)
                'index += 1
                'txtIDpengajuan.Text = CStr(.Item(index, baris).Value)
                'index += 1
                ''tanggal
                'index += 1
                'txtNominal.Text = CStr(.Item(index, baris).Value)
                'index += 1
                ''created
                'index += 1
                ''updated
                'index += 1
                'txtidanggota.Text = CStr(.Item(index, baris).Value)
                'index += 1
                'Txtnamaanggota.Text = CStr(.Item(index, baris).Value)
                'index += 1
                'Txtjabatan.Text = CStr(.Item(index, baris).Value)
                'dst
            End With
        End If
    End Sub
    Friend Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from view_pengajuan_pinjaman where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by IDpengajuan")
            dgpengajuan.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgpengajuan
                .Columns(index).HeaderText = "UUID"
                index = index + 1
                .Columns(index).HeaderText = "ID Pengajuan"
                index = index + 1
                .Columns(index).HeaderText = "UUID Anggota"
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Bayar"
                index = index + 1
                .Columns(index).HeaderText = "Nominal"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "ID Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Nama Anggota"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
            End With
            dgpengajuan.DefaultCellStyle.Font = New Font("Candara", 12)
            dgpengajuan.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgpengajuan.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub

    Private Sub cmdtambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdtambah.Click
        clearData()
        txtIDpengajuan.Text = getCode("kp_pengajuan_pinjaman", "PP")
        tombol(False)
        aktif(True)
        pil = 1 'insert
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FormAnggota.Close()
        FormAnggota.StartPosition = FormStartPosition.CenterScreen
        FormAnggota.Show()
        FormAnggota.cmdkeluar.Visible = True
        FormAnggota.cmdpilih.Visible = True
        FormAnggota.cmdbatal.Visible = False
        FormAnggota.cmdtambah.Visible = False
        FormAnggota.cmdedit.Visible = False
        FormAnggota.cmdSimpan.Visible = False

    End Sub

    Private Sub cmdsimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsimpan.Click
        Dim msql, bungaAngsuran As String
        bungaAngsuran = "1.25"
        If pil = 1 Then                         'tambah
            msql = "INSERT INTO kp_pengajuan_pinjaman (uuid, IDPengajuan, UUIDanggota, tanggal, nominal, banyak_angsuran, bunga_angsuran, status) VALUES ('" & _
                UUID4() & "','" & _
                txtIDpengajuan.Text & "','" & _
                txtUUIDanggota.Text & "','" & _
                DateTime.Now.ToString("yyyy-MM-dd") & "','" & _
                txtNominal.Text & "', '" & _
                cmbBanyakAngsuran.SelectedItem.ToString & "', '" & _
                bungaAngsuran & "', '" & _
                cmbStatusPengajuan.SelectedItem.ToString & "')"

        Else                                    'koreksi
            If uuidPengajuan = "" Or uuidPengajuan = vbNullString Then
                MessageBox.Show("Belum memilih data!")
                Exit Sub
            End If
            Exit Sub
        End If
        If proses.ExecuteNonQuery(msql) = False Then
            Exit Sub
        End If
        refreshGrid()
        tombol(True)
        aktif(False)
        clearData()
        MsgBox("Berhasil disimpan...", vbInformation, "Info")
    End Sub

    Private Sub txtUUIDanggota_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUUIDanggota.TextChanged
        If txtUUIDanggota.Text.Length <= 5 Then
            Exit Sub
        End If
        tabel = proses.ExecuteQuery("SELECT IDanggota, nama, jabatan FROM ms_anggota where uuid = '" & txtUUIDanggota.Text & "'")
        If tabel.Rows.Count < 1 Then
            MsgBox("ID Anggota Tidak Ditemukan!")
        Else
            txtidanggota.Text = tabel.Rows(0).Item(0)
            Txtnamaanggota.Text = tabel.Rows(0).Item(1)
            Txtjabatan.Text = tabel.Rows(0).Item(2)
            cmbBanyakAngsuran.Enabled = True
            cmbStatusPengajuan.SelectedItem = "Menunggu"
            txtIDpengajuan.Text = getCode("kp_pengajuan_pinjaman", "PP")
            tombol(False)
            aktif(True)
            pil = 1 'insert
        End If
    End Sub

    Private Sub cmdbatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdbatal.Click
        clearData()
        tombol(True)
        aktif(False)
        txtUUIDanggota.Enabled = True
        txtIDpengajuan.Enabled = False
        txtidanggota.Enabled = False
        Txtnamaanggota.Enabled = False
        Txtjabatan.Enabled = False
    End Sub

    Private Sub cmbBanyakAngsuran_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBanyakAngsuran.SelectedIndexChanged
        HitungAngsuran()
    End Sub


    Private Sub HitungAngsuran()
        Dim bungaangsr, angsrblnpokok As Integer
        If cmbBanyakAngsuran.Text.Length > 0 And txtNominal.Text.Length > 0 Then
            angsrblnpokok = Convert.ToInt32(txtNominal.Text) / Convert.ToInt32(cmbBanyakAngsuran.SelectedItem.ToString)
            bungaangsr = Convert.ToInt32(txtNominal.Text) * (1.25 / 100)

            txtBungaAngsuran.Text = bungaangsr
            txtNominalAngsuran.Text = angsrblnpokok
            txtTotalAngsuran.Text = pembualatanNominal(angsrblnpokok + bungaangsr)
            'Dtpelunasan.Value = datelunas
        Else
            MsgBox("Nominal & Banyak Angsuran Wajib!")
            Exit Sub
        End If

    End Sub

    Private Sub txtNominal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNominal.TextChanged
        If txtNominal.Text.Length > 4 And cmbBanyakAngsuran.Text.Length > 0 Then
            HitungAngsuran()
        End If
    End Sub

    Private Sub dgpengajuan_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgpengajuan.CellContentClick
        baca_data()
    End Sub

    Private Sub cmdkeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdkeluar.Click
        Me.Close()
    End Sub

    Private Sub cmdpilih_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdpilih.Click
        If uuidPengajuan = "" Then
            MsgBox("Anda belum memilih pengajuan!")
            Exit Sub
        End If
        FormPinjaman.txtUUIDpengajuan.Text = uuidPengajuan
        Me.Close()
    End Sub

    Private Sub dgpengajuan_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgpengajuan.MouseClick
        baca_data()
    End Sub

    Private Sub Txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Txtcari.TextChanged
        If Txtcari.Text.Length < 1 Then
            Exit Sub
        End If
        refreshGridCari(Cbocari.SelectedItem.ToString, Txtcari.Text)
    End Sub
End Class