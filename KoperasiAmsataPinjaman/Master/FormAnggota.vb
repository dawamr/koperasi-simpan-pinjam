﻿Public Class FormAnggota
    Dim strsql, uuid As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Public uuidAnggota As String = ""
    Private Sub Formanggota_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        tombol(True)
        aktif(False)
        refreshGrid()
        load_cbocari(tabel, Cbocari)
        Cbocari.SelectedIndex = 0
        cmdSimpan.Location = New Point(cmdedit.Location)
        cmdbatal.Location = New Point(cmdtambah.Location)
        cmdpilih.Location = New Point(cmdedit.Location)
        cmdkeluar.Location = New Point(cmdtambah.Location)
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from ms_anggota order by IDanggota")
            Me.Dganggota.DataSource = tabel
            Dim index As Integer = 0
            With Me.Dganggota
                .Columns(index).HeaderText = "UUID"
                index = index + 1
                .Columns(index).HeaderText = "ID Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Nama Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Jabatan Anggota"
                index = index + 1
                .Columns(index).HeaderText = "No Telpon"
                index = index + 1
                .Columns(index).HeaderText = "Alamat Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Bergabung"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
            End With
            Dganggota.DefaultCellStyle.Font = New Font("Candara", 12)
            Dganggota.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            Dganggota.ReadOnly = True
            Dganggota.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from ms_anggota where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by IDanggota")
            Me.Dganggota.DataSource = tabel
            Dim index As Integer = 0
            With Me.Dganggota
                .Columns(index).HeaderText = "UUID"
                index = index + 1
                .Columns(index).HeaderText = "ID Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Nama Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Jabatan Anggota"
                index = index + 1
                .Columns(index).HeaderText = "No Telpon"
                index = index + 1
                .Columns(index).HeaderText = "Alamat Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Bergabung"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
            End With
            Dganggota.DefaultCellStyle.Font = New Font("Candara", 12)
            Dganggota.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            Dganggota.ReadOnly = True
            Dganggota.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub baca_data()
        Dim index As Integer = 0
        If Dganggota.RowCount > 0 Then
            With Dganggota
                Dim baris As Integer = .CurrentRow.Index
                If baris >= Dganggota.RowCount - 1 Then
                    Exit Sub
                End If
                uuidAnggota = CStr(.Item(index, baris).Value)
                index += 1
                Txtidanggota.Text = CStr(.Item(index, baris).Value)
                index += 1
                Txtnamaanggota.Text = CStr(.Item(index, baris).Value)
                index += 1
                Txtjabatan.Text = CStr(.Item(index, baris).Value)
                index += 1
                Txtnotelp.Text = CStr(.Item(index, baris).Value)
                index += 1
                Txtalamat.Text = CStr(.Item(index, baris).Value)
                index += 1
                txtTanggalGabung.Text = DateTime.ParseExact(CStr(.Item(index, baris).Value), "dd/MM/yyyy", Globalization.CultureInfo.InvariantCulture)
                index += 1
            End With
        End If
    End Sub
    Private Sub clearData()
        Txtidanggota.Text = ""
        Txtnamaanggota.Text = ""
        Txtjabatan.Text = ""
        Txtalamat.Text = ""
        Txtnotelp.Text = ""
        txtTanggalGabung.Text = ""
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        cmdtambah.Visible = x
        cmdSimpan.Visible = Not x
        cmdedit.Visible = x
        cmdbatal.Visible = Not x
    End Sub
    Private Sub aktif(ByVal x As Boolean)
        Txtidanggota.Enabled = x
        Txtnamaanggota.Enabled = x
        Txtjabatan.Enabled = x
        Txtalamat.Enabled = x
        Txtnotelp.Enabled = x
    End Sub
    Private Sub cmdTambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdtambah.Click
        kosong()
        clearData()
        Txtidanggota.Text = getCode("ms_anggota", "A")
        tombol(False)
        aktif(True)
        pil = 1 'insert
    End Sub
    Private Sub kosong()
        Txtidanggota.Text = ""
        Txtnamaanggota.Text = ""
        Txtjabatan.Text = ""
        Txtalamat.Text = ""
        Txtnotelp.Text = ""
    End Sub
    Private Sub cmdBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdbatal.Click
        tombol(True)
        aktif(False)
    End Sub
    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        Dim msql As String
        If pil = 1 Then                         'tambah
            msql = "INSERT INTO ms_anggota (uuid, IDanggota, nama, jabatan, telp, alamat, tanggal_bergabung) VALUES ('" & _
                UUID4() & "','" & _
                Txtidanggota.Text & "','" & _
                Txtnamaanggota.Text & "','" & _
                Txtjabatan.Text & "','" & _
                Txtnotelp.Text & "','" & _
                Txtalamat.Text & "','" & _
                DateTime.Now.ToString("yyyy-MM-dd") & "')"

        Else                                    'koreksi
            If uuidAnggota = "" Or uuidAnggota = vbNullString Then
                MessageBox.Show("Belum memilih data!")
                Exit Sub
            End If
            msql = "UPDATE ms_anggota SET " _
                & "nama = '" & Txtnamaanggota.Text _
                & "', jabatan = '" & Txtjabatan.Text _
                & "', telp = '" & Txtnotelp.Text _
                & "', alamat = '" & Txtalamat.Text _
                & "' WHERE uuid = '" & uuidAnggota & "';"
        End If
        If proses.ExecuteNonQuery(msql) = False Then
            Exit Sub
        End If
        refreshGrid()
        tombol(True)
        aktif(False)
        kosong()
        MsgBox("Berhasil disimpan...", vbInformation, "Info")
    End Sub
    Private Sub dgpengajuan_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Dganggota.CellContentClick
        kosong()
        baca_data()
    End Sub
    Private Sub dgpengajuan_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles Dganggota.CellMouseClick
        kosong()
        baca_data()
    End Sub
    'Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdhapus.Click
    '    Dim psn As Long
    '    psn = MsgBox("Yakin menghapus data ini ", vbQuestion + vbYesNo, "Pesan")
    '    If psn = vbYes Then
    '        proses.ExecuteNonQuery("delete from anggota where id_anggota='" & _
    '                               Txtidanggota.Text & "'")
    '    End If
    '    refreshGrid()
    'End Sub
    Private Sub cmdedit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdedit.Click
        pil = 2
        aktif(True)
        tombol(False)
        Txtidanggota.Enabled = False
        Txtnamaanggota.Focus()
    End Sub
    Private Sub txtCari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Txtcari.TextChanged
        refreshGridCari(Cbocari.Text, Txtcari.Text)
    End Sub

    Private Sub cmdkeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdkeluar.Click
        Me.Close()
    End Sub

    Private Sub cmdpilih_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdpilih.Click
        If uuidAnggota = "" Then
            MsgBox("Anda belum memilih anggota!")
            Exit Sub
        End If
        FormSimpananPokok.txtUUIDanggota.Text = uuidAnggota
        FormSimpsukarela.txtUUIDanggota.Text = uuidAnggota
        FormSimpananWajib.txtUUIDanggota.Text = uuidAnggota
        Formpengajuanpinjm.txtUUIDanggota.Text = uuidAnggota
        Form_Penjualan.txtUUIDanggota.Text = uuidAnggota
        Me.Close()
    End Sub
End Class