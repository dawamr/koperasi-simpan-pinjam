﻿Public Class FormAngsuran
    Dim strsql, uuid As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Public uuidAngsuran, uuidPinjaman, uuidAnggota As String
    Private Sub clearData()
        txtIDangsuran.Text = ""
        txtUUIDpinjaman.Text = ""
        txtidanggota.Text = ""
        Txtnamaanggota.Text = ""
        Txtjabatan.Text = ""
        txtNominal.Text = ""
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        cmdtambah.Visible = x
        cmdSimpan.Visible = Not x
        cmdedit.Visible = x
        cmdbatal.Visible = Not x
    End Sub
    Private Sub aktif(ByVal x As Boolean)

    End Sub

    Private Sub FormSimpananPokok_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tombol(True)
        aktif(False)
        refreshGrid()
        load_cbocari(tabel, Cbocari)
        Cbocari.SelectedIndex = 0

        cmdsimpan.Location = New Point(cmdedit.Location)
        cmdbatal.Location = New Point(cmdtambah.Location)

        txtUUIDpinjaman.Enabled = True
        txtIDangsuran.Enabled = False
        txtidanggota.Enabled = False
        Txtnamaanggota.Enabled = False
        Txtjabatan.Enabled = False
        txtNominal.Enabled = False

        cmdedit.Enabled = False
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("SELECT * FROM view_angsuran ORDER BY IDangsuran")
            dgangsuran.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgangsuran
                .Columns(index).HeaderText = "UUID"
                index = index + 1
                .Columns(index).HeaderText = "UUID Pinjaman"
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "UUID Anggota"
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "ID Simpanan Pokok"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Angsuran"
                index = index + 1
                .Columns(index).HeaderText = "Nominal"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "ID Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Nama Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Jabatan"
                index = index + 1
                .Columns(index).HeaderText = "ID Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Nominal Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Banyak Angsuran Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Total Bayar Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Status Pinjaman"
            End With
            dgangsuran.DefaultCellStyle.Font = New Font("Candara", 12)
            dgangsuran.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgangsuran.ReadOnly = True
            dgangsuran.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub baca_data()
        Dim index As Integer = 0
        If dgangsuran.RowCount > 0 Then
            With dgangsuran
                Dim baris As Integer = .CurrentRow.Index
                If baris >= dgangsuran.RowCount - 1 Then
                    Exit Sub
                End If
                uuidAngsuran = CStr(.Item(index, baris).Value)
                index += 1
                txtUUIDpinjaman.Text = CStr(.Item(index, baris).Value)
                index += 1
                txtIDangsuran.Text = CStr(.Item(index, baris).Value)
                index += 1
                'tanggal
                index += 1
                txtNominal.Text = CStr(.Item(index, baris).Value)
                index += 1
                'created
                index += 1
                'updated
                index += 1
                txtidanggota.Text = CStr(.Item(index, baris).Value)
                index += 1
                Txtnamaanggota.Text = CStr(.Item(index, baris).Value)
                index += 1
                Txtjabatan.Text = CStr(.Item(index, baris).Value)
                'dst
            End With
        End If
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from view_angsuran where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by IDanggota")
            Me.dgangsuran.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgangsuran
                .Columns(index).HeaderText = "UUID"
                index = index + 1
                .Columns(index).HeaderText = "UUID Pinjaman"
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "UUID Anggota"
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "ID Simpanan Pokok"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Angsuran"
                index = index + 1
                .Columns(index).HeaderText = "Nominal"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "ID Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Nama Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Jabatan"
                index = index + 1
                .Columns(index).HeaderText = "ID Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Nominal Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Banyak Angsuran Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Total Bayar Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Status Pinjaman"
            End With
            dgangsuran.DefaultCellStyle.Font = New Font("Candara", 12)
            dgangsuran.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgangsuran.ReadOnly = True
            dgangsuran.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub

    Private Sub cmdtambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdtambah.Click
        clearData()
        txtIDangsuran.Text = getCode("kp_angsuran", "PA")
        txtNominal.Text = ConfigSimpananPokok()
        tombol(False)
        aktif(True)
        pil = 1 'insert
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FormPinjaman.Close()
        FormPinjaman.StartPosition = FormStartPosition.CenterScreen
        FormPinjaman.Show()
        FormPinjaman.cmdkeluar.Visible = True
        FormPinjaman.cmdpilih.Visible = True
        FormPinjaman.cmdbatal.Visible = False
        FormPinjaman.cmdtambah.Visible = False
        FormPinjaman.cmdedit.Visible = False
        FormPinjaman.cmdsimpan.Visible = False
        FormPinjaman.Panel1.Visible = False
    End Sub

    Private Sub cmdsimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsimpan.Click
        Dim msql, msql2 As String
        If pil = 1 And txtSisaPembayaran.Text > 0 Then                         'tambah
            msql = "INSERT INTO kp_angsuran (uuid, UUIDpinjaman, UUIDanggota, IDangsuran, tanggal, nominal) VALUES ('" & _
                UUID4() & "','" & _
                txtUUIDpinjaman.Text & "','" & _
                uuidAnggota & "','" & _
                txtIDangsuran.Text & "','" & _
                DateTime.Now.ToString("yyyy-MM-dd") & "','" & _
                txtNominal.Text & "')"
            If proses.ExecuteNonQuery(msql) Then
                MsgBox("Berhasil disimpan...", vbInformation, "Info")
            End If
            If Convert.ToInt32(txtSisaPembayaran.Text) - Convert.ToInt32(txtNominal.Text) < 1 Then
                msql2 = "UPDATE kp_pinjaman SET " _
                & "status = 'Lunas' WHERE uuid = '" & txtUUIDpinjaman.Text & "';"
                If proses.ExecuteNonQuery(msql2) Then
                    MsgBox("Pinjaman Lunas!", vbInformation, "Info")
                End If
            End If
        ElseIf pil = 1 And txtSisaPembayaran.Text = 0 Then
            MsgBox("Pinjaman Sudah Lunas!", vbInformation, "Info")
        Else                                    'koreksi
            If uuidAngsuran = "" Or uuidAngsuran = vbNullString Then
                MessageBox.Show("Belum memilih data!")
                Exit Sub
            End If
            Exit Sub
        End If
        
        refreshGrid()
        tombol(True)
        aktif(False)
        clearData()

    End Sub

    Private Sub txtUUIDpinjaman_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUUIDpinjaman.TextChanged
        If txtUUIDpinjaman.Text.Length <= 5 Then
            Exit Sub
        End If
        tabel = proses.ExecuteQuery("SELECT uuid, UUIDanggota, IDpinjaman, tanggal, banyak_angsuran, nominal_angsuran, total_bayar, status, IDanggota, nama, jabatan FROM view_pinjaman where uuid = '" & txtUUIDpinjaman.Text & "'")
        If tabel.Rows.Count = 0 Then
            MsgBox("Data Pinajaman Tidak Ditemukan!")
        Else
            Dim index = 0
            Dim tabel2 As DataTable
            tabel2 = proses.ExecuteQuery("SELECT * FROM kp_angsuran where UUIDpinjaman = '" & txtUUIDpinjaman.Text & "'")
            uuidPinjaman = tabel.Rows(0).Item(index)
            index += 1
            uuidAnggota = tabel.Rows(0).Item(index)
            index += 1
            txtIDpinjaman.Text = tabel.Rows(0).Item(index)
            index += 1
            txtTanggalPinjaman.Text = tabel.Rows(0).Item(index)
            index += 1
            cmbBanyakAngsuran.Text = tabel.Rows(0).Item(index)
            index += 1
            txtNominal.Text = tabel.Rows(0).Item(index)
            index += 1
            txtTotalPembayaran.Text = tabel.Rows(0).Item(index)
            index += 1
            txtidanggota.Text = tabel.Rows(0).Item(index)
            index += 1
            Txtnamaanggota.Text = tabel.Rows(0).Item(index)
            index += 1
            Txtjabatan.Text = tabel.Rows(0).Item(index)
            index += 1
            txtAngsuranke.Text = tabel2.Rows.Count() + 1
            txtTotalSetor.Text = tabel2.Rows.Count() * Convert.ToInt32(txtNominal.Text)
            txtSisaPembayaran.Text = Convert.ToInt32(txtTotalPembayaran.Text) - Convert.ToInt32(txtTotalSetor.Text)
            txtIDangsuran.Text = getCode("kp_angsuran", "PA")
            tombol(False)
            aktif(True)
        End If
        pil = 1 'insert
    End Sub

    Private Sub cmdbatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdbatal.Click
        clearData()
        tombol(True)
        aktif(False)
        txtUUIDpinjaman.Enabled = True
        txtIDangsuran.Enabled = False
        txtidanggota.Enabled = False
        Txtnamaanggota.Enabled = False
        Txtjabatan.Enabled = False
        txtNominal.Enabled = False
    End Sub

    Private Sub txtNominal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNominal.TextChanged

    End Sub

    Private Sub Txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Txtcari.TextChanged
        refreshGridCari(Cbocari.Text, Txtcari.Text)
    End Sub
End Class