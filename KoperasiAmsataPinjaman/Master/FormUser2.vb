﻿Public Class FormUser2
    Dim strsql As String
    Dim proses As New ClsKoneksi
    Dim tabel, ms_akses As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Dim uuidPengguna, IDrole As String
    Private Sub FormUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        refreshGrid()
        baca_data(0)
        tombol(True)
        aktif(False)
        load_cbocari(tabel, cbcari)
        cbcari.SelectedIndex = 0
        cbohakakses.Items.Add("Administrasi")
        cbohakakses.Items.Add("Kepala")
        cbohakakses.Items.Add("Pengawas")
        cbohakakses.Items.Add("Staf Simpan Pinjam")
        cbohakakses.Items.Add("Staf Pembelian")
        cbohakakses.Items.Add("Staf Penjualan")
        ms_akses = getData("SELECT * FROM ms_akses")
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        btntambah.Enabled = x
        btnsimpan.Enabled = Not x
        btnkoreksi.Enabled = x
        btnhapus.Enabled = x
        btnbatal.Enabled = Not x
        btnhapus.Enabled = x
        btnawal.Enabled = x
        btnakhir.Enabled = x
        btnsebelum.Enabled = x
        btnsesudah.Enabled = x
    End Sub
    Private Sub aktif(ByVal x As Boolean)
        txtuserid.Enabled = x
        txtnama.Enabled = x
        txtusername.Enabled = x
        txtpassword.Enabled = x
        txtulangipassword.Enabled = x
        cbohakakses.Enabled = x
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from view_auth order by nama")
            Me.dguser.DataSource = tabel
            Dim index As Integer = 0
            With Me.dguser
                .Columns(index).HeaderText = "UUID"
                index += 1
                .Columns(index).HeaderText = "ID Pengguna"
                index += 1
                .Columns(index).HeaderText = "username"
                index += 1
                .Columns(index).HeaderText = "Nama"
                index += 1
                .Columns(index).HeaderText = "Password"
                index += 1
                .Columns(index).HeaderText = "ID Role"
                index += 1
                ' created
                .Columns(index).Visible = False
                index += 1
                'updated
                .Columns(index).Visible = False
                index += 1
                .Columns(index).HeaderText = "Akses"
            End With
            dguser.DefaultCellStyle.Font = New Font("Candara", 12)
            dguser.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dguser.ReadOnly = True
            dguser.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from view_auth where " & _
            Trim(namaKolom) & _
            " like ('%" & Trim(sCari) & "%') order by name")

            Dim index As Integer = 0
            With Me.dguser
                .Columns(index).HeaderText = "UUID"
                index += 1
                .Columns(index).HeaderText = "ID Pengguna"
                index += 1
                .Columns(index).HeaderText = "username"
                index += 1
                .Columns(index).HeaderText = "Nama"
                index += 1
                .Columns(index).HeaderText = "Password"
                index += 1
                .Columns(index).HeaderText = "ID Role"
                index += 1
                ' created
                index += 1
                'updated
                index += 1
                .Columns(index).HeaderText = "Akses"
            End With
            dguser.DefaultCellStyle.Font = New Font("Candara", 12)
            dguser.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dguser.ReadOnly = True
            dguser.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub baca_data(ByVal posisi As Integer)
        Dim index As Integer = 0
        If dguser.RowCount > 0 Then
            With dguser
                Dim baris As Integer = .CurrentRow.Index
                If baris >= dguser.RowCount - 1 Then
                    Exit Sub
                End If
                uuidPengguna = CStr(.Item(index, baris).Value)
                index += 1
                txtuserid.Text = CStr(.Item(index, baris).Value)
                index += 1
                txtusername.Text = CStr(.Item(index, baris).Value)
                index += 1
                txtnama.Text = CStr(.Item(index, baris).Value)
                index += 1
                txtpassword.Text = "*****"
                txtulangipassword.Text = "*****"
                index += 1
                IDrole = CStr(.Item(index, baris).Value)
                index += 1
                ' created
                index += 1
                'updated
                index += 1
                cbohakakses.SelectedItem = CStr(.Item(index, baris).Value)
            End With
        End If
    End Sub
    Private Sub kosong()
        txtuserid.Text = ""
        txtnama.Text = ""
        txtusername.Text = ""
        txtpassword.Text = ""
        txtulangipassword.Text = ""
        cbohakakses.Text = ""
    End Sub

    Private Sub btnsimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsimpan.Click
        Dim msql As String
        Dim idRole As String = ""
        Dim dataView As DataView = ms_akses.DefaultView

        If Not String.IsNullOrEmpty(cbohakakses.SelectedItem.ToString) Then
            dataView.RowFilter = "nama = '" & cbohakakses.SelectedItem.ToString & "'"
            idRole = dataView.Item(0).Row.ItemArray(0)
        End If

        If idRole = "" Then
            MsgBox("Belum memilih Hak Akses")
            Exit Sub
        End If

        If txtpassword.Text <> txtulangipassword.Text Then
            MsgBox("Password Tidak sama!")
            Exit Sub
        End If

        If pil = 1 Then 'tambah
            If checkExits("ms_pengguna", "username", txtusername.Text) = True Then
                MsgBox("Username sudah digunakan!")
                Exit Sub
            End If
            msql = "insert into ms_pengguna (uuid, IDpengguna, username, nama, password, IDrole) values('" _
            & UUID4() _
            & "','" & getCode("ms_pengguna", "U") _
            & "','" & txtusername.Text _
            & "','" & txtnama.Text _
            & "', md5('" & txtpassword.Text _
            & "'),'" & idRole & "')"
        Else 'koreksi
            msql = "UPDATE ms_pengguna SET username='" _
            & txtusername.Text _
            & "',nama='" & txtnama.Text _
            & "',IDrole='" & idRole _
            & "' where uuid='" & uuidPengguna & "'"
        End If
        If proses.ExecuteNonQuery(msql) = False Then
            Exit Sub
        End If
        tombol(True)
        aktif(False)
        kosong()
        refreshGrid()
        MsgBox("Saved Succsessfully", vbInformation, "Info")
    End Sub
    Private Sub dguser_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dguser.CellContentClick
        baca_data(1)
    End Sub
    Private Sub dguser_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dguser.CellMouseClick
        baca_data(1)
    End Sub

    Private Sub btnhapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnhapus.Click
        Dim psn As Long
        psn = MsgBox("Are you sure you want to delete this data?", vbQuestion +
       vbYesNo, "Question")
        If psn = vbYes Then
            proses.ExecuteNonQuery("DELETE FROM ms_pengguna WHERE uuid='" & _
            uuidPengguna & "'")
        End If
        refreshGrid()
    End Sub

    Private Sub btnkoreksi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnkoreksi.Click
        pil = 2
        aktif(True)
        txtpassword.Enabled = False
        txtulangipassword.Enabled = False
        tombol(False)
        txtuserid.Enabled = False
        txtnama.Focus()
    End Sub

    Private Sub txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        refreshGridCari(cbcari.Text, txtcari.Text)
    End Sub

    Private Sub btnawal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnawal.Click
        Dim nextRow As DataGridViewRow = dguser.Rows(0)
        ' Move the Glyph arrow to the next row
        dguser.CurrentCell = nextRow.Cells(0)
        ' Select the next row
        nextRow.Selected = True
        baca_data(1)
    End Sub

    Private Sub btnakhir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnakhir.Click
        Dim maxRowIndex As Integer = (Me.dguser.Rows.Count - 1 - 1)
        Dim nextRow As DataGridViewRow = dguser.Rows(maxRowIndex)
        ' Move the Glyph arrow to the next row
        dguser.CurrentCell = nextRow.Cells(0)
        ' Select the next row
        nextRow.Selected = True
        baca_data(1)
    End Sub

    Private Sub btnsesudah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsesudah.Click
        Dim maxRowIndex As Integer = (Me.dguser.Rows.Count - 1 - 1)
        Dim curDataGridViewRow As DataGridViewRow = dguser.CurrentRow
        Dim curRowIndex As Integer = curDataGridViewRow.Index
        If (curRowIndex >= maxRowIndex) Then
            MsgBox("No More Rows Left")
        Else
            Dim nextRow As DataGridViewRow = dguser.Rows(curRowIndex + 1)
            dguser.CurrentCell = nextRow.Cells(0)
            nextRow.Selected = True
        End If
        baca_data(1)
    End Sub

    Private Sub btnsebelum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsebelum.Click
        Dim maxRowIndex As Integer = (Me.dguser.Rows.Count - 1 - 1)
        Dim curDataGridViewRow As DataGridViewRow = dguser.CurrentRow
        Dim curRowIndex As Integer = curDataGridViewRow.Index
        If (curRowIndex < 1) Then
            MsgBox("First Record.....")
        Else
            Dim nextRow As DataGridViewRow = dguser.Rows(curRowIndex - 1)
            dguser.CurrentCell = nextRow.Cells(0)
            nextRow.Selected = True
        End If
        baca_data(1)
    End Sub

    Private Sub btnkeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnkeluar.Click
        Me.Close()
    End Sub
    Private Sub txtulangipassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtulangipassword.KeyPress
        If (e.KeyChar = Chr(13)) Then
            If txtulangipassword.Text = txtpassword.Text Then
                cbohakakses.Focus()
            Else
                MsgBox("Password is diferent!!!", vbCritical, "Warning")
                Exit Sub
            End If
        End If
    End Sub

    Private Sub btntambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btntambah.Click
        tombol(False)
        aktif(True)
        txtuserid.Enabled = False
        kosong()
        txtuserid.Text = getCode("ms_pengguna", "U")
        pil = 1 'insert
    End Sub

    Private Sub btnbatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbatal.Click
        tombol(True)
        aktif(False)
    End Sub
End Class