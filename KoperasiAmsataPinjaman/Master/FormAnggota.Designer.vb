﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAnggota
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.cmdkeluar = New System.Windows.Forms.Button()
        Me.cmdpilih = New System.Windows.Forms.Button()
        Me.Txtnotelp = New System.Windows.Forms.TextBox()
        Me.Txtalamat = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmdSimpan = New System.Windows.Forms.Button()
        Me.cmdedit = New System.Windows.Forms.Button()
        Me.cmdbatal = New System.Windows.Forms.Button()
        Me.cmdtambah = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtTanggalGabung = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Txtnamaanggota = New System.Windows.Forms.TextBox()
        Me.Txtidanggota = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Txtcari = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Cbocari = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Dganggota = New System.Windows.Forms.DataGridView()
        Me.Txtjabatan = New System.Windows.Forms.ComboBox()
        'Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.Dganggota, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(820, 172)
        Me.Panel1.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.cmdkeluar)
        Me.Panel3.Controls.Add(Me.cmdpilih)
        Me.Panel3.Controls.Add(Me.Txtnotelp)
        Me.Panel3.Controls.Add(Me.Txtalamat)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.cmdSimpan)
        Me.Panel3.Controls.Add(Me.cmdedit)
        Me.Panel3.Controls.Add(Me.cmdbatal)
        Me.Panel3.Controls.Add(Me.cmdtambah)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(422, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(398, 172)
        Me.Panel3.TabIndex = 51
        '
        'cmdkeluar
        '
        Me.cmdkeluar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdkeluar.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.logout
        Me.cmdkeluar.Location = New System.Drawing.Point(142, 98)
        Me.cmdkeluar.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdkeluar.Name = "cmdkeluar"
        Me.cmdkeluar.Size = New System.Drawing.Size(70, 29)
        Me.cmdkeluar.TabIndex = 58
        Me.cmdkeluar.UseVisualStyleBackColor = True
        Me.cmdkeluar.Visible = False
        '
        'cmdpilih
        '
        Me.cmdpilih.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdpilih.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.check_mark_16
        Me.cmdpilih.Location = New System.Drawing.Point(142, 132)
        Me.cmdpilih.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdpilih.Name = "cmdpilih"
        Me.cmdpilih.Size = New System.Drawing.Size(70, 29)
        Me.cmdpilih.TabIndex = 57
        Me.cmdpilih.UseVisualStyleBackColor = True
        Me.cmdpilih.Visible = False
        '
        'Txtnotelp
        '
        Me.Txtnotelp.Location = New System.Drawing.Point(188, 59)
        Me.Txtnotelp.Name = "Txtnotelp"
        Me.Txtnotelp.Size = New System.Drawing.Size(180, 23)
        Me.Txtnotelp.TabIndex = 56
        '
        'Txtalamat
        '
        Me.Txtalamat.Location = New System.Drawing.Point(188, 21)
        Me.Txtalamat.Name = "Txtalamat"
        Me.Txtalamat.Size = New System.Drawing.Size(180, 23)
        Me.Txtalamat.TabIndex = 55
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(52, 59)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 26)
        Me.Label6.TabIndex = 54
        Me.Label6.Text = "No Telpon"
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(52, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(120, 26)
        Me.Label7.TabIndex = 53
        Me.Label7.Text = "Alamat Anggota"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.simpan
        Me.cmdSimpan.Location = New System.Drawing.Point(298, 132)
        Me.cmdSimpan.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(70, 29)
        Me.cmdSimpan.TabIndex = 52
        Me.cmdSimpan.UseVisualStyleBackColor = True
        Me.cmdSimpan.Visible = False
        '
        'cmdedit
        '
        Me.cmdedit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdedit.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.edit
        Me.cmdedit.Location = New System.Drawing.Point(298, 98)
        Me.cmdedit.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdedit.Name = "cmdedit"
        Me.cmdedit.Size = New System.Drawing.Size(70, 29)
        Me.cmdedit.TabIndex = 51
        Me.cmdedit.UseVisualStyleBackColor = True
        '
        'cmdbatal
        '
        Me.cmdbatal.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.keluar
        Me.cmdbatal.Location = New System.Drawing.Point(220, 132)
        Me.cmdbatal.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdbatal.Name = "cmdbatal"
        Me.cmdbatal.Size = New System.Drawing.Size(70, 29)
        Me.cmdbatal.TabIndex = 50
        Me.cmdbatal.UseVisualStyleBackColor = True
        Me.cmdbatal.Visible = False
        '
        'cmdtambah
        '
        Me.cmdtambah.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdtambah.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.tambah
        Me.cmdtambah.Location = New System.Drawing.Point(220, 98)
        Me.cmdtambah.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdtambah.Name = "cmdtambah"
        Me.cmdtambah.Size = New System.Drawing.Size(70, 29)
        Me.cmdtambah.TabIndex = 49
        Me.cmdtambah.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Txtjabatan)
        Me.Panel4.Controls.Add(Me.txtTanggalGabung)
        Me.Panel4.Controls.Add(Me.Label8)
        Me.Panel4.Controls.Add(Me.Txtnamaanggota)
        Me.Panel4.Controls.Add(Me.Txtidanggota)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(398, 172)
        Me.Panel4.TabIndex = 50
        '
        'txtTanggalGabung
        '
        Me.txtTanggalGabung.Location = New System.Drawing.Point(158, 129)
        Me.txtTanggalGabung.Name = "txtTanggalGabung"
        Me.txtTanggalGabung.Size = New System.Drawing.Size(180, 23)
        Me.txtTanggalGabung.TabIndex = 60
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(23, 129)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 23)
        Me.Label8.TabIndex = 47
        Me.Label8.Text = "Bergabung"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Txtnamaanggota
        '
        Me.Txtnamaanggota.Location = New System.Drawing.Point(158, 55)
        Me.Txtnamaanggota.Name = "Txtnamaanggota"
        Me.Txtnamaanggota.Size = New System.Drawing.Size(180, 23)
        Me.Txtnamaanggota.TabIndex = 46
        '
        'Txtidanggota
        '
        Me.Txtidanggota.Location = New System.Drawing.Point(158, 20)
        Me.Txtidanggota.Name = "Txtidanggota"
        Me.Txtidanggota.Size = New System.Drawing.Size(180, 23)
        Me.Txtidanggota.TabIndex = 45
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(23, 93)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(120, 23)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Jabatan Anggota"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(23, 51)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 23)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "Nama Anggota"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(24, 21)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(120, 23)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "ID Anggota"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Panel7)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 172)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(820, 41)
        Me.Panel5.TabIndex = 28
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Txtcari)
        Me.Panel7.Controls.Add(Me.Label1)
        Me.Panel7.Controls.Add(Me.Cbocari)
        Me.Panel7.Controls.Add(Me.Label2)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel7.Location = New System.Drawing.Point(226, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(594, 41)
        Me.Panel7.TabIndex = 0
        '
        'Txtcari
        '
        Me.Txtcari.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Txtcari.Location = New System.Drawing.Point(388, 6)
        Me.Txtcari.Margin = New System.Windows.Forms.Padding(4)
        Me.Txtcari.Name = "Txtcari"
        Me.Txtcari.Size = New System.Drawing.Size(180, 26)
        Me.Txtcari.TabIndex = 46
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label1.Location = New System.Drawing.Point(306, 10)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 17)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Text Dicari"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Cbocari
        '
        Me.Cbocari.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Cbocari.FormattingEnabled = True
        Me.Cbocari.Location = New System.Drawing.Point(114, 6)
        Me.Cbocari.Margin = New System.Windows.Forms.Padding(4)
        Me.Cbocari.Name = "Cbocari"
        Me.Cbocari.Size = New System.Drawing.Size(180, 28)
        Me.Cbocari.TabIndex = 45
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label2.Location = New System.Drawing.Point(15, 10)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 17)
        Me.Label2.TabIndex = 43
        Me.Label2.Text = "Berdasarkan"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Dganggota)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 213)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(820, 317)
        Me.Panel2.TabIndex = 29
        '
        'Dganggota
        '
        Me.Dganggota.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dganggota.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Dganggota.Location = New System.Drawing.Point(0, 0)
        Me.Dganggota.Name = "Dganggota"
        Me.Dganggota.Size = New System.Drawing.Size(820, 317)
        Me.Dganggota.TabIndex = 43
        '
        'Txtjabatan
        '
        Me.Txtjabatan.FormattingEnabled = True
        Me.Txtjabatan.Items.AddRange(New Object() {"Senior Staf", "Supervisor", "Karyawan"})
        Me.Txtjabatan.Location = New System.Drawing.Point(158, 98)
        Me.Txtjabatan.Name = "Txtjabatan"
        Me.Txtjabatan.Size = New System.Drawing.Size(180, 24)
        Me.Txtjabatan.TabIndex = 61
        '
        'FormAnggota
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(820, 530)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FormAnggota"
        Me.Text = "FormAnggota2"
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.Dganggota, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents cmdSimpan As System.Windows.Forms.Button
    Friend WithEvents cmdedit As System.Windows.Forms.Button
    Friend WithEvents cmdbatal As System.Windows.Forms.Button
    Friend WithEvents cmdtambah As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Txtnotelp As System.Windows.Forms.TextBox
    Friend WithEvents Txtalamat As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Dganggota As System.Windows.Forms.DataGridView
    Friend WithEvents Txtnamaanggota As System.Windows.Forms.TextBox
    Friend WithEvents Txtidanggota As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtTanggalGabung As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmdpilih As System.Windows.Forms.Button
    Friend WithEvents cmdkeluar As System.Windows.Forms.Button
    Friend WithEvents Txtjabatan As System.Windows.Forms.ComboBox
End Class
