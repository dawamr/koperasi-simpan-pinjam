﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormSimpananWajib
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtidanggota = New System.Windows.Forms.TextBox()
        Me.txtTanggalBayar = New System.Windows.Forms.DateTimePicker()
        Me.digsimpananwajib = New System.Windows.Forms.DataGridView()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Txtnamaanggota = New System.Windows.Forms.TextBox()
        Me.Txtjabatan = New System.Windows.Forms.TextBox()
        Me.txtUUIDanggota = New System.Windows.Forms.TextBox()
        Me.txtNominal = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Txtcari = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Cbocari = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtIDsimpanan = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmdtambah = New System.Windows.Forms.Button()
        Me.cmdbatal = New System.Windows.Forms.Button()
        Me.cmdedit = New System.Windows.Forms.Button()
        Me.cmdsimpan = New System.Windows.Forms.Button()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.digsimpananwajib, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtidanggota
        '
        Me.txtidanggota.Location = New System.Drawing.Point(173, 20)
        Me.txtidanggota.Margin = New System.Windows.Forms.Padding(4)
        Me.txtidanggota.Name = "txtidanggota"
        Me.txtidanggota.Size = New System.Drawing.Size(180, 23)
        Me.txtidanggota.TabIndex = 48
        '
        'txtTanggalBayar
        '
        Me.txtTanggalBayar.Enabled = False
        Me.txtTanggalBayar.Location = New System.Drawing.Point(164, 112)
        Me.txtTanggalBayar.Name = "txtTanggalBayar"
        Me.txtTanggalBayar.Size = New System.Drawing.Size(180, 23)
        Me.txtTanggalBayar.TabIndex = 61
        '
        'digsimpananwajib
        '
        Me.digsimpananwajib.Dock = System.Windows.Forms.DockStyle.Fill
        Me.digsimpananwajib.Location = New System.Drawing.Point(0, 0)
        Me.digsimpananwajib.Name = "digsimpananwajib"
        Me.digsimpananwajib.Size = New System.Drawing.Size(820, 235)
        Me.digsimpananwajib.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(36, 21)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(130, 23)
        Me.Label8.TabIndex = 47
        Me.Label8.Text = "ID Anggota"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.digsimpananwajib)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(0, 295)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(820, 235)
        Me.Panel6.TabIndex = 39
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(17, 52)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(130, 23)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "UUID Anggota"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Txtnamaanggota
        '
        Me.Txtnamaanggota.Location = New System.Drawing.Point(173, 50)
        Me.Txtnamaanggota.Margin = New System.Windows.Forms.Padding(4)
        Me.Txtnamaanggota.Name = "Txtnamaanggota"
        Me.Txtnamaanggota.Size = New System.Drawing.Size(180, 23)
        Me.Txtnamaanggota.TabIndex = 46
        '
        'Txtjabatan
        '
        Me.Txtjabatan.Location = New System.Drawing.Point(173, 81)
        Me.Txtjabatan.Margin = New System.Windows.Forms.Padding(4)
        Me.Txtjabatan.Name = "Txtjabatan"
        Me.Txtjabatan.Size = New System.Drawing.Size(180, 23)
        Me.Txtjabatan.TabIndex = 44
        '
        'txtUUIDanggota
        '
        Me.txtUUIDanggota.Location = New System.Drawing.Point(164, 51)
        Me.txtUUIDanggota.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUUIDanggota.Name = "txtUUIDanggota"
        Me.txtUUIDanggota.Size = New System.Drawing.Size(140, 23)
        Me.txtUUIDanggota.TabIndex = 45
        '
        'txtNominal
        '
        Me.txtNominal.Enabled = False
        Me.txtNominal.Location = New System.Drawing.Point(164, 82)
        Me.txtNominal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNominal.Name = "txtNominal"
        Me.txtNominal.Size = New System.Drawing.Size(180, 23)
        Me.txtNominal.TabIndex = 55
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtidanggota)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Txtnamaanggota)
        Me.Panel3.Controls.Add(Me.Txtjabatan)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(441, 0)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(379, 172)
        Me.Panel3.TabIndex = 51
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(36, 81)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(130, 23)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Jabatan Anggota"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(36, 51)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(130, 23)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "Nama Anggota"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Panel7)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 254)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(820, 41)
        Me.Panel5.TabIndex = 38
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Txtcari)
        Me.Panel7.Controls.Add(Me.Label1)
        Me.Panel7.Controls.Add(Me.Cbocari)
        Me.Panel7.Controls.Add(Me.Label2)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel7.Location = New System.Drawing.Point(226, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(594, 41)
        Me.Panel7.TabIndex = 0
        '
        'Txtcari
        '
        Me.Txtcari.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Txtcari.Location = New System.Drawing.Point(388, 6)
        Me.Txtcari.Margin = New System.Windows.Forms.Padding(4)
        Me.Txtcari.Name = "Txtcari"
        Me.Txtcari.Size = New System.Drawing.Size(180, 26)
        Me.Txtcari.TabIndex = 46
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label1.Location = New System.Drawing.Point(306, 10)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 17)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Text Dicari"
        '
        'Cbocari
        '
        Me.Cbocari.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Cbocari.FormattingEnabled = True
        Me.Cbocari.Location = New System.Drawing.Point(114, 6)
        Me.Cbocari.Margin = New System.Windows.Forms.Padding(4)
        Me.Cbocari.Name = "Cbocari"
        Me.Cbocari.Size = New System.Drawing.Size(180, 28)
        Me.Cbocari.TabIndex = 45
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label2.Location = New System.Drawing.Point(15, 10)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 17)
        Me.Label2.TabIndex = 43
        Me.Label2.Text = "Berdasarkan"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtTanggalBayar)
        Me.Panel4.Controls.Add(Me.Label9)
        Me.Panel4.Controls.Add(Me.Button1)
        Me.Panel4.Controls.Add(Me.txtIDsimpanan)
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Controls.Add(Me.txtUUIDanggota)
        Me.Panel4.Controls.Add(Me.txtNominal)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(379, 172)
        Me.Panel4.TabIndex = 50
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(17, 113)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(130, 23)
        Me.Label9.TabIndex = 59
        Me.Label9.Text = "Tanggal Setor"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Button1.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.loupe
        Me.Button1.Location = New System.Drawing.Point(308, 50)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(36, 25)
        Me.Button1.TabIndex = 58
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtIDsimpanan
        '
        Me.txtIDsimpanan.Enabled = False
        Me.txtIDsimpanan.Location = New System.Drawing.Point(164, 20)
        Me.txtIDsimpanan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIDsimpanan.Name = "txtIDsimpanan"
        Me.txtIDsimpanan.Size = New System.Drawing.Size(180, 23)
        Me.txtIDsimpanan.TabIndex = 57
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(17, 20)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(150, 23)
        Me.Label6.TabIndex = 56
        Me.Label6.Text = "ID Simpanan Wajib"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(17, 83)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(130, 23)
        Me.Label7.TabIndex = 53
        Me.Label7.Text = "Nominal"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Panel8)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 172)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(820, 82)
        Me.Panel2.TabIndex = 37
        '
        'cmdtambah
        '
        Me.cmdtambah.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdtambah.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.tambah
        Me.cmdtambah.Location = New System.Drawing.Point(19, 8)
        Me.cmdtambah.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdtambah.Name = "cmdtambah"
        Me.cmdtambah.Size = New System.Drawing.Size(70, 29)
        Me.cmdtambah.TabIndex = 49
        Me.cmdtambah.UseVisualStyleBackColor = True
        '
        'cmdbatal
        '
        Me.cmdbatal.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.keluar
        Me.cmdbatal.Location = New System.Drawing.Point(19, 42)
        Me.cmdbatal.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdbatal.Name = "cmdbatal"
        Me.cmdbatal.Size = New System.Drawing.Size(70, 29)
        Me.cmdbatal.TabIndex = 50
        Me.cmdbatal.UseVisualStyleBackColor = True
        Me.cmdbatal.Visible = False
        '
        'cmdedit
        '
        Me.cmdedit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdedit.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.edit
        Me.cmdedit.Location = New System.Drawing.Point(97, 8)
        Me.cmdedit.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdedit.Name = "cmdedit"
        Me.cmdedit.Size = New System.Drawing.Size(70, 29)
        Me.cmdedit.TabIndex = 51
        Me.cmdedit.UseVisualStyleBackColor = True
        '
        'cmdsimpan
        '
        Me.cmdsimpan.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.simpan
        Me.cmdsimpan.Location = New System.Drawing.Point(97, 42)
        Me.cmdsimpan.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdsimpan.Name = "cmdsimpan"
        Me.cmdsimpan.Size = New System.Drawing.Size(70, 29)
        Me.cmdsimpan.TabIndex = 52
        Me.cmdsimpan.UseVisualStyleBackColor = True
        Me.cmdsimpan.Visible = False
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.cmdtambah)
        Me.Panel8.Controls.Add(Me.cmdedit)
        Me.Panel8.Controls.Add(Me.cmdbatal)
        Me.Panel8.Controls.Add(Me.cmdsimpan)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel8.Location = New System.Drawing.Point(630, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(190, 82)
        Me.Panel8.TabIndex = 53
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(820, 172)
        Me.Panel1.TabIndex = 36
        '
        'FormSimpananWajib
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(820, 530)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FormSimpananWajib"
        Me.Text = "FormSimpananWajib"
        CType(Me.digsimpananwajib, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtidanggota As System.Windows.Forms.TextBox
    Friend WithEvents txtTanggalBayar As System.Windows.Forms.DateTimePicker
    Friend WithEvents digsimpananwajib As System.Windows.Forms.DataGridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Txtnamaanggota As System.Windows.Forms.TextBox
    Friend WithEvents Txtjabatan As System.Windows.Forms.TextBox
    Friend WithEvents txtUUIDanggota As System.Windows.Forms.TextBox
    Friend WithEvents txtNominal As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtIDsimpanan As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmdtambah As System.Windows.Forms.Button
    Friend WithEvents cmdbatal As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cmdedit As System.Windows.Forms.Button
    Friend WithEvents cmdsimpan As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
End Class
