﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMaster
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btnMinimize = New System.Windows.Forms.PictureBox()
        Me.btnExit = New System.Windows.Forms.PictureBox()
        Me.btnExpandOut = New System.Windows.Forms.PictureBox()
        Me.btnExpandIn = New System.Windows.Forms.PictureBox()
        Me.panelSidebar = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.panelBody = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.btnMinimize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnExit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnExpandOut, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnExpandIn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1000, 31)
        Me.Panel1.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btnMinimize)
        Me.Panel3.Controls.Add(Me.btnExit)
        Me.Panel3.Controls.Add(Me.btnExpandOut)
        Me.Panel3.Controls.Add(Me.btnExpandIn)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(890, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(110, 31)
        Me.Panel3.TabIndex = 0
        '
        'btnMinimize
        '
        Me.btnMinimize.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.minimize_2
        Me.btnMinimize.Location = New System.Drawing.Point(14, 3)
        Me.btnMinimize.Name = "btnMinimize"
        Me.btnMinimize.Size = New System.Drawing.Size(25, 25)
        Me.btnMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.btnMinimize.TabIndex = 2
        Me.btnMinimize.TabStop = False
        '
        'btnExit
        '
        Me.btnExit.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.cancel_2
        Me.btnExit.Location = New System.Drawing.Point(64, 3)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(25, 25)
        Me.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.btnExit.TabIndex = 0
        Me.btnExit.TabStop = False
        '
        'btnExpandOut
        '
        Me.btnExpandOut.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.expand
        Me.btnExpandOut.Location = New System.Drawing.Point(39, 3)
        Me.btnExpandOut.Name = "btnExpandOut"
        Me.btnExpandOut.Size = New System.Drawing.Size(25, 25)
        Me.btnExpandOut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.btnExpandOut.TabIndex = 1
        Me.btnExpandOut.TabStop = False
        '
        'btnExpandIn
        '
        Me.btnExpandIn.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.minimize
        Me.btnExpandIn.Location = New System.Drawing.Point(47, 3)
        Me.btnExpandIn.Name = "btnExpandIn"
        Me.btnExpandIn.Size = New System.Drawing.Size(25, 25)
        Me.btnExpandIn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.btnExpandIn.TabIndex = 3
        Me.btnExpandIn.TabStop = False
        '
        'panelSidebar
        '
        Me.panelSidebar.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.panelSidebar.Dock = System.Windows.Forms.DockStyle.Left
        Me.panelSidebar.Location = New System.Drawing.Point(0, 31)
        Me.panelSidebar.Name = "panelSidebar"
        Me.panelSidebar.Size = New System.Drawing.Size(180, 540)
        Me.panelSidebar.TabIndex = 1
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel10.Location = New System.Drawing.Point(180, 558)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(820, 13)
        Me.Panel10.TabIndex = 2
        '
        'panelBody
        '
        Me.panelBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelBody.Location = New System.Drawing.Point(180, 31)
        Me.panelBody.Name = "panelBody"
        Me.panelBody.Size = New System.Drawing.Size(820, 527)
        Me.panelBody.TabIndex = 3
        '
        'FrmMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(225, Byte), Integer), CType(CType(225, Byte), Integer), CType(CType(225, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1000, 571)
        Me.Controls.Add(Me.panelBody)
        Me.Controls.Add(Me.Panel10)
        Me.Controls.Add(Me.panelSidebar)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmMaster"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Master"
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.btnMinimize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnExit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnExpandOut, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnExpandIn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents panelSidebar As System.Windows.Forms.Panel
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents panelBody As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnExit As System.Windows.Forms.PictureBox
    Friend WithEvents btnMinimize As System.Windows.Forms.PictureBox
    Friend WithEvents btnExpandOut As System.Windows.Forms.PictureBox
    Friend WithEvents btnExpandIn As System.Windows.Forms.PictureBox
End Class
