﻿Public Class FrmMaster

    Dim frmMax As Boolean = False

    Sub loadSidebarMaster()
        Me.panelSidebar.Controls.Clear()
        Master.TopLevel = False
        Master.TopMost = True
        Master.Dock = DockStyle.Fill
        Me.panelSidebar.Controls.Add(Master)
        Master.Show()
        btnExpandIn.Visible = False
        btnExpandIn.Location = New Point(btnExpandOut.Location)
    End Sub

    Private Sub FrmMaster_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        loadSidebarMaster()
    End Sub

    Private Sub btnMinimize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMinimize.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btnExpandOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExpandOut.Click
        Me.WindowState = FormWindowState.Maximized
        btnExpandOut.Visible = False
        btnExpandIn.Visible = True
        frmMax = True
    End Sub

    Private Sub btnExpandIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExpandIn.Click
        Me.WindowState = FormWindowState.Normal
        btnExpandOut.Visible = True
        btnExpandIn.Visible = False
        frmMax = False
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Application.Exit()
    End Sub
End Class