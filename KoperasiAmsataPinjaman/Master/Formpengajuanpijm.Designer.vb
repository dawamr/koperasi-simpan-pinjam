﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Formpengajuanpinjm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtIDpengajuan = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgpengajuan = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.cmbStatusPengajuan = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.cmbBanyakAngsuran = New System.Windows.Forms.ComboBox()
        Me.txtTotalAngsuran = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtNominalAngsuran = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.txtBungaAngsuran = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtTanggalPengajuan = New System.Windows.Forms.DateTimePicker()
        Me.txtUUIDanggota = New System.Windows.Forms.TextBox()
        Me.txtNominal = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Txtcari = New System.Windows.Forms.TextBox()
        Me.cmdtambah = New System.Windows.Forms.Button()
        Me.cmdbatal = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.cmdedit = New System.Windows.Forms.Button()
        Me.cmdkeluar = New System.Windows.Forms.Button()
        Me.cmdsimpan = New System.Windows.Forms.Button()
        Me.cmdpilih = New System.Windows.Forms.Button()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Cbocari = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.txtidanggota = New System.Windows.Forms.TextBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Txtnamaanggota = New System.Windows.Forms.TextBox()
        Me.Txtjabatan = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.dgpengajuan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label9.Location = New System.Drawing.Point(17, 115)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(150, 23)
        Me.Label9.TabIndex = 59
        Me.Label9.Text = "Tanggal Pengajuan"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIDpengajuan
        '
        Me.txtIDpengajuan.Enabled = False
        Me.txtIDpengajuan.Location = New System.Drawing.Point(182, 20)
        Me.txtIDpengajuan.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIDpengajuan.Name = "txtIDpengajuan"
        Me.txtIDpengajuan.Size = New System.Drawing.Size(180, 23)
        Me.txtIDpengajuan.TabIndex = 57
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Button1.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.loupe
        Me.Button1.Location = New System.Drawing.Point(326, 50)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(36, 25)
        Me.Button1.TabIndex = 58
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dgpengajuan
        '
        Me.dgpengajuan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgpengajuan.Location = New System.Drawing.Point(0, 0)
        Me.dgpengajuan.Name = "dgpengajuan"
        Me.dgpengajuan.Size = New System.Drawing.Size(820, 191)
        Me.dgpengajuan.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(17, 20)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(150, 23)
        Me.Label6.TabIndex = 56
        Me.Label6.Text = "ID Pengajuan"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel4
        '
        Me.Panel4.AutoScroll = True
        Me.Panel4.Controls.Add(Me.cmbStatusPengajuan)
        Me.Panel4.Controls.Add(Me.Label15)
        Me.Panel4.Controls.Add(Me.cmbBanyakAngsuran)
        Me.Panel4.Controls.Add(Me.txtTotalAngsuran)
        Me.Panel4.Controls.Add(Me.Label14)
        Me.Panel4.Controls.Add(Me.Label13)
        Me.Panel4.Controls.Add(Me.txtNominalAngsuran)
        Me.Panel4.Controls.Add(Me.Label12)
        Me.Panel4.Controls.Add(Me.Panel8)
        Me.Panel4.Controls.Add(Me.txtBungaAngsuran)
        Me.Panel4.Controls.Add(Me.Label10)
        Me.Panel4.Controls.Add(Me.Label11)
        Me.Panel4.Controls.Add(Me.Label9)
        Me.Panel4.Controls.Add(Me.txtTanggalPengajuan)
        Me.Panel4.Controls.Add(Me.Button1)
        Me.Panel4.Controls.Add(Me.txtIDpengajuan)
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Controls.Add(Me.txtUUIDanggota)
        Me.Panel4.Controls.Add(Me.txtNominal)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(393, 229)
        Me.Panel4.TabIndex = 50
        '
        'cmbStatusPengajuan
        '
        Me.cmbStatusPengajuan.Enabled = False
        Me.cmbStatusPengajuan.FormattingEnabled = True
        Me.cmbStatusPengajuan.Items.AddRange(New Object() {"Menunggu", "Ditolak", "Disetujui"})
        Me.cmbStatusPengajuan.Location = New System.Drawing.Point(180, 280)
        Me.cmbStatusPengajuan.Name = "cmbStatusPengajuan"
        Me.cmbStatusPengajuan.Size = New System.Drawing.Size(180, 24)
        Me.cmbStatusPengajuan.TabIndex = 74
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(16, 279)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(130, 23)
        Me.Label15.TabIndex = 73
        Me.Label15.Text = "Status Pengajuan"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbBanyakAngsuran
        '
        Me.cmbBanyakAngsuran.Enabled = False
        Me.cmbBanyakAngsuran.FormattingEnabled = True
        Me.cmbBanyakAngsuran.Items.AddRange(New Object() {"3", "6", "12"})
        Me.cmbBanyakAngsuran.Location = New System.Drawing.Point(181, 147)
        Me.cmbBanyakAngsuran.Name = "cmbBanyakAngsuran"
        Me.cmbBanyakAngsuran.Size = New System.Drawing.Size(180, 24)
        Me.cmbBanyakAngsuran.TabIndex = 72
        '
        'txtTotalAngsuran
        '
        Me.txtTotalAngsuran.Enabled = False
        Me.txtTotalAngsuran.Location = New System.Drawing.Point(181, 246)
        Me.txtTotalAngsuran.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalAngsuran.Name = "txtTotalAngsuran"
        Me.txtTotalAngsuran.Size = New System.Drawing.Size(180, 23)
        Me.txtTotalAngsuran.TabIndex = 71
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(16, 247)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(130, 23)
        Me.Label14.TabIndex = 70
        Me.Label14.Text = "Nominal Angsuran"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Bold)
        Me.Label13.Location = New System.Drawing.Point(127, 178)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(46, 18)
        Me.Label13.TabIndex = 69
        Me.Label13.Text = "(1,25%)"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNominalAngsuran
        '
        Me.txtNominalAngsuran.Enabled = False
        Me.txtNominalAngsuran.Location = New System.Drawing.Point(182, 212)
        Me.txtNominalAngsuran.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNominalAngsuran.Name = "txtNominalAngsuran"
        Me.txtNominalAngsuran.Size = New System.Drawing.Size(180, 23)
        Me.txtNominalAngsuran.TabIndex = 68
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(17, 213)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(130, 23)
        Me.Label12.TabIndex = 67
        Me.Label12.Text = "Angsuran Pokok"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel8
        '
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel8.Location = New System.Drawing.Point(0, 304)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(376, 29)
        Me.Panel8.TabIndex = 66
        '
        'txtBungaAngsuran
        '
        Me.txtBungaAngsuran.Enabled = False
        Me.txtBungaAngsuran.Location = New System.Drawing.Point(182, 180)
        Me.txtBungaAngsuran.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBungaAngsuran.Name = "txtBungaAngsuran"
        Me.txtBungaAngsuran.Size = New System.Drawing.Size(180, 23)
        Me.txtBungaAngsuran.TabIndex = 65
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(17, 146)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(130, 23)
        Me.Label10.TabIndex = 62
        Me.Label10.Text = "Banyak Angsuran"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(17, 181)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(130, 23)
        Me.Label11.TabIndex = 64
        Me.Label11.Text = "Angsuran Bunga"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTanggalPengajuan
        '
        Me.txtTanggalPengajuan.Enabled = False
        Me.txtTanggalPengajuan.Location = New System.Drawing.Point(182, 115)
        Me.txtTanggalPengajuan.Name = "txtTanggalPengajuan"
        Me.txtTanggalPengajuan.Size = New System.Drawing.Size(180, 23)
        Me.txtTanggalPengajuan.TabIndex = 61
        '
        'txtUUIDanggota
        '
        Me.txtUUIDanggota.Location = New System.Drawing.Point(182, 51)
        Me.txtUUIDanggota.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUUIDanggota.Name = "txtUUIDanggota"
        Me.txtUUIDanggota.Size = New System.Drawing.Size(140, 23)
        Me.txtUUIDanggota.TabIndex = 45
        '
        'txtNominal
        '
        Me.txtNominal.Enabled = False
        Me.txtNominal.Location = New System.Drawing.Point(182, 83)
        Me.txtNominal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNominal.Name = "txtNominal"
        Me.txtNominal.Size = New System.Drawing.Size(180, 23)
        Me.txtNominal.TabIndex = 55
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(17, 52)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(130, 23)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "UUID Anggota"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(17, 84)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(150, 23)
        Me.Label7.TabIndex = 53
        Me.Label7.Text = "Nominal Pengajuan"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Txtcari
        '
        Me.Txtcari.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Txtcari.Location = New System.Drawing.Point(388, 12)
        Me.Txtcari.Margin = New System.Windows.Forms.Padding(4)
        Me.Txtcari.Name = "Txtcari"
        Me.Txtcari.Size = New System.Drawing.Size(180, 26)
        Me.Txtcari.TabIndex = 46
        '
        'cmdtambah
        '
        Me.cmdtambah.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdtambah.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.tambah
        Me.cmdtambah.Location = New System.Drawing.Point(128, 7)
        Me.cmdtambah.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdtambah.Name = "cmdtambah"
        Me.cmdtambah.Size = New System.Drawing.Size(70, 29)
        Me.cmdtambah.TabIndex = 49
        Me.cmdtambah.UseVisualStyleBackColor = True
        '
        'cmdbatal
        '
        Me.cmdbatal.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.keluar
        Me.cmdbatal.Location = New System.Drawing.Point(128, 41)
        Me.cmdbatal.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdbatal.Name = "cmdbatal"
        Me.cmdbatal.Size = New System.Drawing.Size(70, 29)
        Me.cmdbatal.TabIndex = 50
        Me.cmdbatal.UseVisualStyleBackColor = True
        Me.cmdbatal.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Panel9)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 276)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(820, 63)
        Me.Panel2.TabIndex = 41
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.cmdedit)
        Me.Panel9.Controls.Add(Me.cmdkeluar)
        Me.Panel9.Controls.Add(Me.cmdsimpan)
        Me.Panel9.Controls.Add(Me.cmdpilih)
        Me.Panel9.Controls.Add(Me.cmdbatal)
        Me.Panel9.Controls.Add(Me.cmdtambah)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel9.Location = New System.Drawing.Point(518, 0)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(302, 63)
        Me.Panel9.TabIndex = 63
        '
        'cmdedit
        '
        Me.cmdedit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdedit.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.edit
        Me.cmdedit.Location = New System.Drawing.Point(206, 7)
        Me.cmdedit.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdedit.Name = "cmdedit"
        Me.cmdedit.Size = New System.Drawing.Size(70, 29)
        Me.cmdedit.TabIndex = 51
        Me.cmdedit.UseVisualStyleBackColor = True
        '
        'cmdkeluar
        '
        Me.cmdkeluar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdkeluar.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.logout
        Me.cmdkeluar.Location = New System.Drawing.Point(50, 7)
        Me.cmdkeluar.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdkeluar.Name = "cmdkeluar"
        Me.cmdkeluar.Size = New System.Drawing.Size(70, 29)
        Me.cmdkeluar.TabIndex = 62
        Me.cmdkeluar.UseVisualStyleBackColor = True
        Me.cmdkeluar.Visible = False
        '
        'cmdsimpan
        '
        Me.cmdsimpan.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.simpan
        Me.cmdsimpan.Location = New System.Drawing.Point(206, 41)
        Me.cmdsimpan.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdsimpan.Name = "cmdsimpan"
        Me.cmdsimpan.Size = New System.Drawing.Size(70, 29)
        Me.cmdsimpan.TabIndex = 52
        Me.cmdsimpan.UseVisualStyleBackColor = True
        Me.cmdsimpan.Visible = False
        '
        'cmdpilih
        '
        Me.cmdpilih.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdpilih.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.check_mark_16
        Me.cmdpilih.Location = New System.Drawing.Point(50, 41)
        Me.cmdpilih.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdpilih.Name = "cmdpilih"
        Me.cmdpilih.Size = New System.Drawing.Size(70, 29)
        Me.cmdpilih.TabIndex = 61
        Me.cmdpilih.UseVisualStyleBackColor = True
        Me.cmdpilih.Visible = False
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Txtcari)
        Me.Panel7.Controls.Add(Me.Label1)
        Me.Panel7.Controls.Add(Me.Cbocari)
        Me.Panel7.Controls.Add(Me.Label2)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel7.Location = New System.Drawing.Point(226, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(594, 47)
        Me.Panel7.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label1.Location = New System.Drawing.Point(306, 17)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 17)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Text Dicari"
        '
        'Cbocari
        '
        Me.Cbocari.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Cbocari.FormattingEnabled = True
        Me.Cbocari.Location = New System.Drawing.Point(114, 11)
        Me.Cbocari.Margin = New System.Windows.Forms.Padding(4)
        Me.Cbocari.Name = "Cbocari"
        Me.Cbocari.Size = New System.Drawing.Size(180, 28)
        Me.Cbocari.TabIndex = 45
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label2.Location = New System.Drawing.Point(15, 17)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 17)
        Me.Label2.TabIndex = 43
        Me.Label2.Text = "Berdasarkan"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Panel7)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 229)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(820, 47)
        Me.Panel5.TabIndex = 42
        '
        'txtidanggota
        '
        Me.txtidanggota.Location = New System.Drawing.Point(173, 20)
        Me.txtidanggota.Margin = New System.Windows.Forms.Padding(4)
        Me.txtidanggota.Name = "txtidanggota"
        Me.txtidanggota.Size = New System.Drawing.Size(180, 23)
        Me.txtidanggota.TabIndex = 48
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.dgpengajuan)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(0, 339)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(820, 191)
        Me.Panel6.TabIndex = 43
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(36, 21)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(130, 23)
        Me.Label8.TabIndex = 47
        Me.Label8.Text = "ID Anggota"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Txtnamaanggota
        '
        Me.Txtnamaanggota.Location = New System.Drawing.Point(173, 50)
        Me.Txtnamaanggota.Margin = New System.Windows.Forms.Padding(4)
        Me.Txtnamaanggota.Name = "Txtnamaanggota"
        Me.Txtnamaanggota.Size = New System.Drawing.Size(180, 23)
        Me.Txtnamaanggota.TabIndex = 46
        '
        'Txtjabatan
        '
        Me.Txtjabatan.Location = New System.Drawing.Point(173, 81)
        Me.Txtjabatan.Margin = New System.Windows.Forms.Padding(4)
        Me.Txtjabatan.Name = "Txtjabatan"
        Me.Txtjabatan.Size = New System.Drawing.Size(180, 23)
        Me.Txtjabatan.TabIndex = 44
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(36, 51)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(130, 23)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "Nama Anggota"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtidanggota)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Txtnamaanggota)
        Me.Panel3.Controls.Add(Me.Txtjabatan)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(441, 0)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(379, 229)
        Me.Panel3.TabIndex = 51
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(36, 81)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(130, 23)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Jabatan Anggota"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(820, 229)
        Me.Panel1.TabIndex = 40
        '
        'Formpengajuanpinjm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(820, 530)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Formpengajuanpinjm"
        Me.Text = "Input Simpanan Sukarela"
        CType(Me.dgpengajuan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtIDpengajuan As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents dgpengajuan As System.Windows.Forms.DataGridView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtTanggalPengajuan As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtUUIDanggota As System.Windows.Forms.TextBox
    Friend WithEvents txtNominal As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Txtcari As System.Windows.Forms.TextBox
    Friend WithEvents cmdtambah As System.Windows.Forms.Button
    Friend WithEvents cmdbatal As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cmdedit As System.Windows.Forms.Button
    Friend WithEvents cmdsimpan As System.Windows.Forms.Button
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents txtidanggota As System.Windows.Forms.TextBox
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Txtnamaanggota As System.Windows.Forms.TextBox
    Friend WithEvents Txtjabatan As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtBungaAngsuran As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtNominalAngsuran As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents cmbStatusPengajuan As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cmbBanyakAngsuran As System.Windows.Forms.ComboBox
    Friend WithEvents txtTotalAngsuran As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmdkeluar As System.Windows.Forms.Button
    Friend WithEvents cmdpilih As System.Windows.Forms.Button
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
End Class
