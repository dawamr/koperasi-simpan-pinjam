﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormUser2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormUser2))
        Me.btnsesudah = New System.Windows.Forms.Button()
        Me.btnsebelum = New System.Windows.Forms.Button()
        Me.btnakhir = New System.Windows.Forms.Button()
        Me.btnawal = New System.Windows.Forms.Button()
        Me.btnkeluar = New System.Windows.Forms.Button()
        Me.btnbatal = New System.Windows.Forms.Button()
        Me.btnhapus = New System.Windows.Forms.Button()
        Me.btnkoreksi = New System.Windows.Forms.Button()
        Me.btnsimpan = New System.Windows.Forms.Button()
        Me.btntambah = New System.Windows.Forms.Button()
        Me.dguser = New System.Windows.Forms.DataGridView()
        Me.cbohakakses = New System.Windows.Forms.ComboBox()
        Me.txtulangipassword = New System.Windows.Forms.TextBox()
        Me.txtpassword = New System.Windows.Forms.TextBox()
        Me.txtnama = New System.Windows.Forms.TextBox()
        Me.txtuserid = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gridcari = New System.Windows.Forms.GroupBox()
        Me.txtcari = New System.Windows.Forms.TextBox()
        Me.cbcari = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtusername = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        CType(Me.dguser, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gridcari.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnsesudah
        '
        Me.btnsesudah.BackColor = System.Drawing.Color.SeaShell
        Me.btnsesudah.Location = New System.Drawing.Point(689, 204)
        Me.btnsesudah.Margin = New System.Windows.Forms.Padding(4)
        Me.btnsesudah.Name = "btnsesudah"
        Me.btnsesudah.Size = New System.Drawing.Size(100, 28)
        Me.btnsesudah.TabIndex = 71
        Me.btnsesudah.Text = "Sesudah"
        Me.btnsesudah.UseVisualStyleBackColor = False
        '
        'btnsebelum
        '
        Me.btnsebelum.BackColor = System.Drawing.Color.SeaShell
        Me.btnsebelum.Location = New System.Drawing.Point(581, 204)
        Me.btnsebelum.Margin = New System.Windows.Forms.Padding(4)
        Me.btnsebelum.Name = "btnsebelum"
        Me.btnsebelum.Size = New System.Drawing.Size(100, 28)
        Me.btnsebelum.TabIndex = 70
        Me.btnsebelum.Text = "Sebelum"
        Me.btnsebelum.UseVisualStyleBackColor = False
        '
        'btnakhir
        '
        Me.btnakhir.BackColor = System.Drawing.Color.SeaShell
        Me.btnakhir.Location = New System.Drawing.Point(689, 176)
        Me.btnakhir.Margin = New System.Windows.Forms.Padding(4)
        Me.btnakhir.Name = "btnakhir"
        Me.btnakhir.Size = New System.Drawing.Size(100, 28)
        Me.btnakhir.TabIndex = 69
        Me.btnakhir.Text = "Akhir"
        Me.btnakhir.UseVisualStyleBackColor = False
        '
        'btnawal
        '
        Me.btnawal.BackColor = System.Drawing.Color.SeaShell
        Me.btnawal.Location = New System.Drawing.Point(581, 176)
        Me.btnawal.Margin = New System.Windows.Forms.Padding(4)
        Me.btnawal.Name = "btnawal"
        Me.btnawal.Size = New System.Drawing.Size(100, 28)
        Me.btnawal.TabIndex = 68
        Me.btnawal.Text = "Awal"
        Me.btnawal.UseVisualStyleBackColor = False
        '
        'btnkeluar
        '
        Me.btnkeluar.BackColor = System.Drawing.Color.SeaShell
        Me.btnkeluar.Image = CType(resources.GetObject("btnkeluar.Image"), System.Drawing.Image)
        Me.btnkeluar.Location = New System.Drawing.Point(689, 114)
        Me.btnkeluar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnkeluar.Name = "btnkeluar"
        Me.btnkeluar.Size = New System.Drawing.Size(100, 50)
        Me.btnkeluar.TabIndex = 67
        Me.btnkeluar.Text = "Kelua&r"
        Me.btnkeluar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnkeluar.UseVisualStyleBackColor = False
        '
        'btnbatal
        '
        Me.btnbatal.BackColor = System.Drawing.Color.SeaShell
        Me.btnbatal.Image = CType(resources.GetObject("btnbatal.Image"), System.Drawing.Image)
        Me.btnbatal.Location = New System.Drawing.Point(581, 114)
        Me.btnbatal.Margin = New System.Windows.Forms.Padding(4)
        Me.btnbatal.Name = "btnbatal"
        Me.btnbatal.Size = New System.Drawing.Size(100, 50)
        Me.btnbatal.TabIndex = 66
        Me.btnbatal.Text = "&Batal"
        Me.btnbatal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnbatal.UseVisualStyleBackColor = False
        '
        'btnhapus
        '
        Me.btnhapus.BackColor = System.Drawing.Color.SeaShell
        Me.btnhapus.Image = CType(resources.GetObject("btnhapus.Image"), System.Drawing.Image)
        Me.btnhapus.Location = New System.Drawing.Point(689, 64)
        Me.btnhapus.Margin = New System.Windows.Forms.Padding(4)
        Me.btnhapus.Name = "btnhapus"
        Me.btnhapus.Size = New System.Drawing.Size(100, 50)
        Me.btnhapus.TabIndex = 65
        Me.btnhapus.Text = "&Hapus"
        Me.btnhapus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnhapus.UseVisualStyleBackColor = False
        '
        'btnkoreksi
        '
        Me.btnkoreksi.BackColor = System.Drawing.Color.SeaShell
        Me.btnkoreksi.Image = CType(resources.GetObject("btnkoreksi.Image"), System.Drawing.Image)
        Me.btnkoreksi.Location = New System.Drawing.Point(581, 64)
        Me.btnkoreksi.Margin = New System.Windows.Forms.Padding(4)
        Me.btnkoreksi.Name = "btnkoreksi"
        Me.btnkoreksi.Size = New System.Drawing.Size(100, 50)
        Me.btnkoreksi.TabIndex = 64
        Me.btnkoreksi.Text = "&Koreksi"
        Me.btnkoreksi.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnkoreksi.UseVisualStyleBackColor = False
        '
        'btnsimpan
        '
        Me.btnsimpan.BackColor = System.Drawing.Color.SeaShell
        Me.btnsimpan.Image = CType(resources.GetObject("btnsimpan.Image"), System.Drawing.Image)
        Me.btnsimpan.Location = New System.Drawing.Point(689, 13)
        Me.btnsimpan.Margin = New System.Windows.Forms.Padding(4)
        Me.btnsimpan.Name = "btnsimpan"
        Me.btnsimpan.Size = New System.Drawing.Size(100, 50)
        Me.btnsimpan.TabIndex = 63
        Me.btnsimpan.Text = "&Simpan"
        Me.btnsimpan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnsimpan.UseVisualStyleBackColor = False
        '
        'btntambah
        '
        Me.btntambah.BackColor = System.Drawing.Color.SeaShell
        Me.btntambah.Image = CType(resources.GetObject("btntambah.Image"), System.Drawing.Image)
        Me.btntambah.Location = New System.Drawing.Point(581, 13)
        Me.btntambah.Margin = New System.Windows.Forms.Padding(4)
        Me.btntambah.Name = "btntambah"
        Me.btntambah.Size = New System.Drawing.Size(100, 50)
        Me.btntambah.TabIndex = 62
        Me.btntambah.Text = "&Tambah"
        Me.btntambah.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btntambah.UseVisualStyleBackColor = False
        '
        'dguser
        '
        Me.dguser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dguser.Location = New System.Drawing.Point(4, 291)
        Me.dguser.Margin = New System.Windows.Forms.Padding(4)
        Me.dguser.Name = "dguser"
        Me.dguser.Size = New System.Drawing.Size(814, 237)
        Me.dguser.TabIndex = 61
        '
        'cbohakakses
        '
        Me.cbohakakses.FormattingEnabled = True
        Me.cbohakakses.Location = New System.Drawing.Point(172, 183)
        Me.cbohakakses.Margin = New System.Windows.Forms.Padding(4)
        Me.cbohakakses.Name = "cbohakakses"
        Me.cbohakakses.Size = New System.Drawing.Size(162, 24)
        Me.cbohakakses.TabIndex = 60
        '
        'txtulangipassword
        '
        Me.txtulangipassword.Location = New System.Drawing.Point(172, 150)
        Me.txtulangipassword.Margin = New System.Windows.Forms.Padding(4)
        Me.txtulangipassword.Name = "txtulangipassword"
        Me.txtulangipassword.Size = New System.Drawing.Size(162, 23)
        Me.txtulangipassword.TabIndex = 59
        '
        'txtpassword
        '
        Me.txtpassword.Location = New System.Drawing.Point(172, 118)
        Me.txtpassword.Margin = New System.Windows.Forms.Padding(4)
        Me.txtpassword.Name = "txtpassword"
        Me.txtpassword.Size = New System.Drawing.Size(162, 23)
        Me.txtpassword.TabIndex = 58
        '
        'txtnama
        '
        Me.txtnama.Location = New System.Drawing.Point(172, 91)
        Me.txtnama.Margin = New System.Windows.Forms.Padding(4)
        Me.txtnama.Name = "txtnama"
        Me.txtnama.Size = New System.Drawing.Size(162, 23)
        Me.txtnama.TabIndex = 57
        '
        'txtuserid
        '
        Me.txtuserid.Location = New System.Drawing.Point(172, 24)
        Me.txtuserid.Margin = New System.Windows.Forms.Padding(4)
        Me.txtuserid.Name = "txtuserid"
        Me.txtuserid.Size = New System.Drawing.Size(162, 23)
        Me.txtuserid.TabIndex = 56
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(30, 183)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(134, 23)
        Me.Label5.TabIndex = 55
        Me.Label5.Text = "Hak Akses"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(30, 150)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(134, 23)
        Me.Label4.TabIndex = 54
        Me.Label4.Text = "Ulangi Password"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(30, 117)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(134, 23)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "Password"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(30, 89)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(134, 23)
        Me.Label2.TabIndex = 52
        Me.Label2.Text = "Nama"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(30, 22)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(134, 23)
        Me.Label1.TabIndex = 51
        Me.Label1.Text = "ID User"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gridcari
        '
        Me.gridcari.BackColor = System.Drawing.Color.FloralWhite
        Me.gridcari.Controls.Add(Me.txtcari)
        Me.gridcari.Controls.Add(Me.cbcari)
        Me.gridcari.Controls.Add(Me.Label6)
        Me.gridcari.Controls.Add(Me.Label7)
        Me.gridcari.Location = New System.Drawing.Point(4, 240)
        Me.gridcari.Margin = New System.Windows.Forms.Padding(4)
        Me.gridcari.Name = "gridcari"
        Me.gridcari.Padding = New System.Windows.Forms.Padding(4)
        Me.gridcari.Size = New System.Drawing.Size(564, 48)
        Me.gridcari.TabIndex = 73
        Me.gridcari.TabStop = False
        '
        'txtcari
        '
        Me.txtcari.Location = New System.Drawing.Point(387, 18)
        Me.txtcari.Margin = New System.Windows.Forms.Padding(4)
        Me.txtcari.Name = "txtcari"
        Me.txtcari.Size = New System.Drawing.Size(162, 23)
        Me.txtcari.TabIndex = 7
        '
        'cbcari
        '
        Me.cbcari.FormattingEnabled = True
        Me.cbcari.Location = New System.Drawing.Point(129, 19)
        Me.cbcari.Margin = New System.Windows.Forms.Padding(4)
        Me.cbcari.Name = "cbcari"
        Me.cbcari.Size = New System.Drawing.Size(162, 24)
        Me.cbcari.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(304, 22)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 17)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Text Dicari"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(14, 24)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(89, 17)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Berdasarkan"
        '
        'txtusername
        '
        Me.txtusername.Location = New System.Drawing.Point(172, 60)
        Me.txtusername.Margin = New System.Windows.Forms.Padding(4)
        Me.txtusername.Name = "txtusername"
        Me.txtusername.Size = New System.Drawing.Size(162, 23)
        Me.txtusername.TabIndex = 75
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(30, 58)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(134, 23)
        Me.Label8.TabIndex = 74
        Me.Label8.Text = "Username"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FormUser2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(820, 530)
        Me.Controls.Add(Me.txtusername)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.gridcari)
        Me.Controls.Add(Me.btnsesudah)
        Me.Controls.Add(Me.btnsebelum)
        Me.Controls.Add(Me.btnakhir)
        Me.Controls.Add(Me.btnawal)
        Me.Controls.Add(Me.btnkeluar)
        Me.Controls.Add(Me.btnbatal)
        Me.Controls.Add(Me.btnhapus)
        Me.Controls.Add(Me.btnkoreksi)
        Me.Controls.Add(Me.btnsimpan)
        Me.Controls.Add(Me.btntambah)
        Me.Controls.Add(Me.dguser)
        Me.Controls.Add(Me.cbohakakses)
        Me.Controls.Add(Me.txtulangipassword)
        Me.Controls.Add(Me.txtpassword)
        Me.Controls.Add(Me.txtnama)
        Me.Controls.Add(Me.txtuserid)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FormUser2"
        Me.Text = "FormUser"
        CType(Me.dguser, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gridcari.ResumeLayout(False)
        Me.gridcari.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnsesudah As System.Windows.Forms.Button
    Friend WithEvents btnsebelum As System.Windows.Forms.Button
    Friend WithEvents btnakhir As System.Windows.Forms.Button
    Friend WithEvents btnawal As System.Windows.Forms.Button
    Friend WithEvents btnkeluar As System.Windows.Forms.Button
    Friend WithEvents btnbatal As System.Windows.Forms.Button
    Friend WithEvents btnhapus As System.Windows.Forms.Button
    Friend WithEvents btnkoreksi As System.Windows.Forms.Button
    Friend WithEvents btnsimpan As System.Windows.Forms.Button
    Friend WithEvents btntambah As System.Windows.Forms.Button
    Friend WithEvents dguser As System.Windows.Forms.DataGridView
    Friend WithEvents cbohakakses As System.Windows.Forms.ComboBox
    Friend WithEvents txtulangipassword As System.Windows.Forms.TextBox
    Friend WithEvents txtpassword As System.Windows.Forms.TextBox
    Friend WithEvents txtnama As System.Windows.Forms.TextBox
    Friend WithEvents txtuserid As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents gridcari As System.Windows.Forms.GroupBox
    Friend WithEvents txtcari As System.Windows.Forms.TextBox
    Friend WithEvents cbcari As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtusername As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
End Class
