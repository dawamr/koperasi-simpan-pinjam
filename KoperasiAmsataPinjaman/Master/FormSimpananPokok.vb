﻿
Imports System.Data
Imports Excel = Microsoft.Office.Interop.Excel

Public Class FormSimpananPokok
    Dim strsql, uuid As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Public uuidSimpPokok As String = ""
    Private Sub clearData()
        txtIDsimpanan.Text = ""
        txtUUIDanggota.Text = ""
        txtidanggota.Text = ""
        Txtnamaanggota.Text = ""
        Txtjabatan.Text = ""
        txtNominal.Text = ""
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        cmdtambah.Visible = x
        cmdsimpan.Visible = Not x
        cmdedit.Visible = x
        cmdbatal.Visible = Not x
    End Sub
    Private Sub aktif(ByVal x As Boolean)

    End Sub

    Private Sub FormSimpananPokok_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tombol(True)
        aktif(False)
        refreshGrid()
        load_cbocari(tabel, Cbocari)
        Cbocari.SelectedIndex = 0

        cmdsimpan.Location = New Point(cmdedit.Location)
        cmdbatal.Location = New Point(cmdtambah.Location)

        txtUUIDanggota.Enabled = True
        txtIDsimpanan.Enabled = False
        txtidanggota.Enabled = False
        Txtnamaanggota.Enabled = False
        Txtjabatan.Enabled = False
        txtNominal.Enabled = False

        cmdedit.Enabled = False
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("SELECT * FROM view_simpanan_pokok ORDER BY IDsimpanan")
            dgsimapanpokok.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgsimapanpokok
                .Columns(index).HeaderText = "UUID"
                index = index + 1
                .Columns(index).HeaderText = "UUID Anggota"
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "ID Simpanan Pokok"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Bayar"
                index = index + 1
                .Columns(index).HeaderText = "Nominal"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "ID Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Nama Anggota"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
            End With
            dgsimapanpokok.DefaultCellStyle.Font = New Font("Candara", 12)
            dgsimapanpokok.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgsimapanpokok.ReadOnly = True
            dgsimapanpokok.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub baca_data()
        Dim index As Integer = 0
        If dgsimapanpokok.RowCount > 0 Then
            With dgsimapanpokok
                Dim baris As Integer = .CurrentRow.Index
                If baris >= dgsimapanpokok.RowCount - 1 Then
                    Exit Sub
                End If
                uuidSimpPokok = CStr(.Item(index, baris).Value)
                index += 1
                txtUUIDanggota.Text = CStr(.Item(index, baris).Value)
                index += 1
                txtIDsimpanan.Text = CStr(.Item(index, baris).Value)
                index += 1
                'tanggal
                index += 1
                txtNominal.Text = CStr(.Item(index, baris).Value)
                index += 1
                'created
                index += 1
                'updated
                index += 1
                txtidanggota.Text = CStr(.Item(index, baris).Value)
                index += 1
                Txtnamaanggota.Text = CStr(.Item(index, baris).Value)
                index += 1
                Txtjabatan.Text = CStr(.Item(index, baris).Value)
                'dst
            End With
        End If
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from ms_anggota where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by IDanggota")
            Me.dgsimapanpokok.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgsimapanpokok
                .Columns(index).HeaderText = "UUID"
                index = index + 1
                .Columns(index).HeaderText = "ID Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Nama Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Jabatan Anggota"
                index = index + 1
                .Columns(index).HeaderText = "No Telpon"
                index = index + 1
                .Columns(index).HeaderText = "Alamat Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Bergabung"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
            End With
            dgsimapanpokok.DefaultCellStyle.Font = New Font("Candara", 12)
            dgsimapanpokok.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgsimapanpokok.ReadOnly = True
            dgsimapanpokok.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub

    Private Sub cmdtambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdtambah.Click
        clearData()
        txtIDsimpanan.Text = getCode("kp_simpanan_pokok", "SP")
        txtNominal.Text = ConfigSimpananPokok()
        tombol(False)
        aktif(True)
        pil = 1 'insert
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FormAnggota.Close()
        FormAnggota.StartPosition = FormStartPosition.CenterScreen
        FormAnggota.Show()
        FormAnggota.cmdkeluar.Visible = True
        FormAnggota.cmdpilih.Visible = True
        FormAnggota.cmdbatal.Visible = False
        FormAnggota.cmdtambah.Visible = False
        FormAnggota.cmdedit.Visible = False
        FormAnggota.cmdSimpan.Visible = False

    End Sub

    Private Sub cmdsimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsimpan.Click
        Dim msql As String
        If pil = 1 Then                         'tambah
            msql = "INSERT INTO kp_simpanan_pokok (uuid, UUIDanggota, IDsimpanan, tanggal, nominal) VALUES ('" & _
                UUID4() & "','" & _
                txtUUIDanggota.Text & "','" & _
                txtIDsimpanan.Text & "','" & _
                DateTime.Now.ToString("yyyy-MM-dd") & "','" & _
                txtNominal.Text & "')"

        Else                                    'koreksi
            If uuidSimpPokok = "" Or uuidSimpPokok = vbNullString Then
                MessageBox.Show("Belum memilih data!")
                Exit Sub
            End If
            Exit Sub
        End If
        If proses.ExecuteNonQuery(msql) = False Then
            Exit Sub
        End If
        refreshGrid()
        tombol(True)
        aktif(False)
        clearData()
        MsgBox("Berhasil disimpan...", vbInformation, "Info")
    End Sub

    Private Sub txtUUIDanggota_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUUIDanggota.TextChanged
        If txtUUIDanggota.Text.Length <= 5 Then
            Exit Sub
        End If
        tabel = proses.ExecuteQuery("SELECT IDanggota, nama, jabatan FROM ms_anggota where uuid = '" & txtUUIDanggota.Text & "'")
        If tabel.Rows.Count < 1 Then
            MsgBox("ID Anggota Tidak Ditemukan!")
        Else
            txtidanggota.Text = tabel.Rows(0).Item(0)
            Txtnamaanggota.Text = tabel.Rows(0).Item(1)
            Txtjabatan.Text = tabel.Rows(0).Item(2)
            txtNominal.Text = ConfigSimpananPokok()
        End If
        txtIDsimpanan.Text = getCode("kp_simpanan_pokok", "SP")
        tombol(False)
        aktif(True)
        pil = 1 'insert
    End Sub

    Private Sub cmdbatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdbatal.Click
        clearData()
        tombol(True)
        aktif(False)
        txtUUIDanggota.Enabled = True
        txtIDsimpanan.Enabled = False
        txtidanggota.Enabled = False
        Txtnamaanggota.Enabled = False
        Txtjabatan.Enabled = False
        txtNominal.Enabled = False
    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
End Class