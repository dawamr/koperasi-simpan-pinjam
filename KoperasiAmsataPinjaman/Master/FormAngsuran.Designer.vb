﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAngsuran
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtTanggalBayar = New System.Windows.Forms.DateTimePicker()
        Me.cmdbatal = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtIDangsuran = New System.Windows.Forms.TextBox()
        Me.cmdedit = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmdsimpan = New System.Windows.Forms.Button()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.dgangsuran = New System.Windows.Forms.DataGridView()
        Me.Txtcari = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Cbocari = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmdtambah = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.txtidanggota = New System.Windows.Forms.TextBox()
        Me.Txtnamaanggota = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Txtjabatan = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtSisaPembayaran = New System.Windows.Forms.TextBox()
        Me.txtTotalPembayaran = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.txtTotalSetor = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cmbBanyakAngsuran = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtTanggalPinjaman = New System.Windows.Forms.DateTimePicker()
        Me.txtIDpinjaman = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.txtAngsuranke = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtUUIDpinjaman = New System.Windows.Forms.TextBox()
        Me.txtNominal = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel6.SuspendLayout()
        CType(Me.dgangsuran, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtTanggalBayar
        '
        Me.txtTanggalBayar.Enabled = False
        Me.txtTanggalBayar.Location = New System.Drawing.Point(160, 113)
        Me.txtTanggalBayar.Name = "txtTanggalBayar"
        Me.txtTanggalBayar.Size = New System.Drawing.Size(180, 23)
        Me.txtTanggalBayar.TabIndex = 61
        '
        'cmdbatal
        '
        Me.cmdbatal.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.keluar
        Me.cmdbatal.Location = New System.Drawing.Point(26, 42)
        Me.cmdbatal.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdbatal.Name = "cmdbatal"
        Me.cmdbatal.Size = New System.Drawing.Size(70, 29)
        Me.cmdbatal.TabIndex = 50
        Me.cmdbatal.UseVisualStyleBackColor = True
        Me.cmdbatal.Visible = False
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(22, 113)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(130, 23)
        Me.Label9.TabIndex = 59
        Me.Label9.Text = "Tanggal Setor"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Button1.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.loupe
        Me.Button1.Location = New System.Drawing.Point(304, 50)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(36, 25)
        Me.Button1.TabIndex = 58
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtIDangsuran
        '
        Me.txtIDangsuran.Enabled = False
        Me.txtIDangsuran.Location = New System.Drawing.Point(160, 20)
        Me.txtIDangsuran.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIDangsuran.Name = "txtIDangsuran"
        Me.txtIDangsuran.Size = New System.Drawing.Size(180, 23)
        Me.txtIDangsuran.TabIndex = 57
        '
        'cmdedit
        '
        Me.cmdedit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdedit.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.edit
        Me.cmdedit.Location = New System.Drawing.Point(104, 8)
        Me.cmdedit.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdedit.Name = "cmdedit"
        Me.cmdedit.Size = New System.Drawing.Size(70, 29)
        Me.cmdedit.TabIndex = 51
        Me.cmdedit.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(22, 20)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(130, 23)
        Me.Label6.TabIndex = 56
        Me.Label6.Text = "IDAngsuran"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdsimpan
        '
        Me.cmdsimpan.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.simpan
        Me.cmdsimpan.Location = New System.Drawing.Point(104, 42)
        Me.cmdsimpan.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdsimpan.Name = "cmdsimpan"
        Me.cmdsimpan.Size = New System.Drawing.Size(70, 29)
        Me.cmdsimpan.TabIndex = 52
        Me.cmdsimpan.UseVisualStyleBackColor = True
        Me.cmdsimpan.Visible = False
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.dgangsuran)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(0, 295)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(820, 235)
        Me.Panel6.TabIndex = 35
        '
        'dgangsuran
        '
        Me.dgangsuran.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgangsuran.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgangsuran.Location = New System.Drawing.Point(0, 0)
        Me.dgangsuran.Name = "dgangsuran"
        Me.dgangsuran.Size = New System.Drawing.Size(820, 235)
        Me.dgangsuran.TabIndex = 0
        '
        'Txtcari
        '
        Me.Txtcari.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Txtcari.Location = New System.Drawing.Point(388, 6)
        Me.Txtcari.Margin = New System.Windows.Forms.Padding(4)
        Me.Txtcari.Name = "Txtcari"
        Me.Txtcari.Size = New System.Drawing.Size(180, 26)
        Me.Txtcari.TabIndex = 46
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Panel7)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 254)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(820, 41)
        Me.Panel5.TabIndex = 34
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Txtcari)
        Me.Panel7.Controls.Add(Me.Label1)
        Me.Panel7.Controls.Add(Me.Cbocari)
        Me.Panel7.Controls.Add(Me.Label2)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel7.Location = New System.Drawing.Point(226, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(594, 41)
        Me.Panel7.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label1.Location = New System.Drawing.Point(306, 10)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 17)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Text Dicari"
        '
        'Cbocari
        '
        Me.Cbocari.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Cbocari.FormattingEnabled = True
        Me.Cbocari.Location = New System.Drawing.Point(114, 6)
        Me.Cbocari.Margin = New System.Windows.Forms.Padding(4)
        Me.Cbocari.Name = "Cbocari"
        Me.Cbocari.Size = New System.Drawing.Size(180, 28)
        Me.Cbocari.TabIndex = 45
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label2.Location = New System.Drawing.Point(15, 10)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 17)
        Me.Label2.TabIndex = 43
        Me.Label2.Text = "Berdasarkan"
        '
        'cmdtambah
        '
        Me.cmdtambah.Enabled = False
        Me.cmdtambah.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cmdtambah.Image = Global.KoperasiAmsataPinjaman.My.Resources.Resources.tambah
        Me.cmdtambah.Location = New System.Drawing.Point(26, 8)
        Me.cmdtambah.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdtambah.Name = "cmdtambah"
        Me.cmdtambah.Size = New System.Drawing.Size(70, 29)
        Me.cmdtambah.TabIndex = 49
        Me.cmdtambah.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Panel8)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 172)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(820, 82)
        Me.Panel2.TabIndex = 33
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.cmdedit)
        Me.Panel8.Controls.Add(Me.cmdtambah)
        Me.Panel8.Controls.Add(Me.cmdsimpan)
        Me.Panel8.Controls.Add(Me.cmdbatal)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel8.Location = New System.Drawing.Point(620, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(200, 82)
        Me.Panel8.TabIndex = 53
        '
        'txtidanggota
        '
        Me.txtidanggota.Location = New System.Drawing.Point(173, 20)
        Me.txtidanggota.Margin = New System.Windows.Forms.Padding(4)
        Me.txtidanggota.Name = "txtidanggota"
        Me.txtidanggota.Size = New System.Drawing.Size(180, 23)
        Me.txtidanggota.TabIndex = 48
        '
        'Txtnamaanggota
        '
        Me.Txtnamaanggota.Location = New System.Drawing.Point(173, 50)
        Me.Txtnamaanggota.Margin = New System.Windows.Forms.Padding(4)
        Me.Txtnamaanggota.Name = "Txtnamaanggota"
        Me.Txtnamaanggota.Size = New System.Drawing.Size(180, 23)
        Me.Txtnamaanggota.TabIndex = 46
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(16, 21)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(150, 23)
        Me.Label8.TabIndex = 47
        Me.Label8.Text = "ID Anggota"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(22, 83)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(130, 23)
        Me.Label7.TabIndex = 53
        Me.Label7.Text = "Nominal"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Txtjabatan
        '
        Me.Txtjabatan.Location = New System.Drawing.Point(173, 81)
        Me.Txtjabatan.Margin = New System.Windows.Forms.Padding(4)
        Me.Txtjabatan.Name = "Txtjabatan"
        Me.Txtjabatan.Size = New System.Drawing.Size(180, 23)
        Me.Txtjabatan.TabIndex = 44
        '
        'Panel3
        '
        Me.Panel3.AutoScroll = True
        Me.Panel3.Controls.Add(Me.txtSisaPembayaran)
        Me.Panel3.Controls.Add(Me.txtTotalPembayaran)
        Me.Panel3.Controls.Add(Me.Label18)
        Me.Panel3.Controls.Add(Me.Label14)
        Me.Panel3.Controls.Add(Me.Panel9)
        Me.Panel3.Controls.Add(Me.txtTotalSetor)
        Me.Panel3.Controls.Add(Me.Label13)
        Me.Panel3.Controls.Add(Me.cmbBanyakAngsuran)
        Me.Panel3.Controls.Add(Me.Label20)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.txtTanggalPinjaman)
        Me.Panel3.Controls.Add(Me.txtIDpinjaman)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.txtidanggota)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Txtnamaanggota)
        Me.Panel3.Controls.Add(Me.Txtjabatan)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(441, 0)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(379, 172)
        Me.Panel3.TabIndex = 51
        '
        'txtSisaPembayaran
        '
        Me.txtSisaPembayaran.Enabled = False
        Me.txtSisaPembayaran.Location = New System.Drawing.Point(173, 269)
        Me.txtSisaPembayaran.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSisaPembayaran.Name = "txtSisaPembayaran"
        Me.txtSisaPembayaran.Size = New System.Drawing.Size(180, 23)
        Me.txtSisaPembayaran.TabIndex = 68
        '
        'txtTotalPembayaran
        '
        Me.txtTotalPembayaran.Enabled = False
        Me.txtTotalPembayaran.Location = New System.Drawing.Point(173, 207)
        Me.txtTotalPembayaran.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalPembayaran.Name = "txtTotalPembayaran"
        Me.txtTotalPembayaran.Size = New System.Drawing.Size(180, 23)
        Me.txtTotalPembayaran.TabIndex = 86
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(16, 204)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(150, 23)
        Me.Label18.TabIndex = 85
        Me.Label18.Text = "Total Pembayaran"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(16, 269)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(150, 23)
        Me.Label14.TabIndex = 67
        Me.Label14.Text = "Sisa Pembayaran"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel9
        '
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel9.Location = New System.Drawing.Point(0, 292)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(362, 29)
        Me.Panel9.TabIndex = 84
        '
        'txtTotalSetor
        '
        Me.txtTotalSetor.Enabled = False
        Me.txtTotalSetor.Location = New System.Drawing.Point(173, 238)
        Me.txtTotalSetor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalSetor.Name = "txtTotalSetor"
        Me.txtTotalSetor.Size = New System.Drawing.Size(180, 23)
        Me.txtTotalSetor.TabIndex = 65
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(16, 238)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(150, 23)
        Me.Label13.TabIndex = 64
        Me.Label13.Text = "Total Setor"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbBanyakAngsuran
        '
        Me.cmbBanyakAngsuran.Enabled = False
        Me.cmbBanyakAngsuran.FormattingEnabled = True
        Me.cmbBanyakAngsuran.Items.AddRange(New Object() {"3", "6", "12"})
        Me.cmbBanyakAngsuran.Location = New System.Drawing.Point(173, 175)
        Me.cmbBanyakAngsuran.Name = "cmbBanyakAngsuran"
        Me.cmbBanyakAngsuran.Size = New System.Drawing.Size(180, 24)
        Me.cmbBanyakAngsuran.TabIndex = 83
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(16, 176)
        Me.Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(150, 23)
        Me.Label20.TabIndex = 82
        Me.Label20.Text = "Banyak Angsuran"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label11.Location = New System.Drawing.Point(16, 145)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(150, 23)
        Me.Label11.TabIndex = 62
        Me.Label11.Text = "Tanggal Pinjaman"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTanggalPinjaman
        '
        Me.txtTanggalPinjaman.Enabled = False
        Me.txtTanggalPinjaman.Location = New System.Drawing.Point(173, 143)
        Me.txtTanggalPinjaman.Name = "txtTanggalPinjaman"
        Me.txtTanggalPinjaman.Size = New System.Drawing.Size(180, 23)
        Me.txtTanggalPinjaman.TabIndex = 63
        '
        'txtIDpinjaman
        '
        Me.txtIDpinjaman.Enabled = False
        Me.txtIDpinjaman.Location = New System.Drawing.Point(173, 112)
        Me.txtIDpinjaman.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIDpinjaman.Name = "txtIDpinjaman"
        Me.txtIDpinjaman.Size = New System.Drawing.Size(180, 23)
        Me.txtIDpinjaman.TabIndex = 59
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(16, 112)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(150, 23)
        Me.Label10.TabIndex = 58
        Me.Label10.Text = "ID Pinjaman"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(16, 81)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(150, 23)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Jabatan Anggota"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(16, 51)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(150, 23)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "Nama Anggota"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(820, 172)
        Me.Panel1.TabIndex = 32
        '
        'Panel4
        '
        Me.Panel4.AutoScroll = True
        Me.Panel4.Controls.Add(Me.Panel10)
        Me.Panel4.Controls.Add(Me.txtAngsuranke)
        Me.Panel4.Controls.Add(Me.Label12)
        Me.Panel4.Controls.Add(Me.txtTanggalBayar)
        Me.Panel4.Controls.Add(Me.Label9)
        Me.Panel4.Controls.Add(Me.Button1)
        Me.Panel4.Controls.Add(Me.txtIDangsuran)
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Controls.Add(Me.txtUUIDpinjaman)
        Me.Panel4.Controls.Add(Me.txtNominal)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(379, 172)
        Me.Panel4.TabIndex = 50
        '
        'Panel10
        '
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel10.Location = New System.Drawing.Point(0, 166)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(362, 29)
        Me.Panel10.TabIndex = 66
        '
        'txtAngsuranke
        '
        Me.txtAngsuranke.Enabled = False
        Me.txtAngsuranke.Location = New System.Drawing.Point(160, 143)
        Me.txtAngsuranke.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAngsuranke.Name = "txtAngsuranke"
        Me.txtAngsuranke.Size = New System.Drawing.Size(180, 23)
        Me.txtAngsuranke.TabIndex = 63
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(22, 143)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(130, 23)
        Me.Label12.TabIndex = 62
        Me.Label12.Text = "Angsuran Ke"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUUIDpinjaman
        '
        Me.txtUUIDpinjaman.Location = New System.Drawing.Point(160, 51)
        Me.txtUUIDpinjaman.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUUIDpinjaman.Name = "txtUUIDpinjaman"
        Me.txtUUIDpinjaman.Size = New System.Drawing.Size(140, 23)
        Me.txtUUIDpinjaman.TabIndex = 45
        '
        'txtNominal
        '
        Me.txtNominal.Enabled = False
        Me.txtNominal.Location = New System.Drawing.Point(160, 82)
        Me.txtNominal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNominal.Name = "txtNominal"
        Me.txtNominal.Size = New System.Drawing.Size(180, 23)
        Me.txtNominal.TabIndex = 55
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(22, 52)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(130, 23)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "UUID Pinjaman"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FormAngsuran
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(820, 530)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FormAngsuran"
        Me.Text = "FormAngusran"
        Me.Panel6.ResumeLayout(False)
        CType(Me.dgangsuran, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtTanggalBayar As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmdbatal As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtIDangsuran As System.Windows.Forms.TextBox
    Friend WithEvents cmdedit As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmdsimpan As System.Windows.Forms.Button
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents dgangsuran As System.Windows.Forms.DataGridView
    Friend WithEvents Txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Cbocari As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdtambah As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents txtidanggota As System.Windows.Forms.TextBox
    Friend WithEvents Txtnamaanggota As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Txtjabatan As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtUUIDpinjaman As System.Windows.Forms.TextBox
    Friend WithEvents txtNominal As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtIDpinjaman As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtTanggalPinjaman As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents cmbBanyakAngsuran As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtTotalPembayaran As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Private WithEvents txtAngsuranke As System.Windows.Forms.TextBox
    Private WithEvents Label12 As System.Windows.Forms.Label
    Private WithEvents txtTotalSetor As System.Windows.Forms.TextBox
    Private WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Private WithEvents txtSisaPembayaran As System.Windows.Forms.TextBox
    Private WithEvents Label14 As System.Windows.Forms.Label
End Class
