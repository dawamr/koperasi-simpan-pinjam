Public Class FormPinjaman
    Dim strsql, uuid As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Public uuidPengajuan, uuidAnggota, uuidPinjaman As String
    Private Sub Formpengajuanpinjm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tombol(True)
        aktif(False)
        refreshGrid()
        load_cbocari(tabel, Cbocari)
        Cbocari.SelectedIndex = 0

        cmdsimpan.Location = New Point(cmdedit.Location)
        cmdbatal.Location = New Point(cmdtambah.Location)
        cmdpilih.Location = New Point(cmdedit.Location)
        cmdkeluar.Location = New Point(cmdtambah.Location)

        txtUUIDpengajuan.Enabled = True
        txtIDpengajuan.Enabled = False
        txtidanggota.Enabled = False
        Txtnamaanggota.Enabled = False
        Txtjabatan.Enabled = False

        cmdedit.Enabled = False
    End Sub
    Private Sub clearData()
        txtIDpengajuan.Text = ""
        txtUUIDpengajuan.Text = ""
        txtidanggota.Text = ""
        Txtnamaanggota.Text = ""
        Txtjabatan.Text = ""
        txtNominal.Text = ""
        txtBungaAngsuran.Text = ""
        txtNominalAngsuran.Text = ""
        txtTotalAngsuran.Text = ""
        cmbStatusPengajuan.Text = ""
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        cmdtambah.Visible = x
        cmdsimpan.Visible = Not x
        cmdedit.Visible = x
        cmdbatal.Visible = Not x
    End Sub
    Private Sub aktif(ByVal x As Boolean)
        txtNominal.Enabled = x
    End Sub

    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("SELECT * FROM view_pinjaman ORDER BY IDpinjaman")
            dgpinjaman.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgpinjaman
                .Columns(index).HeaderText = "UUID"
                index = index + 1
                .Columns(index).HeaderText = "ID Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "UUID Pengajuan"
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "UUID Anggota"
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Nominal Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Banyak Angsuran"
                index = index + 1
                .Columns(index).HeaderText = "Bunga Angsuran"
                index = index + 1
                .Columns(index).HeaderText = "Nominal Angsuran"
                index = index + 1
                .Columns(index).HeaderText = "Total Bayar"
                index = index + 1
                .Columns(index).HeaderText = "Status"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "ID Pengajuan"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Pengajuan"
                index = index + 1
                .Columns(index).HeaderText = "Nominal Pengajuan"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "Status Pengajuan"
                index = index + 1
                .Columns(index).HeaderText = "ID Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Nama Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Jabatan"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
            End With
            dgpinjaman.DefaultCellStyle.Font = New Font("Candara", 12)
            dgpinjaman.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgpinjaman.ReadOnly = True
            dgpinjaman.AutoSizeColumnsMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
            Exit Sub
        End Try
    End Sub
    Private Sub baca_data()
        Dim index As Integer = 0
        If dgpinjaman.RowCount > 0 Then
            With dgpinjaman
                Dim baris As Integer = .CurrentRow.Index
                If baris >= dgpinjaman.RowCount - 1 Then
                    Exit Sub
                End If
                uuidPinjaman = CStr(.Item(index, baris).Value)
                'index += 1
                'txtUUIDanggota.Text = CStr(.Item(index, baris).Value)
                'index += 1
                'txtIDpengajuan.Text = CStr(.Item(index, baris).Value)
                'index += 1
                ''tanggal
                'index += 1
                'txtNominal.Text = CStr(.Item(index, baris).Value)
                'index += 1
                ''created
                'index += 1
                ''updated
                'index += 1
                'txtidanggota.Text = CStr(.Item(index, baris).Value)
                'index += 1
                'Txtnamaanggota.Text = CStr(.Item(index, baris).Value)
                'index += 1
                'Txtjabatan.Text = CStr(.Item(index, baris).Value)
                'dst
            End With
        End If
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from view_pinjaman where " & _
                            Trim(namaKolom) & _
                            " like ('%" & Trim(sCari) & "%') order by IDpengajuan")
            dgpinjaman.DataSource = tabel
            Dim index As Integer = 0
            With Me.dgpinjaman
                .Columns(index).HeaderText = "UUID"
                index = index + 1
                .Columns(index).HeaderText = "ID Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "UUID Pengajuan"
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "UUID Anggota"
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Nominal Pinjaman"
                index = index + 1
                .Columns(index).HeaderText = "Banyak Angsuran"
                index = index + 1
                .Columns(index).HeaderText = "Bunga Angsuran"
                index = index + 1
                .Columns(index).HeaderText = "Nominal Angsuran"
                index = index + 1
                .Columns(index).HeaderText = "Total Bayar"
                index = index + 1
                .Columns(index).HeaderText = "Status"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "ID Pengajuan"
                index = index + 1
                .Columns(index).HeaderText = "Tanggal Pengajuan"
                index = index + 1
                .Columns(index).HeaderText = "Nominal Pengajuan"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).HeaderText = "Status Pengajuan"
                index = index + 1
                .Columns(index).HeaderText = "ID Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Nama Anggota"
                index = index + 1
                .Columns(index).HeaderText = "Jabatan"
                index = index + 1
                .Columns(index).Visible = False
                index = index + 1
                .Columns(index).Visible = False
            End With
            dgpinjaman.DefaultCellStyle.Font = New Font("Candara", 12)
            dgpinjaman.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            dgpinjaman.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub

    Private Sub cmdtambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdtambah.Click
        clearData()
        txtIDpinjaman.Text = getCode("kp_pinjaman", "P")
        tombol(False)
        aktif(True)
        pil = 1 'insert
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Formpengajuanpinjm.Close()
        Formpengajuanpinjm.StartPosition = FormStartPosition.CenterScreen
        Formpengajuanpinjm.Show()
        Formpengajuanpinjm.cmdkeluar.Visible = True
        Formpengajuanpinjm.cmdpilih.Visible = True
        Formpengajuanpinjm.cmdbatal.Visible = False
        Formpengajuanpinjm.cmdtambah.Visible = False
        Formpengajuanpinjm.cmdedit.Visible = False
        Formpengajuanpinjm.cmdsimpan.Visible = False
        Formpengajuanpinjm.Panel1.Visible = False
        Formpengajuanpinjm.refreshGridCari("status", "Menunggu")

    End Sub

    Private Sub cmdsimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsimpan.Click
        Dim msql1, msql2, bungaAngsuran As String
        bungaAngsuran = "1.25"

        If pil = 1 And cmbStatusPengajuan.SelectedItem.ToString = "Disetujui" Then                         'tambah
            msql1 = "UPDATE kp_pengajuan_pinjaman SET " _
               & "status = '" & cmbStatusPengajuan.SelectedItem.ToString _
               & "' WHERE uuid = '" & txtUUIDpengajuan.Text & "';"

            msql2 = "INSERT INTO kp_pinjaman (uuid, IDpinjaman, UUIDPengajuan, UUIDanggota, tanggal, nominal, banyak_angsuran, bunga_angsuran, nominal_angsuran, total_bayar, status) VALUES ('" & _
                UUID4() & "','" & _
                txtIDpinjaman.Text & "','" & _
                txtUUIDpengajuan.Text & "','" & _
                uuidAnggota & "','" & _
                Format(txtTanggalPinjaman.Value, "yyyy-MM-dd") & "','" & _
                txtNominal.Text & "', '" & _
                cmbBanyakAngsuran.SelectedItem.ToString & "', '" & _
                bungaAngsuran & "', '" & _
                txtTotalAngsuran.Text & "', '" & _
                txtTotalPembayaran.Text & "', '" & _
                cmbStatusPinjaman.Text & "')"

            If proses.ExecuteNonQuery(msql1) = False Then
                Exit Sub
            End If

            If proses.ExecuteNonQuery(msql2) = False Then
                Exit Sub
            End If
        ElseIf pil = 1 And cmbStatusPengajuan.SelectedItem.ToString = "Ditolak" Then

            msql1 = "UPDATE kp_pengajuan_pinjaman SET " _
               & "status = '" & cmbStatusPengajuan.SelectedItem.ToString _
               & "' WHERE uuid = '" & txtUUIDpengajuan.Text & "';"

            If proses.ExecuteNonQuery(msql1) = False Then
                Exit Sub
            End If
        ElseIf pil = 1 And cmbStatusPengajuan.SelectedItem.ToString = "Menunggu" Then
            Exit Sub
        Else                                    'koreksi
            If uuidPengajuan = "" Or uuidPengajuan = vbNullString Then
                MessageBox.Show("Belum memilih data!")
                Exit Sub
            End If
            Exit Sub
        End If


        refreshGrid()
        tombol(True)
        aktif(False)
        clearData()
        MsgBox("Berhasil disimpan...", vbInformation, "Info")
    End Sub

    Private Sub txtUUIDpinjaman_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUUIDpengajuan.TextChanged
        If txtUUIDpengajuan.Text.Length <= 5 Then
            Exit Sub
        End If
        tabel = proses.ExecuteQuery("SELECT * FROM view_pengajuan_pinjaman where uuid = '" & txtUUIDpengajuan.Text & "'")
        If tabel.Rows.Count = 0 Then
            MsgBox("Data Pengajuan Tidak Ditemukan!")
        Else
            Dim index As Integer = 0
            Dim bungaangsuran As String
            uuidPengajuan = tabel.Rows(0).Item(index)
            index += 1
            txtIDpengajuan.Text = tabel.Rows(0).Item(index)
            index += 1
            uuidAnggota = tabel.Rows(0).Item(index)
            index += 1
            txtTanggalPengajuan.Text = tabel.Rows(0).Item(index)
            index += 1
            txtNominal.Text = tabel.Rows(0).Item(index)
            txtNominalPengajuan.Text = tabel.Rows(0).Item(index)
            index += 1
            cmbBanyakAngsuran.Text = tabel.Rows(0).Item(index)
            index += 1
            bungaangsuran = tabel.Rows(0).Item(index)
            index += 1
            cmbStatusPengajuan.Text = tabel.Rows(0).Item(index)
            index += 1
            'CREATED
            index += 1
            'UPDATED
            index += 1
            txtidanggota.Text = tabel.Rows(0).Item(index)
            index += 1
            Txtnamaanggota.Text = tabel.Rows(0).Item(index)
            index += 1
            Txtjabatan.Text = tabel.Rows(0).Item(index)
            tombol(False)
            aktif(True)
            HitungAngsuran()
            txtTotalPembayaran.Text = Convert.ToInt32(txtTotalAngsuran.Text) * Convert.ToInt32(cmbBanyakAngsuran.Text)
            cmbStatusPinjaman.Text = "Belum Lunas"
            txtIDpinjaman.Text = getCode("kp_pinjaman", "P")
            pil = 1 'insert
        End If
    End Sub

    Private Sub cmdbatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdbatal.Click
        clearData()
        tombol(True)
        aktif(False)
        txtUUIDpengajuan.Enabled = True
        txtIDpengajuan.Enabled = False
        txtidanggota.Enabled = False
        Txtnamaanggota.Enabled = False
        Txtjabatan.Enabled = False
    End Sub

    Private Sub cmbBanyakAngsuran_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        HitungAngsuran()
    End Sub


    Private Sub HitungAngsuran()
        Dim bungaangsr, angsrblnpokok As Integer
        If cmbBanyakAngsuran.Text.Length > 0 And txtNominal.Text.Length > 0 Then
            angsrblnpokok = Convert.ToInt32(txtNominal.Text) / Convert.ToInt32(cmbBanyakAngsuran.SelectedItem.ToString)
            bungaangsr = Convert.ToInt32(txtNominal.Text) * (1.25 / 100)

            txtBungaAngsuran.Text = bungaangsr
            txtNominalAngsuran.Text = angsrblnpokok
            txtTotalAngsuran.Text = pembualatanNominal(angsrblnpokok + bungaangsr)
            'Dtpelunasan.Value = datelunas
        Else
            MsgBox("Nominal & Banyak Angsuran Wajib!")
            Exit Sub
        End If

    End Sub

    Private Sub txtNominal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNominal.TextChanged
        If txtNominal.Text.Length > 4 And cmbBanyakAngsuran.Text.Length > 0 Then
            HitungAngsuran()
        End If
    End Sub

    Private Sub Txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Txtcari.TextChanged
        If Txtcari.Text.Length < 1 Then
            Exit Sub
        End If
        refreshGridCari(Cbocari.SelectedItem.ToString, Txtcari.Text)
    End Sub

    Private Sub cmdkeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdkeluar.Click
        Me.Close()
    End Sub

    Private Sub cmdpilih_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdpilih.Click
        If uuidPinjaman = "" Then
            MsgBox("Anda belum memilih Pinjaman!")
            Exit Sub
        End If
        FormAngsuran.txtUUIDpinjaman.Text = uuidPinjaman
        Me.Close()
    End Sub

    Private Sub dgpinjaman_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgpinjaman.CellContentClick
        clearData()
        baca_data()
    End Sub
End Class