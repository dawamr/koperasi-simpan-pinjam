﻿Public Class FormUser
    Dim strsql As String
    Dim proses As New ClsKoneksi
    Dim tabel As DataTable
    Dim pil As Byte = 1 '1=insert/data baru, 2=update
    Dim i As Integer = 0
    Private Sub FormUser_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        tombol(True)
        aktif(False)
    End Sub
    Private Sub FormUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshGrid()
        baca_data(0)
        load_cbocari(tabel, Cbocari)
        Cbocari.SelectedIndex = 0
        Cbohakakses.Items.Add("Administrasi")
        Cbohakakses.Items.Add("Kepala")
        Cbohakakses.Items.Add("Pengawas")
        Cbohakakses.Items.Add("Staf Simpan Pinjam")
        Cbohakakses.Items.Add("Staf Pembelian")
        Cbohakakses.Items.Add("Staf Penjualan")
    End Sub
    Private Sub tombol(ByVal x As Boolean)
        cmdtambah.Enabled = x
        cmdsimpan.Enabled = Not x
        cmdedit.Enabled = x
        cmdhapus.Enabled = x
        cmdbatal.Enabled = Not x
        cmdkeluar.Enabled = x
    End Sub

    Private Sub aktif(ByVal x As Boolean)
        txtuserid.Enabled = x
        txtnama.Enabled = x
        txtpassword.Enabled = x
        txtulangi.Enabled = x
        cbohakakses.Enabled = x
    End Sub
    Private Sub refreshGrid()
        Try
            tabel = proses.ExecuteQuery("select * from view_auth order by nama")
            Me.Dguser.DataSource = tabel
            Dim index As Integer = 0
            With Me.dguser
                .Columns(index).HeaderText = "UUID"
                index += 1
                .Columns(index).HeaderText = "ID Pengguna"
                index += 1
                .Columns(index).HeaderText = "username"
                index += 1
                .Columns(index).HeaderText = "Nama"
                index += 1
                .Columns(index).HeaderText = "Password"
                index += 1
                .Columns(index).HeaderText = "ID Role"
                index += 1
                ' created
                index += 1
                'updated
                index += 1
                .Columns(index).HeaderText = "Akses"
            End With
            Dguser.DefaultCellStyle.Font = New Font("Candara", 12)
            Dguser.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            Dguser.ReadOnly = True
            Dguser.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub refreshGridCari(ByVal namaKolom As String, ByVal sCari As String)
        Try
            tabel = proses.ExecuteQuery("select * from view_auth where " & _
            Trim(namaKolom) & _
            " like ('%" & Trim(sCari) & "%') order by name")

            Dim index As Integer = 0
            With Me.Dguser
                .Columns(index).HeaderText = "UUID"
                index += 1
                .Columns(index).HeaderText = "ID Pengguna"
                index += 1
                .Columns(index).HeaderText = "username"
                index += 1
                .Columns(index).HeaderText = "Nama"
                index += 1
                .Columns(index).HeaderText = "Password"
                index += 1
                .Columns(index).HeaderText = "ID Role"
                index += 1
                ' created
                index += 1
                'updated
                index += 1
                .Columns(index).HeaderText = "Akses"
            End With
            Dguser.DefaultCellStyle.Font = New Font("Candara", 12)
            Dguser.RowHeadersDefaultCellStyle.Font = New Font("Candara", 12, FontStyle.Bold)
            Dguser.ReadOnly = True
            Dguser.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Exclamation, "Warning")
        End Try
    End Sub
    Private Sub baca_data(ByVal posisi As Integer)
        If posisi = 0 Then
            Txtuserid.Text = IIf(IsDBNull(Dguser.Rows(0).Cells(1).Value) = True, "-", _
            Dguser.Rows(0).Cells(1).Value) 'user id

            txtnama.Text = IIf(IsDBNull(dguser.Rows(0).Cells(2).Value) = True, "-", _
            dguser.Rows(0).Cells(2).Value) 'nama
            Txtpassword.Text = IIf(IsDBNull(Dguser.Rows(0).Cells(3).Value) = True, "- ", _
            Dguser.Rows(0).Cells(3).Value) 'pass
            cbohakakses.Text = dguser.Rows(0).Cells(4).Value 'hak akses
        Else
            txtuserid.Text =
            IIf(IsDBNull(dguser.Rows(dguser.CurrentCell.RowIndex).Cells(1).Value) = True, _
            "-", dguser.Rows(dguser.CurrentCell.RowIndex).Cells(1).Value)
            txtnama.Text =
            IIf(IsDBNull(dguser.Rows(dguser.CurrentCell.RowIndex).Cells(2).Value) = True, _
            "-", dguser.Rows(dguser.CurrentCell.RowIndex).Cells(2).Value)
            txtpassword.Text =
            IIf(IsDBNull(dguser.Rows(dguser.CurrentCell.RowIndex).Cells(3).Value) = True, _
            "-", dguser.Rows(dguser.CurrentCell.RowIndex).Cells(3).Value)
            cbohakakses.Text = dguser.Rows(dguser.CurrentCell.RowIndex).Cells(4).Value
        End If
    End Sub
    Private Sub kosong()
        txtuserid.Text = ""
        txtnama.Text = ""
        txtpassword.Text = ""
        txtulangi.Text = ""
        cbohakakses.Text = ""
    End Sub
    Private Sub cmdtambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdtambah.Click
        tombol(False)
        aktif(True)
        kosong()
        pil = 1 'insert
    End Sub
    Private Sub cmdcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdbatal.Click
        tombol(True)
        aktif(False)
    End Sub
    Private Sub cmdsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsimpan.Click
        Dim msql As String
        If pil = 1 Then 'tambah
            msql = "insert into user (user_id,name,password,hak_akses)" _
            & "values('" & Txtuserid.Text _
            & "','" & Txtnama.Text & "','" & _
            Txtpassword.Text & "','" & IIf(Cbohakakses.Text = "Administrator", 0,
            IIf(Cbohakakses.Text = "Operator", 1, 2)) & "')"
        Else 'koreksi
            msql = "update user set name='" & _
            Txtnama.Text _
            & "',password='" & Txtpassword.Text & "',hak_akses='" &
            IIf(Cbohakakses.Text = "Administrator", 0, IIf(Cbohakakses.Text = "Operator", 1, 2)) _
            & "' where user_id='" & Txtuserid.Text & "'"
        End If
        proses.ExecuteNonQuery(msql)
        tombol(True)
        aktif(False)
        refreshGrid()
        MsgBox("Saved Succsessfully", vbInformation, "Info")
    End Sub
    Private Sub dguser_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Dguser.CellContentClick
        baca_data(1)
    End Sub
    Private Sub dguser_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles Dguser.CellMouseClick
        baca_data(1)
    End Sub
    Private Sub cmdhapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdhapus.Click
        Dim psn As Long
        psn = MsgBox("Are you sure you want to delete this data?", vbQuestion +
        vbYesNo, "Question")
        If psn = vbYes Then
            proses.ExecuteNonQuery("delete from user where user_id='" & _
            Txtuserid.Text & "'")
        End If
        refreshGrid()
    End Sub
    Private Sub cmdedit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdedit.Click
        pil = 2
        aktif(True)
        tombol(False)
        Txtuserid.Enabled = False
        Txtnama.Focus()
    End Sub
    Private Sub txtcari_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Txtcari.TextChanged
        refreshGridCari(Cbocari.Text, Txtcari.Text)
    End Sub
    Private Sub btnexit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdkeluar.Click
        Me.Close()
    End Sub
    Private Sub txtulangi_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Txtulangi.KeyPress
        If (e.KeyChar = Chr(13)) Then
            If Txtulangi.Text = Txtpassword.Text Then
                Cbohakakses.Focus()
            Else
                MsgBox("Password is diferent!!!", vbCritical, "Warning")
                Exit Sub
            End If
        End If
    End Sub
End Class