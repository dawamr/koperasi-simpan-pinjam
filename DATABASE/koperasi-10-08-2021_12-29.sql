-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2021 at 07:28 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koperasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `KodeAkun` char(4) NOT NULL,
  `NamaAkun` varchar(50) DEFAULT NULL,
  `SaldoAwal` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`KodeAkun`, `NamaAkun`, `SaldoAwal`) VALUES
('1101', 'Kas', 0),
('1103', 'Piutang', 0),
('1108', 'Perlengkapan', 0),
('1110', 'Sewa dibayar dimuka', 0),
('2101', 'Hutang BCA', 0),
('2102', 'Hutang Usaha', 0),
('3101', 'Modal', 12172000),
('3102', 'Prive', 0),
('3103', 'Laba/Rugi', 17172000),
('4111', 'Pendapatan Jasa', 0),
('6101', 'Beban Gaji', 0),
('6108', 'Beban Sewa', 0),
('6109', 'Beban Perlengkapan', 0),
('6110', 'Beban Listrik', 0),
('6111', 'Beban Air', 0),
('6120', 'Beban Adm', 0);

-- --------------------------------------------------------

--
-- Table structure for table `akun_aruskas`
--

CREATE TABLE `akun_aruskas` (
  `kodeakun` varchar(6) NOT NULL,
  `grup` tinyint(1) NOT NULL DEFAULT '1',
  `jenis` char(1) NOT NULL,
  `jenisSum` char(1) NOT NULL,
  `jumlah` decimal(14,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun_aruskas`
--

INSERT INTO `akun_aruskas` (`kodeakun`, `grup`, `jenis`, `jenisSum`, `jumlah`) VALUES
('1103', 1, 'D', 'K', '0.00'),
('1108', 1, 'K', 'D', '0.00'),
('1110', 1, 'K', 'D', '0.00'),
('2101', 3, 'D', 'K', '0.00'),
('2102', 2, 'D', 'K', '0.00'),
('3101', 3, 'D', 'K', '0.00'),
('3102', 3, 'K', 'D', '0.00'),
('3103', 1, 'K', 'D', '0.00'),
('4111', 1, 'D', 'K', '0.00'),
('6101', 1, 'K', 'D', '0.00'),
('6108', 1, 'K', 'D', '0.00'),
('6109', 1, 'K', 'D', '0.00'),
('6110', 1, 'K', 'D', '0.00'),
('6111', 1, 'K', 'D', '0.00'),
('6120', 1, 'K', 'D', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kd_brg` char(6) NOT NULL,
  `nm_brg` varchar(30) NOT NULL,
  `harga` double NOT NULL,
  `harga_beli` double NOT NULL,
  `stok` int(4) NOT NULL,
  `stok_min` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kd_brg`, `nm_brg`, `harga`, `harga_beli`, `stok`, `stok_min`) VALUES
('B0001', 'Chopper Mitochiba', 900000, 830000, 42, 3),
('B0002', 'LED TV Toshiba 24\"', 1800000, 1600000, 18, 2);

-- --------------------------------------------------------

--
-- Table structure for table `bayar_hutang`
--

CREATE TABLE `bayar_hutang` (
  `no_bayar` int(11) NOT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `no_beli` varchar(30) NOT NULL,
  `jml_hutang` decimal(10,0) NOT NULL DEFAULT '0',
  `jml_bayar` decimal(10,0) NOT NULL DEFAULT '0',
  `sisa_hutang` decimal(10,0) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bayar_piutang`
--

CREATE TABLE `bayar_piutang` (
  `no_bayar` int(11) NOT NULL,
  `no_jual` varchar(30) NOT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `jml_bayar` double DEFAULT NULL,
  `jml_piutang` double DEFAULT NULL,
  `sisa_piutang` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `beli`
--

CREATE TABLE `beli` (
  `no_beli` varchar(50) NOT NULL DEFAULT '''''',
  `kd_sup` char(6) NOT NULL DEFAULT '''''',
  `tgl_beli` date DEFAULT NULL,
  `jml_hutang` decimal(10,0) NOT NULL,
  `jml_bayar` decimal(10,0) NOT NULL,
  `sisa_hutang` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beli`
--

INSERT INTO `beli` (`no_beli`, `kd_sup`, `tgl_beli`, `jml_hutang`, `jml_bayar`, `sisa_hutang`) VALUES
('B00001', 'S0001', '2021-08-10', '1660000', '0', '1660000'),
('B00002', 'S0001', '2021-08-10', '1660000', '0', '1660000'),
('B00003', 'S0001', '2021-08-10', '3320000', '0', '3320000');

-- --------------------------------------------------------

--
-- Table structure for table `dbeli`
--

CREATE TABLE `dbeli` (
  `no_beli` varchar(50) NOT NULL DEFAULT '''''',
  `kd_brg` char(6) NOT NULL,
  `nm_brg` varchar(30) NOT NULL,
  `harga_beli` float DEFAULT NULL,
  `jml_beli` int(4) DEFAULT NULL,
  `total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbeli`
--

INSERT INTO `dbeli` (`no_beli`, `kd_brg`, `nm_brg`, `harga_beli`, `jml_beli`, `total`) VALUES
('B00001', 'B0001', 'Chopper Mitochiba', 830000, 4, 3320000),
('B00002', 'B0001', 'Chopper Mitochiba', 830000, 2, 1660000),
('B00003', 'B0001', 'Chopper Mitochiba', 830000, 4, 3320000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_jurnal`
--

CREATE TABLE `detail_jurnal` (
  `NoJurnal` varchar(30) NOT NULL,
  `KodeAkun` varchar(4) NOT NULL,
  `Debit` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Kredit` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_jurnal`
--

INSERT INTO `detail_jurnal` (`NoJurnal`, `KodeAkun`, `Debit`, `Kredit`) VALUES
('JU0001', '1101', '45000000.00', '0.00'),
('JU0001', '3101', '0.00', '45000000.00'),
('JU0002', '1101', '25000000.00', '0.00'),
('JU0002', '2101', '0.00', '25000000.00'),
('JU0003', '1101', '0.00', '7000000.00'),
('JU0003', '1108', '7000000.00', '0.00'),
('JU0004', '1101', '0.00', '5000000.00'),
('JU0004', '3102', '5000000.00', '0.00'),
('JU0005', '1108', '20000000.00', '0.00'),
('JU0005', '2102', '0.00', '20000000.00'),
('JU0006', '1101', '15000000.00', '0.00'),
('JU0006', '4111', '0.00', '15000000.00'),
('JU0007', '1108', '9700000.00', '0.00'),
('JU0007', '2102', '0.00', '9700000.00'),
('JU0008', '1101', '0.00', '6200000.00'),
('JU0008', '6101', '6200000.00', '0.00'),
('JU0009', '1101', '11000000.00', '0.00'),
('JU0009', '4111', '0.00', '11000000.00'),
('JU0010', '1101', '0.00', '8000000.00'),
('JU0010', '1108', '8000000.00', '0.00'),
('JU0011', '1101', '0.00', '720000.00'),
('JU0011', '6110', '600000.00', '0.00'),
('JU0011', '6111', '120000.00', '0.00'),
('JU0012', '1101', '0.00', '7500000.00'),
('JU0012', '1110', '7500000.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `djual`
--

CREATE TABLE `djual` (
  `id` int(11) NOT NULL,
  `no_jual` varchar(30) NOT NULL,
  `kd_brg` varchar(6) NOT NULL,
  `hpp` decimal(10,2) DEFAULT NULL,
  `harga` decimal(10,2) DEFAULT NULL,
  `jml_jual` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `djual`
--

INSERT INTO `djual` (`id`, `no_jual`, `kd_brg`, `hpp`, `harga`, `jml_jual`) VALUES
(2, 'FP0000000220200402012851', 'B00003', NULL, '15000.00', 10),
(3, 'FP0000000220200402012851', 'B00001', NULL, '2000.00', 15),
(4, 'FP0000000320200409224651', 'B00004', NULL, '3000.00', 25),
(5, 'FP0000000320200409224651', 'B00002', NULL, '300.00', 120),
(6, 'FP0000000320200409224651', 'B00001', NULL, '2000.00', 0),
(7, 'FP0000000420200409230642', 'B00001', NULL, '2000.00', 15),
(8, 'FP0000000420200409230642', 'B00003', NULL, '15000.00', 10),
(9, 'FP0000000420200409230642', 'B00004', NULL, '3000.00', 30),
(10, 'FP0000000520200410072324', 'B00001', NULL, '2000.00', 10),
(11, 'FP0000000520200410072324', 'B00002', NULL, '300.00', 200),
(12, 'FP0000000520200410072324', 'B00004', NULL, '3000.00', 120),
(13, 'FP0000000620200617214517', 'B00005', NULL, '3500.00', 100),
(14, 'FP0000000720200618090430', 'B00005', NULL, '3500.00', 200),
(15, 'FP0000000820200625084318', 'B00003', NULL, '15000.00', 200),
(16, 'FP0000000920200701181345', 'B00003', NULL, '15000.00', 10),
(17, 'NB0006', 'B00002', '0.00', '15000000.00', 1),
(18, 'NB0010', 'B00003', '0.00', '11000000.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dreturbeli`
--

CREATE TABLE `dreturbeli` (
  `no_retur` varchar(30) NOT NULL,
  `kd_brg` char(6) NOT NULL,
  `harga_beli` float NOT NULL,
  `jml_retur` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dreturjual`
--

CREATE TABLE `dreturjual` (
  `no_retur` varchar(30) NOT NULL,
  `kd_brg` varchar(6) NOT NULL,
  `harga_jual` float NOT NULL,
  `jml_retur` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dreturjual`
--

INSERT INTO `dreturjual` (`no_retur`, `kd_brg`, `harga_jual`, `jml_retur`) VALUES
('RP0000000120200415074125', 'B00001', 2000, 20),
('RP0000000220200415074931', 'B00004', 3000, 10),
('RP0000000320200415074957', 'B00004', 3000, 30),
('RP0000000420200617214908', 'B00004', 3000, 5);

-- --------------------------------------------------------

--
-- Table structure for table `hutang`
--

CREATE TABLE `hutang` (
  `no_hutang` int(11) NOT NULL,
  `no_beli` varchar(30) DEFAULT NULL,
  `tgl_hutang` datetime NOT NULL,
  `jml_hutang` decimal(10,0) NOT NULL,
  `jml_bayar` decimal(10,0) NOT NULL,
  `sisa_hutang` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hutang`
--

INSERT INTO `hutang` (`no_hutang`, `no_beli`, `tgl_hutang`, `jml_hutang`, `jml_bayar`, `sisa_hutang`) VALUES
(1, 'FB0000000520200618082739', '2020-06-18 00:00:00', '850000', '0', '850000'),
(2, 'FB0000000620200618084019', '2020-06-18 00:00:00', '700000', '600000', '100000'),
(3, 'FB0000000720200625084134', '2020-06-25 00:00:00', '400000', '0', '400000'),
(4, 'FB0000000820200625084200', '2020-06-25 00:00:00', '550000', '0', '550000'),
(5, 'FB0000000920200625084248', '2020-06-25 00:00:00', '1000000', '0', '1000000');

-- --------------------------------------------------------

--
-- Table structure for table `jual`
--

CREATE TABLE `jual` (
  `no_jual` varchar(30) NOT NULL,
  `kd_konsumen` char(6) NOT NULL,
  `tgl_jual` date DEFAULT NULL,
  `jenis` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jual`
--

INSERT INTO `jual` (`no_jual`, `kd_konsumen`, `tgl_jual`, `jenis`) VALUES
('FP0000000120200402012531', 'K00003', '2020-04-02', 0),
('FP0000000220200402012851', 'K00003', '2020-04-02', 0),
('FP0000000320200409224651', 'K00002', '2020-04-09', 0),
('FP0000000420200409230642', 'K00003', '2020-04-09', 0),
('FP0000000520200410072324', 'K00001', '2020-04-10', 0),
('FP0000000620200617214517', 'K00004', '2020-06-17', 0),
('FP0000000720200618090430', 'K00004', '2020-06-18', 1),
('FP0000000820200625084318', 'K00004', '2020-06-25', 1),
('FP0000000920200701181345', 'K00003', '2020-07-01', 1),
('NB0006', 'K00001', '2021-02-18', 0),
('NB0010', 'K00002', '2021-02-18', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jurnal`
--

CREATE TABLE `jurnal` (
  `NoJurnal` varchar(8) NOT NULL,
  `Tanggal` datetime NOT NULL,
  `NoBukti` varchar(60) NOT NULL,
  `Uraian` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal`
--

INSERT INTO `jurnal` (`NoJurnal`, `Tanggal`, `NoBukti`, `Uraian`) VALUES
('JU0001', '2021-02-18 14:34:28', 'NB0001', 'Setor Modal'),
('JU0002', '2021-02-18 14:34:28', 'NB0002', 'Menambah Modal dari Hutang'),
('JU0003', '2021-02-18 14:34:28', 'NB0003', 'Membeli Perlengkapan'),
('JU0004', '2021-02-18 14:34:28', 'NB0004', 'Prive'),
('JU0005', '2021-02-18 14:34:28', 'NB0005', 'Membeli perlengkapan kredit'),
('JU0006', '2021-02-18 14:34:28', 'NB0006', 'Pendapatan Jasa'),
('JU0007', '2021-02-18 14:34:28', 'NB0007', 'Membeli perlengkapan secara kredit'),
('JU0008', '2021-02-18 14:34:28', 'NB0008', 'Membayar Gaji'),
('JU0009', '2021-02-18 14:34:28', 'NB0009', 'Pendapatan Jasa'),
('JU0010', '2021-02-18 14:34:28', 'NB0010', 'Membeli Perlengkapan'),
('JU0011', '2021-02-18 14:34:28', 'NB0011', 'Membayar Listrik dan Air'),
('JU0012', '2021-02-18 14:34:28', 'NB0012', 'Membayar Sewa');

-- --------------------------------------------------------

--
-- Table structure for table `konsumen`
--

CREATE TABLE `konsumen` (
  `kd_konsumen` char(6) NOT NULL,
  `nm_konsumen` char(25) NOT NULL,
  `alamat` char(60) NOT NULL,
  `kota` char(25) NOT NULL,
  `kd_pos` char(6) NOT NULL,
  `phone` char(15) NOT NULL,
  `email` char(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konsumen`
--

INSERT INTO `konsumen` (`kd_konsumen`, `nm_konsumen`, `alamat`, `kota`, `kd_pos`, `phone`, `email`) VALUES
('K00001', 'PT KERMBAR', 'JL MULAWARMAN 9', 'SEMARANG', '59513', '0812243785', 'kermbar@gmail.com'),
('K00002', 'PT MERDEKA', 'JL MAJAPAHIT', 'SEMARANG', '59564', '0853378271', 'merdeka@yahoo.co.id'),
('K00003', 'PT TERANG JAYA', 'JL SINGOSARI 4', 'SEMARANG', '59456', '0812245378', 'terangjaya@gmail.com'),
('K00004', 'PT MEGA BUNGA', 'JL FATMAWATI', 'SEMARANG', '59987', '0821356879', 'megabunga@gmail.com'),
('K00005', 'PT KUSUMA', 'JL PUSPONJOLO 3', 'SEMARANG', '59111', '08122317890', 'kusuma@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `kp_angsuran`
--

CREATE TABLE `kp_angsuran` (
  `uuid` varchar(50) NOT NULL,
  `UUIDpinjaman` varchar(50) NOT NULL,
  `UUIDanggota` varchar(50) NOT NULL,
  `IDangsuran` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `nominal` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kp_angsuran`
--

INSERT INTO `kp_angsuran` (`uuid`, `UUIDpinjaman`, `UUIDanggota`, `IDangsuran`, `tanggal`, `nominal`, `created`, `updated`) VALUES
('05dddc6d-995e-460b-87f3-44afd9552b33', 'fc3c7df4-419b-4988-80e0-6a259239754f', '1fe836ce-c530-4d65-bb2c-32c58a75ab22', 'PA00003', '2021-08-08', 104000, '2021-08-07 17:02:23', '2021-08-07 17:02:23'),
('3e97a7ea-a966-40eb-846d-6151307b210a', 'fc3c7df4-419b-4988-80e0-6a259239754f', '1fe836ce-c530-4d65-bb2c-32c58a75ab22', 'PA00002', '2021-08-07', 104000, '2021-08-07 09:19:43', '2021-08-07 09:19:43'),
('b42fe1b3-1c51-42e4-be37-c53fa1002e3b', '73b3431f-cc97-4a3d-8a89-74f3b4f2a608', '0a479cc7-a3c5-4460-8f48-e689ae9bf43e', 'PA00005', '2021-08-08', 21000, '2021-08-07 17:05:34', '2021-08-07 17:05:34'),
('b5afadec-44e1-48f2-ac38-f9180b9c371e', '73b3431f-cc97-4a3d-8a89-74f3b4f2a608', '0a479cc7-a3c5-4460-8f48-e689ae9bf43e', 'PA00004', '2021-08-08', 21000, '2021-08-07 17:05:25', '2021-08-07 17:05:25'),
('df4c14ce-025f-4691-98f9-a2ad5247bbfe', '73b3431f-cc97-4a3d-8a89-74f3b4f2a608', '0a479cc7-a3c5-4460-8f48-e689ae9bf43e', 'PA00006', '2021-08-08', 21000, '2021-08-07 17:05:50', '2021-08-07 17:05:50'),
('fbb2dd50-922c-44ac-b099-c60fb76a447a', 'fc3c7df4-419b-4988-80e0-6a259239754f', '1fe836ce-c530-4d65-bb2c-32c58a75ab22', 'PA00001', '2021-08-07', 104000, '2021-08-07 09:13:58', '2021-08-07 09:13:58');

-- --------------------------------------------------------

--
-- Table structure for table `kp_pengajuan_pinjaman`
--

CREATE TABLE `kp_pengajuan_pinjaman` (
  `uuid` varchar(50) NOT NULL,
  `IDpengajuan` varchar(50) NOT NULL,
  `UUIDanggota` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `nominal` int(11) NOT NULL,
  `banyak_angsuran` smallint(4) NOT NULL,
  `bunga_angsuran` double NOT NULL,
  `status` enum('Ditolak','Disetujui','Menunggu') NOT NULL DEFAULT 'Menunggu',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kp_pengajuan_pinjaman`
--

INSERT INTO `kp_pengajuan_pinjaman` (`uuid`, `IDpengajuan`, `UUIDanggota`, `tanggal`, `nominal`, `banyak_angsuran`, `bunga_angsuran`, `status`, `created`, `updated`) VALUES
('111fad0a-14f0-47a3-a2e8-1f982f36efe4', 'PP00002', '1fe836ce-c530-4d65-bb2c-32c58a75ab22', '2021-08-07', 100000, 3, 1.25, 'Ditolak', '2021-08-06 17:13:54', '2021-08-06 17:13:54'),
('7b46853b-ad3a-4d98-bf5c-9dc171ea7672', 'PP00004', '0a479cc7-a3c5-4460-8f48-e689ae9bf43e', '2021-08-07', 60000, 3, 1.25, 'Disetujui', '2021-08-07 07:57:46', '2021-08-07 07:57:46'),
('80faf421-1f7d-4351-814a-dec9b0540ce7', 'PP00001', '1fe836ce-c530-4d65-bb2c-32c58a75ab22', '2021-08-06', 300000, 3, 1.25, 'Disetujui', '2021-08-06 13:23:48', '2021-08-06 13:23:48'),
('8efb44f9-3757-48a8-aade-081766134fe6', 'PP00003', '1fe836ce-c530-4d65-bb2c-32c58a75ab22', '2021-08-07', 500000, 3, 1.25, 'Disetujui', '2021-08-07 03:48:04', '2021-08-07 03:48:04');

-- --------------------------------------------------------

--
-- Table structure for table `kp_pinjaman`
--

CREATE TABLE `kp_pinjaman` (
  `uuid` varchar(50) NOT NULL,
  `IDPinjaman` varchar(50) NOT NULL,
  `UUIDpengajuan` varchar(50) NOT NULL,
  `UUIDanggota` varchar(50) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `nominal` int(11) NOT NULL,
  `banyak_angsuran` tinyint(3) DEFAULT NULL,
  `bunga_angsuran` double NOT NULL,
  `nominal_angsuran` int(11) NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `status` enum('Lunas','Belum Lunas') NOT NULL DEFAULT 'Belum Lunas',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kp_pinjaman`
--

INSERT INTO `kp_pinjaman` (`uuid`, `IDPinjaman`, `UUIDpengajuan`, `UUIDanggota`, `tanggal`, `nominal`, `banyak_angsuran`, `bunga_angsuran`, `nominal_angsuran`, `total_bayar`, `status`, `created`, `updated`) VALUES
('29711573-d719-495f-8ce6-120d0f806bbd', 'P00002', '8efb44f9-3757-48a8-aade-081766134fe6', '1fe836ce-c530-4d65-bb2c-32c58a75ab22', '2021-08-07', 500000, 3, 1.25, 166667, 519000, 'Belum Lunas', '2021-08-07 03:48:34', '2021-08-07 03:48:34'),
('73b3431f-cc97-4a3d-8a89-74f3b4f2a608', 'P00003', '7b46853b-ad3a-4d98-bf5c-9dc171ea7672', '0a479cc7-a3c5-4460-8f48-e689ae9bf43e', '2021-08-07', 60000, 3, 1.25, 21000, 63000, 'Lunas', '2021-08-07 07:59:57', '2021-08-07 17:05:53'),
('fc3c7df4-419b-4988-80e0-6a259239754f', 'P00001', '80faf421-1f7d-4351-814a-dec9b0540ce7', '1fe836ce-c530-4d65-bb2c-32c58a75ab22', '2021-08-07', 300000, 3, 1.25, 104000, 312000, 'Lunas', '2021-08-07 01:24:07', '2021-08-07 16:58:55');

-- --------------------------------------------------------

--
-- Table structure for table `kp_simpanan_pokok`
--

CREATE TABLE `kp_simpanan_pokok` (
  `uuid` varchar(50) NOT NULL,
  `UUIDanggota` varchar(50) NOT NULL,
  `IDsimpanan` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `nominal` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kp_simpanan_pokok`
--

INSERT INTO `kp_simpanan_pokok` (`uuid`, `UUIDanggota`, `IDsimpanan`, `tanggal`, `nominal`, `created`, `updated`) VALUES
('0fbf40c7-a470-4dbd-98ed-5c11b89f496b', '0a479cc7-a3c5-4460-8f48-e689ae9bf43e', 'SP00002', '2021-08-06', 10000, '2021-08-06 13:21:56', '2021-08-06 13:21:56'),
('17f6429c-9222-4203-9ca4-7d38a192f3e6', '1fe836ce-c530-4d65-bb2c-32c58a75ab22', 'SP00001', '2021-08-06', 10000, '2021-08-06 13:13:36', '2021-08-06 13:13:36');

-- --------------------------------------------------------

--
-- Table structure for table `kp_simpanan_sukarela`
--

CREATE TABLE `kp_simpanan_sukarela` (
  `uuid` varchar(50) NOT NULL,
  `UUIDanggota` varchar(50) NOT NULL,
  `IDsimpanan` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `nominal` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kp_simpanan_sukarela`
--

INSERT INTO `kp_simpanan_sukarela` (`uuid`, `UUIDanggota`, `IDsimpanan`, `tanggal`, `nominal`, `created`, `updated`) VALUES
('ea132e13-81c1-4e50-a702-0a50f22162e0', '1fe836ce-c530-4d65-bb2c-32c58a75ab22', 'SP00001', '2021-08-06', 50000, '2021-08-06 08:41:27', '2021-08-06 08:41:27');

-- --------------------------------------------------------

--
-- Table structure for table `kp_simpanan_wajib`
--

CREATE TABLE `kp_simpanan_wajib` (
  `uuid` varchar(50) NOT NULL,
  `UUIDanggota` varchar(50) NOT NULL,
  `IDsimpanan` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `nominal` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kp_simpanan_wajib`
--

INSERT INTO `kp_simpanan_wajib` (`uuid`, `UUIDanggota`, `IDsimpanan`, `tanggal`, `nominal`, `created`, `updated`) VALUES
('182e3ab6-83cd-4f98-9c2c-73ed0b56fad6', '0a479cc7-a3c5-4460-8f48-e689ae9bf43e', 'SW00003', '2021-08-06', 2000, '2021-08-06 13:22:46', '2021-08-06 13:22:46'),
('21beac94-9602-44c3-b9ce-308223a66630', '0a479cc7-a3c5-4460-8f48-e689ae9bf43e', 'SS00001', '2021-08-06', 30000, '2021-08-06 09:19:41', '2021-08-06 09:19:41'),
('daa5fdcc-2ffe-469c-9cf4-04e3661652bd', '0a479cc7-a3c5-4460-8f48-e689ae9bf43e', 'SW00002', '2021-08-06', 2000, '2021-08-06 13:19:10', '2021-08-06 13:19:10');

-- --------------------------------------------------------

--
-- Stand-in structure for view `lapbeli`
-- (See below for the actual view)
--
CREATE TABLE `lapbeli` (
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `lapjual`
-- (See below for the actual view)
--
CREATE TABLE `lapjual` (
`no_jual` varchar(30)
,`tgl_jual` date
,`nm_konsumen` char(25)
,`kd_brg` varchar(6)
,`nm_brg` varchar(30)
,`harga` decimal(10,2)
,`hpp` decimal(10,2)
,`jml_jual` int(4)
,`totjual` decimal(20,2)
,`jenis` tinyint(1)
,`tothpp` decimal(20,2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `lapreturbeli`
-- (See below for the actual view)
--
CREATE TABLE `lapreturbeli` (
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `lapreturjual`
-- (See below for the actual view)
--
CREATE TABLE `lapreturjual` (
`no_retur` varchar(30)
,`no_jual` varchar(30)
,`tgl_retur` datetime
,`nm_konsumen` char(25)
,`kd_brg` varchar(6)
,`nm_brg` varchar(30)
,`harga_jual` float
,`jml_retur` int(4)
,`totretur` double
);

-- --------------------------------------------------------

--
-- Table structure for table `ms_akses`
--

CREATE TABLE `ms_akses` (
  `IDrole` varchar(50) NOT NULL,
  `nama` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ms_akses`
--

INSERT INTO `ms_akses` (`IDrole`, `nama`) VALUES
('R0001', 'Administrasi'),
('R0002', 'Kepala'),
('R0003', 'Staf Simpan Pinjam'),
('R0004', 'Staf Pembelian'),
('R0005', 'Staf Penjualan'),
('R0006', 'Pengawas');

-- --------------------------------------------------------

--
-- Table structure for table `ms_anggota`
--

CREATE TABLE `ms_anggota` (
  `uuid` varchar(50) NOT NULL,
  `IDanggota` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jabatan` varchar(20) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `tanggal_bergabung` date DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ms_anggota`
--

INSERT INTO `ms_anggota` (`uuid`, `IDanggota`, `nama`, `jabatan`, `telp`, `alamat`, `tanggal_bergabung`, `created`, `updated`) VALUES
('0a479cc7-a3c5-4460-8f48-e689ae9bf43e', 'A00002', 'Test', 'Karyawan', '0897474744', 'Jl Jalan', '2021-08-06', '2021-08-06 05:55:50', '2021-08-06 13:18:42'),
('1fe836ce-c530-4d65-bb2c-32c58a75ab22', 'A00001', 'Dawam Raja', 'Senior Staf', '089646673330', 'Jl Jalan 2', '2021-08-05', '2021-08-05 04:31:36', '2021-08-06 13:21:13');

-- --------------------------------------------------------

--
-- Table structure for table `ms_pengguna`
--

CREATE TABLE `ms_pengguna` (
  `uuid` varchar(50) NOT NULL,
  `IDpengguna` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `IDrole` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ms_pengguna`
--

INSERT INTO `ms_pengguna` (`uuid`, `IDpengguna`, `username`, `nama`, `password`, `IDrole`, `created`, `updated`) VALUES
('1fe836ce-c530-4d65-bb2c-32c58a75ab21', 'U0001', 'test1', 'Test', 'ac43724f16e9241d990427ab7c8f4228', 'R0004', '2021-08-04 14:55:14', '2021-08-04 14:55:14'),
('8e700ceb-b93b-49ea-a3eb-8563314a9e41', 'U00003', 'test3', 'Test 3', 'ac43724f16e9241d990427ab7c8f4228', 'R0004', '2021-08-09 17:48:27', '2021-08-09 17:48:27'),
('b0e4d121-0b0d-4ed0-87e6-a2b6c341ce69', 'U00004', 'test4', 'Test 4', 'ac43724f16e9241d990427ab7c8f4228', 'R0005', '2021-08-09 17:49:03', '2021-08-09 17:49:03'),
('f137b3d3-1a4c-4d66-8b1b-7ba0bb04a9c1', 'U00002', 'test2', 'Test 2', 'rahasia', 'R0002', '2021-08-09 17:46:13', '2021-08-09 17:46:13');

-- --------------------------------------------------------

--
-- Table structure for table `piutang`
--

CREATE TABLE `piutang` (
  `no_piutang` int(11) NOT NULL,
  `no_jual` varchar(30) DEFAULT NULL,
  `tgl_piutang` datetime NOT NULL,
  `jml_piutang` double NOT NULL,
  `jml_bayar` double NOT NULL,
  `sisa_piutang` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `piutang`
--

INSERT INTO `piutang` (`no_piutang`, `no_jual`, `tgl_piutang`, `jml_piutang`, `jml_bayar`, `sisa_piutang`) VALUES
(1, 'FP0000000720200618090430', '2020-06-18 00:00:00', 600000, 300000, 300000),
(2, 'FP0000000820200625084318', '2020-06-25 00:00:00', 2000000, 0, 2000000),
(3, 'FP0000000920200701181345', '2020-07-01 00:00:00', 100000, 0, 100000),
(1, 'FP0000000720200618090430', '2020-06-18 00:00:00', 600000, 300000, 300000),
(2, 'FP0000000820200625084318', '2020-06-25 00:00:00', 2000000, 0, 2000000),
(3, 'FP0000000920200701181345', '2020-07-01 00:00:00', 100000, 0, 100000);

-- --------------------------------------------------------

--
-- Table structure for table `returbeli`
--

CREATE TABLE `returbeli` (
  `no_retur` varchar(30) NOT NULL,
  `no_beli` varchar(30) NOT NULL,
  `tgl_retur` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `returjual`
--

CREATE TABLE `returjual` (
  `no_retur` varchar(30) NOT NULL,
  `no_jual` varchar(30) NOT NULL,
  `tgl_retur` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `returjual`
--

INSERT INTO `returjual` (`no_retur`, `no_jual`, `tgl_retur`) VALUES
('RP0000000120200415074125', 'FP0000000320200409224651', '2020-04-15 00:00:00'),
('RP0000000220200415074931', 'FP0000000520200410072324', '2020-04-15 00:00:00'),
('RP0000000320200415074957', 'FP0000000320200409224651', '2020-04-15 00:00:00'),
('RP0000000420200617214908', 'FP0000000320200409224651', '2020-06-17 00:00:00'),
('RP0000000120200415074125', 'FP0000000320200409224651', '2020-04-15 00:00:00'),
('RP0000000220200415074931', 'FP0000000520200410072324', '2020-04-15 00:00:00'),
('RP0000000320200415074957', 'FP0000000320200409224651', '2020-04-15 00:00:00'),
('RP0000000420200617214908', 'FP0000000320200409224651', '2020-06-17 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stock_opname`
--

CREATE TABLE `stock_opname` (
  `no_so` varchar(30) NOT NULL,
  `tgl_so` datetime NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `kd_brg` varchar(10) NOT NULL,
  `jml_sebelum` int(10) NOT NULL,
  `jml_so` int(5) NOT NULL,
  `jml_sesudah` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_opname`
--

INSERT INTO `stock_opname` (`no_so`, `tgl_so`, `keterangan`, `kd_brg`, `jml_sebelum`, `jml_so`, `jml_sesudah`) VALUES
('SO-000', '2020-09-02 00:00:00', '', '', 0, 0, 0),
('SO-0001', '2020-09-02 00:00:00', 'STOK KURANG', 'B00002', -2460, 3000, 540),
('SO-001', '2020-04-21 00:00:00', 'Barang PEN ada kelebihan di Gudang', 'B00003', 229, 8, 237),
('SO-000', '2020-09-02 00:00:00', '', '', 0, 0, 0),
('SO-0001', '2020-09-02 00:00:00', 'STOK KURANG', 'B00002', -2460, 3000, 540),
('SO-001', '2020-04-21 00:00:00', 'Barang PEN ada kelebihan di Gudang', 'B00003', 229, 8, 237);

-- --------------------------------------------------------

--
-- Table structure for table `stok_barang`
--

CREATE TABLE `stok_barang` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` int(4) NOT NULL,
  `kd_brg` varchar(6) DEFAULT NULL,
  `stok` int(5) NOT NULL DEFAULT '0',
  `harga_beli` decimal(10,2) NOT NULL,
  `harga_jual` decimal(10,2) NOT NULL,
  `keterangan` varchar(50) NOT NULL DEFAULT 'awal bulan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_barang`
--

INSERT INTO `stok_barang` (`id`, `tanggal`, `bulan`, `tahun`, `kd_brg`, `stok`, `harga_beli`, `harga_jual`, `keterangan`) VALUES
(1, '2020-07-01', 6, 2020, 'B00001', -220, '1500.00', '2000.00', 'stok awal bulan : 6'),
(2, '2020-07-01', 6, 2020, 'B00001', -220, '1500.00', '2000.00', 'stok awal bulan : 6'),
(3, '2020-07-01', 5, 2020, 'B00001', -220, '1500.00', '2000.00', 'stok awal bulan : 5'),
(8, '2020-07-01', 7, 2020, 'B00001', -220, '1500.00', '2000.00', 'stok awal bulan : 7'),
(9, '2020-07-01', 7, 2020, 'B00002', -2460, '100.00', '300.00', 'stok awal bulan : 7'),
(10, '2020-07-01', 7, 2020, 'B00003', 57, '12000.00', '15000.00', 'stok awal bulan : 7'),
(11, '2020-07-01', 7, 2020, 'B00004', -245, '1000.00', '3000.00', 'stok awal bulan : 7'),
(12, '2020-07-01', 7, 2020, 'B00005', 1300, '2000.00', '3500.00', 'stok awal bulan : 7'),
(13, '2020-08-22', 8, 2020, 'B00002', -2460, '100.00', '300.00', 'stok awal bulan : 8'),
(14, '2020-08-22', 8, 2020, 'B00003', 47, '12000.00', '15000.00', 'stok awal bulan : 8'),
(15, '2020-08-22', 8, 2020, 'B00004', -245, '1000.00', '3000.00', 'stok awal bulan : 8'),
(16, '2020-08-22', 8, 2020, 'B00005', 1300, '2000.00', '3500.00', 'stok awal bulan : 8'),
(17, '2020-08-22', 8, 2020, 'B00006', 12, '2000.00', '5000.00', 'stok awal bulan : 8'),
(18, '2020-09-02', 9, 2020, 'B00002', 540, '100.00', '300.00', 'stok awal bulan : 9'),
(19, '2020-09-02', 9, 2020, 'B00003', 47, '12000.00', '15000.00', 'stok awal bulan : 9'),
(20, '2020-09-02', 9, 2020, 'B00004', -245, '1000.00', '3000.00', 'stok awal bulan : 9'),
(21, '2020-09-02', 9, 2020, 'B00005', 1210, '2000.00', '3500.00', 'stok awal bulan : 9'),
(22, '2020-09-02', 9, 2020, 'B00006', 12, '2000.00', '5000.00', 'stok awal bulan : 9'),
(23, '2020-09-02', 9, 2020, 'B00007', 120, '1000.00', '2500.00', 'stok awal bulan : 9'),
(24, '2020-09-02', 9, 2020, 'B00008', 100, '2000.00', '3000.00', 'stok awal bulan : 9'),
(1, '2020-07-01', 6, 2020, 'B00001', -220, '1500.00', '2000.00', 'stok awal bulan : 6'),
(2, '2020-07-01', 6, 2020, 'B00001', -220, '1500.00', '2000.00', 'stok awal bulan : 6'),
(3, '2020-07-01', 5, 2020, 'B00001', -220, '1500.00', '2000.00', 'stok awal bulan : 5'),
(8, '2020-07-01', 7, 2020, 'B00001', -220, '1500.00', '2000.00', 'stok awal bulan : 7'),
(9, '2020-07-01', 7, 2020, 'B00002', -2460, '100.00', '300.00', 'stok awal bulan : 7'),
(10, '2020-07-01', 7, 2020, 'B00003', 57, '12000.00', '15000.00', 'stok awal bulan : 7'),
(11, '2020-07-01', 7, 2020, 'B00004', -245, '1000.00', '3000.00', 'stok awal bulan : 7'),
(12, '2020-07-01', 7, 2020, 'B00005', 1300, '2000.00', '3500.00', 'stok awal bulan : 7'),
(13, '2020-08-22', 8, 2020, 'B00002', -2460, '100.00', '300.00', 'stok awal bulan : 8'),
(14, '2020-08-22', 8, 2020, 'B00003', 47, '12000.00', '15000.00', 'stok awal bulan : 8'),
(15, '2020-08-22', 8, 2020, 'B00004', -245, '1000.00', '3000.00', 'stok awal bulan : 8'),
(16, '2020-08-22', 8, 2020, 'B00005', 1300, '2000.00', '3500.00', 'stok awal bulan : 8'),
(17, '2020-08-22', 8, 2020, 'B00006', 12, '2000.00', '5000.00', 'stok awal bulan : 8'),
(18, '2020-09-02', 9, 2020, 'B00002', 540, '100.00', '300.00', 'stok awal bulan : 9'),
(19, '2020-09-02', 9, 2020, 'B00003', 47, '12000.00', '15000.00', 'stok awal bulan : 9'),
(20, '2020-09-02', 9, 2020, 'B00004', -245, '1000.00', '3000.00', 'stok awal bulan : 9'),
(21, '2020-09-02', 9, 2020, 'B00005', 1210, '2000.00', '3500.00', 'stok awal bulan : 9'),
(22, '2020-09-02', 9, 2020, 'B00006', 12, '2000.00', '5000.00', 'stok awal bulan : 9'),
(23, '2020-09-02', 9, 2020, 'B00007', 120, '1000.00', '2500.00', 'stok awal bulan : 9'),
(24, '2020-09-02', 9, 2020, 'B00008', 100, '2000.00', '3000.00', 'stok awal bulan : 9');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kd_sup` char(6) NOT NULL,
  `nm_sup` char(25) NOT NULL,
  `alamat` char(50) NOT NULL,
  `kota` char(20) NOT NULL,
  `phone` char(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kd_sup`, `nm_sup`, `alamat`, `kota`, `phone`) VALUES
('S0001', 'Mito', 'Jl Bla', 'Solo', '08234'),
('S0002', 'Toshiba', 'Jl lala', 'Semarang', '98765'),
('S0003', 'Philips', 'Jl banana', 'Semarang', '98778');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vbayarhutang`
-- (See below for the actual view)
--
CREATE TABLE `vbayarhutang` (
`no_bayar` int(11)
,`tgl_bayar` datetime
,`no_beli` varchar(30)
,`nm_sup` char(25)
,`jml_hutang` decimal(10,0)
,`jml_bayar` decimal(10,0)
,`sisa_hutang` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vbayarpiutang`
-- (See below for the actual view)
--
CREATE TABLE `vbayarpiutang` (
`no_bayar` int(11)
,`tgl_bayar` datetime
,`no_jual` varchar(30)
,`nm_konsumen` char(25)
,`jml_piutang` double
,`jml_bayar` double
,`sisa_piutang` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vhutang`
-- (See below for the actual view)
--
CREATE TABLE `vhutang` (
`no_hutang` int(11)
,`no_beli` varchar(30)
,`tgl_hutang` datetime
,`nm_sup` char(25)
,`jml_hutang` decimal(10,0)
,`jml_bayar` decimal(10,0)
,`sisa_hutang` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_angsuran`
-- (See below for the actual view)
--
CREATE TABLE `view_angsuran` (
`uuid` varchar(50)
,`UUIDpinjaman` varchar(50)
,`UUIDanggota` varchar(50)
,`IDangsuran` varchar(50)
,`tanggal` date
,`nominal` int(11)
,`created` timestamp
,`updated` timestamp
,`IDanggota` varchar(50)
,`nama` varchar(100)
,`jabatan` varchar(20)
,`IDPinjaman` varchar(50)
,`tanggal_pinjam` date
,`nominal_pinjaman` int(11)
,`banyak_angsuran_pinjam` tinyint(3)
,`total_bayar` int(11)
,`status_pinjaman` enum('Lunas','Belum Lunas')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_auth`
-- (See below for the actual view)
--
CREATE TABLE `view_auth` (
`uuid` varchar(50)
,`IDpengguna` varchar(50)
,`username` varchar(50)
,`nama` varchar(100)
,`password` varchar(50)
,`IDrole` varchar(50)
,`created` timestamp
,`updated` timestamp
,`akses` varchar(55)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pengajuan_pinjaman`
-- (See below for the actual view)
--
CREATE TABLE `view_pengajuan_pinjaman` (
`uuid` varchar(50)
,`IDpengajuan` varchar(50)
,`UUIDanggota` varchar(50)
,`tanggal` date
,`nominal` int(11)
,`banyak_angsuran` smallint(4)
,`bunga_angsuran` double
,`status` enum('Ditolak','Disetujui','Menunggu')
,`created` timestamp
,`updated` timestamp
,`IDanggota` varchar(50)
,`nama` varchar(100)
,`jabatan` varchar(20)
,`telp` varchar(20)
,`tanggal_bergabung` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pinjaman`
-- (See below for the actual view)
--
CREATE TABLE `view_pinjaman` (
`uuid` varchar(50)
,`IDPinjaman` varchar(50)
,`UUIDpengajuan` varchar(50)
,`UUIDanggota` varchar(50)
,`tanggal` date
,`nominal` int(11)
,`banyak_angsuran` tinyint(3)
,`bunga_angsuran` double
,`nominal_angsuran` int(11)
,`total_bayar` int(11)
,`status` enum('Lunas','Belum Lunas')
,`created` timestamp
,`updated` timestamp
,`IDpengajuan` varchar(50)
,`tanggal_pengajuan` date
,`nominal_pengajuan` int(11)
,`banyak_angsuran_pengajuan` smallint(4)
,`bunga_angsuran_pengajuan` double
,`status_pengajuan` enum('Ditolak','Disetujui','Menunggu')
,`IDanggota` varchar(50)
,`nama` varchar(100)
,`jabatan` varchar(20)
,`telp` varchar(20)
,`tanggal_bergabung` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_simpanan_pokok`
-- (See below for the actual view)
--
CREATE TABLE `view_simpanan_pokok` (
`uuid` varchar(50)
,`UUIDanggota` varchar(50)
,`IDsimpanan` varchar(50)
,`tanggal` date
,`nominal` int(11)
,`created` timestamp
,`updated` timestamp
,`IDanggota` varchar(50)
,`nama` varchar(100)
,`jabatan` varchar(20)
,`telp` varchar(20)
,`alamat` text
,`tanggal_bergabung` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_simpanan_sukarela`
-- (See below for the actual view)
--
CREATE TABLE `view_simpanan_sukarela` (
`uuid` varchar(50)
,`UUIDanggota` varchar(50)
,`IDsimpanan` varchar(50)
,`tanggal` date
,`nominal` int(11)
,`created` timestamp
,`updated` timestamp
,`IDanggota` varchar(50)
,`nama` varchar(100)
,`jabatan` varchar(20)
,`telp` varchar(20)
,`tanggal_bergabung` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_simpanan_wajib`
-- (See below for the actual view)
--
CREATE TABLE `view_simpanan_wajib` (
`uuid` varchar(50)
,`UUIDanggota` varchar(50)
,`IDsimpanan` varchar(50)
,`tanggal` date
,`nominal` int(11)
,`created` timestamp
,`updated` timestamp
,`IDanggota` varchar(50)
,`nama` varchar(100)
,`jabatan` varchar(20)
,`telp` varchar(20)
,`tanggal_bergabung` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vjurnal`
-- (See below for the actual view)
--
CREATE TABLE `vjurnal` (
`nojurnal` varchar(30)
,`tanggal` datetime
,`nobukti` varchar(60)
,`uraian` varchar(60)
,`kodeakun` varchar(4)
,`namaakun` varchar(50)
,`debit` decimal(10,2)
,`kredit` decimal(10,2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vpiutang`
-- (See below for the actual view)
--
CREATE TABLE `vpiutang` (
`no_piutang` int(11)
,`no_jual` varchar(30)
,`tgl_piutang` datetime
,`nm_konsumen` char(25)
,`jml_piutang` double
,`jml_bayar` double
,`sisa_piutang` double
);

-- --------------------------------------------------------

--
-- Structure for view `lapbeli`
--
DROP TABLE IF EXISTS `lapbeli`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lapbeli`  AS  select `d`.`no_beli` AS `no_beli`,`b`.`tgl_beli` AS `tgl_beli`,`p`.`nm_sup` AS `nm_sup`,`d`.`kd_brg` AS `kd_brg`,`a`.`nm_brg` AS `nm_brg`,`d`.`harga_beli` AS `harga_beli`,`d`.`jml_beli` AS `jml_beli`,(`d`.`harga_beli` * `d`.`jml_beli`) AS `totbeli`,`b`.`jenis` AS `jenis` from (((`dbeli` `d` join `beli` `b`) join `supplier` `p`) join `barang` `a`) where ((`d`.`no_beli` = `b`.`no_beli`) and (`b`.`kd_sup` = `p`.`kd_sup`) and (`d`.`kd_brg` = `a`.`kd_brg`)) ;

-- --------------------------------------------------------

--
-- Structure for view `lapjual`
--
DROP TABLE IF EXISTS `lapjual`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lapjual`  AS  select `c`.`no_jual` AS `no_jual`,`j`.`tgl_jual` AS `tgl_jual`,`k`.`nm_konsumen` AS `nm_konsumen`,`c`.`kd_brg` AS `kd_brg`,`a`.`nm_brg` AS `nm_brg`,`c`.`harga` AS `harga`,`c`.`hpp` AS `hpp`,`c`.`jml_jual` AS `jml_jual`,(`c`.`harga` * `c`.`jml_jual`) AS `totjual`,`j`.`jenis` AS `jenis`,(`c`.`hpp` * `c`.`jml_jual`) AS `tothpp` from (((`barang` `a` join `djual` `c`) join `jual` `j`) join `konsumen` `k`) where ((`c`.`kd_brg` = `a`.`kd_brg`) and (`c`.`no_jual` = `j`.`no_jual`) and (`j`.`kd_konsumen` = `k`.`kd_konsumen`)) ;

-- --------------------------------------------------------

--
-- Structure for view `lapreturbeli`
--
DROP TABLE IF EXISTS `lapreturbeli`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lapreturbeli`  AS  (select `d`.`no_retur` AS `no_retur`,`r`.`no_beli` AS `no_Beli`,`r`.`tgl_retur` AS `tgl_retur`,`j`.`nm_sup` AS `nm_sup`,`d`.`kd_brg` AS `kd_brg`,`j`.`nm_brg` AS `nm_brg`,`d`.`harga_beli` AS `harga_beli`,`d`.`jml_retur` AS `jml_retur`,(`d`.`harga_beli` * `d`.`jml_retur`) AS `totretur` from ((`dreturbeli` `d` join `returbeli` `r`) join `lapbeli` `j`) where ((`d`.`no_retur` = `r`.`no_retur`) and (convert(`r`.`no_beli` using utf8) = `j`.`no_beli`) and (convert(`d`.`kd_brg` using utf8) = `j`.`kd_brg`))) ;

-- --------------------------------------------------------

--
-- Structure for view `lapreturjual`
--
DROP TABLE IF EXISTS `lapreturjual`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lapreturjual`  AS  (select `d`.`no_retur` AS `no_retur`,`r`.`no_jual` AS `no_jual`,`r`.`tgl_retur` AS `tgl_retur`,`j`.`nm_konsumen` AS `nm_konsumen`,`d`.`kd_brg` AS `kd_brg`,`j`.`nm_brg` AS `nm_brg`,`d`.`harga_jual` AS `harga_jual`,`d`.`jml_retur` AS `jml_retur`,(`d`.`harga_jual` * `d`.`jml_retur`) AS `totretur` from ((`dreturjual` `d` join `returjual` `r`) join `lapjual` `j`) where ((`d`.`no_retur` = `r`.`no_retur`) and (convert(`r`.`no_jual` using utf8) = `j`.`no_jual`) and (convert(`d`.`kd_brg` using utf8) = `j`.`kd_brg`))) ;

-- --------------------------------------------------------

--
-- Structure for view `vbayarhutang`
--
DROP TABLE IF EXISTS `vbayarhutang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vbayarhutang`  AS  (select `bh`.`no_bayar` AS `no_bayar`,`bh`.`tgl_bayar` AS `tgl_bayar`,`bh`.`no_beli` AS `no_beli`,`s`.`nm_sup` AS `nm_sup`,`bh`.`jml_hutang` AS `jml_hutang`,`bh`.`jml_bayar` AS `jml_bayar`,`bh`.`sisa_hutang` AS `sisa_hutang` from (((`bayar_hutang` `bh` join `hutang` `h`) join `beli` `b`) join `supplier` `s`) where ((`bh`.`no_beli` = `h`.`no_beli`) and (convert(`h`.`no_beli` using utf8) = `b`.`no_beli`) and (`b`.`kd_sup` = `s`.`kd_sup`))) ;

-- --------------------------------------------------------

--
-- Structure for view `vbayarpiutang`
--
DROP TABLE IF EXISTS `vbayarpiutang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vbayarpiutang`  AS  (select `b`.`no_bayar` AS `no_bayar`,`b`.`tgl_bayar` AS `tgl_bayar`,`b`.`no_jual` AS `no_jual`,`h`.`nm_konsumen` AS `nm_konsumen`,`b`.`jml_piutang` AS `jml_piutang`,`b`.`jml_bayar` AS `jml_bayar`,`b`.`sisa_piutang` AS `sisa_piutang` from (`bayar_piutang` `b` join `vpiutang` `h`) where (`b`.`no_jual` = `h`.`no_jual`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vhutang`
--
DROP TABLE IF EXISTS `vhutang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vhutang`  AS  (select `hutang`.`no_hutang` AS `no_hutang`,`hutang`.`no_beli` AS `no_beli`,`hutang`.`tgl_hutang` AS `tgl_hutang`,`supplier`.`nm_sup` AS `nm_sup`,`hutang`.`jml_hutang` AS `jml_hutang`,`hutang`.`jml_bayar` AS `jml_bayar`,`hutang`.`sisa_hutang` AS `sisa_hutang` from ((`hutang` join `beli`) join `supplier`) where ((`hutang`.`no_beli` = `beli`.`no_beli`) and (`beli`.`kd_sup` = `supplier`.`kd_sup`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_angsuran`
--
DROP TABLE IF EXISTS `view_angsuran`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_angsuran`  AS  select `kp_angsuran`.`uuid` AS `uuid`,`kp_angsuran`.`UUIDpinjaman` AS `UUIDpinjaman`,`kp_angsuran`.`UUIDanggota` AS `UUIDanggota`,`kp_angsuran`.`IDangsuran` AS `IDangsuran`,`kp_angsuran`.`tanggal` AS `tanggal`,`kp_angsuran`.`nominal` AS `nominal`,`kp_angsuran`.`created` AS `created`,`kp_angsuran`.`updated` AS `updated`,`ms_anggota`.`IDanggota` AS `IDanggota`,`ms_anggota`.`nama` AS `nama`,`ms_anggota`.`jabatan` AS `jabatan`,`kp_pinjaman`.`IDPinjaman` AS `IDPinjaman`,`kp_pinjaman`.`tanggal` AS `tanggal_pinjam`,`kp_pinjaman`.`nominal` AS `nominal_pinjaman`,`kp_pinjaman`.`banyak_angsuran` AS `banyak_angsuran_pinjam`,`kp_pinjaman`.`total_bayar` AS `total_bayar`,`kp_pinjaman`.`status` AS `status_pinjaman` from ((`kp_angsuran` left join `ms_anggota` on((`kp_angsuran`.`UUIDanggota` = `ms_anggota`.`uuid`))) left join `kp_pinjaman` on((`kp_angsuran`.`UUIDpinjaman` = `kp_pinjaman`.`uuid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_auth`
--
DROP TABLE IF EXISTS `view_auth`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_auth`  AS  select `ms_pengguna`.`uuid` AS `uuid`,`ms_pengguna`.`IDpengguna` AS `IDpengguna`,`ms_pengguna`.`username` AS `username`,`ms_pengguna`.`nama` AS `nama`,`ms_pengguna`.`password` AS `password`,`ms_pengguna`.`IDrole` AS `IDrole`,`ms_pengguna`.`created` AS `created`,`ms_pengguna`.`updated` AS `updated`,`ms_akses`.`nama` AS `akses` from (`ms_pengguna` left join `ms_akses` on((`ms_pengguna`.`IDrole` = `ms_akses`.`IDrole`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pengajuan_pinjaman`
--
DROP TABLE IF EXISTS `view_pengajuan_pinjaman`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pengajuan_pinjaman`  AS  select `kp_pengajuan_pinjaman`.`uuid` AS `uuid`,`kp_pengajuan_pinjaman`.`IDpengajuan` AS `IDpengajuan`,`kp_pengajuan_pinjaman`.`UUIDanggota` AS `UUIDanggota`,`kp_pengajuan_pinjaman`.`tanggal` AS `tanggal`,`kp_pengajuan_pinjaman`.`nominal` AS `nominal`,`kp_pengajuan_pinjaman`.`banyak_angsuran` AS `banyak_angsuran`,`kp_pengajuan_pinjaman`.`bunga_angsuran` AS `bunga_angsuran`,`kp_pengajuan_pinjaman`.`status` AS `status`,`kp_pengajuan_pinjaman`.`created` AS `created`,`kp_pengajuan_pinjaman`.`updated` AS `updated`,`ms_anggota`.`IDanggota` AS `IDanggota`,`ms_anggota`.`nama` AS `nama`,`ms_anggota`.`jabatan` AS `jabatan`,`ms_anggota`.`telp` AS `telp`,`ms_anggota`.`tanggal_bergabung` AS `tanggal_bergabung` from (`kp_pengajuan_pinjaman` left join `ms_anggota` on((`kp_pengajuan_pinjaman`.`UUIDanggota` = `ms_anggota`.`uuid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pinjaman`
--
DROP TABLE IF EXISTS `view_pinjaman`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pinjaman`  AS  select `kp_pinjaman`.`uuid` AS `uuid`,`kp_pinjaman`.`IDPinjaman` AS `IDPinjaman`,`kp_pinjaman`.`UUIDpengajuan` AS `UUIDpengajuan`,`kp_pinjaman`.`UUIDanggota` AS `UUIDanggota`,`kp_pinjaman`.`tanggal` AS `tanggal`,`kp_pinjaman`.`nominal` AS `nominal`,`kp_pinjaman`.`banyak_angsuran` AS `banyak_angsuran`,`kp_pinjaman`.`bunga_angsuran` AS `bunga_angsuran`,`kp_pinjaman`.`nominal_angsuran` AS `nominal_angsuran`,`kp_pinjaman`.`total_bayar` AS `total_bayar`,`kp_pinjaman`.`status` AS `status`,`kp_pinjaman`.`created` AS `created`,`kp_pinjaman`.`updated` AS `updated`,`kp_pengajuan_pinjaman`.`IDpengajuan` AS `IDpengajuan`,`kp_pengajuan_pinjaman`.`tanggal` AS `tanggal_pengajuan`,`kp_pengajuan_pinjaman`.`nominal` AS `nominal_pengajuan`,`kp_pengajuan_pinjaman`.`banyak_angsuran` AS `banyak_angsuran_pengajuan`,`kp_pengajuan_pinjaman`.`bunga_angsuran` AS `bunga_angsuran_pengajuan`,`kp_pengajuan_pinjaman`.`status` AS `status_pengajuan`,`ms_anggota`.`IDanggota` AS `IDanggota`,`ms_anggota`.`nama` AS `nama`,`ms_anggota`.`jabatan` AS `jabatan`,`ms_anggota`.`telp` AS `telp`,`ms_anggota`.`tanggal_bergabung` AS `tanggal_bergabung` from ((`kp_pinjaman` left join `kp_pengajuan_pinjaman` on((`kp_pinjaman`.`UUIDpengajuan` = `kp_pengajuan_pinjaman`.`uuid`))) left join `ms_anggota` on((`kp_pinjaman`.`UUIDanggota` = `ms_anggota`.`uuid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_simpanan_pokok`
--
DROP TABLE IF EXISTS `view_simpanan_pokok`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_simpanan_pokok`  AS  select `kp_simpanan_pokok`.`uuid` AS `uuid`,`kp_simpanan_pokok`.`UUIDanggota` AS `UUIDanggota`,`kp_simpanan_pokok`.`IDsimpanan` AS `IDsimpanan`,`kp_simpanan_pokok`.`tanggal` AS `tanggal`,`kp_simpanan_pokok`.`nominal` AS `nominal`,`kp_simpanan_pokok`.`created` AS `created`,`kp_simpanan_pokok`.`updated` AS `updated`,`ms_anggota`.`IDanggota` AS `IDanggota`,`ms_anggota`.`nama` AS `nama`,`ms_anggota`.`jabatan` AS `jabatan`,`ms_anggota`.`telp` AS `telp`,`ms_anggota`.`alamat` AS `alamat`,`ms_anggota`.`tanggal_bergabung` AS `tanggal_bergabung` from (`kp_simpanan_pokok` left join `ms_anggota` on((`kp_simpanan_pokok`.`UUIDanggota` = `ms_anggota`.`uuid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_simpanan_sukarela`
--
DROP TABLE IF EXISTS `view_simpanan_sukarela`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_simpanan_sukarela`  AS  select `kp_simpanan_sukarela`.`uuid` AS `uuid`,`kp_simpanan_sukarela`.`UUIDanggota` AS `UUIDanggota`,`kp_simpanan_sukarela`.`IDsimpanan` AS `IDsimpanan`,`kp_simpanan_sukarela`.`tanggal` AS `tanggal`,`kp_simpanan_sukarela`.`nominal` AS `nominal`,`kp_simpanan_sukarela`.`created` AS `created`,`kp_simpanan_sukarela`.`updated` AS `updated`,`ms_anggota`.`IDanggota` AS `IDanggota`,`ms_anggota`.`nama` AS `nama`,`ms_anggota`.`jabatan` AS `jabatan`,`ms_anggota`.`telp` AS `telp`,`ms_anggota`.`tanggal_bergabung` AS `tanggal_bergabung` from (`kp_simpanan_sukarela` left join `ms_anggota` on((`kp_simpanan_sukarela`.`UUIDanggota` = `ms_anggota`.`uuid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_simpanan_wajib`
--
DROP TABLE IF EXISTS `view_simpanan_wajib`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_simpanan_wajib`  AS  select `kp_simpanan_wajib`.`uuid` AS `uuid`,`kp_simpanan_wajib`.`UUIDanggota` AS `UUIDanggota`,`kp_simpanan_wajib`.`IDsimpanan` AS `IDsimpanan`,`kp_simpanan_wajib`.`tanggal` AS `tanggal`,`kp_simpanan_wajib`.`nominal` AS `nominal`,`kp_simpanan_wajib`.`created` AS `created`,`kp_simpanan_wajib`.`updated` AS `updated`,`ms_anggota`.`IDanggota` AS `IDanggota`,`ms_anggota`.`nama` AS `nama`,`ms_anggota`.`jabatan` AS `jabatan`,`ms_anggota`.`telp` AS `telp`,`ms_anggota`.`tanggal_bergabung` AS `tanggal_bergabung` from (`kp_simpanan_wajib` left join `ms_anggota` on((`kp_simpanan_wajib`.`UUIDanggota` = `ms_anggota`.`uuid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `vjurnal`
--
DROP TABLE IF EXISTS `vjurnal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vjurnal`  AS  (select `d`.`NoJurnal` AS `nojurnal`,`j`.`Tanggal` AS `tanggal`,`j`.`NoBukti` AS `nobukti`,`j`.`Uraian` AS `uraian`,`d`.`KodeAkun` AS `kodeakun`,`a`.`NamaAkun` AS `namaakun`,`d`.`Debit` AS `debit`,`d`.`Kredit` AS `kredit` from ((`detail_jurnal` `d` join `jurnal` `j`) join `akun` `a`) where ((`d`.`NoJurnal` = `j`.`NoJurnal`) and (`d`.`KodeAkun` = `a`.`KodeAkun`))) ;

-- --------------------------------------------------------

--
-- Structure for view `vpiutang`
--
DROP TABLE IF EXISTS `vpiutang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vpiutang`  AS  (select `p`.`no_piutang` AS `no_piutang`,`p`.`no_jual` AS `no_jual`,`p`.`tgl_piutang` AS `tgl_piutang`,`k`.`nm_konsumen` AS `nm_konsumen`,`p`.`jml_piutang` AS `jml_piutang`,`p`.`jml_bayar` AS `jml_bayar`,`p`.`sisa_piutang` AS `sisa_piutang` from ((`piutang` `p` join `jual` `j`) join `konsumen` `k`) where ((`p`.`no_jual` = `j`.`no_jual`) and (`j`.`kd_konsumen` = `k`.`kd_konsumen`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`KodeAkun`);

--
-- Indexes for table `akun_aruskas`
--
ALTER TABLE `akun_aruskas`
  ADD PRIMARY KEY (`kodeakun`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kd_brg`);

--
-- Indexes for table `bayar_hutang`
--
ALTER TABLE `bayar_hutang`
  ADD PRIMARY KEY (`no_bayar`);

--
-- Indexes for table `bayar_piutang`
--
ALTER TABLE `bayar_piutang`
  ADD PRIMARY KEY (`no_bayar`);

--
-- Indexes for table `beli`
--
ALTER TABLE `beli`
  ADD PRIMARY KEY (`no_beli`);

--
-- Indexes for table `dbeli`
--
ALTER TABLE `dbeli`
  ADD PRIMARY KEY (`no_beli`,`kd_brg`);

--
-- Indexes for table `detail_jurnal`
--
ALTER TABLE `detail_jurnal`
  ADD PRIMARY KEY (`NoJurnal`,`KodeAkun`);

--
-- Indexes for table `djual`
--
ALTER TABLE `djual`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dreturbeli`
--
ALTER TABLE `dreturbeli`
  ADD PRIMARY KEY (`no_retur`,`kd_brg`);

--
-- Indexes for table `dreturjual`
--
ALTER TABLE `dreturjual`
  ADD PRIMARY KEY (`no_retur`,`kd_brg`);

--
-- Indexes for table `kp_angsuran`
--
ALTER TABLE `kp_angsuran`
  ADD PRIMARY KEY (`uuid`),
  ADD UNIQUE KEY `uuid` (`IDangsuran`),
  ADD KEY `IDpinjaman` (`UUIDpinjaman`),
  ADD KEY `IDanggota` (`UUIDanggota`);

--
-- Indexes for table `kp_pengajuan_pinjaman`
--
ALTER TABLE `kp_pengajuan_pinjaman`
  ADD PRIMARY KEY (`uuid`),
  ADD UNIQUE KEY `IDpengajuan` (`IDpengajuan`),
  ADD KEY `IDanggota` (`UUIDanggota`);

--
-- Indexes for table `kp_pinjaman`
--
ALTER TABLE `kp_pinjaman`
  ADD PRIMARY KEY (`uuid`),
  ADD UNIQUE KEY `IDPinjaman` (`IDPinjaman`),
  ADD KEY `IDpengajuan` (`UUIDpengajuan`),
  ADD KEY `IDanggota` (`UUIDanggota`),
  ADD KEY `IDpengajuan_2` (`UUIDpengajuan`);

--
-- Indexes for table `kp_simpanan_pokok`
--
ALTER TABLE `kp_simpanan_pokok`
  ADD PRIMARY KEY (`uuid`),
  ADD KEY `IDanggota` (`UUIDanggota`),
  ADD KEY `uuid` (`IDsimpanan`);

--
-- Indexes for table `kp_simpanan_sukarela`
--
ALTER TABLE `kp_simpanan_sukarela`
  ADD PRIMARY KEY (`uuid`),
  ADD KEY `IDanggota` (`UUIDanggota`),
  ADD KEY `uuid` (`IDsimpanan`);

--
-- Indexes for table `kp_simpanan_wajib`
--
ALTER TABLE `kp_simpanan_wajib`
  ADD PRIMARY KEY (`uuid`),
  ADD KEY `IDanggota` (`UUIDanggota`),
  ADD KEY `uuid` (`IDsimpanan`);

--
-- Indexes for table `ms_akses`
--
ALTER TABLE `ms_akses`
  ADD PRIMARY KEY (`IDrole`);

--
-- Indexes for table `ms_anggota`
--
ALTER TABLE `ms_anggota`
  ADD PRIMARY KEY (`uuid`),
  ADD UNIQUE KEY `uuid` (`IDanggota`);

--
-- Indexes for table `ms_pengguna`
--
ALTER TABLE `ms_pengguna`
  ADD PRIMARY KEY (`uuid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `IDrole` (`IDrole`),
  ADD KEY `uuid` (`IDpengguna`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kd_sup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bayar_hutang`
--
ALTER TABLE `bayar_hutang`
  MODIFY `no_bayar` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bayar_piutang`
--
ALTER TABLE `bayar_piutang`
  MODIFY `no_bayar` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kp_angsuran`
--
ALTER TABLE `kp_angsuran`
  ADD CONSTRAINT `kp_angsuran_ibfk_1` FOREIGN KEY (`UUIDanggota`) REFERENCES `ms_anggota` (`uuid`),
  ADD CONSTRAINT `kp_angsuran_ibfk_2` FOREIGN KEY (`UUIDpinjaman`) REFERENCES `kp_pinjaman` (`uuid`);

--
-- Constraints for table `kp_pengajuan_pinjaman`
--
ALTER TABLE `kp_pengajuan_pinjaman`
  ADD CONSTRAINT `kp_pengajuan_pinjaman_ibfk_1` FOREIGN KEY (`UUIDanggota`) REFERENCES `ms_anggota` (`uuid`);

--
-- Constraints for table `kp_pinjaman`
--
ALTER TABLE `kp_pinjaman`
  ADD CONSTRAINT `kp_pinjaman_ibfk_1` FOREIGN KEY (`UUIDanggota`) REFERENCES `ms_anggota` (`uuid`),
  ADD CONSTRAINT `kp_pinjaman_ibfk_2` FOREIGN KEY (`UUIDpengajuan`) REFERENCES `kp_pengajuan_pinjaman` (`uuid`);

--
-- Constraints for table `kp_simpanan_pokok`
--
ALTER TABLE `kp_simpanan_pokok`
  ADD CONSTRAINT `kp_simpanan_pokok_ibfk_1` FOREIGN KEY (`UUIDanggota`) REFERENCES `ms_anggota` (`uuid`);

--
-- Constraints for table `kp_simpanan_sukarela`
--
ALTER TABLE `kp_simpanan_sukarela`
  ADD CONSTRAINT `kp_simpanan_sukarela_ibfk_1` FOREIGN KEY (`UUIDanggota`) REFERENCES `ms_anggota` (`uuid`);

--
-- Constraints for table `kp_simpanan_wajib`
--
ALTER TABLE `kp_simpanan_wajib`
  ADD CONSTRAINT `kp_simpanan_wajib_ibfk_1` FOREIGN KEY (`UUIDanggota`) REFERENCES `ms_anggota` (`uuid`);

--
-- Constraints for table `ms_pengguna`
--
ALTER TABLE `ms_pengguna`
  ADD CONSTRAINT `ms_pengguna_ibfk_1` FOREIGN KEY (`IDrole`) REFERENCES `ms_akses` (`IDrole`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
