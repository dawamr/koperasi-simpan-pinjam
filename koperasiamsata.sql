-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2021 at 01:00 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koperasiamsata`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` varchar(15) NOT NULL,
  `nama_anggota` varchar(20) NOT NULL,
  `jabatan_anggota` varchar(10) NOT NULL,
  `alamat_anggota` varchar(30) NOT NULL,
  `telp_anggota` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `nama_anggota`, `jabatan_anggota`, `alamat_anggota`, `telp_anggota`) VALUES
('K00001', 'Nayla', 'Staff', 'Jl. Mawar', 875650000);

-- --------------------------------------------------------

--
-- Table structure for table `angsuran`
--

CREATE TABLE `angsuran` (
  `NoAngsuran` varchar(10) NOT NULL,
  `IDAnggota` varchar(10) NOT NULL,
  `NamaAnggota` varchar(30) NOT NULL,
  `TanggalBayar` datetime NOT NULL,
  `JumlahAngsuran` varchar(30) NOT NULL,
  `TotalAngsuran` varchar(30) NOT NULL,
  `AngsuranKe` varchar(30) NOT NULL,
  `SisaAngsuran` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan_pinjaman`
--

CREATE TABLE `pengajuan_pinjaman` (
  `IDPengajuan` varchar(15) NOT NULL,
  `TanggalPengajuan` datetime DEFAULT NULL,
  `IDAnggota` char(10) NOT NULL,
  `NamaAnggota` varchar(30) DEFAULT NULL,
  `Nominal` double DEFAULT NULL,
  `Status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajuan_pinjaman`
--

INSERT INTO `pengajuan_pinjaman` (`IDPengajuan`, `TanggalPengajuan`, `IDAnggota`, `NamaAnggota`, `Nominal`, `Status`) VALUES
('P00001', '2021-07-25 18:25:05', 'K00001', 'Nayla', 10000, 'Ditolak');

-- --------------------------------------------------------

--
-- Table structure for table `pinjaman`
--

CREATE TABLE `pinjaman` (
  `NoPinjaman` varchar(15) NOT NULL,
  `IDPengajuan` varchar(10) NOT NULL,
  `IDAnggota` varchar(10) NOT NULL,
  `NamaAnggota` varchar(20) NOT NULL,
  `TanggalPinjam` datetime(6) NOT NULL,
  `TanggalPelunasan` datetime(6) NOT NULL,
  `NominalPinjaman` double NOT NULL,
  `BungaPinjaman` double NOT NULL,
  `NominalAngsuran` double NOT NULL,
  `BanyakAngsuran` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pinjaman`
--

INSERT INTO `pinjaman` (`NoPinjaman`, `IDPengajuan`, `IDAnggota`, `NamaAnggota`, `TanggalPinjam`, `TanggalPelunasan`, `NominalPinjaman`, `BungaPinjaman`, `NominalAngsuran`, `BanyakAngsuran`) VALUES
('PA210721001', 'PP001', 'AMS2107721', 'Bambang', '2021-07-21 12:07:07.000000', '2021-07-29 12:07:07.000000', 2000000, 300000, 100000, '6');

-- --------------------------------------------------------

--
-- Table structure for table `simpanansukarela`
--

CREATE TABLE `simpanansukarela` (
  `NoSimpananSukarela` varchar(15) NOT NULL,
  `TanggalSimpanan` datetime NOT NULL,
  `IDAnggota` varchar(10) NOT NULL,
  `NamaAnggota` varchar(30) NOT NULL,
  `SimpananSukarela` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `simpanansukarela`
--

INSERT INTO `simpanansukarela` (`NoSimpananSukarela`, `TanggalSimpanan`, `IDAnggota`, `NamaAnggota`, `SimpananSukarela`) VALUES
('S00001', '2021-07-25 16:46:10', 'K00001', 'Nayla', '10000');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(4) UNSIGNED NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `password` varchar(254) DEFAULT NULL,
  `hak_akses` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_id`, `name`, `password`, `hak_akses`) VALUES
(1, 'naylaaa', 'Nayla', 'nayla123', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `angsuran`
--
ALTER TABLE `angsuran`
  ADD PRIMARY KEY (`NoAngsuran`);

--
-- Indexes for table `pengajuan_pinjaman`
--
ALTER TABLE `pengajuan_pinjaman`
  ADD PRIMARY KEY (`IDAnggota`);

--
-- Indexes for table `pinjaman`
--
ALTER TABLE `pinjaman`
  ADD PRIMARY KEY (`NoPinjaman`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
